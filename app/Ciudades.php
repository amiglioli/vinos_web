<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ciudades extends Model
{
    protected $table = 'ciudades';


      public function provincia()
   {
      return $this->hasOne('App\Provincia','provincia_id','provincia_id')->with('pais');
   }

}
