<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Provincia extends Model
{
   protected $table = 'provincias';
   protected $primaryKey = 'provincia_id';

   public function informacion_adicional()
   {
        return $this->hasMany('App\InformacionAdicional','provincia','provincia_id');
   }

   public function pais()
   {
      return $this->hasOne('App\Pais','id','pais_id');
   }


   public function municipio()
   {
      return $this->hasMany('App\Municipio','provincia_id','provincia_id')->orderBy('municipio','ASC');
   }
}
