<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ranking extends Model
{
   protected $table = 'ranking';

   public function cata()
   {
      return $this->hasOne('App\Cata','id','cata_id');
   }

	public function resultado()
 	{
    	return $this->hasMany('App\Resultado','ranking_id','id')->with('producto');
 	}

 	public function resultado_distribuidora()
 	{
    	return $this->hasMany('App\Resultado','ranking_id','id')->with('producto_distribuidora');
 	}
}
