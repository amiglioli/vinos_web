<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Resultado extends Model
{
    public $incrementing = false;
    protected $primaryKey = 'ranking_id';

   public function producto()
   {
      return $this->hasOne('App\Productos','id','producto_id')
      ->with('bodega')
      ->with('varietales')
      ->with('tengo_el_producto')
      ->with('no_tengo_el_producto')
      ->with('distribuyo_el_producto')
      ->with('distribuidoras_que_reparten_mi_zona')
      ->with('quiero_producto')
      ->with('pedido_admin');
   }


   public function producto_mobile()
   {
      return $this->hasOne('App\Productos','id','producto_id')
      ->with('provincia')
      ->with('region')
      ->with('bodega'); 
   }


   public function producto_distribuidora()
   {

      return $this->hasOne('App\Productos','id','producto_id')->with('bodega')->with('varietales')->with('producto_en_zona');
   }




    public function ranking()
   {
      return $this->hasOne('App\Ranking','id','ranking_id')->with('cata');
   }
}
