<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PromocionRecurrente extends Model
{
    protected $table = 'promocion_recurrente';
    public $incrementing = false; //para que no intente retornar el id en el save()
    protected $primaryKey = 'promocion_id';


}
