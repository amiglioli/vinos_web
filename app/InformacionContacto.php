<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class InformacionContacto extends Model
{
   protected $table = 'informacion_contacto';

   public function provincias()
   {
     return $this->hasOne('App\Provincia','provincia_id','provincia');
   }
}
