<?php

namespace App\Http\Middleware;

use Closure;

class DatosVerificados
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        if (!$request->user()->completado) {
            return redirect('/activaciones');
        }
    
        return $next($request);
    }
}
