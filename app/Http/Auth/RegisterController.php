<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\User;
use App\Role;
use App\VerifyUser;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use DB;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
     */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/login';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {

        return Validator::make($data, [
            'tipo' => 'required|regex:/(^[A-Za-z0-9 ]+$)+/|max:255|',
            'entidad' => 'required',
            'persona_contacto' => 'required',
            'email' => 'required|string|email|max:255|unique:users',
            //'telefono' => 'required|string|max:15',
            'provincia' => 'required|regex:/(^[A-Za-z0-9 ]+$)+/|max:255|',
            //'localidad' => 'required|regex:/(^[A-Za-z0-9 ]+$)+/|max:255|',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    /*protected function create(array $data)
    {
        return User::create([
            'name'     => $data['name'],
            'email'    => $data['email'],
            'password' => Hash::make($data['password']),
        ]);
    }
    */

    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    public function index($tipo)
    {

        $provincias = DB::table('provincias')->get();

        return view('auth.register', ['tipo' => $tipo, 'provincias' => $provincias]);
    }

    protected function registered($request, $user)
    {

        $verifyUser = VerifyUser::where('user_id', $user->id)->first();
        $token      = $verifyUser->token;

        $this->guard()->logout();
        return redirect('/login')->with('status', 'Ingresar a  <a href="' . url('user/verify', $token) . '"> esta url </a> para seguir el proceso de registración (para obviar el mail)');
    }

    protected function create(array $data)
    {

            $aprobado = false;
        if ($data['tipo'] == 'distribuidora' || $data['tipo'] == 'vinoteca'|| $data['tipo'] == 'sommelier') {
            $aprobado = true;
        }

        $password = str_random(10);
        $email    = $data['email'];
        $user     = User::create([
            'name'     => $data['entidad'],
            'email'    => $data['email'],
            'password' => bcrypt($password),
            'aprobado' => $aprobado,
        ]);

        DB::table('informacion_contacto')
            ->insert(['user_id' => $user->id, 'persona_contacto' => $data['persona_contacto'], 'email' => $data['email'], 'provincia' => $data['provincia']]);

        $verifyUser = VerifyUser::create([
            'user_id' => $user->id,
            'token'   => str_random(40),
        ]);

        $rol = Role::where('name', $data['tipo'])->first();

        $user->roles()->sync($rol ?: [], true);

        /*
        if($data['tipo'] == 'distribuidora' || $data['tipo'] == 'vinoteca'){
        Mail::send('emails.registro_entidad', ['pass' => $password, 'email' => $email,'token' => $verifyUser->token], function($message) use($email)
        {
        $message->to($email, 'Bienvenido')->subject('Wines!');
        });
        }*/

        return $user;
    }

    public function cambiarContrasena(Request $request)
    {
        $pw1 = $request->input('password');
        $pw2 = $request->input('password2');
        if ($pw1 != $pw2) {
            Session::flash('error_verify', 'Contraseñas distintas');
            return back();
        } else {
            $token                      = $request->input('token_verify');
            $verifyUser                 = VerifyUser::where('token', $token)->first();
            $verifyUser->user->password = bcrypt($pw1);
            $verifyUser->user->verified = 1;
            $verifyUser->user->save();
            $status = "Contraseña generada correctamente, ya puedes ingresar!";
            $this->guard()->login($verifyUser->user);
            return redirect('/login')->with('status', $status);

        }
    }

    public function verifyUser($token)
    {

        $verifyUser = VerifyUser::where('token', $token)->first();
        if (isset($verifyUser)) {
            $user = $verifyUser->user;
            if (!$user->verified) {
                //$verifyUser->user->verified = 1;
                //$verifyUser->user->save();
                return view('auth.cambiar_contrasena', ['token' => $token]);

                //$status = "Your e-mail is verified. You can now login.";

            } else {
                $status = "Su email ya ha sido verificado. Puede iniciar sesión";
            }
        } else {
            return redirect('/login')->with('warning', "No encontramos datos para ese mail.");
        }

        return redirect('/login')->with('status', $status);
    }
}
