<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\ZonaDistribucion;
use App\VinotecaQuiereProducto;
use App\InformacionAdicional;
use App\User;
use App\PedidosAdmin;
use App\Provincia;
use Auth;
use DB;
use Redirect;
class PortfolioController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
     $bodegas = User::with('roles')->whereHas('roles', function($q){
        $q->where('name', 'bodega');
    })->get();


     return view('portfolio.index',compact('bodegas','provincias'));
 }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        $data = ZonaDistribucion::select('bodega_id')->where('distribuidora_id', Auth::user()->id)->groupBy('bodega_id')->get();
        $id_bodegas = [];
        foreach ($data as $bodega) {
            $id_bodegas[] = $bodega->bodega_id;
        }


        $bodegas = User::select('users.name','users.id as id', 'users.email', 'roles.label as rol_descripcion')
        ->join('role_user', 'role_user.user_id', '=', 'users.id')
        ->join('roles', 'role_user.role_id', '=', 'roles.id')
        ->where('roles.name', 'bodega')
        ->whereNotIn('users.id',$id_bodegas)
        ->orderBy('users.name','ASC')
        ->get();

        $provincias = Provincia::orderBy('provincia','ASC')->get();

        return view('portfolio.agregar',compact('bodegas','provincias'));
    }   

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      $distribuidora_id = Auth::user()->id;
      $bodegas = $request->bodegas;
      $provincias = $request->provincia;
      $municipios = $request->municipios;
      if(!empty($municipios)){
        foreach($municipios as $municipio){
          $municipio = explode('-',$municipio);
          foreach($bodegas as $b){
            $dbm = new ZonaDistribucion();
            $dbm->timestamps = false;
            $dbm->distribuidora_id = $distribuidora_id;
            $dbm->bodega_id = $b;
            $dbm->provincia_id = $municipio[0];
            $dbm->municipio_id = $municipio[1];
            $dbm->save();

            $data = VinotecaQuiereProducto::where('bodega_id',$b)
            ->where('provincia_id',$municipio[0])
            ->where('municipio_id',$municipio[1])
            ->get();

            $pedidos_admin = PedidosAdmin::where('bodega_id',$b)
            ->where('provincia_id',$municipio[0])
            ->where('municipio_id',$municipio[1])
            ->get();

            foreach($pedidos_admin as $pa){
                if($pa != null){
                    $pedido = PedidosAdmin::where('id_pedido',$pa->id_pedido)->first();
                    $pedido->timestamps = false;
                    $pedido->resuelto   = true;
                    $pedido->save();

                    $pedido_vinoteca        = new VinotecaQuiereProducto();
                    $pedido_vinoteca->timestamps   = false;
                    $pedido_vinoteca->vinoteca_id  = $pa->vinoteca_id;
                    $pedido_vinoteca->producto_id  = $pa->producto_id;
                    $pedido_vinoteca->provincia_id = $pa->provincia_id;
                    $pedido_vinoteca->municipio_id = $pa->municipio_id;
                    $pedido_vinoteca->bodega_id = $pa->bodega_id;
                    $pedido_vinoteca->distribuidora_id = Auth::user()->id;
                    $pedido_vinoteca->save();
                }
            }

            foreach($data as $d)
            {   
                if($d != null){
                    $producto = VinotecaQuiereProducto::where('distribuidora_id',Auth::user()->id)
                    ->where('bodega_id',$d->bodega_id)->where('vinoteca_id',$d->vinoteca_id)
                    ->where('provincia_id',$d->provincia_id)
                    ->where('municipio_id',$d->municipio_id)
                    ->where('producto_id',$d->producto_id)
                    ->get();

                    if($producto->isEmpty()){

                       $pedido                   = new VinotecaQuiereProducto();
                       $pedido->vinoteca_id      = $d->vinoteca_id;
                       $pedido->timestamps       = false;
                       $pedido->producto_id      = $d->producto_id;
                       $pedido->distribuidora_id = Auth::user()->id;
                       $pedido->provincia_id     = $municipio[0];
                       $pedido->municipio_id     = $municipio[1];
                       $pedido->bodega_id        = $d->bodega_id;
                       $pedido->save();
                   }else{
                    echo Auth::user()->id;
                    echo "hay coincidencias";
                }
            }else{

             echo "estos no tienen pedidos".$d;   
         }


     }

 }
}
}

return Redirect::action('PortfolioController@index')->with('message', 'Sus zonas han sido dadas de alta'); 
}

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $provincias = Provincia::get();
        $provincias_sel = ZonaDistribucion::
        select('provincia_id')
        ->where('distribuidora_id', Auth::user()->id)
        ->where('bodega_id',$id)
                        //->with('provincia')
        ->groupBy('provincia_id')
        ->get();

        foreach ($provincias as $prov) {
          foreach ($provincias_sel as $prov_m) {
            if($prov->provincia_id == $prov_m->provincia_id)
              $prov->selected = true;
      }
      if(empty($prov->selected))
        $prov->selected = false;
}

$bodega = User::whereId($id)->first();


                    //todos los municipios que tiene la distr con esa/s bodegas
$municipios = ZonaDistribucion::
where('distribuidora_id', Auth::user()->id)
->where('bodega_id',$id)
->get();


return view('portfolio.editar',compact('bodega','municipios','provincias'));
}

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        DB::table('distribuidora_bodega_municipio')
        ->where('distribuidora_id', '=', Auth::user()->id)
        ->where('bodega_id',$id)
        ->delete();

        VinotecaQuiereProducto::where('distribuidora_id',Auth::user()->id)->where('bodega_id',$request->bodegas)->delete();

        $distribuidora_id = Auth::user()->id;
        $bodegas = $request->bodegas;
        $provincias = $request->provincia;
        $municipios = $request->municipios;

        if(!empty($municipios)){
            foreach($municipios as $municipio){
              $municipio = explode('-',$municipio);
              foreach($bodegas as $b){         

                $dbm = new ZonaDistribucion();
                $dbm->timestamps = false;
                $dbm->distribuidora_id = $distribuidora_id;
                $dbm->bodega_id = $b;
                $dbm->provincia_id = $municipio[0];
                $dbm->municipio_id = $municipio[1];
                $dbm->save();

                $data = VinotecaQuiereProducto::where('bodega_id',$b)
                ->where('provincia_id',$municipio[0])
                ->where('municipio_id',$municipio[1])
                ->get();

                $pedidos_admin = PedidosAdmin::where('bodega_id',$b)
                ->where('provincia_id',$municipio[0])
                ->where('municipio_id',$municipio[1])
                ->get();

                foreach($pedidos_admin as $pa){
                    if($pa != null){
                        $pedido = PedidosAdmin::where('id_pedido',$pa->id_pedido)->first();
                        $pedido->timestamps = false;
                        $pedido->resuelto   = true;
                        $pedido->save();

                        $pedido_vinoteca        = new VinotecaQuiereProducto();
                        $pedido_vinoteca->timestamps   = false;
                        $pedido_vinoteca->vinoteca_id  = $pa->vinoteca_id;
                        $pedido_vinoteca->producto_id  = $pa->producto_id;
                        $pedido_vinoteca->provincia_id = $pa->provincia_id;
                        $pedido_vinoteca->municipio_id = $pa->municipio_id;
                        $pedido_vinoteca->bodega_id = $pa->bodega_id;
                        $pedido_vinoteca->distribuidora_id = Auth::user()->id;
                        $pedido_vinoteca->save();
                    }
                }

                foreach($data as $d)
                {   
                    if($d != null){
                        $producto = VinotecaQuiereProducto::where('distribuidora_id',Auth::user()->id)
                        ->where('bodega_id',$d->bodega_id)->where('vinoteca_id',$d->vinoteca_id)
                        ->where('provincia_id',$d->provincia_id)
                        ->where('municipio_id',$d->municipio_id)
                        ->where('producto_id',$d->producto_id)
                        ->get();

                        if($producto->isEmpty()){

                           $pedido                   = new VinotecaQuiereProducto();
                           $pedido->vinoteca_id      = $d->vinoteca_id;
                           $pedido->timestamps       = false;
                           $pedido->producto_id      = $d->producto_id;
                           $pedido->distribuidora_id = Auth::user()->id;
                           $pedido->provincia_id     = $municipio[0];
                           $pedido->municipio_id     = $municipio[1];
                           $pedido->bodega_id        = $d->bodega_id;
                           $pedido->save();
                       }else{
                        echo Auth::user()->id;
                        echo "hay coincidencias";
                    }
                }else{

                 echo "estos no tienen pedidos".$d;   
             }


         }

               /* if($data != null){
                   
                    
                    $producto = VinotecaQuiereProducto::where('bodega_id',$b)->get();
                   
                    foreach($producto as $p)
                    {

                       $pedido                   = new VinotecaQuiereProducto();
                       $pedido->vinoteca_id      = $p->vinoteca_id;
                       $pedido->timestamps       = false;
                       $pedido->producto_id      = $p->producto_id;
                       $pedido->distribuidora_id = Auth::user()->id;
                       $pedido->provincia_id     = $municipio[0];
                       $pedido->municipio_id     = $municipio[1];
                       $pedido->bodega_id        = $b;
                       $pedido->save();
                   }
               }else{
                echo "no hay pedidos en esta zona";
                echo '<br>';
            }
*/


              /* $dbm = new ZonaDistribucion();
                $dbm->timestamps = false;
                $dbm->distribuidora_id = $distribuidora_id;
                $dbm->bodega_id = $b;
                $dbm->provincia_id = $municipio[0];
                $dbm->municipio_id = $municipio[1];
                $dbm->save();
              */

            }

        } 
        
    }
        //dd($data);

    return Redirect::action('PortfolioController@index')->with('message', 'Sus zonas han sido dadas de alta'); 
}



public function apilocalidades(Request $request){

    $usuario = ZonaDistribucion::where('distribuidora_id',$request->id_distribiudora)
    ->where('bodega_id',$request->bodega_id)
    ->leftJoin('municipios',function($join)
    {
      $join->on('municipios.municipio_id','=','distribuidora_bodega_municipio.municipio_id');
      $join->on('municipios.provincia_id','=','distribuidora_bodega_municipio.provincia_id');
    })
    ->get();

    return $usuario;
}

public function api(Request $request )
{



    if($request->id_distribiudora)
        $id_distribiudora = $request->id_distribiudora;
    else
        $id_distribiudora = Auth::user()->id;

    $data = User::with('roles')
            ->whereHas('roles', function($q){
                $q->where('name', 'bodega');
            })
            ->whereHas('bodega_provincias_distribucion', function($q) use ($id_distribiudora){
                $q->where('distribuidora_id', $id_distribiudora);
            })
            ->whereHas('bodega_municipios_distribucion', function($q) use ($id_distribiudora){
                $q->where('distribuidora_id', $id_distribiudora);
            })

            ->with(['bodega_municipios_distribucion' => function($query) use ($id_distribiudora)
            {
                $query->where('distribuidora_id', $id_distribiudora);
            }])            
            ->with(['bodega_provincias_distribucion' => function($query) use ($id_distribiudora)
            {
                $query->where('distribuidora_id', $id_distribiudora);
            }])
            //->with('bodega_municipios_distribucion')
            //->with('bodega_provincias_distribucion')
            ->paginate(25);


        return $data;
}

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        ZonaDistribucion::where('distribuidora_id', '=', Auth::user()->id)->where('bodega_id',$id)->delete();
        VinotecaQuiereProducto::where('distribuidora_id', '=', Auth::user()->id)->where('bodega_id',$id)->delete();
        return 'Bodega Eliminada !';
    }
}
