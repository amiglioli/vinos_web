<?php

namespace App\Http\Controllers;

use App\Pais;
use Illuminate\Http\Request;
use Redirect;
use App\Especializacion;

class PaisesController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $paises = Pais::orderBy('nombre', 'ASC')->get();
        return view('paises.index', compact('paises'));
    }

    public function create()
    {

        return view('paises.agregar');
    }

      public function store(Request $request)
    {

      $request->validate([
            'pais' => 'required',
        ]);
        $paises = new Pais();
        $paises->timestamps    = false;
        $paises->nombre = $request->pais;
        $paises->save();

        return Redirect::action('PaisesController@index')->with('message', 'Su Especializacion ha sido agregada');
    }

    public function edit($id)
    {

         $pais = Pais::whereId($id)->first();

         return view('paises.edit',compact('pais'));

    }

  public function update(Request $request){

    $request->validate([
          'pais' => 'required',
      ]);
        $paises = Pais::where('id', '=', $request->id)->first();

        $paises->timestamps = false;
        $paises->nombre         = $request->pais;
        $paises->save();

       return Redirect::action('PaisesController@index')->with('message', 'Su Especializacion ha sido editado correctamente');
    }

    public function api(Request $request)
    {
        $data = Pais::orderBy('nombre', 'ASC')
            ->when($request->pais, function ($query) use ($request) {
                return $query->where('id', $request->pais);
            })
            ->paginate(25);
        return $data;
    }

    public function destroy($id)
    {
        $pais = Pais::whereId($id)->delete();
         return  'Su Pais ha sido eliminado correctamente';
    }
}
