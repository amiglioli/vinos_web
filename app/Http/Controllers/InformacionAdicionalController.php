<?php

namespace App\Http\Controllers;

use App\InformacionAdicional;
use App\InformacionContacto;
use App\Municipio;
use App\Provincia;
use App\User;
use Auth;
use Illuminate\Http\Request;
use Redirect;

class InformacionAdicionalController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        //$this->middleware('can:admin');
    }

    public function index()
    {

        if (Auth::user()->completado) {
            return Redirect::action('HomeController@index');
        }

        $id_usuario  = Auth::user()->id;
        $informacion = Auth::user()->informacion_adicional;

        //dd($informacion);
        $provincias = Provincia::all();

        $informacion_contacto = InformacionContacto::where('user_id', $id_usuario)->get();

        if (empty($informacion)) {
            return view('informacion_adicional.index', ['info_contacto' => $informacion_contacto[0], 'provincias' => $provincias]);
        }
    }

    public function completar_registro(Request $request, $id)
    {

        $data = $this->validate(request(), [
            'provincia'                   => 'required|string|max:255',
            'domicilio_partido'        => 'required|string|max:255',
            'domicilio_localidad'     => 'required|string|max:255',
            'cp'                            => 'required|string|max:8',
            'direccion_domicilio'    => 'required|string|max:255',
            'direccion_numero'      => 'required|string|max:255',
            'geo_latitud'                => 'required|string|max:255',
            'geo_longitud'              => 'required|string|max:255',
            //'web' => 'required|string|max:255',
        ]);

        $informacion                = new InformacionAdicional;
        $informacion->timestamps    = false;
        $informacion->user_id       = $id;
        $informacion->provincia     = $request->input('provincia');
        $informacion->partido       = $request->input('domicilio_partido');
        $informacion->localidad     = $request->input('domicilio_localidad');
        $informacion->codigo_postal = $request->input('cp');
        $informacion->domicilio     = $request->input('direccion_domicilio');
        $informacion->numero        = $request->input('direccion_numero');
        $informacion->geo_latitud   = $request->input('geo_latitud');
        $informacion->geo_longitud  = $request->input('geo_longitud');
        $informacion->telefono      = $request->input('telefono');
        $informacion->web           = $request->input('web');
        $informacion->save();

        User::where('id', $id)->update(['completado' => 't']);

        return Redirect::action('HomeController@index')->with('message', 'Sus datos han sido completados exitosamente');

    }

    public function localidades($id)
    {
        $localidades = Municipio::where('provincia_id', $id)->orderBy('municipio','ASC')->get();
        return $localidades;
    }

}
