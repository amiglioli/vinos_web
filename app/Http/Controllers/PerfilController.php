<?php

namespace App\Http\Controllers;
use App\InformacionAdicional;
use App\InformacionContacto;
use App\UsuarioImagen;
use App\Horarios;

use App\User;
use App\Provincia;
use Illuminate\Http\Request;
use Auth;
use File;
use Redirect;

class PerfilController extends Controller
{
  /**
  * Display a listing of the resource.
  *
  * @return \Illuminate\Http\Response
  */
  public function index()
  {
    $usuario = InformacionAdicional::select('informacion_adicional.*','municipios.municipio','municipios.municipio')
    ->leftJoin('municipios',function($join)
    {
      $join->on('municipios.municipio_id','=','informacion_adicional.partido');
      $join->on('municipios.provincia_id','=','informacion_adicional.provincia');
    })
    ->where('informacion_adicional.user_id',Auth::user()->id)
    ->with('provincias')
    ->with('user')
    ->first();




    $provincias = Provincia::orderBy('provincia','ASC')->get();


    //dd($usuario);
    return view('perfil.index',compact('usuario','provincias'));
  }

  /**
  * Show the form for creating a new resource.
  *
  * @return \Illuminate\Http\Response
  */
  public function create()
  {
    return view('perfil.subir');
  }

    public function upload()
  {
  
   
  }

  /**
  * Store a newly created resource in storage.
  *
  * @param  \Illuminate\Http\Request  $request
  * @return \Illuminate\Http\Response
  */
  public function store(Request $request)
  {
    
      $data = base64_decode(preg_replace('#^data:image/\w+;base64,#i', '', $request->img));
      $png_url = "profile-".time().".png";
      $filepath = public_path().'/imgusuario/'.$png_url;
      $ruta_guardado = $png_url;
      file_put_contents($filepath,$data);


      $im = getimagesize($filepath);
      $width = $im[0];

      $height = $im[1];

      $radio = $width/$height;

      $usuario_imagen                 = new UsuarioImagen();
      $usuario_imagen->timestamps     = false;
      $usuario_imagen->user_id        = Auth::user()->id;
      $usuario_imagen->path           = $ruta_guardado; 
      $usuario_imagen->radio           = $radio; 
      $usuario_imagen->perfil         = false; 
      $usuario_imagen->save();
  }

  /**
  * Display the specified resource.
  *
  * @param  int  $id
  * @return \Illuminate\Http\Response
  */
  public function show($id)
  {
    $data = UsuarioImagen::where('user_id',Auth::user()->id)->orderBy('id','DESC')->get();
    return view('perfil.imagenes',compact('data'));
  }


  /*public function api_img_perfil(){
    $data = UsuarioImagen::where('user_id',Auth::user()->id)->orderBy('id','DESC')->get();
    return view('perfil.imagenes',compact('data'));
  }
  */

  /**
  * Show the form for editing the specified resource.
  *
  * @param  int  $id
  * @return \Illuminate\Http\Response
  */
  public function edit($id)
  {
  }

  /**
  * Update the specified resource in storage.
  *
  * @param  \Illuminate\Http\Request  $request
  * @param  int  $id
  * @return \Illuminate\Http\Response
  */
  public function update(Request $request, $id)
  {



    $request->validate([
      'entidad' => 'required|string|max:25',
      'provincia' => 'required|string|max:25',
      'partido' => 'required|string|max:25',
      'localidad' => 'required|string|max:255',
      'codigo_postal' => 'required|string|max:8',
      'domicilio' => 'required|string|max:25',
      'numero' => 'required|string|max:25',
      'geo_latitud' => 'required|string|max:255',
      'geo_longitud' => 'required|string|max:255',
    ]);

    $imagen  = $request->imagen;
    $imagen_actual = $request->imagen_actual;
    $entidad = $request->entidad;

    $info_adicional = InformacionAdicional::where('user_id',$id)->first();
    $info_adicional->timestamps    = false;
    $info_adicional->provincia    = $request->provincia;
    $info_adicional->partido = $request->partido;
    $info_adicional->localidad = $request->localidad;
    $info_adicional->codigo_postal = $request->codigo_postal;
    $info_adicional->domicilio = $request->domicilio;
    $info_adicional->numero = $request->numero;
    $info_adicional->telefono = $request->telefono;
    $info_adicional->geo_latitud = $request->geo_latitud;
    $info_adicional->geo_longitud = $request->geo_longitud;
    $info_adicional->web = $request->web;
    $info_adicional->save();




    if($imagen != null){
      //echo "ad";die;
  			if(file_exists(public_path().'/imgprofile/'.$imagen_actual)){
  			 File::delete(public_path().'/imgprofile/'.$imagen_actual);
  		 	}
          UsuarioImagen::where('perfil',true)->where('user_id',Auth::user()->id)->delete();
          $data = base64_decode(preg_replace('#^data:image/\w+;base64,#i', '', $request->imagen));
          $png_url = "profile-".time().".png";
          $filepath = public_path().'/imgprofile/'.$png_url;
          $ruta_guardado = $png_url;
          file_put_contents($filepath,$data);



            $im = getimagesize($filepath);
            $width = $im[0];

            $height = $im[1];

            $radio = $width/$height;


          $usuario_imagen                 = new UsuarioImagen();
          $usuario_imagen->timestamps     = false;
          $usuario_imagen->user_id        = Auth::user()->id;
          $usuario_imagen->path           = $ruta_guardado; 
          $usuario_imagen->radio          = $radio; 
          $usuario_imagen->perfil         = true; 
          $usuario_imagen->save();


  		}else{
  			$ruta_guardado = $imagen_actual;
  		}

      InformacionContacto::where('user_id', $id)->update(['provincia' => $request->provincia,'localidad' => $request->partido]);

      //User::whereId($id)->update(['name' => $entidad,'imagen_usuario' => $ruta_guardado]);
      return Redirect::action('PerfilController@index')->with('message', 'Su perfil ha sido editado correctamente');

  }


  public function establecer_imagen_perfil($id)
  {
    UsuarioImagen::where('user_id',Auth::user()->id)->update(['perfil' => false]);
    UsuarioImagen::whereId($id)->update(['perfil' => true]);
    return 'Su imagen de perfil ha sido editada correctamente';
  }

  /**
  * Remove the specified resource from storage.
  *
  * @param  int  $id
  * @return \Illuminate\Http\Response
  */
  public function destroy($id)
  {
    $data = UsuarioImagen::whereId($id)->delete();

    return 'Su imagen ha sido eliminada correctamente';
  }
}
