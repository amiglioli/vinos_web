<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Permission;
use Redirect;

class PermissionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $permissions = Permission::orderBy('name','ASC')->get();
        return view('permission.index',compact('permissions'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('permission.agregar');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      $request->validate([
            'permiso' => 'required',
            'descripcion' => 'required',
        ]);

        $permiso = new Permission();
        $permiso->name = $request->permiso;
        $permiso->label = $request->descripcion;
        $permiso->save();

        return Redirect::action('PermissionController@index')->with('message', 'Su permiso ha sido agregado');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $permission = Permission::whereId($id)->first();

        return view('permission.edit',compact('permission'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
      $request->validate([
            'permiso' => 'required',
            'descripcion' => 'required',
        ]);

        $permiso        = Permission::whereId($id)->first();
        $permiso->name = $request->permiso;
        $permiso->label = $request->descripcion;
        $permiso->save();

        return Redirect::action('PermissionController@index')->with('message', 'Su permiso ha sido editado correctamente');
    }

    public function api(Request $request)
    {
        $data = Permission::orderBy('name', 'ASC')
            ->when($request->pais, function ($query) use ($request) {
                return $query->where('id', $request->pais);
            })
            ->paginate(25);
        return $data;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Permission::whereId($id)->delete();
         return 'Su permiso ha sido eliminado correctamente';
    }
}
