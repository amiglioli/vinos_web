<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\SugerirProducto;
use App\Varietal;
use Auth;
use Redirect;

class SugerirProductoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $varietales = Varietal::get();
        return view('sugerir_producto.index',compact('varietales'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $varietales = Varietal::get();
        return view('sugerir_producto.agregar',compact('varietales'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        if(Auth::user()->hasRole('vinoteca')){
            $request->validate([
                'bodega' => 'required',
                'nombre_vino' => 'required',
                'varietal' => 'required',
                'anio' => 'required',
            ]);
        }else{
            $request->validate([
                'bodega' => 'required',
                'nombre_vino' => 'required',
                'varietal' => 'required',
                'anio' => 'required',
                'persona_contacto' => 'required',
                'celular' => 'required',
                'email' => 'required',
            ]);
        }
        

       $sugerencia                      = new SugerirProducto();
       $sugerencia->timestamps          = false;
       $sugerencia->usuario_que_sugiere = Auth::user()->id;
       $sugerencia->bodega              = $request->bodega;
       $sugerencia->producto            = $request->nombre_vino;
       $sugerencia->varietal            = $request->varietal;
       $sugerencia->anio                = $request->anio;
       $sugerencia->persona_contacto    = $request->persona_contacto;
       $sugerencia->celular             = $request->celular;
       $sugerencia->email               = $request->email;
       $sugerencia->save();  

        return Redirect::action('SugerirProductoController@create')->with('message', 'Gracias por sugerir un vino para las próximas Catas REDVIN');        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }


    public function api(Request $request)
    {
        $data = SugerirProducto::orderBy('id', 'DESC')
        ->when($request->bodega, function ($query) use ($request) {
            return $query->where('bodega','ilike',"%".$request->bodega."%");
        })
        ->when($request->producto, function ($query) use ($request) {
            return $query->where('producto','ilike',"%".$request->producto."%");
        })
        ->when($request->varietal, function ($query) use ($request) {
           return $query->where('varietal','ilike',"%".$request->varietal."%");
        })
        ->when($request->anio, function ($query) use ($request) {
            return $query->where('anio','=',$request->anio);
        })
        ->when($request->persona_contacto, function ($query) use ($request) {
            return $query->where('persona_contacto','ilike',"%".$request->persona_contacto."%");
        })
        ->when($request->celular, function ($query) use ($request) {
            return $query->where('celular','=',$request->celular);
        })
        ->when($request->email, function ($query) use ($request) {
            return $query->where('email','ilike',"%".$request->email."%");
        })
        ->paginate(25);
        return $data;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
