<?php

namespace App\Http\Controllers;

use App\User;
use App\InformacionContacto;
use App\Provincia;
use App\VerifyUser;
use Redirect;
use Illuminate\Http\Request;

class AprobacionBodegaController extends Controller
{
  /**
  * Display a listing of the resource.
  *
  * @return \Illuminate\Http\Response
  */
  public function index()
  {
    $provincias = Provincia::orderBy('provincia','ASC')->get();
    return view('aprobacion_bodegas.index',compact('provincias'));
  }

  /**
  * Show the form for creating a new resource.
  *
  * @return \Illuminate\Http\Response
  */
  public function create()
  {
    //
  }

  /**
  * Store a newly created resource in storage.
  *
  * @param  \Illuminate\Http\Request  $request
  * @return \Illuminate\Http\Response
  */
  public function store(Request $request)
  {
    $id_user = $request->id;
    $id_random = $request->input('bodega');

    if($id_random != 'no_quiero_vincular' && $id_random != null){

      $datos_usuario = User::whereId($id_user)->first();
      User::whereId( $id_random)
      ->update(['password' => $datos_usuario->password,
      'email' => $datos_usuario->email,
      'name' =>$datos_usuario->name,
      'remember_token' =>$datos_usuario->remember_token,
      'usuario_real' => 1,
      'aprobado' => true,
      'created_at' => date('Y-m-d h:i:s')]);

      InformacionContacto::where('user_id', '=', $id_user)->update(['user_id' => $id_random]);
      VerifyUser::where('user_id', '=', $id_user)->update(['user_id' => $id_random]);
      User::whereId($id_user)->delete();

    }else{
        User::whereId($id_user)->update(['aprobado' => true]);
    }
    return Redirect::action('AprobacionBodegaController@index')->with('message', 'Su bodega ha sido agregada correctamente');
  }

  /**
  * Display the specified resource.
  *
  * @param  int  $id
  * @return \Illuminate\Http\Response
  */
  public function show($id)
  {

  }

  /**
  * Show the form for editing the specified resource.
  *
  * @param  int  $id
  * @return \Illuminate\Http\Response
  */
  public function edit($id)
  {
    $data = User::orderBy('name', 'ASC')
    ->with('roles')
    ->with('informacion_contacto')
    ->where('users.id',$id)
    ->first();
    $bodegas_vincular = User::where('usuario_real',0)->get();

    return view('aprobacion_bodegas.edit',compact('data','bodegas_vincular'));
  }

  /**
  * Update the specified resource in storage.
  *
  * @param  \Illuminate\Http\Request  $request
  * @param  int  $id
  * @return \Illuminate\Http\Response
  */
  public function update(Request $request, $id)
  {
    //
  }

  public function api(Request $request)
  {
    $data = User::orderBy('name', 'ASC')
    ->when($request->nombre, function ($query) use ($request) {
      return $query->where('name','ilike',"%".$request->nombre."%");
    })
    ->when($request->persona, function ($query) use ($request) {
      return  $query->whereHas('informacion_contacto', function($query) use ($request){
          $query->where('persona_contacto','ilike',"%".$request->persona."%");
      });
    })
    ->when($request->email, function($query) use ($request) {
      $query->where('email','ilike',"%".$request->email."%");
    })
    ->when($request->perfil, function($query) use ($request) {
      return  $query->whereHas('roles', function($query) use ($request){
        $query->where('name', '=',$request->perfil);
      });
    })
    ->when($request->provincia, function($query) use ($request) {
      return  $query->whereHas('informacion_contacto', function($query) use ($request){
        $query->where('provincia', '=',$request->provincia);
      });
    })
    ->where('aprobado','false')
    ->with('roles')
    ->with('informacion_contacto')
    ->paginate(25);
    return $data;
  }

  /**
  * Remove the specified resource from storage.
  *
  * @param  int  $id
  * @return \Illuminate\Http\Response
  */
  public function destroy($id)
  {
    //
  }
}
