<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Especializacion;
use App\Participa;
use Redirect;
use Auth;

class ParticipaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $especializaciones = Especializacion::get();

        return view('participa.index',compact('especializaciones'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $especializacion = Especializacion::get();
        return view('participa.agregar',compact('especializacion'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        
         $request->validate([
            'dni' => 'required|max:8',
            'apellido' => 'required',
            'nombre' => 'required',
            'especializacion' => 'required',
            'edad' => 'required',
            'especializacion' => 'required',
            'celular' => 'required',
            'email' => 'required',
        ]);

        $participante                   = new Participa();
        $participante->timestamps       = false;
        $participante->dni              = $request->dni;
        $participante->apellido         = $request->apellido;
        $participante->nombre           = $request->nombre;
        $participante->edad             = $request->edad;
        $participante->especializacion  = $request->especializacion;
        $participante->celular          = $request->celular;
        $participante->email            = $request->email;
        $participante->id_solicitante   = Auth::user()->id;
        $participante->save();

        return Redirect::action('ParticipaController@create')->with('message', 'Su solicitud para participar ha sido enviada');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

        public function api(Request $request)
    {
        $data = Participa::orderBy('id', 'DESC')
        ->when($request->dni, function ($query) use ($request) {
            return $query->where('dni','=',$request->dni);
        })
        ->when($request->apellido, function ($query) use ($request) {
            return $query->where('apellido','ilike',"%".$request->apellido."%");
        })
        ->when($request->nombre, function ($query) use ($request) {
            return $query->where('nombre','=',$request->nombre);
        })
        ->when($request->edad, function ($query) use ($request) {
            return $query->where('edad','=',$request->edad);
        })
        ->when($request->especializacion, function ($query) use ($request) {
            return $query->where('especializacion','=',$request->especializacion);
        })
        ->when($request->solicitante, function($query) use ($request) {
                return  $query->whereHas('solicitante', function($query) use ($request){
                    $query->where('name','ilike',"%".$request->solicitante."%");
                });
        })
        ->when($request->email, function ($query) use ($request) {
            return $query->where('email','ilike',"%".$request->email."%");
        })
        ->with('solicitante')
        ->with('especializacion')
        ->paginate(25);
        return $data;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
