<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Cata;
use App\CataProductos;
use App\Ranking;
use App\VinotecaProducto;
use App\VinotecaQuiereProducto;
use App\Productos;
use App\FichaTranquilo;
use App\PedidosAdmin;
use App\User;
use App\Resultado;
use App\Varietal;
use App\Puntuacion;
use App\Especializacion;
use DB;
use Auth;
class CataRankingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      return view('catas_ranking.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    public function vervino($id)
    {
      return view('catas_ranking.vervino',compact('id'));
    }
    public function ver_ranking($id)
    {
      $varietales = Varietal::orderBy('varietal','ASC')->get();
      $cata = Cata::where('id',$id)->first();

      return view('catas_ranking.ver_ranking',compact('id','varietales','cata'));
    }
    public function lotengo(Request $request)
    {

      if($request->id != null && $request->estado == 3){
        VinotecaProducto::where('producto_id',$request->id)->where('vinoteca_id',Auth::user()->id)->delete();
        return 'Su producto ha pasado al estado, no informado';
      }

      $producto = VinotecaProducto::where('producto_id', '=',$request->id)->where('vinoteca_id',Auth::user()->id)->first();

      if(empty($producto)){
        $producto  = new VinotecaProducto();

        $producto->producto_id   = $request->id;
        $producto->timestamps    = false;
        $producto->vinoteca_id   = Auth::user()->id;
        $producto->estado        = $request->estado;
        $producto->save();
        return 'Su producto ha sido adherido correctamente';
      }
      else{

        $producto  = VinotecaProducto::where('producto_id', '=', $request->id)->where('vinoteca_id',Auth::user()->id)->first();

        $producto->producto_id   = $request->id;
        $producto->timestamps    = false;
        $producto->vinoteca_id   = Auth::user()->id;
        $producto->estado        = $request->estado;
        $producto->save();
        return 'Su producto ha sido editado correctamente';


        //VinotecaProducto::where('producto_id',$request->producto_id)->where('vinoteca_id',Auth::user()->id)->delete();
        //return 'Su producto ha sido eliminado correctamente';
      }
    }


    public function loquiero(Request $request)
    { 

      $producto = VinotecaQuiereProducto::where('producto_id',$request->producto_id)
      ->where('vinoteca_id',Auth::user()->id)
      ->first();

      $lo_distribuye = Productos::whereId($request->producto_id)->with('distribuidoras_que_reparten_mi_zona')->first();

      if(empty($producto) && count($lo_distribuye->distribuidoras_que_reparten_mi_zona) > 0){

        foreach($lo_distribuye->distribuidoras_que_reparten_mi_zona as $reparten){

          $producto                   = new VinotecaQuiereProducto();
          $producto->vinoteca_id      = Auth::user()->id;
          $producto->producto_id      = $request->producto_id;
          $producto->distribuidora_id = $reparten->distribuidora_id;
          $producto->distribuidora_id = $reparten->distribuidora_id;
          $producto->bodega_id        = $request->bodega;
          $producto->provincia_id     = Auth::user()->informacion_adicional->provincia;
          $producto->municipio_id     = Auth::user()->informacion_adicional->partido;
          $producto->timestamps       = false;
          $producto->save();

        }

        return 'Su pedido ha sido realizado correctamente';

      }elseif(empty($producto) && count($lo_distribuye->distribuidoras_que_reparten_mi_zona) == 0){

          $pedido_admin = PedidosAdmin::where('producto_id',$request->producto_id)
          ->where('bodega_id',$request->bodega)
          ->where('vinoteca_id',Auth::user()->id)
          ->where('provincia_id',Auth::user()->informacion_adicional->provincia)
          ->where('municipio_id',Auth::user()->informacion_adicional->partido)->first();

          if(count($pedido_admin) == 0){
          $producto                    = new PedidosAdmin();
          $producto->vinoteca_id       = Auth::user()->id;
          $producto->producto_id       = $request->producto_id;
          $producto->bodega_id         = $request->bodega;
          $producto->provincia_id      = Auth::user()->informacion_adicional->provincia;
          $producto->municipio_id      = Auth::user()->informacion_adicional->partido;
          $producto->timestamps        = false;
          $producto->save();
           return 'Se transfirio al admin';
        }else{
        PedidosAdmin::where('producto_id',$request->producto_id)->where('bodega_id',$request->bodega)
          ->where('vinoteca_id',Auth::user()->id)->delete();
          return 'Su pedido se ha cancelado';
        }
         
      }else{
        VinotecaQuiereProducto::where('producto_id',$request->producto_id)->where('vinoteca_id',Auth::user()->id)->delete();
        return 'Su pedido ha sido cancelado correctamente';

      }

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    public function ver_quien_distribuye($id)
    {
     $producto = Productos::
     with('distribuidoras_que_reparten_mi_zona')
     ->with('tengo_el_producto')
     ->with('resultado')
     ->where('id', $id)
     ->first();


     return view('catas_ranking.quien_distribuye',compact('producto'));

   }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    public  function verficha($id_cata,$id_producto, $id_ranking)
    {   

      $producto = Productos::
      select('productos.*','resultados.puesto','resultados.puntaje')
      ->join('resultados','resultados.producto_id','productos.id')
      ->where('resultados.producto_id', $id_producto)
      ->where('resultados.ranking_id', $id_ranking)
      ->with('varietales')
      ->with('bodega')
      ->first();

      //dd($producto);
      $data = Puntuacion::orderBy('puntuacion','DESC')->get();
      $especializaciones = Especializacion::all();

        //dd($data);
      return view('catas_ranking.verficha',compact('producto','id_cata','id_producto','data','especializaciones'));
    }


    public function apiverficha(Request $request)
    {
      $data = FichaTranquilo::orderBy('puntaje', 'DESC')->where('id_cata',$request->id)
      ->where('id_producto',$request->id_producto)
      ->when($request->vista, function ($query) use ($request) {
        return $query->where('vista','=',$request->vista);
      })
      ->when($request->nariz, function ($query) use ($request) {
        return $query->where('nariz','=',$request->nariz);
      })
      ->when($request->muestra, function ($query) use ($request) {
        return $query->where('nro_muestra','=',$request->muestra);
      })
      ->when($request->gusto, function ($query) use ($request) {
        return $query->where('gusto','=',$request->gusto);
      })
      ->when($request->marca, function ($query) use ($request) {
        return $query->where('marca','=',$request->marca);
      })
      ->when($request->descripcion, function($query) use ($request) {
        return  $query->whereHas('cata', function($query) use ($request){
          $query->where('descripcion','ilike',"%".$request->descripcion."%");
        });
      })
      ->when($request->vino, function($query) use ($request) {
        return  $query->whereHas('producto', function($query) use ($request){
          $query->where('marca','ilike',"%".$request->vino."%");
        });
      })
      ->when($request->lugar, function($query) use ($request) {
        return  $query->whereHas('cata', function($query) use ($request){
          $query->where('lugar','ilike',"%".$request->lugar."%");
        });
      })
      ->when($request->catador, function($query) use ($request) {

        return $query->whereHas('catador', function($query2) use ($request){
          $query2->where('apellido','ilike',"%".$request->catador."%")
          ->orWhere('nombre','ilike',"%".$request->catador."%");
        });
      })
      ->when($request->anio, function($query) use ($request) {
        return  $query->whereHas('cata', function($query) use ($request){
          $query->where('fecha','=',$request->anio);
        });
      })
      ->when($request->especializacion, function($query) use ($request) {
        return  $query->whereHas('especializacion', function($query) use ($request){
          $query->where('id','=',$request->especializacion);
        });
      })
        //especializacion:self.especializacion
      ->with('vista')
      ->with('nariz')
      ->with('gusto')
      ->with('marca')
      ->with('cata')
      ->with('catador')
      ->with('producto')
      ->with('especializacion')
      ->paginate(25);
      return $data;
    }



    public function apivervino(Request $request)
    {
      $data = CataProductos::where('id_cata',$request->id)
      ->when($request->realizadas, function ($query) use ($request) {
        return $query->where('fecha','<',$request->realizadas);
      })
      ->when($request->a_realizarse, function ($query) use ($request) {
        return $query->where('fecha','>',$request->a_realizarse);
      })
      ->with('producto')
      ->paginate(25);
      return $data;
    }



        public function apiver_ranking_mobile(Request $request)
    {

      $data = Resultado::with('ranking')->with('producto_mobile')
      ->when($request->puesto, function($query) use ($request) {
        return $query->where('puesto', '=',$request->puesto);
      })
      ->when($request->puntaje, function($query) use ($request) {
        return $query->where('puntaje', '=',$request->puntaje);
      })
      ->when($request->bodega, function($query) use ($request) {
        return  $query->whereHas('producto.bodega', function($query) use ($request){
         $query->where('name','ilike',"%".$request->bodega."%");
       });
      })
      ->when($request->marca, function($query) use ($request) {
        return  $query->whereHas('producto', function($query) use ($request){
         $query->where('marca','ilike',"%".$request->marca."%");
       });
      })
      ->whereHas('ranking.cata', function($q) use($request){
        $q->where('id', $request->id_cata);
      })->paginate(25);

      return  $data;
    }







    public function apiver_ranking(Request $request)
    {

      $data = Resultado::with('ranking')->with('producto')
      ->when($request->puesto, function($query) use ($request) {
        return $query->where('puesto', '=',$request->puesto);
      })
      ->when($request->puntaje, function($query) use ($request) {
        return $query->where('puntaje', '=',$request->puntaje);
      })
      ->when($request->bodega, function($query) use ($request) {
        return  $query->whereHas('producto.bodega', function($query) use ($request){
         $query->where('name','ilike',"%".$request->bodega."%");
       });
      })
      ->when($request->marca, function($query) use ($request) {
        return  $query->whereHas('producto', function($query) use ($request){
         $query->where('marca','ilike',"%".$request->marca."%");
       });
      })
      ->when($request->varietal, function($query) use ($request) {
        return  $query->whereHas('producto', function($query) use ($request){
         $query->where('varietal','=',$request->varietal);
       });
      })
      ->whereHas('ranking.cata', function($q) use($request){
        $q->where('id', $request->id_cata);
      })->paginate(25);

      return  $data;
    }


    public function ver_vinotecas_zona($id_cata){
      $vinotecas_zona = User::
      select('users.name',
        'users.id as id_entidad',
        'users.email as id_vinoteca',
        'informacion_adicional.domicilio',
        'informacion_adicional.telefono',
        'informacion_adicional.localidad',
        'informacion_adicional.numero',
        'informacion_adicional.geo_latitud',
        'informacion_adicional.geo_longitud',
        'provincias.provincia as nombre_provincia',
        'municipios.municipio'
        )
      ->join('role_user', 'role_user.user_id', '=', 'users.id')
      ->join('roles', 'role_user.role_id', '=', 'roles.id')
      ->join('informacion_adicional', 'informacion_adicional.user_id', '=', 'users.id')
      ->join('provincias','provincias.provincia_id','informacion_adicional.provincia')
      ->join('municipios', function($join)
      {
        $join->on('municipios.provincia_id', '=', 'informacion_adicional.provincia');
        $join->on('municipios.municipio_id', '=', 'informacion_adicional.partido');
                       // ->where('dbm.distribuidora_id', '=', Auth::user()->id );
      })
      ->join('distribuidora_bodega_municipio as dbm', function($join)
      {
        $join->on('dbm.provincia_id', '=', 'informacion_adicional.provincia');
        $join->on('dbm.municipio_id', '=', 'informacion_adicional.partido')
        ->where('dbm.distribuidora_id', '=', Auth::user()->id );
      })
      ->leftJoin('vinoteca_producto as vp', function($leftjoin)
      {
        $leftjoin->on('vp.vinoteca_id', '=', 'users.id');
      })
                    //->with('informacion_adicional')
      ->distinct()
      ->whereIn('roles.name', ['vinoteca'])
      ->get();

      $mis_productos = DB::table('cata_productos')->select(
        'productos.marca',
          //'cata_productos.id_cata',
          //'productos.bodega_id',
        'dbm.bodega_id as lo_tengo',
        'productos.id as id_producto')
      ->join('productos','productos.id','cata_productos.id_producto')
      ->leftJoin('distribuidora_bodega_municipio as dbm', function($leftJoin)
      {
        $leftJoin->on('dbm.bodega_id', '=', 'productos.bodega_id')
        ->where('dbm.distribuidora_id', '=', Auth::user()->id );
      })
      ->where('cata_productos.id_cata',$id_cata)
      ->distinct()
      ->get();

          //dd($mis_productos);
      foreach ($mis_productos as $p) {
        if($p->lo_tengo != null){

          foreach ($vinotecas_zona as $v) {

            $verificador = DB::table('vinoteca_producto')
            ->select()
            ->join('productos','productos.id','vinoteca_producto.producto_id')
            ->where('vinoteca_producto.vinoteca_id', $v->id_entidad)
            ->where('vinoteca_producto.producto_id', $p->id_producto)
            ->count();
            if($verificador == 0){

             $p->vinotecas[] = (object) ['vionteca_nombre' => $v->name,
             'vinoteca_id' => $v->id_vinoteca,
             'email' => $v->email,
             'telefono' => $v->telefono,
             'domicilio' => $v->domicilio,
             'localidad' => $v->localidad,
             'nombre_provincia' => $v->nombre_provincia,
             'municipio' => $v->municipio,
             'numero' => $v->numero,
             'lo_tiene' => false];    
           }
         }
       }
     }

     return view ('catas_ranking.vinoteca_sin_productos',['productos_cata' => $mis_productos,'vinotecas_zona' => $vinotecas_zona]);

   }
   public function api(Request $request)
   {
    $data = Cata::orderBy('descripcion', 'ASC')
    ->when($request->realizadas, function ($query) use ($request) {
      return $query->where('fecha','<=',$request->realizadas);
    })
    ->when($request->a_realizarse, function ($query) use ($request) {
      return $query->where('fecha','>',$request->a_realizarse);
    })
    ->with('cantidad_productos')
    ->paginate(25);
    return $data;
  }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
  }
