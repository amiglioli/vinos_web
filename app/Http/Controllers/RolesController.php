<?php

namespace App\Http\Controllers;

use App\Permission;
use App\Role;
use Illuminate\Http\Request;

class RolesController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('can:admin');
    }

    public function index()
    {
        $roles = Role::with('permissions')->paginate(20);
        return view('admin.roles', $data = array('roles' => $roles));
    }

    public function show($id)
    {
        $role            = Role::whereId($request->id)->with('permissions')->first();
        $permissions = Permission::all();
        $url              =  action('RoleController@update');
        return view('admin.role', compact('role', 'permissions', 'url'));
    }

    public function create()
    {
        $d           = new Role;
        $permissions = Permission::all();
        $url         = '';
        return view('admin.role', compact('d', 'permissions', 'url'));
    }

    public function edit($id)
    {
        $d           = Role::whereId($id)->with('permissions')->first();
        $permissions = Permission::all();
        $url         = '';
        return view('admin.role', compact('d', 'permissions', 'url'));
    }

    public function store(Request $request)
    {
        $role        = new Role;
        $role->name  = $request->name;
        $role->label = $request->label;
        $role->save();
        $role->permissions()->sync($request->permission ?: [], true);
        return \Redirect::to('/admin/roles');
    }

    public function update(Request $request)
    {
        $role        = Role::whereId($request->id)->first();
        $role->name  = $request->name;
        $role->label = $request->label;
        $role->save();
        $role->permissions()->sync($request->permission ?: [], true);
        return \Redirect::to('/admin/roles');
    }

    public function destroy($id)
    {
        $role = Role::whereId($id)->delete();
        return \Redirect::to('/admin/roles');
    }

}
