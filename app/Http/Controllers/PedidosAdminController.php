<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Productos;
use App\PedidosAdmin;
use App\Varietal;
use Auth;

class PedidosAdminController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    { 
       $varietales = Varietal::all();
      return view('pedidos_admin.index',compact('varietales'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

        public function resuelto($id,Request $request)
    {   

        $pedido = PedidosAdmin::where('id_pedido',$id)->first();
        $pedido->resuelto      = $request->estado;
        $pedido->timestamps    = false;
        $pedido->save();

        return 'Su pedido ha cambiado de estado !';

    }

        public function api(Request $request)
    {
        //$data = VinotecaQuiereProducto::with('producto')->with('vinoteca')->get();


        $data = Productos::
        select('productos.id as producto_id',
            'productos.marca as producto_marca',
            'productos.anio as producto_anio',
            'varietales.varietal',
            'bodega.name as nombre_bodega',
            'users.id as vinoteca_id',
            'users.name as nombre_vinoteca',
            'provincias.provincia as nombre_provincia',
            'municipios.municipio as nombre_municipio',
            'informacion_adicional.localidad',
            'informacion_adicional.domicilio',
            'informacion_adicional.numero',
            'informacion_adicional.telefono',
            'pedidos_admin.resuelto',
            'pedidos_admin.id_pedido as record_id'
            )
        ->join('productos_varietal as varietales','varietales.id','productos.varietal')
        ->join('users as bodega','bodega.id','productos.bodega_id')
        ->join('pedidos_admin', function($join)
        {
            $join->on('pedidos_admin.producto_id', '=', 'productos.id');
        })
        ->join('users','users.id','pedidos_admin.vinoteca_id')
         ->join('informacion_adicional', 'informacion_adicional.user_id', '=', 'users.id')
        ->join('provincias','provincias.provincia_id','pedidos_admin.provincia_id')
        ->join('municipios', function($join)
        {
            $join->on('municipios.provincia_id', '=', 'pedidos_admin.provincia_id');
            $join->on('municipios.municipio_id', '=', 'pedidos_admin.municipio_id');
                                   // ->where('dbm.distribuidora_id', '=', Auth::user()->id );
        })

        ->when($request->vinoteca, function ($query) use ($request) {
          return $query->where('users.name', 'ilike','%'.$request->vinoteca.'%');
      })
        ->when($request->municipio, function ($query) use ($request) {
          return $query->where('municipios.municipio', 'ilike','%'.$request->municipio.'%');
      })
        ->when($request->localidad, function ($query) use ($request) {
          return $query->where('informacion_adicional.localidad', 'ilike','%'.$request->localidad.'%');
      })
        ->when($request->direccion, function ($query) use ($request) {
          return $query->where('informacion_adicional.domicilio', 'ilike','%'.$request->direccion.'%');
      })
        ->when($request->numero, function ($query) use ($request) {
          return $query->where('informacion_adicional.numero', 'ilike','%'.$request->numero.'%');
      })
        ->when($request->nombre_vino, function ($query) use ($request) {
          return $query->where('productos.marca', 'ilike','%'.$request->nombre_vino.'%');
      })
        ->when($request->bodega, function ($query) use ($request) {
          return $query->where('bodega.name', 'ilike','%'.$request->bodega.'%');
      })
        ->when($request->varietal, function ($query) use ($request) {
          return $query->where('productos.varietal', '=',$request->varietal);
      })

        //->distinct()
        ->paginate(25);

        
        return $data;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
       $pedido_admin = PedidosAdmin::where('id_pedido',$id)->delete();
       
       return 'Su pedido ha sido eliminado';
    }
}
