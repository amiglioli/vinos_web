<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Catador;
use App\Especializacion;
use App\CataCatadores;
use Redirect;
use DB;
class CatadorController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //$catadores = Catador::with('especializacion')->get();
        $especializaciones = Especializacion::all();

        return view('catadores.index', compact('catadores','especializaciones'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
      $especializacion = Especializacion::all();
      return view('catadores.agregar', compact('especializacion'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      $request->validate([
            'dni' => 'required|max:8',
            'apellido' => 'required',
            'nombre' => 'required',
            'especializacion' => 'required',
            'edad' => 'required',
        ]);

        $catador = new Catador();
        $catador->timestamps    = false;
        $catador->dni = $request->dni;
        $catador->apellido = $request->apellido;
        $catador->nombre = $request->nombre;
        $catador->especializacion_id = $request->especializacion;
        $catador->edad = $request->edad;

        $catador->save();

        return Redirect::action('CatadorController@index')->with('message', 'Su catador ha sido agregado');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
      return view('catadores.show',compact('id'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $catador = Catador::whereId($id)->first();
        $especializacion = Especializacion::all();

        return view('catadores.edit',compact('catador','especializacion'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
      $request->validate([
            'dni' => 'required|min:8',
            'apellido' => 'required',
            'nombre' => 'required',
            'especializacion' => 'required',
            'edad' => 'required',
        ]);
        $catador = Catador::where('id', '=', $id)->first();
        $catador->dni                       = $request->dni;
        $catador->apellido               = $request->apellido;
        $catador->nombre                = $request->nombre;
        $catador->especializacion_id         = $request->especializacion;
        $catador->edad                      = $request->edad;
        $catador->save();

       return Redirect::action('CatadorController@index')->with('message', 'Su catador ha sido editado correctamente');
    }

        public function apiver_cerradas(Request $request)
        {
            $data = CataCatadores::where('id_catador',$request->id_catador)
            ->when($request->descripcion, function($query) use ($request) {
              return  $query->whereHas('cata', function($query) use ($request){
                $query->where('descripcion','ilike',"%".$request->descripcion."%");
              });
            })
            ->when($request->lugar, function($query) use ($request) {
              return  $query->whereHas('cata', function($query) use ($request){
                $query->where('lugar','ilike',"%".$request->lugar."%");
              });
            })
            ->when($request->fecha, function($query) use ($request) {
              return  $query->whereHas('cata', function($query) use ($request){
                $query->where('fecha','=',$request->fecha);
              });
            })
            ->whereHas('cata', function($q) use($request){
              $q->where('cerrada', true);
            })
            ->with('cata')
            ->paginate(25);

            return $data;
        }

        public function api(Request $request)
    {
        $data = Catador::orderBy('nombre', 'ASC')
            ->when($request->dni, function ($query) use ($request) {
                return $query->where('dni','ilike',"%".$request->dni."%");
            })
            ->when($request->apellido, function ($query) use ($request) {
                return $query->where('apellido','ilike',"%".$request->apellido."%");
            })
            ->when($request->nombre, function ($query) use ($request) {
                return $query->where('nombre','ilike',"%".$request->nombre."%");
            })
            ->when($request->edad, function ($query) use ($request) {
              return $query->where('edad',$request->edad);
            })
            ->when($request->especializacion, function($query) use ($request) {
                return  $query->whereHas('especializacion', function($query) use ($request){
                    $query->where('id', '=',$request->especializacion);
                });
            })
            ->when($request->descripcion, function($query) use ($request) {
                return  $query->whereHas('catas_cerradas', function($query) use ($request){
                    $query->where('descripcion','ilike',"%".$request->descripcion."%");
                });
            })
            ->when($request->id_catador, function ($query) use ($request) {
                return $query->where('id',$request->id_catador);
            })
            ->with('catas_cerradas')
            ->with('especializacion')
            ->paginate(25);
        return $data;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
         $catador = Catador::whereId($id)->delete();

         return 'Su Catador ha sido eliminado correctamente';
    }
}
