<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Horarios;
use Auth;
use Redirect;
class HorariosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {   

    $horarios[0] = Horarios::where('id_vinoteca',Auth::user()->id)->where('dia',0)->get();
    $horarios[1] = Horarios::where('id_vinoteca',Auth::user()->id)->where('dia',1)->get();
    $horarios[2] = Horarios::where('id_vinoteca',Auth::user()->id)->where('dia',2)->get();
    $horarios[3] = Horarios::where('id_vinoteca',Auth::user()->id)->where('dia',3)->get();
    $horarios[4] = Horarios::where('id_vinoteca',Auth::user()->id)->where('dia',4)->get();
    $horarios[5] = Horarios::where('id_vinoteca',Auth::user()->id)->where('dia',5)->get();
    $horarios[6] = Horarios::where('id_vinoteca',Auth::user()->id)->where('dia',6)->get();

        return view('horarios.index',compact('horarios'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $dias = ['Domingo','Lunes','Martes','Miércoles','Jueves','Viernes','Sábado'];
        $horarios = ['00:00','00:30','01:00','01:30','02:00','02:30','03:00','03:30','04:00','04:30','05:00','05:30','06:00','06:30','07:00','07:30','08:00','08:30','09:00','09:30','10:00','10:30','11:00','11:30','12:00','12:30','13:00','13:30','14:00','14:30','15:00','15:30','16:00','16:30','17:00','17:30','18:00','18:30','19:00','19:30','20:00','20:30','21:00','21:30','22:00','22:30','23:00','23:30'];

        $data[0] = Horarios::where('id_vinoteca',Auth::user()->id)->where('dia',0)->get();
        $data[1] = Horarios::where('id_vinoteca',Auth::user()->id)->where('dia',1)->get();
        $data[2] = Horarios::where('id_vinoteca',Auth::user()->id)->where('dia',2)->get();
        $data[3] = Horarios::where('id_vinoteca',Auth::user()->id)->where('dia',3)->get();
        $data[4] = Horarios::where('id_vinoteca',Auth::user()->id)->where('dia',4)->get();
        $data[5] = Horarios::where('id_vinoteca',Auth::user()->id)->where('dia',5)->get();
        $data[6] = Horarios::where('id_vinoteca',Auth::user()->id)->where('dia',6)->get();
    
        return view('horarios.agregar',compact('dias','horarios','data'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {


        //dd($request->all());
        Horarios::where('id_vinoteca',Auth::user()->id)->delete();
        foreach($request->dia as $numero_dia => $d){
            foreach ($d as $horarios) {
                $horario              = new Horarios();
                $horario->timestamps  = false;
                $horario->id_vinoteca = Auth::user()->id;
                $horario->dia         = $numero_dia;
                $horario->hora_apertura      = $horarios["apertura"];
                $horario->hora_cierre   = $horarios["cierre"];
                $horario->save();
            }

        }

        return Redirect::action('HorariosController@index')->with('message', 'Sus horarios han sido guardados');
        


       //dd($lunes);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
     $horarios = ['00:00','00:30','01:00','01:30','02:00','02:30','03:00','03:30','04:00','04:30','05:00','05:30','06:00','06:30','07:00','07:30','08:00','08:30','09:30','10:00','10:30','11:30','12:00','12:30','13:00','13:30','14:30','15:00','15:30','16:00','16:30','17:00','17:30','18:00','18:30','19:00','19:30','20:00','20:30','21:00','21:30','22:00','22:30','23:00','23:30'];

       $data = Horarios::where('id_vinoteca',Auth::user()->id)->get();

       return view('horarios.edit',compact('data','horarios'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        
        Horarios::where('id_vinoteca',$id)->delete();

        foreach($request->dia as $key => $hora){

            $horario              = new Horarios();
            $horario->timestamps  = false;
            $horario->id_vinoteca = Auth::user()->id;
            $horario->dia         = $key;
            $horario->hora1       = $hora[0];
            $horario->hora1_fin   = $hora[1];
            $horario->hora2       = $hora[2];
            $horario->hora2_fin   = $hora[3];
            $horario->save();

        }


      return Redirect::action('HorariosController@index')->with('message', 'Sus horarios han sido guardados');
    }

    public function api(Request $request)
    {
        $data = Horarios::where('id_vinoteca', Auth::user()->id)
            ->when($request->producto, function ($query) use ($request) {
                return $query->where('id', $request->producto);
            })
            ->paginate(25);
        return $data;
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
