<?php

namespace App\Http\Controllers;

use App\Producto;
use Illuminate\Http\Request;
use Redirect;
class ProductoController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $nombres = Producto::orderBy('nombre', 'ASC')->get();
        return view('producto.index', compact('nombres'));
    }

    public function create()
    {

        return view('producto.agregar');
    }

      public function store(Request $request)
    {
      $request->validate([
            'nombre' => 'required',
        ]);
        $producto = new Producto();
        $producto->timestamps    = false;
        $producto->nombre = $request->nombre;
        $producto->save();

        return Redirect::action('ProductoController@index')->with('message', 'Su producto ha sido agregado');
    }

    public function edit($id)
    {

         $producto = Producto::whereId($id)->first();

         return view('producto.edit',compact('producto'));

    }

  public function update(Request $request){

      $request->validate([
            'nombre' => 'required',
        ]);
        $producto = Producto::where('id', '=', $request->id)->first();
        $producto->timestamps = false;
        $producto->nombre         = $request->nombre;
        $producto->save();

       return Redirect::action('ProductoController@index')->with('message', 'Su producto ha sido editado correctamente');
    }

    public function api(Request $request)
    {
        $data = Producto::orderBy('nombre', 'ASC')
            ->when($request->producto, function ($query) use ($request) {
                return $query->where('id', $request->producto);
            })
            ->paginate(25);
        return $data;
    }


    public function destroy($id)
    {
        $producto = Producto::whereId($id)->delete();
         return 'Su producto ha sido eliminado correctamente';
    }
}
