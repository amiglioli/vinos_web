<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Cata;
use App\Productos;
use App\Catador;
use App\CataProductos;
use App\CataCatadores;
use App\FichaTranquilo;
use App\Ranking;
use App\Resultado;
use Redirect;
use DB;
class CataController extends Controller
{


  public function __construct()
  {
    $this->middleware('auth');
  }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //$catas = Cata::orderBy('descripcion','ASC')->get();

      return view('catas.index', compact('catas'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
      $productos = Productos::orderBy('marca','ASC')->get();
      $catadores = Catador::orderBy('nombre','ASC')->get();

      return view('catas.agregar',compact('productos','catadores'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      $request->validate([
        'fecha'       => 'required|string|max:15',
        'lugar'       => 'required|string|max:255',
        'descripcion' => 'required|string',
        'productos'   => 'required',
        'catadores'   => 'required',
        ]);

      $productos = $request->productos;
      $catadores = $request->catadores;

      $cata = new Cata();
      $cata->timestamps = false;
      $cata->descripcion = $request->descripcion;
      $cata->fecha = $request->fecha;
      $cata->lugar = $request->lugar;
      $cata->save();
      $id_cata = $cata->id;

      if(!empty($productos)){
       foreach ($productos as $k => $v) {
        $cata_producto = new CataProductos();
        $cata_producto->timestamps    = false;
        $cata_producto->id_cata = $id_cata;
        $cata_producto->id_producto = $k;
        $cata_producto->muestra_nro = $v;
        $cata_producto->save();
      }
    }

    if(!empty($catadores)){
     foreach($catadores as $c){

       $cata_catador = new CataCatadores();
       $cata_catador->timestamps    = false;
       $cata_catador->id_cata = $id_cata;
       $cata_catador->id_catador = $c;
       $cata_catador->save();
     }
   }

   return Redirect::action('CataController@index')->with('message', 'SU cata ha sido dada de alta correctamente!');


 }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data = Cata::whereId($id)
        ->with('catadores_cata')
        ->with('productos_cata')
        ->with('ranking')
        ->first();

        return view('catas.show',compact('data'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
      $cata = Cata::whereId($id)->with('productos')->with('catadores')->first();
      $productos = Productos::orderBy('marca','ASC')->get();
      $catadores = Catador::orderBy('nombre','ASC')->get();

      $prod_arr = [];
      foreach ($cata->productos as $p) {
        $prod_arr[] = $p->id_producto;
      }


      foreach ($catadores as $ca) {
        $ca->selected = false;
        foreach ($cata->catadores as $ca_se) {
          if ($ca_se->id_catador == $ca->id) {
            $ca->selected = true;
          }
        }
      }

      return view('catas.edit',compact('cata','productos','catadores','prod_arr'));
    }


    public function update(Request $request, $id)
    {
     $request->validate([
      'fecha'       => 'required|string|max:15',
      'lugar'       => 'required|string|max:255',
      'descripcion' => 'required|string',
      'productos'   => 'required',
      'catadores'   => 'required',
      ]);

     $descripcion = $request->descripcion;
     $fecha = $request->fecha;
     $lugar = $request->lugar;
     $productos = $request->productos;
     $catadores = $request->catadores;

     $cata = Cata::whereId($id)->first();
     $cata->timestamps    = false;
     $cata->descripcion = $request->descripcion;
     $cata->fecha = $request->fecha;
     $cata->lugar = $request->lugar;
     $cata->save();



     CataProductos::where('id_cata',$id)->delete();

     if(!empty($productos)){
       foreach ($productos as $k => $v) {

        $cata_producto = new CataProductos();
        $cata_producto->timestamps    = false;
        $cata_producto->id_cata = $id;
        $cata_producto->id_producto = $k;
        $cata_producto->muestra_nro = $v;
        $cata_producto->save();
      }
    }

    CataCatadores::where('id_cata',$id)->delete();

    if(!empty($catadores)){
      foreach($catadores as $c){

       $cata_catador = new CataCatadores();
       $cata_catador->timestamps    = false;
       $cata_catador->id_cata = $id;
       $cata_catador->id_catador = $c;
       $cata_catador->save();
     }
   }

   return Redirect::action('CataController@index')->with('message', 'SU cata ha sido editada correctamente!');

 }


 public function cerrarCata($id)
 {

  $productos_cata =  CataProductos::where('id_cata',$id)->with('producto')->count();
  $catadores_selected = CataCatadores::where('id_cata',$id)->with('catador')->count();
  $productos_a_catar = ($catadores_selected * $productos_cata);
  $fichas_cata = FichaTranquilo::where('id_cata', $id)->count();


  if ($fichas_cata < $productos_a_catar) {

    return Redirect::action('CataController@index')->with('message', 'Su cata no se ha podido cerrar porque faltan cargar muestras');

  } else {

    $cata  = Cata::whereId($id)->first();

    $ranking = new Ranking();
    $ranking->cata_id     = $id;
    $ranking->timestamps    = false;
    $ranking->descripcion = $cata->descripcion;
    $ranking->fecha       = $cata->fecha;
    $ranking->tipo        = 1;
    $ranking->save();

    $calculo = DB::select('select *, ROW_NUMBER () OVER (ORDER BY 2 desc )  puesto
      from(
      select f1.nro_muestra, sum(f1.puntaje) /
      (select count(f2.id_catador) from ficha_tranquilos_datos f2
      WHERE f2.id_cata =f1.id_cata and f2.nro_muestra = f1.nro_muestra
      group by f2.nro_muestra)  puntaje
      from ficha_tranquilos_datos f1
      WHERE f1.id_cata = ' . $id . '
      group by f1.nro_muestra,f1.id_cata
      order by puntaje desc) f3');

    foreach ($calculo as $calculo) {

      $nro_muestra_producto = CataProductos::where('id_cata', $id)->where('muestra_nro', $calculo->nro_muestra)->first();

      if ($nro_muestra_producto->id_producto != null) {

        $resultado = new Resultado();
        $resultado->ranking_id  = $ranking->id;
        $resultado->timestamps    = false;
        $resultado->puesto      = $calculo->puesto;
        $resultado->producto_id = $nro_muestra_producto->id_producto;
        $resultado->puntaje     = $calculo->puntaje;
        $resultado->save();
      }
    }

    $cata = Cata::whereId($id)->first();
    $cata->timestamps    = false;
    $cata->cerrada = true;
    $cata->save();

  }
  return Redirect::action('CataController@index')->with('message', 'Su cata ha sido cerrada correctamente');
 
}


public function api(Request $request)
{
  $data = Cata::orderBy('descripcion', 'ASC')
  ->when($request->descripcion, function ($query) use ($request) {
    return $query->where('descripcion','ilike',"%".$request->descripcion."%");
  })
  ->when($request->lugar, function ($query) use ($request) {
    return $query->where('lugar','ilike',"%".$request->lugar."%");
  })
  ->when($request->fecha, function ($query) use ($request) {
    return $query->where('fecha','=',$request->fecha);
  })
  ->paginate(25);
  return $data;
}

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
      $producto = Cata::whereId($id)->delete();

      return 'Su cata ha sido eliminado correctamente';
    }
  }
