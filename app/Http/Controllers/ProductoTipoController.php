<?php

namespace App\Http\Controllers;

use App\ProductoTipo;
use App\Producto;
use Illuminate\Http\Request;
use Redirect;
class ProductoTipoController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $tipos = ProductoTipo::orderBy('tipo', 'ASC')->get();
        $producto = Producto::orderBy('nombre', 'ASC')->get();
        return view('producto_tipo.index', compact('tipos','producto'));
    }

    public function create()
    {
        $productos = Producto::all();
        return view('producto_tipo.agregar',compact('productos'));
    }

      public function store(Request $request)
    {
      $request->validate([
            'tipo' => 'required',
            'producto' => 'required',
        ]);
        $producto = new ProductoTipo();
        $producto->timestamps    = false;
        $producto->tipo = $request->tipo;
        $producto->producto_id = $request->producto;
        $producto->save();

        return Redirect::action('ProductoTipoController@index')->with('message', 'Su producto ha sido agregado');
    }

    public function edit($id)
    {

         $tipo = ProductoTipo::whereId($id)->first();
          $producto = Producto::all();
         return view('producto_tipo.edit',compact('tipo','producto'));

    }

  public function update(Request $request){

          $request->validate([
                'nombre' => 'required',
            ]);

        $producto = Producto::where('id', '=', $request->id)->first();
        $producto->timestamps = false;
        $producto->nombre         = $request->nombre;
        $producto->save();

       return Redirect::action('ProductoController@index')->with('message', 'Su producto ha sido editado correctamente');
    }

    public function api(Request $request)
    {
        $data = ProductoTipo::orderBy('tipo', 'ASC')
            ->when($request->tipo, function ($query) use ($request) {
                return $query->where('id', $request->tipo);
            })
            ->when($request->producto, function($query) use ($request) {
                return  $query->whereHas('Producto', function($query) use ($request){
                    $query->where('id', '=',$request->producto);
                });
            })
            ->with('Producto')
            ->paginate(25);
        return $data;
    }


    public function destroy($id)
    {
        $producto = ProductoTipo::whereId($id)->delete();
         return  'Su tipo de producto  ha sido eliminado correctamente';
    }
}
