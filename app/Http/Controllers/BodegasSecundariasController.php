<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Role;
use Redirect;
use DB;
use Auth;

class BodegasSecundariasController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $usuarios = User::all();
        return view('bodegas_secundarias.index',compact('usuarios'));
    }

    public function store(Request $request)
    {
      $request->validate([
            'nombre_bodega' => 'required',
            'email'                     => 'required|email|unique:users',
        ]);

    $user = User::where('email', '=', $request->input('email'))->first();
    $rol = Role::where('name', 'bodega')->first();

    if ($user === null) {
       $users = new User();
       $users->name = $request->input('nombre_bodega');
       $users->email = $request->input('email');
       $users->usuario_real = "0";
       $users->password = bcrypt($request->input('password'));
       $users->save();

      $user->roles()->sync($rol ?: [], true);

       $msj = 'La bodega se ha agregado correctamente';


     }else{

       $msj = 'No se ha podido agregar la bodega';
    }

      return Redirect::action('BodegasSecundariasController@index')->with('message', $msj);

    }


        public function api(Request $request){
        $data = User::with('roles')
                ->when($request->nombre, function($query) use ($request) {
                    return $query->where('id', $request->nombre);
                })
                ->when($request->email, function($query) use ($request) {
                    return $query->where('id', $request->email);
                })
                ->where('users.usuario_real',0)
                ->orderBy('users.name','ASC')
                ->paginate(25);
        return $data;
    }
}
