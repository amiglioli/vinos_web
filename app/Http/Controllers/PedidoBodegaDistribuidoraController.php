<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\SolicitudBodega;
use Redirect;
use Auth;

class PedidoBodegaDistribuidoraController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $request->validate([
          'nombre_bodega' => 'required|string|max:255',
          //'descripcion_solicitud' => 'required|string|max:255'
        ]);

      $nombre_bodega = $request->nombre_bodega;
      $descripcion = $request->descripcion_solicitud;
      $id_solicitante = Auth::user()->id;

      $solicitud_bodega = new SolicitudBodega();
      $solicitud_bodega->timestamps = false;
      $solicitud_bodega->nombre_bodega = $request->nombre_bodega;
      $solicitud_bodega->descripcion = $request->descripcion_solicitud;
      $solicitud_bodega->id_solicitante = $id_solicitante;
      $solicitud_bodega->leido = 0;
      $solicitud_bodega->leido_solicitante = 0;
      $solicitud_bodega->save();

      return Redirect::action('PortfolioController@create')->with('message', 'Su solicitud ha sido procesada');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
