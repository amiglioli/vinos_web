<?php

namespace App\Http\Controllers;

use App\User;
use App\Provincia;
use App\Municipio;
use App\InformacionAdicional;
use Illuminate\Http\Request;
use Redirect;
use Auth;


class ListadoController extends Controller
{
  /**
  * Create a new controller instance.
  *
  * @return void
  */
  public function __construct()
  {
    $this->middleware('auth');
  }

  /**
  * Show the application dashboard.
  *
  * @return \Illuminate\Http\Response
  */
  public function index()
  {

  }

  public function create()
  {


  }

  public function store(Request $request)
  {

  }

  public function edit($id)
  {
    $data = InformacionAdicional::select('informacion_adicional.*','municipios.municipio','municipios.municipio')
    ->leftJoin('municipios',function($join)
    {
      $join->on('municipios.municipio_id','=','informacion_adicional.partido');
      $join->on('municipios.provincia_id','=','informacion_adicional.provincia');
    })
    ->where('informacion_adicional.user_id',$id)
    ->with('provincias')
    ->with('user')
    ->first();

    return view('listado.view',compact('data'));


  }

  public function show($perfil)
  {

    $usuarios = User::with('roles')->whereHas('roles', function($q) use($perfil){
      $q->where('name', $perfil);
    })->where('usuario_real',1)->get();

    $provincias   = Provincia::orderBy('provincia','ASC')->get();
    $municipios   = Municipio::orderBy('municipio','ASC')->get();
    $localidades   = InformacionAdicional::orderBy('localidad','ASC')->get();

    return view('listado.index', compact('perfil','usuarios','provincias','municipios','localidades'));



  }

  public function update(Request $request){


  }

  public function api(Request $request)
  {

    $data = User::orderBy('name', 'ASC')->select('users.*','municipios.municipio','provincias.provincia','informacion_adicional.localidad')
    ->join('informacion_adicional','informacion_adicional.user_id','users.id')
    ->leftJoin('municipios',function($join)
    {
      $join->on('municipios.municipio_id','=','informacion_adicional.partido');
      $join->on('municipios.provincia_id','=','informacion_adicional.provincia');
    })
    ->join('provincias','provincias.provincia_id','informacion_adicional.provincia')

    ->when($request->nombre, function ($query) use ($request) {
      return $query->where('id', $request->nombre);
    })
    ->when($request->activa, function ($query) use ($request) {
      return $query->where('aprobado', $request->activa);
    })
    ->when($request->email, function ($query) use ($request) {
      return $query->where('email','ilike',"%".$request->email."%");
    })
    ->when($request->perfil, function($query) use ($request) {
      return  $query->whereHas('roles', function($query) use ($request){
        $query->where('name', '=', $request->perfil);
      });
    })
    ->when($request->localidad, function($query) use ($request) {
      return  $query->whereHas('informacion_adicional', function($query) use ($request){
        $query->where('localidad','ilike',"%".$request->localidad."%");
      });
    })
    ->when($request->provincia, function($query) use ($request) {
      return  $query->whereHas('informacion_adicional', function($query) use ($request){
        $query->where('provincia', '=', $request->provincia);
      });
    })
    ->when($request->municipio, function($query) use ($request) {
      return  $query->whereHas('informacion_adicional', function($query) use ($request){
        $query->where('partido', '=', $request->municipio);
      });
    })
    ->where('usuario_real',1)
    ->with('roles')
    ->paginate(25);
    return $data;
  }


  public function destroy($id)
  {

    $producto = Producto::whereId($id)->delete();

    return 'Su producto ha sido eliminado correctamente';
  }
}
