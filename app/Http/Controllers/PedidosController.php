<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\InformacionAdicional;
use Auth;
use DB;
use App\VinotecaQuiereProducto;
use App\Productos;
use App\Varietal;
use PDF;

class PedidosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      $varietales = Varietal::all();

      return view('pedidos.index',compact('varietales'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }


    public function ver_imprimir($id)
    {

      $data = Productos::where('vinoteca_quiere_productos.id_promocion',$id)->
      select('productos.id as producto_id',
        'productos.marca as producto_marca',
        'productos.anio as producto_anio',
        'varietales.varietal',
        'bodega.name as nombre_bodega',
        'users.id as vinoteca_id',
        'users.name as nombre_vinoteca',
        'provincias.provincia as nombre_provincia',
        'municipios.municipio as nombre_municipio',
        'informacion_adicional.localidad',
        'informacion_adicional.domicilio',
        'informacion_adicional.numero',
        'informacion_adicional.telefono',
        'vinoteca_quiere_productos.id_promocion as record_id',
        'vinoteca_quiere_productos.atendido'
        )
      ->join('productos_varietal as varietales','varietales.id','productos.varietal')
      ->join('users as bodega','bodega.id','productos.bodega_id')
      ->join('vinoteca_quiere_productos', function($join)
      {
        $join->on('vinoteca_quiere_productos.producto_id', '=', 'productos.id')
        ->where('vinoteca_quiere_productos.distribuidora_id', '=', Auth::user()->id);
      })
      ->join('users','users.id','vinoteca_quiere_productos.vinoteca_id')
      ->join('informacion_adicional', 'informacion_adicional.user_id', '=', 'users.id')
      ->join('provincias','provincias.provincia_id','informacion_adicional.provincia')
      ->join('municipios', function($join)
      {
        $join->on('municipios.provincia_id', '=', 'informacion_adicional.provincia');
        $join->on('municipios.municipio_id', '=', 'informacion_adicional.partido');
                                   // ->where('dbm.distribuidora_id', '=', Auth::user()->id );
      })
      ->join('distribuidora_bodega_municipio as dbm', function($join)
      {
        $join->on('dbm.provincia_id', '=', 'informacion_adicional.provincia');
        $join->on('dbm.municipio_id', '=', 'informacion_adicional.partido');
        $join->on('dbm.provincia_id', '=', 'vinoteca_quiere_productos.provincia_id');
        $join->on('dbm.municipio_id', '=', 'vinoteca_quiere_productos.municipio_id');
        $join->on('dbm.bodega_id'   , '=', 'vinoteca_quiere_productos.bodega_id')
        ->where('dbm.distribuidora_id', '=', Auth::user()->id );
      })->first();

      return $data;

    }

    public function show($id)
    {
      $data = self::ver_imprimir($id);

      return $data;

      return view('pedidos.visualizar',compact('data'));
    }



    public function print($id)
    {
      $data = self::ver_imprimir($id);

      $pdf = PDF::loadView('pedidos.view',compact('data'));

      return $pdf->download('pedido'.time().'.pdf');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    public function atendido($id,Request $request)
    {   

      $pedido = VinotecaQuiereProducto::where('id_promocion',$id)->first();
      $pedido->atendido         = $request->estado;
      $pedido->timestamps    = false;
      $pedido->save();

      return 'Su producto ha cambiado de estado !';

    }



    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    public function api(Request $request)
    {
        //$data = VinotecaQuiereProducto::with('producto')->with('vinoteca')->get();


      $data = Productos::
      select('productos.id as producto_id',
        'productos.marca as producto_marca',
        'productos.anio as producto_anio',
        'varietales.varietal',
        'bodega.name as nombre_bodega',
        'users.id as vinoteca_id',
        'users.name as nombre_vinoteca',
        'provincias.provincia as nombre_provincia',
        'municipios.municipio as nombre_municipio',
        'informacion_adicional.localidad',
        'informacion_adicional.domicilio',
        'informacion_adicional.numero',
        'informacion_adicional.telefono',
        'vinoteca_quiere_productos.id_promocion as record_id',
        'vinoteca_quiere_productos.atendido'
        )
      ->join('productos_varietal as varietales','varietales.id','productos.varietal')
      ->join('users as bodega','bodega.id','productos.bodega_id')
      ->join('vinoteca_quiere_productos', function($join)
      {
        $join->on('vinoteca_quiere_productos.producto_id', '=', 'productos.id')
        ->where('vinoteca_quiere_productos.distribuidora_id', '=', Auth::user()->id);
      })
      ->join('users','users.id','vinoteca_quiere_productos.vinoteca_id')
      ->join('informacion_adicional', 'informacion_adicional.user_id', '=', 'users.id')
      ->join('provincias','provincias.provincia_id','informacion_adicional.provincia')
      ->join('municipios', function($join)
      {
        $join->on('municipios.provincia_id', '=', 'informacion_adicional.provincia');
        $join->on('municipios.municipio_id', '=', 'informacion_adicional.partido');
                                   // ->where('dbm.distribuidora_id', '=', Auth::user()->id );
      })
      ->join('distribuidora_bodega_municipio as dbm', function($join)
      {
        $join->on('dbm.provincia_id', '=', 'informacion_adicional.provincia');
        $join->on('dbm.municipio_id', '=', 'informacion_adicional.partido');
        $join->on('dbm.provincia_id', '=', 'vinoteca_quiere_productos.provincia_id');
        $join->on('dbm.municipio_id', '=', 'vinoteca_quiere_productos.municipio_id');
        $join->on('dbm.bodega_id'   , '=', 'vinoteca_quiere_productos.bodega_id')
        ->where('dbm.distribuidora_id', '=', Auth::user()->id );
      })
      ->when($request->vinoteca, function ($query) use ($request) {
        return $query->where('users.name', 'ilike','%'.$request->vinoteca.'%');
      })
      ->when($request->municipio, function ($query) use ($request) {
        return $query->where('municipios.municipio', 'ilike','%'.$request->municipio.'%');
      })
      ->when($request->localidad, function ($query) use ($request) {
        return $query->where('informacion_adicional.localidad', 'ilike','%'.$request->localidad.'%');
      })
      ->when($request->telefono, function ($query) use ($request) {
        return $query->where('informacion_adicional.telefono',$request->telefono);
      })
      ->when($request->direccion, function ($query) use ($request) {
        return $query->where('informacion_adicional.domicilio', 'ilike','%'.$request->direccion.'%');
      })
      ->when($request->numero, function ($query) use ($request) {
        return $query->where('informacion_adicional.numero', 'ilike','%'.$request->numero.'%');
      })
      ->when($request->nombre_vino, function ($query) use ($request) {
        return $query->where('productos.marca', 'ilike','%'.$request->nombre_vino.'%');
      })
      ->when($request->bodega, function ($query) use ($request) {
        return $query->where('bodega.name', 'ilike','%'.$request->bodega.'%');
      })
      ->when($request->varietal, function ($query) use ($request) {
        return $query->where('productos.varietal', '=',$request->varietal);
      })
      ->when($request->anio, function ($query) use ($request) {
        return $query->where('productos.anio', '=',$request->anio);
      })

        //->distinct()
      ->paginate(25);


      return $data;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

     $pedido = VinotecaQuiereProducto::where('id_promocion',$id)->delete();

     return 'Su pedido ha sido eliminado correctamente';
   }
 }
