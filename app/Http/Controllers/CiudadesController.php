<?php

namespace App\Http\Controllers;

use App\Pais;
use App\Ciudades;
use App\Provincia;

use Illuminate\Http\Request;
use Redirect;
use App\Especializacion;

class CiudadesController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $ciudades = CIudades::orderBy('nombre', 'ASC')->get();
        $paises = Pais::orderBy('nombre','ASC')->get();
        $provincias = Provincia::orderBy('provincia','ASC')->get();

        return view('ciudades.index', compact('paises','ciudades','provincias'));
    }

    public function create()
    {
        $paises = Pais::orderBy('nombre','ASC')->get();

        return view('ciudades.agregar',compact('paises'));
    }

      public function store(Request $request)
    {
      $request->validate([
            'ciudad' => 'required',
            'provincia' => 'required',
        ]);

        $ciudad = new CIudades();
        $ciudad->timestamps    = false;
        $ciudad->nombre = $request->ciudad;
        $ciudad->provincia_id = $request->provincia;
        $ciudad->save();

        return Redirect::action('CiudadesController@index')->with('message', 'Su ciudad ha sido agregada');
    }

    public function edit($id)
    {
        $ciudad = Ciudades::whereId($id)->with('provincia')->first();
        $paises = Pais::orderBy('nombre','ASC')->get();

         return view('ciudades.edit',compact('paises','ciudad'));

    }

  public function update(Request $request){

    $request->validate([
          'ciudad' => 'required',
          'provincia' => 'required',
      ]);
        $ciudad = Ciudades::where('id', '=', $request->id)->first();

        $ciudad->timestamps = false;
        $ciudad->nombre         = $request->ciudad;
        $ciudad->provincia_id        = $request->provincia;
        $ciudad->save();

       return Redirect::action('CiudadesController@index')->with('message', 'Su ciudad ha sido editada correctamente');
    }

    public function api(Request $request)
    {
        $data = Ciudades::orderBy('nombre', 'ASC')
            ->when($request->ciudad, function ($query) use ($request) {
                return $query->where('id','=',$request->ciudad);
            })
           ->when($request->provincia, function ($query) use ($request) {
                return $query->where('provincia_id', $request->provincia);
            })
            ->when($request->pais, function($query) use ($request) {
                return  $query->whereHas('provincia', function($query) use ($request){
                    $query->where('pais_id', '=',$request->pais);
                });
            })
            ->with('provincia')
            ->paginate(25);


       return $data;
    }


    public function destroy($id)
    {
        $ciudad = Ciudades::whereId($id)->delete();
         return  'Su Ciudad ha sido eliminado correctamente';
    }
}
