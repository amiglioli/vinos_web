<?php

namespace App\Http\Controllers;

use App\Puntuacion;
use Illuminate\Http\Request;
use Redirect;
class PuntuacionTranquilosController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $puntuaciones = Puntuacion::all();
        return view('puntuaciones.index', compact('puntuaciones'));
    }

   /* public function create()
    {

        return view('puntuaciones.agregar');
    }

      public function store(Request $request)
    {
        $puntaje = new Producto();
        $puntaje->timestamps    = false;
        $puntaje->nombre = $request->nombre;
        $puntaje->save();

        return Redirect::action('ProductoController@index')->with('message', 'Su producto ha sido agregado');
    }
    */
    public function edit($id)
    {

         $puntuacion = Puntuacion::whereId($id)->first();

         return view('puntuaciones.edit',compact('puntuacion'));

    }

  public function update(Request $request){

    $request->validate([
          'puntuacion' => 'required'
      ]);
        $puntaje = Puntuacion::where('id', '=', $request->id)->first();
        $puntaje->timestamps = false;
        $puntaje->puntuacion         = $request->puntuacion;
        $puntaje->save();

       return Redirect::action('PuntuacionTranquilosController@index')->with('message', 'Su puntaje ha sido editado correctamente');
    }

    public function api(Request $request)
    {
        $data = Puntuacion::orderBy('puntuacion', 'DESC')
            ->when($request->nombre, function ($query) use ($request) {
                return $query->where('id', $request->nombre);
            })
           ->when($request->puntaje, function ($query) use ($request) {
                return $query->where('id', $request->puntaje);
            })
            ->paginate(25);
        return $data;
    }


      public function destroy($id)
    {
        $puntuacion = Puntuacion::whereId($id)->delete();

           return 'Su puntuacion ha sido eliminado correctamente';
    }

}
