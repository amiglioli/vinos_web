<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\Http\Requests;
use App\User;
use App\Role;
use URL;
use Redirect;

use Hash;
use DB;
use Cache;
use Session;
use Carbon\Carbon;
use App\Personal;

class UsersController extends Controller
{
    public function __construct(){
        $this->middleware('auth');
        $this->middleware('can:admin')->only('index','show','create','edit');
    }
    
    function index(){
        $usuarios   = User::orderby('name')->get();
        $perfiles   = Role::orderby('id')->get();
        return view('admin.users', compact('usuarios', 'perfiles'));
    }

    function show($id){
        $usersall = User::all();
        $d =  User::when($request->term, function($query) use ($request){
                        return $query->where('name', 'like', '%'.$request->term.'%')->orWhere('email', 'like', '%'.$request->term.'%')->orWhere('apellido', 'like', '%'.$request->term.'%');
                    })
                    ->when($request->tipo, function($query) use ($request){
                        return $query->where('tipo_id', $request->tipo);
                    })
                    ->when($request->id, function($query) use ($request){
                        return $query->where('id', $request->id);
                    })
                    ->paginate(20);
    	return view('admin.users', compact('d', 'usersall'));
    }

    function create(){
        $d   = new User;
        $roles  = Role::all();
    	return view('admin.user', compact('d', 'roles'));
    }

    public function store(Request $request){
        $this->validate($request, [
            'nombre'                    => 'required|max:255',
            'email'                     => 'required|email|unique:users',
            'password'                  => 'required|min:6|confirmed',
            'password_confirmation'     => 'required|min:6',
            'role'                      => 'required|min:1',
        ]);
        $user               = new User;
        $user->name         = $request->nombre;
        $user->email        = $request->email;
        $user->password     = bcrypt($request->password);
        $user->save();
        if(Auth::user()->hasRole('admin')){
            $user->roles()->sync($request->role ?: [], true);
        }else{

            $user->roles()->sync($request->role ?: [], true);
        }
        return Redirect::to('/admin/usuarios')->with('message', 'El usuario fue creado');
    }

    public function update(Request $request){

        $user = User::where('id', '=', $request->id)->first();

        if($request->password != ""){

            $this->validate($request, [
                'nombre'                => 'required|max:255',
                'email'                 => 'required|email|unique:users,email,'.$user->id,
                'password_actual'       => 'required|min:6',
                'password'              => 'required|min:6|confirmed',
                'password_confirmation' => 'required|min:6'
            ]);

            if(!Hash::check($request->password_actual, $user->password)){
                return Redirect::back()->with('error','La contraseña especificada no coincide con la contraseña actual');
            }

        }else{
            $this->validate($request, [
                'nombre'    =>  'required|max:255',
                'email'     =>  'required|email|unique:users,email,'.$user->id
            ]);
        }

        $user = User::whereId($request->id)->first();        
        $user->name  		= $request->nombre;
        $user->email 		= $request->email;
        if($request->password != ""){
            $user->password = bcrypt($request->password);
        }

        $user->save();
        $user->roles()->sync($request->role ?: [], true);
        return Redirect::to('/admin/usuarios')->with('message', 'El usuario fue actualizado');
    }


    public function destroy($id){
        $user = User::whereId($id)->delete();
        return Redirect::to('admin/usuarios')->with('message', 'El usuario fue eliminado');
    }

    public function edit($id){
        $d      = User::whereId($id)->with('roles')->first();
        $roles  = Role::all();
        $url    = '';
        return view('admin.user', compact('d', 'roles', 'url'));
    }

    public function api(Request $request){
        $data = User::with('roles')
                ->when($request->usuario, function($query) use ($request) {
                    return $query->where('id', $request->usuario);
                })
                ->when($request->nombre, function($query) use ($request) {
                    return $query->where('name','ilike', '%'.$request->nombre.'%');
                })
                ->when($request->mail, function($query) use ($request) {
                    return $query->where('id', $request->mail);
                })
                ->when($request->perfil, function($query) use ($request) {
                    return $query->whereHas('roles', function($q) use ($request) {
                        $q->where('id', $request->perfil);
                    });
                })
                ->when($request->nombre_rol, function($query) use ($request) {
                    return $query->whereHas('roles', function($q) use ($request) {
                        $q->where('name', $request->nombre_rol);
                    });
                })
                ->when($request->user_real, function($query) use ($request) {
                     return $query->where('usuario_real','=',$request->user_real);
                })
                
                ->with('imagen')
                ->with('informacion_adicional')
                ->with('bodega_distribucion')
                ->with('productos_vinoteca')
                ->paginate(25);
        return $data;
    }        

}
