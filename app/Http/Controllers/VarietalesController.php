<?php

namespace App\Http\Controllers;

use App\Producto;
use App\Varietal;
use App\ProductoTipo;
use Illuminate\Http\Request;
use Redirect;
use App\Especializacion;

class VarietalesController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $varietales = Varietal::orderBy('varietal', 'ASC')->get();
        $producto_tipo = ProductoTipo::orderBy('tipo', 'ASC')->get();

        return view('varietales.index', compact('varietales','producto_tipo'));
    }

    public function create()
    {
        $producto_tipo = ProductoTipo::all();
        return view('varietales.agregar',compact('producto_tipo'));
    }

      public function store(Request $request)
    {
      $request->validate([
            'varietal' => 'required',
            'tipo' => 'required',
        ]);
        $varietales = new Varietal();
        $varietales->timestamps    = false;
        $varietales->varietal = $request->varietal;
        $varietales->producto_tipo_id = $request->tipo;
        $varietales->save();

        return Redirect::action('VarietalesController@index')->with('message', 'Su varietal ha sido agregado');
    }

    public function edit($id)
    {

         $varietal = Varietal::whereId($id)->first();
         $producto_tipo = ProductoTipo::all();

         return view('varietales.edit',compact('varietal','producto_tipo'));

    }

  public function update(Request $request){

    $request->validate([
          'varietal' => 'required',
          'tipo' => 'required',
      ]);
        $varietales = Varietal::where('id', '=', $request->id)->first();
        $varietales->timestamps = false;
        $varietales->varietal = $request->varietal;
        $varietales->producto_tipo_id = $request->tipo;
        $varietales->save();

       return Redirect::action('VarietalesController@index')->with('message', 'Su varietal ha sido editado correctamente');
    }

    public function api(Request $request)
    {
        $data = Varietal::orderBy('varietal', 'ASC')
            ->when($request->varietal, function ($query) use ($request) {
                return $query->where('id', $request->varietal);
            })
            ->when($request->tipo, function($query) use ($request) {
                return  $query->whereHas('Producto_tipo', function($query) use ($request){
                    $query->where('id', '=',$request->tipo);
                });
            })
            ->with('Producto_tipo')
            ->paginate(25);
        return $data;
    }


    public function destroy($id)
    {
        $varietal = Varietal::whereId($id)->delete();
         return  'Su varietal ha sido eliminado correctamente';
    }
}
