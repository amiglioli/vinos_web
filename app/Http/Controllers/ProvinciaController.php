<?php

namespace App\Http\Controllers;

use App\Pais;
use App\Provincia;
use Illuminate\Http\Request;
use Redirect;
use App\Especializacion;

class ProvinciaController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $provincias = Provincia::orderBy('provincia', 'ASC')->get();
        $paises = Pais::orderBy('nombre', 'ASC')->get();

        return view('provincias.index', compact('paises','provincias'));
    }

    public function create()
    {
        $paises = Pais::all();

        return view('provincias.agregar',compact('paises'));
    }

      public function store(Request $request)
    {
      $request->validate([
            'provincia' => 'required',
            'pais' => 'required',
        ]);
        $provincia = new Provincia();
        $provincia->timestamps    = false;
        $provincia->provincia = $request->provincia;
        $provincia->pais_id = $request->pais;
        $provincia->save();

        return Redirect::action('ProvinciaController@index')->with('message', 'Su Provincia ha sido agregada');
    }

    public function edit($id)
    {

         $provincia = Provincia::where('provincia_id',$id)->first();
         $paises = Pais::all();

         return view('provincias.edit',compact('provincia','paises'));

    }

  public function update(Request $request){

    $request->validate([
          'provincia' => 'required',
          'pais' => 'required',
      ]);
        $provincia = Provincia::where('provincia_id', '=', $request->id)->first();

        $provincia->timestamps = false;
        $provincia->provincia         = $request->provincia;
        $provincia->pais_id        = $request->pais;
        $provincia->save();

       return Redirect::action('ProvinciaController@index')->with('message', 'Su Provincia ha sido editado correctamente');
    }

    public function api(Request $request)
    {
        $data = Provincia::orderBy('provincia', 'ASC')
            ->when($request->provincia, function ($query) use ($request) {
                return $query->where('provincia_id', $request->provincia);
            })
            ->when($request->pais, function($query) use ($request) {
                return  $query->whereHas('pais', function($query) use ($request){
                    $query->where('id', '=',$request->pais);
                });
            })
            ->with('pais')
            ->with('municipio')
            ->paginate(25);
        return $data;
    }

    public function destroy($id)
    {
        $provincia = Provincia::where('provincia_id',$id)->delete();
         return  'Su Provincia ha sido eliminado correctamente';
    }
}
