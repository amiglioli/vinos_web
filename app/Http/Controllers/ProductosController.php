<?php

namespace App\Http\Controllers;
use App\Productos;
use App\ProductoTipo;
use App\Producto;
use App\Varietal;
use App\User;
use App\Pais;
use App\Ranking;
use App\Region;
use Redirect;
use File;

use Illuminate\Http\Request;

class ProductosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $producto = Producto::orderBy('nombre','ASC')->get();
        $producto_tipo = ProductoTipo::orderBy('tipo','ASC')->get();
        $varietales = Varietal::orderBy('varietal','ASC')->get();
        $bodegas   = User::with('roles')->whereHas('roles', function($q){$q->where('name', 'bodega');
        })->orderBy('name','ASC')->get();
        $productos = Productos::select('id','marca')->orderBy('marca','ASC')->get();

        return view('productos.index',compact('producto','producto_tipo','varietales','bodegas','productos'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $productos = Producto::orderBy('nombre','ASC')->get();
        $paises = Pais::orderBy('nombre','ASC')->get();
        $bodegas =  User::with('roles')->whereHas('roles', function($q){ $q->where('name', 'bodega');})->get();
        $regiones = Region::orderBy('nombre','ASC')->get();
        return view('productos.agregar',compact('productos','bodegas','paises','regiones'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

      $request->validate([
         'producto' => 'required|string|max:255',
         'tipo' => 'required|string|max:255',
         //'varietal' => 'required|string|max:25',
         //'corte' => 'required|string|max:255',
         'marca' => 'required|string|max:255',
         //'imagen' => 'required|string|max:255',
         'bodega_id' => 'required',
         'origen_pais' => 'required|string|max:255',
         'origen_provincia' => 'required|string|max:255',
         'region' => 'required|string|max:255',
         'ciudad' => 'required|string|max:255',
         'anio' => 'required|string|max:255',
         ]);

      $producto = new Productos();
      $producto->producto = $request->producto;
      $producto->tipo = $request->tipo;
      $producto->varietal = $request->varietal;
      $producto->corte = $request->corte;
      $producto->marca = $request->marca;
      $producto->bodega_id = $request->bodega_id;
      $producto->origen_pais = $request->origen_pais;
      $producto->origen_provincia = $request->origen_provincia;
      $producto->origen_region = $request->region;
      $producto->origen_ciudad = $request->ciudad;
      $producto->anio = $request->anio;

      if($request->imagen != null){
        $data = base64_decode(preg_replace('#^data:image/\w+;base64,#i', '', $request->imagen));
        $png_url = "promocion-".time().".png";
        $filepath = public_path().'/imgproductos/'.$png_url;
        $ruta_guardado = 'imgproductos/'.$png_url;
        file_put_contents($filepath,$data);
        $producto->img_etiqueta = $ruta_guardado;
    }
       $producto->save();

      return Redirect::action('ProductosController@index')->with('message', 'Su producto ha sido agregado correctamente');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $producto_id = $id;
        return view('productos.show',compact('producto_id'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
       $producto = Productos::whereId($id)
                  ->with('producto')
                  ->with('producto_tipo')
                  ->with('varietales')
                  ->with('bodega')
                  ->first();

      $bodegas =  User::with('roles')->whereHas('roles', function($q){ $q->where('name', 'bodega');})->get();
      $productos = Producto::all();
      $paises = Pais::orderBy('nombre','ASC')->get();
      $regiones = Region::orderBy('nombre','ASC')->get();

      return view('productos.edit',compact('producto','productos','bodegas','paises','regiones'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
      //dd($request->input());
      $request->validate([
         'producto' => 'required|string|max:255',
         'tipo' => 'required|string|max:255',
         //'varietal' => 'required|string|max:25',
         //'corte' => 'required|string|max:255',
         'marca' => 'required|string|max:255',
         //'imagen' => 'required|string|max:255',
         'bodega_id' => 'required',
         'origen_pais' => 'required|string|max:255',
         'origen_provincia' => 'required|string|max:255',
         'region' => 'required|string|max:255',
         'ciudad' => 'required|string|max:255',
         'anio' => 'required|string|max:255',
         ]);

      $producto = Productos::whereId($id)->first();
      $producto->producto = $request->producto;
      $producto->tipo = $request->tipo;
      $producto->varietal = $request->varietal;
      $producto->corte = $request->corte;
      $producto->marca = $request->marca;
      $producto->bodega_id = $request->bodega_id;
      $producto->origen_pais = $request->origen_pais;
      $producto->origen_provincia = $request->origen_provincia;
      $producto->origen_region = $request->region;
      $producto->origen_ciudad = $request->ciudad;
      $producto->anio = $request->anio;

      if($producto->img_etiqueta != null){
        if(file_exists(public_path().'/'.$producto->img_etiqueta)){
         File::delete(public_path().'/'.$producto->img_etiqueta);
        }
      }

      if($request->imagen != null){
        $data = base64_decode(preg_replace('#^data:image/\w+;base64,#i', '', $request->imagen));
        $png_url = "producto-".time().".png";
        $filepath = public_path().'/imgproductos/'.$png_url;
        $ruta_guardado = 'imgproductos/'.$png_url;
        file_put_contents($filepath,$data);
        $producto->img_etiqueta = $ruta_guardado;
    }

        $producto->save();
      return Redirect::action('ProductosController@index')->with('message', 'Su producto ha sido editado correctamente');
    }

    public function api(Request $request)
    {
        $data = Productos::orderBy('marca', 'ASC')
            ->when($request->producto_id, function ($query) use ($request) {
                return $query->where('id', $request->producto_id);
            })
            ->when($request->anio, function ($query) use ($request) {
                return $query->where('anio', $request->anio);
            })
            ->when($request->producto, function($query) use ($request) {
                return  $query->whereHas('producto', function($query) use ($request){
                    $query->where('id', '=',$request->producto);
                });
            })
            ->when($request->producto_tipo, function($query) use ($request) {
                return  $query->whereHas('producto_tipo', function($query) use ($request){
                    $query->where('id', '=',$request->producto_tipo);
                });
            })
            ->when($request->tipos, function($query) use ($request) {
                return  $query->whereHas('producto_tipo', function($query) use ($request){
                    $query->where('producto_id', '=',$request->tipos);
                });
            })
            ->when($request->varietal, function($query) use ($request) {
                return  $query->whereHas('varietales', function($query) use ($request){
                    $query->where('id', '=',$request->varietal);
                });
            })
            ->when($request->marca, function($query) use ($request) {
                      return $query->where('marca','ilike',"%".$request->marca."%");
            })
            ->when($request->nombre, function($query) use ($request) {
                      return $query->where('marca','ilike',"%".$request->nombre."%");
            })
            ->when($request->bodega, function($query) use ($request) {
                return  $query->whereHas('bodega', function($query) use ($request){
                   $query->where('id',$request->bodega);
                });
            })
            ->with('producto')
            ->with('producto_tipo')
            ->with('varietales')
            ->with('bodega')
            ->with('pais')
            ->with('provincia')
            ->with('region')
            ->with('ciudad')
            ->with('cata_cerrada')
            ->with('distribuidora_vino')
            ->paginate(25);

          return $data;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
      $producto = Productos::whereId($id)->first();

      if($producto->img_etiqueta != null){
          if(file_exists(public_path().'/'.$producto->img_etiqueta)){
            File::delete(public_path().'/'.$producto->img_etiqueta);
      }
    }
          $data = Productos::whereId($id)->delete();
          return 'Su producto ha sido eliminado correctamente';
  }
}
