<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Redirect;
use App\Promocion;
use App\PromocionRecurrente;
use App\PromocionFija;
use Auth;

class PromocionesController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
       
      return view('promociones.index');
    }

    public function create()
    {
        
        return view('promociones.agregar');
    }

      public function store(Request $request)
    {
        
        $request->validate([
            'tipo'        => 'required',
            'descuento'   => 'required',
            'descripcion' => 'required',
        ]);

        $promocion                = new Promocion();
        $promocion->timestamps    = true;
        $promocion->tipo          = $request->tipo;
        $promocion->estado        = true;
        $promocion->vinoteca_id   = Auth::user()->id;
        $promocion->save();


        //dd($promocion);
        if($request->tipo == 'fija'){
            $promocion_fija                         = new PromocionFija();
            $promocion_fija->timestamps             = false;
            $promocion_fija->promocion_id           = $promocion->id;
            $promocion_fija->fecha                  = $request->fecha;
            $promocion_fija->porcentaje_descuento   = $request->descuento;
            $promocion_fija->descripcion            = $request->descripcion;
            $promocion_fija->save();
        }else if($request->tipo == 'recurrente'){
            $promocion_recurrente                       = new PromocionRecurrente();
            $promocion_recurrente->timestamps           = false;
            $promocion_recurrente->promocion_id         = $promocion->id;
            $promocion_recurrente->porcentaje_descuento = $request->descuento;
            $promocion_recurrente->descripcion          = $request->descripcion;
            $promocion_recurrente->lunes                = (!empty($request->dias[0])) ? true : false;
            $promocion_recurrente->martes               = (!empty($request->dias[1])) ? true : false;
            $promocion_recurrente->miercoles            = (!empty($request->dias[2])) ? true : false;
            $promocion_recurrente->jueves               = (!empty($request->dias[3])) ? true : false;
            $promocion_recurrente->viernes              = (!empty($request->dias[4])) ? true : false;
            $promocion_recurrente->sabado               = (!empty($request->dias[5])) ? true : false;
            $promocion_recurrente->domingo              = (!empty($request->dias[6])) ? true : false;
            $promocion_recurrente->save();
        }


        return Redirect::action('PromocionesController@index')->with('message', 'Promoción agregada correctamente');
    }

    public function edit($id)
    {
        $promocion = Promocion::whereId($id)
                  ->with('promocion_fija')
                  ->with('promocion_recurrente')
                  ->first();
      
         return view('promociones.editar',compact('promocion'));

    }

  public function update(Request $request){

        $request->validate([
            'descuento'   => 'required',
            'descripcion' => 'required',
        ]);

       
        if($request->tipo == 'fija'){
            $promocion_fija                         = PromocionFija::where('promocion_id', '=', $request->id)->first();
            $promocion_fija->timestamps             = false;
            $promocion_fija->fecha                  = $request->fecha;
            $promocion_fija->porcentaje_descuento   = $request->descuento;
            $promocion_fija->descripcion            = $request->descripcion;
            $promocion_fija->save();
        }else if($request->tipo == 'recurrente'){
            $promocion_recurrente                       = PromocionRecurrente::where('promocion_id', '=', $request->id)->first();
            $promocion_recurrente->timestamps           = false;
            $promocion_recurrente->porcentaje_descuento = $request->descuento;
            $promocion_recurrente->descripcion          = $request->descripcion;
            $promocion_recurrente->lunes                = (!empty($request->dias[0])) ? true : false;
            $promocion_recurrente->martes               = (!empty($request->dias[1])) ? true : false;
            $promocion_recurrente->miercoles            = (!empty($request->dias[2])) ? true : false;
            $promocion_recurrente->jueves               = (!empty($request->dias[3])) ? true : false;
            $promocion_recurrente->viernes              = (!empty($request->dias[4])) ? true : false;
            $promocion_recurrente->sabado               = (!empty($request->dias[5])) ? true : false;
            $promocion_recurrente->domingo              = (!empty($request->dias[6])) ? true : false;
            $promocion_recurrente->save();
        }


       return Redirect::action('PromocionesController@index')->with('message', 'Su promoción ha sido editada correctamente');
    }

    public function api(Request $request)
    {
        $data = Promocion::where('vinoteca_id', Auth::user()->id)
                
                /*->whereHas('promocion_recurrente', function($q){
                    $q->where('martes', true);
                    //$q->orWhere('lunes', true);
                    //etc
                })*/
                 ->when($request->dia, function ($query) use ($request) {
                    return $query->where('tipo', $request->dia);
                })
                 ->when($request->estado, function ($query) use ($request) {
                    return $query->where('estado', $request->estado);
                })
                ->when($request->porcentaje_descuento, function ($query) use ($request) {
                    $query->whereHas('promocion_recurrente', function($q) use ($request){
                       return $q->where('porcentaje_descuento', $request->porcentaje_descuento);
                        //$q->orWhere('lunes', true);
                        //etc
                    })
                    ->orWhereHas('promocion_fija', function($q) use ($request){
                       return $q->where('porcentaje_descuento', $request->porcentaje_descuento);
                        //$q->orWhere('lunes', true);
                        //etc
                    });
                })
                ->when($request->descripcion, function ($query) use ($request) {
                    $query->whereHas('promocion_recurrente', function($q) use ($request){
                       return $q->where('descripcion', 'ilike','%'.$request->descripcion.'%');
                        //$q->orWhere('lunes', true);
                        //etc
                    })
                    ->orWhereHas('promocion_fija', function($q) use ($request){
                       return $q->where('descripcion', 'ilike', '%'.$request->descripcion.'%');
                        //$q->orWhere('lunes', true);
                        //etc
                    });
                })
                
                ->where(function($query) {
                
                    return $query->where('tipo', 'fija')
                                ->orWhere('tipo', 'recurrente');
                })
                
                ->with('promocion_fija')
                ->with('promocion_recurrente')
                ->orderBy('id', 'DESC')
                ->paginate(10);
        //dd($data);
       return $data;
    }


    public function destroy($id)
    {
        $promocion = Promocion::whereId($id)->delete();
         return  'Su Promoción ha sido eliminado correctamente';
    }

    public function activar_promocion(Request $request){
        $promocion = Promocion::where('id', '=', $request->id_promocion)->first();

        $promocion->timestamps = true;
        $promocion->estado         = $request->estado;
        $promocion->save();

        return  $promocion;
    }
}
