<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Resultado;

class RankingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }


    public function api(Request $request)
    {   
        $id_producto = $request->id_producto;

        $data = Resultado::where('producto_id',$id_producto)
        ->when($request->cata, function($query) use ($request) {
            return  $query->whereHas('ranking.cata', function($query) use ($request){
               $query->where('descripcion','ilike',"%".$request->cata."%");
            });
        })
        ->when($request->fecha, function($query) use ($request) {
            return  $query->whereHas('ranking.cata', function($query) use ($request){
                $query->where('fecha', '=',$request->fecha);
            });
        })
        ->when($request->puesto, function ($query) use ($request) {
            return $query->where('puesto',$request->puesto);
        })
        ->when($request->puntaje, function ($query) use ($request) {
            //echo ;
              return  $query->where('puntaje','>=',$request->puntaje)->where('puntaje','<=',$request->puntaje+1);
            
        })
        ->with('producto')
        ->with('ranking')
        ->paginate(25);

        return $data;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
