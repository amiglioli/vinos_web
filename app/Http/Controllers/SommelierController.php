<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Provincia;
use App\Sommelier;
use Redirect;
class SommelierController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $provincias = Provincia::get();

        return view('sommelier.agregar',compact('provincias'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //

        $request->validate([
            'dni' => 'required',
            'apellido' => 'required',
            'nombre' => 'required',
            'edad' => 'required',
            'celular' => 'required',
            'email' => 'required',
            'titulo' => 'required',
            'obtenido' => 'required',
            'anio_egreso' => 'required',
            'provincia' => 'required',
            'municipio' => 'required',
            'experiencia' => 'required',
        ]);

        $somellier                   = new Sommelier();
        $somellier->timestamps       = false;
        $somellier->dni = $request->dni;
        $somellier->apellido = $request->apellido;
        $somellier->nombre = $request->nombre;
        $somellier->fecha_nacimiento = $request->edad;
        $somellier->celular = $request->celular;
        $somellier->email = $request->email;
        $somellier->titulo = $request->titulo;
        $somellier->obtenido = $request->obtenido;
        $somellier->anio_egreso = $request->anio_egreso;
        $somellier->provincia_id = $request->provincia;
        $somellier->municipio_id = $request->municipio;
        $somellier->experiencia = $request->experiencia;
        $somellier->save();



        return Redirect::action('SommelierController@create')->with('message', 'Sus datos fueron ingresados correctamente');


    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
