<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Provincia;
use App\Role;
use App\InformacionAdicional;
use DB;
use App\VinotecaPromocion;
use App\CataProductos;
use App\Cata;
use Auth;
use App\Promocion;
use App\Productos;

use App\Ranking;
class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {


           $data = [];
           //$data['promociones'] = VinotecaPromocion::where('estado',1)->count();
           $data['bodegas'] = User::with('roles')->whereHas('roles', function($q){
                                                                        $q->where('name', 'bodega');
                                                                    })->where('usuario_real',1)->count();

           $data['distribuidoras'] = User::with('roles')->whereHas('roles', function($q){
                                                                        $q->where('name', 'distribuidora');
                                                                    })->count();
           $data['vinotecas'] = User::with('roles')->whereHas('roles', function($q){
                                                                        $q->where('name', 'vinoteca');
                                                                    })->count();

            $ultimos['bodega'] = User::with('roles')->whereHas('roles', function($q){
                                                                    $q->where('name', 'bodega');
                                                                    })->where('usuario_real',1)->orderBy('id','DESC')->first();

            $ultimos['vinotecas'] = User::with('roles')->whereHas('roles', function($q){
                                                                        $q->where('name', 'vinoteca');
                                                                        })->orderBy('id','DESC')->first();

            $ultimos['distribuidoras'] = User::with('roles')->whereHas('roles', function($q){
                                                            $q->where('name', 'distribuidora');
                                                        })->orderBy('id','DESC')->first();


            $promociones_activas = Promocion::where('estado',true)->count();

            $perfiles = User::with('roles')
                        ->with('informacion_adicional')
                        ->whereHas('roles', function($q){
                            $q->whereIn('name', ["bodega","distribuidora","vinoteca"]);
                        })->where('usuario_real',1)->get();

                        //dd($perfiles);

        //dd(Auth::user()->informacion_adicional->municipio->municipio);
        $id_personal = Auth::user()->id;


        $informacion_adicional = Auth::user()->informacion_adicional;

        $init = (object) [ "localidad" => $informacion_adicional->localidad, "nombre_provincia" => $informacion_adicional->provincias->provincia];





        return view('home.index', compact('data','init','ultimos','perfiles','promociones_activas'));
    }

  public  function mapa(Request $request)
  {

    $perfiles = $request->input('perfiles');
    if($perfiles != null){

      $vinoteca_promociones = Promocion::where('estado',1)->with('promocion_fija')->with('promocion_recurrente')->with('promocion_imagen')->get();
      $promociones = [];

      foreach($vinoteca_promociones as $vp){

        $promociones[$vp->vinoteca_id][] = $vp;
      }

      $data = [];
      $data['promociones'] = $promociones;


      $data['usuarios'] = User::with('informacion_adicional')
                            ->with('roles')->whereHas('roles', function($q) use ($perfiles){
                            $q->whereIn('name', $perfiles);
                            })->where('usuario_real',1)->get();


      return $data;
    }
  }

  public function cargar_bodegas()
  {
      $bodegas = DB::table('bodegas')->get();
      foreach($bodegas as $b)
      {
        $password = str_random(10);
        $user     = User::create([
            'name'     => $b->bodegas,
            'email'    => $b->bodegas,
            'password' =>bcrypt($password),
            'usuario_real' => 0,
            'aprobado' => false
        ]);

      $rol = Role::where('name','bodega')->first();

      $user->roles()->sync($rol ?: [], true);


  }
        return $user;
}
}
