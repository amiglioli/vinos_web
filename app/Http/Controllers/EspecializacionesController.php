<?php

namespace App\Http\Controllers;

use App\Producto;
use Illuminate\Http\Request;
use Redirect;
use App\Especializacion;

class EspecializacionesController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $especializaciones = Especializacion::orderBy('especializacion', 'ASC')->get();
        return view('especializaciones.index', compact('especializaciones'));
    }

    public function create()
    {

        return view('especializaciones.agregar');
    }

      public function store(Request $request)
    {
      $request->validate([
            'nombre' => 'required',
        ]);
        $especializacion = new Especializacion();
        $especializacion->timestamps    = false;
        $especializacion->nombre = $request->nombre;
        $especializacion->save();

        return Redirect::action('ProductoController@index')->with('message', 'Su Especializacion ha sido agregada');
    }

    public function edit($id)
    {

         $especializacion = Especializacion::whereId($id)->first();

         return view('especializaciones.edit',compact('especializacion'));

    }

  public function update(Request $request){

    $request->validate([
          'especializacion' => 'required',
      ]);

        $especializacion = Especializacion::where('id', '=', $request->id)->first();

        $especializacion->timestamps = false;
        $especializacion->especializacion         = $request->especializacion;
        $especializacion->save();

       return Redirect::action('EspecializacionesController@index')->with('message', 'Su Especializacion ha sido editado correctamente');
    }

    public function api(Request $request)
    {
        $data = Especializacion::orderBy('especializacion', 'ASC')
            ->when($request->especializacion, function ($query) use ($request) {
                return $query->where('id', $request->especializacion);
            })
            ->paginate(25);
        return $data;
    }

    public function destroy($id)
    {
        $especializacion = Especializacion::whereId($id)->delete();
         return  'Su Especializacion ha sido eliminada correctamente';
    }
}
