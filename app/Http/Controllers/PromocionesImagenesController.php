<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Redirect;
use App\Promocion;
use App\PromocionImagen;
use File;
use Auth;

class PromocionesImagenesController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */

    public function create()
    {
    
        return view('promociones.agregar_imagen');
    }

      public function store(Request $request)
    {
        

       
        $request->validate([
            'tipo'        => 'required',
            'img'   => 'required',
            'start' => 'required',
            'end' => 'required',
        ]);


        $url_img = $request->img;
        $start = $request->start;
        $end = $request->end;


        $data = base64_decode(preg_replace('#^data:image/\w+;base64,#i', '', $url_img));
        $png_url = "promocion-".time().".png";
        $filepath = public_path().'/imgpromociones/'.$png_url;
        $ruta_guardado = 'imgpromociones/'.$png_url;

        file_put_contents($filepath,$data);

        if(file_exists($filepath)) {
            $promocion                = new Promocion();
            $promocion->timestamps    = true;
            $promocion->tipo          = $request->tipo;
            $promocion->estado        = true;
            $promocion->vinoteca_id   = Auth::user()->id;
            $promocion->save();

            $promocion_imagen                = new PromocionImagen();
            $promocion_imagen->timestamps    = false;
            $promocion_imagen->promocion_id  = $promocion->id;
            $promocion_imagen->fecha_desde   = $request->start;
            $promocion_imagen->fecha_hasta   = $request->end;
            $promocion_imagen->ruta_imagen   = $ruta_guardado;
            $promocion_imagen->save();
            return "ok";
        }else{
            abort(500);
        }
    }

    public function edit($id)
    {
        $promocion = Promocion::whereId($id)
                  ->with('promocion_imagen')
                  ->first();
      
         return view('promociones.editar_imagen',compact('promocion'));

    }

  public function update(Request $request){



        $request->validate([
            //'tipo'        => 'required',
            //'img'   => 'required',
            'start' => 'required',
            'end' => 'required',
        ]);

        $url_img = $request->img;
        $start = $request->start;
        $end = $request->end;

        $promocion_imagen = PromocionImagen::where('promocion_id', '=', $request->id)->first();
        $ruta = $promocion_imagen->ruta_imagen;
        if($url_img != null){
            //entra cuando se intento editar la imagen
            if(file_exists(public_path().'/'.$ruta))
                File::delete(public_path().'/'.$ruta);
              
              
            $data = base64_decode(preg_replace('#^data:image/\w+;base64,#i', '', $url_img));
            $png_url = "promocion-".time().".png";
            $filepath = public_path().'/imgpromociones/'.$png_url;
            $ruta_guardado = 'imgpromociones/'.$png_url;

            file_put_contents($filepath,$data);

            if(file_exists($filepath)) {

                
                $promocion_imagen->timestamps    = false;
                $promocion_imagen->fecha_desde   = $request->start;
                $promocion_imagen->fecha_hasta   = $request->end;
                $promocion_imagen->ruta_imagen   = $ruta_guardado;
                $promocion_imagen->save();
                return "ok";
            }else{
                abort(500);
            }
           
        }else{
            //entra porque la imagen no fue tocada
            $promocion_imagen->timestamps    = false;
            $promocion_imagen->fecha_desde   = $request->start;
            $promocion_imagen->fecha_hasta   = $request->end;
            //$promocion_imagen->ruta_imagen   = $ruta_guardado;
            $promocion_imagen->save();
            return "ok";
        }

    }

    public function api(Request $request)
    {
        $data = Promocion::where('vinoteca_id', Auth::user()->id)
                ->where('tipo', 'imagen')
                ->orderBy('id', 'DESC')
                ->when($request->fecha_desde, function ($query) use ($request) {
                    $query->whereHas('promocion_imagen', function($q) use ($request){
                       return $q->where('fecha_desde','>=', $request->fecha_desde);
                        //$q->orWhere('lunes', true);
                        //etc
                    });
                })
                ->when($request->fecha_hasta, function ($query) use ($request) {
                    $query->whereHas('promocion_imagen', function($q) use ($request){
                       return $q->where('fecha_hasta','<=', $request->fecha_hasta);
                        //$q->orWhere('lunes', true);
                        //etc
                    });
                })
                ->when($request->estado, function ($query) use ($request) {
                    return $query->where('estado', $request->estado);
                })
                ->with('promocion_imagen')
                ->paginate(10);
        //dd($data);
       return $data;
    }


    public function destroy($id)
    {


        $promocion_imagen = PromocionImagen::where('promocion_id', '=', $id)->first();
        $ruta = $promocion_imagen->ruta_imagen;
        if(file_exists(public_path().'/'.$ruta))
            File::delete(public_path().'/'.$ruta);

        $promocion_imagen = Promocion::whereId($id)->delete();
         return  'Su Promoción ha sido eliminado correctamente';
    }


}
