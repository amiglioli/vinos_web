<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Puntuacion;
use App\Cata;
use App\Catador;
use App\CataProductos;
use App\FichaTranquilo;
use Redirect;
class FichaTranquiloController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = Puntuacion::orderBy('puntuacion','DESC')->get();
        return view('ficha_tranquilo.index',compact('data'));
    }   

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $ficha                     = new FichaTranquilo();
        $ficha->timestamps         = false;
        $ficha->id_cata            = $request->cata;
        $ficha->id_catador         = $request->catadores;
        $ficha->puntaje            = $request->puntaje;
        $ficha->nro_muestra        = $request->muestra_nro;
        $ficha->vista              = $request->vista;
        $ficha->nariz              = $request->nariz;
        $ficha->gusto              = $request->gusto;
        $ficha->marca              = $request->marca;
        $ficha->edad               = $request->edad;
        $ficha->id_especializacion = $request->id_especializacion;
        $ficha->id_producto = $request->id_prod;
        
        $ficha->save();

        return Redirect::action('FichaTranquiloController@show',$request->cata)->with('message', 'Su ficha ha sido agregada');
    }

    public function show($id)
    {
        $campos = ['vista','nariz','gusto','marca'];
        $puntos = Puntuacion::orderBy('puntuacion','DESC')->get();
        $cata   = Cata::whereId($id)->with('catadores')->first();

        return view('ficha_tranquilo.ficha',compact('puntos','campos','cata'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $campos = ['vista','nariz','gusto','marca'];
        $puntos = Puntuacion::orderBy('puntuacion','DESC')->get();

        $data = FichaTranquilo::whereId($id)->with('cata')->with('catador')->with('especializacion')->first();

        return view('ficha_tranquilo.edit',compact('campos','puntos','data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $ficha                     = FichaTranquilo::whereId($id)->first();
        $ficha->timestamps         = false;
        $ficha->puntaje            = $request->puntaje;
        $ficha->vista              = $request->vista;
        $ficha->nariz              = $request->nariz;
        $ficha->gusto              = $request->gusto;
        $ficha->marca              = $request->marca;
        $ficha->save();

        return Redirect::action('FichaTranquiloController@index')->with('message', 'Su ficha ha sido editada correctamente');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $catador = FichaTranquilo::whereId($id)->delete();

         return 'Su ficha ha sido eliminado correctamente';
    }

    public function api(Request $request)
    {
        $data = FichaTranquilo::orderBy('puntaje', 'DESC')
        ->when($request->vista, function ($query) use ($request) {
            return $query->where('vista','=',$request->vista);
        })
        ->when($request->nariz, function ($query) use ($request) {
            return $query->where('nariz','=',$request->nariz);
        })
        ->when($request->muestra, function ($query) use ($request) {
            return $query->where('nro_muestra','=',$request->muestra);
        })
        ->when($request->gusto, function ($query) use ($request) {
            return $query->where('gusto','=',$request->gusto);
        })
        ->when($request->marca, function ($query) use ($request) {
            return $query->where('marca','=',$request->marca);
        })
        ->when($request->descripcion, function($query) use ($request) {
            return  $query->whereHas('cata', function($query) use ($request){
                $query->where('descripcion','ilike',"%".$request->descripcion."%");
            });
        })
        ->when($request->vino, function($query) use ($request) {
            return  $query->whereHas('producto', function($query) use ($request){
                $query->where('marca','ilike',"%".$request->vino."%");
            });
        })
        ->when($request->lugar, function($query) use ($request) {
            return  $query->whereHas('cata', function($query) use ($request){
                $query->where('lugar','ilike',"%".$request->lugar."%");
            });
        })
        ->when($request->catador, function($query) use ($request) {

            return $query->whereHas('catador', function($query2) use ($request){
                            $query2->where('apellido','ilike',"%".$request->catador."%")
                            ->orWhere('nombre','ilike',"%".$request->catador."%");
                        });
        })
        ->when($request->anio, function($query) use ($request) {
            return  $query->whereHas('cata', function($query) use ($request){
                $query->where('fecha','=',$request->anio);
            });
        })
        ->with('vista')
        ->with('nariz')
        ->with('gusto')
        ->with('marca')
        ->with('cata')
        ->with('catador')
        ->with('producto')
        ->paginate(25);
        return $data;
    }

    public function muestras($id_catador,$id_cata)
    {
        $data['catadores'] = Catador::whereId($id_catador)->with('especializacion')->first();

        $data['muestras_pendientes'] = CataProductos::where('id_cata',$id_cata)
        ->whereNotIn('cata_productos.muestra_nro', function ($query) use ($id_cata,$id_catador) {
          $query->select('nro_muestra')
          ->from('ficha_tranquilos_datos')
          ->where('id_cata', $id_cata)
          ->where('id_catador', $id_catador);
      })
        //->orderBy('cata_productos.muestra_nro','DESC')
        ->get();
        
        return $data;
    }
}
