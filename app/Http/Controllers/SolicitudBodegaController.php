<?php

namespace App\Http\Controllers;

use App\SolicitudBodega;
use App\User;
use App\Role;
use DB;
use Illuminate\Http\Request;
use Redirect;
class SolicitudBodegaController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $solicitudes = SolicitudBodega::where('leido',0)->orderBy('nombre_bodega', 'ASC')->get();
        return view('solicitud_bodega.index', compact('solicitudes'));
    }

      public function store(Request $request)
    {
          $this->validate($request, [
              'user'                    => 'required|max:255',
              'email'                     => 'required|email|unique:users',
          ]);

            $user = User::where('email', '=', $request->input('email'))->first();
            $id_solicitud = $request->id_solicitud;
            if ($user === null) {
             $users = new User();
             $users->name = $request->user;
             $users->email = $request->email;
             $users->usuario_real = "0";
             $users->password = bcrypt($request->password);
             $users->save();

             $rol = Role::where('name', 'bodega')->first();
             $users->roles()->sync($rol ?: [], true);
             $data = DB::table('solicitudes_bodegas')
             ->where('id', $id_solicitud)
             ->update(['leido' => 1]);

             return Redirect::action('SolicitudBodegaController@index')->with('message', 'La  bogega ha sido agregada correctamente');
           }else{
             return Redirect::action('SolicitudBodegaController@index')->with('message', 'La bogega no se ha sido podido agregar');
           }
    }

    public function api(Request $request)
    {
        $data = SolicitudBodega::where('leido',0)->orderBy('id', 'DESC')
            ->when($request->nombre, function ($query) use ($request) {
                return $query->where('nombre_bodega','ilike',"%".$request->nombre."%");
            })
            ->with('solicitante')
            ->paginate(25);
        return $data;
    }


    public function destroy($id)
    {
      $data = SolicitudBodega::whereId($id)->delete();
      return 'Su solicitud ha sido eliminada correctamente';
    }
}
