<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\MediosPago;
use App\VinotecaMedioPago;
use Auth;
class MediosPagoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $efectivo = MediosPago::where('tipo',1)->with('lo_tengo')->get();
        $credito  = MediosPago::where('tipo',2)->with('lo_tengo')->get();
        $debito   = MediosPago::where('tipo',3)->with('lo_tengo')->get();

           
        return view('medios_pago.index',compact('efectivo','credito','debito'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $efectivo = MediosPago::where('tipo',1)->with('lo_tengo')->get();
        $credito  = MediosPago::where('tipo',2)->with('lo_tengo')->get();
        $debito   = MediosPago::where('tipo',3)->with('lo_tengo')->get();

        return view('medios_pago.agregar',compact('efectivo','credito','debito'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $tarjeta = $request->tarjeta;
        $estado = $request->estado;
        VinotecaMedioPago::where('medio_pago_id',$tarjeta)->where('vinoteca_id',Auth::user()->id)->delete();
        $mensaje['estado'] = false;
        $mensaje['mensaje'] = 'Se ha elmiinado el medio de pago';
        if($estado == 1){
            $vinoteca_pago                = new VinotecaMedioPago();
            $vinoteca_pago->timestamps    = false;
            $vinoteca_pago->vinoteca_id   = Auth::user()->id;
            $vinoteca_pago->medio_pago_id = $request->tarjeta;
            $vinoteca_pago->save();
            $mensaje['estado'] = true;
            $mensaje['mensaje'] = 'Se ha agregado el medio de pago';
        }
        return $mensaje;

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
