<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use DB;

class SearchMapController extends Controller
{
    public function __construct()
    {
        //$this->middleware('auth');
    }

    public function index()
    {
        return view('searchmap.index', compact(null));
    }

    public function data(Request $request)
    {
        $data = [];
        $perfiles = ['vinoteca'];
        $sql = "SELECT
                    users.id,
                    users.name,
                    informacion_adicional.provincia,
                    informacion_adicional.partido,
                    informacion_adicional.localidad,
                    informacion_adicional.codigo_postal,
                    informacion_adicional.domicilio,
                    informacion_adicional.numero,
                    informacion_adicional.geo_latitud,
                    informacion_adicional.geo_longitud,
                    informacion_adicional.web,
                    informacion_adicional.telefono,
                    usuario_imagenes.path
                FROM
                    users
                INNER JOIN
                    informacion_adicional ON users.id = informacion_adicional.user_id
                LEFT JOIN
                    usuario_imagenes ON users.id = usuario_imagenes.user_id AND usuario_imagenes.perfil = 't'
                WHERE
                    users.usuario_real = 1 AND
                    users.id IN (
                        SELECT
                        role_user.user_id
                        FROM
                        role_user
                        INNER JOIN roles ON role_user.role_id = roles.id
                        WHERE
                        roles.name = 'vinoteca'
                    ) AND
                    informacion_adicional.geo_latitud::float BETWEEN ".$request->lat2." AND ".$request->lat1." AND
                    informacion_adicional.geo_longitud::float BETWEEN ".$request->lon2." AND ".$request->lon1."
                    ";
        $result =   DB::select($sql);
        foreach ($result as $row) {
            $data[] = [
                'id'                =>  $row->id,
                'latitud'           =>  floatval($row->geo_latitud),
                'longitud'          =>  floatval($row->geo_longitud),
                'zona'              =>  1,
                'zona_nombre'       =>  trim($row->localidad),
                'tipo'              =>  1,
                'tipo_descripcion'  =>  'Vinoteca',
                'nombre'            =>  $row->name,
                'domicilio'         =>  trim($row->domicilio.' '.$row->numero),
                'localidad'         =>  trim($row->localidad),
                'web'               =>  trim($row->web),
                'imagen'            =>  trim($row->path),
            ];
        }
        echo json_encode($data);
    }
    
}
