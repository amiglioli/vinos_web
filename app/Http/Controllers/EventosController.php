<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Eventos;
use Redirect;
use Auth;
use File;

class EventosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
       return view('eventos.index');
    }


    public function horarios()
    {
         $horarios = ['00:00','00:30','01:00','01:30','02:00','02:30','03:00','03:30','04:00','04:30','05:00','05:30','06:00','06:30','07:00','07:30','08:00','08:30','09:00','09:30','10:00','10:30','11:00','11:30','12:00','12:30','13:00','13:30','14:00','14:30','15:00','15:30','16:00','16:30','17:00','17:30','18:00','18:30','19:00','19:30','20:00','20:30','21:00','21:30','22:00','22:30','23:00','23:30'];

         return $horarios;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $horarios = self::horarios();

        return view('eventos.agregar',compact('horarios'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {


        $request->validate([
            'nombre_evento'       => 'required',
            'ubicacion'       => 'required',
            'fecha_inicio' => 'required',
            'fecha_fin' => 'required',
            'hora_inicio' => 'required',
            'hora_fin' => 'required',
            'descripcion' => 'required',
            'organizador' => 'required',
            'geo_longitud'   => 'required',
            'geo_latitud'   => 'required',
        ]);


        $evento                 = new Eventos();
        $evento->timestamps     = false;
        $evento->nombre         = $request->nombre_evento;
        $evento->ubicacion      = $request->ubicacion;
        $evento->fecha_inicio   = $request->fecha_inicio;
        $evento->fecha_fin      = $request->fecha_fin;
        $evento->hora_inicio    = $request->hora_inicio;
        $evento->hora_fin       = $request->hora_fin;

        if($request->img){
           $data = base64_decode(preg_replace('#^data:image/\w+;base64,#i', '', $request->img));
           $png_url = "evento-".time().".png";
           $filepath = public_path().'/imgeventos/'.$png_url;
           $ruta_guardado = 'imgeventos/'.$png_url;
           file_put_contents($filepath,$data);
           $evento->img_path    = $ruta_guardado;
        }

        $evento->latitud        = $request->geo_latitud;
        $evento->longitud       = $request->geo_longitud;
        $evento->descripcion    = $request->descripcion;
        $evento->organizador    = $request->organizador;
        $evento->web            = $request->url;
        $evento->valor          = $request->valor;
        $evento->id_usuario     = Auth::user()->id;
        $evento->save();

    return Redirect::action('EventosController@index')->with('message', 'Su evento ha sido cargado correctamente');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
       $data = Eventos::whereId($id)->first();
       $horarios = self::horarios();

       return view('eventos.edit',compact('data','horarios'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        
         $request->validate([
            'nombre_evento'       => 'required',
            'ubicacion'       => 'required',
            'fecha_inicio' => 'required',
            'fecha_fin' => 'required',
            'hora_inicio' => 'required',
            'hora_fin' => 'required',
            'descripcion' => 'required',
            'organizador' => 'required',
            'geo_longitud'   => 'required',
            'geo_latitud'   => 'required',
        ]);


        $evento = Eventos::whereId($id)->first();
        $evento->timestamps     = false;
        $evento->nombre         = $request->nombre_evento;
        $evento->ubicacion      = $request->ubicacion;
        $evento->fecha_inicio   = $request->fecha_inicio;
        $evento->fecha_fin      = $request->fecha_fin;
        $evento->hora_inicio    = $request->hora_inicio;
        $evento->hora_fin       = $request->hora_fin;

        if($request->img != null){
           if(file_exists(public_path().'/'.$evento->img_path))
              File::delete(public_path().'/'.$evento->img_path);

           $data = base64_decode(preg_replace('#^data:image/\w+;base64,#i', '', $request->img));
           $png_url = "evento-".time().".png";
           $filepath = public_path().'/imgeventos/'.$png_url;
           $ruta_guardado = 'imgeventos/'.$png_url;
           file_put_contents($filepath,$data);
           $evento->img_path    = $ruta_guardado;
        }

        $evento->latitud        = $request->geo_latitud;
        $evento->longitud       = $request->geo_longitud;
        $evento->descripcion    = $request->descripcion;
        $evento->organizador    = $request->organizador;
        $evento->web            = $request->url;
        $evento->valor          = $request->valor;
        $evento->id_usuario     = Auth::user()->id;
        $evento->save();

        return Redirect::action('EventosController@index')->with('message', 'Su evento ha sido editado correctamente');
    }

    public function api(Request $request)
    {
        $data = Eventos::orderBy('id', 'DESC')
        ->when($request->nombre, function ($query) use ($request) {
            return $query->where('nombre','ilike',"%".$request->nombre."%");
        })
        ->when($request->ubicacion, function ($query) use ($request) {
            return $query->where('ubicacion','ilike',"%".$request->ubicacion."%");
        })
        ->when($request->fecha_inicio, function ($query) use ($request) {
            return $query->where('fecha_inicio','=',$request->fecha_inicio);
        })
        ->when($request->fecha_fin, function ($query) use ($request) {
            return $query->where('fecha_fin','=',$request->fecha_fin);
        })
        ->when($request->organizador, function ($query) use ($request) {
            return $query->where('organizador','ilike',"%".$request->organizador."%");
        })
        ->paginate(25);
        return $data;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
      $evento = Eventos::whereId($id)->first();
    
      if($evento->id_usuario == Auth::user()->id || Auth::user()->hasRole('admin') == true){

            $ruta = $evento->img_path;
            if(file_exists(public_path().'/'.$ruta))
                File::delete(public_path().'/'.$ruta);
            Eventos::where('id',$id)->delete();
            return 'El evento ha sido eliminado correctamente';
      }else{
            return 'No tiene permisos para realizar esta acción';
      }
    }
}
