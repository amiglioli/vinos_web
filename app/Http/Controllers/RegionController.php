<?php

namespace App\Http\Controllers;

use App\Pais;
use App\Region;
use Illuminate\Http\Request;
use Redirect;

class RegionController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $regiones = Region::orderBy('nombre', 'ASC')->get();
        $paises = Pais::orderBy('nombre', 'ASC')->get();

        return view('regiones.index', compact('paises','regiones'));
    }

    public function create()
    {
        $paises = Pais::all();

        return view('regiones.agregar',compact('paises'));
    }

      public function store(Request $request)
    {
      $request->validate([
            'region' => 'required',
            'pais' => 'required',
        ]);
        $region = new Region();
        $region->timestamps    = false;
        $region->nombre = $request->region;
        $region->pais_id = $request->pais;
        $region->save();

        return Redirect::action('RegionController@index')->with('message', 'Su region ha sido agregada');
    }

    public function edit($id)
    {

         $region = Region::whereId($id)->first();
         $paises = Pais::all();

         return view('regiones.edit',compact('paises','region'));

    }

  public function update(Request $request){

    $request->validate([
          'region' => 'required',
          'pais' => 'required',
      ]);

        $region = Region::where('id', '=', $request->id)->first();

        $region->timestamps = false;
        $region->nombre         = $request->region;
        $region->pais_id        = $request->pais;
        $region->save();

       return Redirect::action('RegionController@index')->with('message', 'Su region ha sido editada correctamente');
    }

    public function api(Request $request)
    {
        $data = Region::orderBy('nombre', 'ASC')
            ->when($request->region, function ($query) use ($request) {
                return $query->where('id', $request->region);
            })
            ->when($request->pais, function($query) use ($request) {
                return  $query->whereHas('pais', function($query) use ($request){
                    $query->where('id', '=',$request->pais);
                });
            })
            ->with('pais')
            ->paginate(25);


        return $data;
    }

    public function destroy($id)
    {
        $region = Region::whereId($id)->delete();
         return  'Su Region ha sido eliminado correctamente';
    }
}
