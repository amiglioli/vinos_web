<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Municipio extends Model
{
    protected $guarded = [];
 	
    protected $table = 'municipios';

       public function Informacion_adicional()
   {
        return $this->hasOne('App\InformacionAdicional','partido','municipio_id');
   }
}
