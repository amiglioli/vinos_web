<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class VinotecaProducto extends Model
{
    protected $table = 'vinoteca_producto';
    protected $primaryKey = 'producto_id';
    public $incrementing = false;

}
