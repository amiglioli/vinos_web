<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Region extends Model
{
    protected $table = 'regiones';


    public function pais()
   {
      return $this->hasOne('App\Pais','id','pais_id');
   }

}
