<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ZonaDistribucion extends Model
{
    
    protected $table = 'distribuidora_bodega_municipio';
    public $incrementing = false;

  
   public function distribuidora()
    {
       return $this->hasOne('App\User','id','distribuidora_id')->with('informacion_adicional');
    }

    public function provincias(){
    	return $this->hasOne('App\Provincia','provincia_id','provincia_id');	
    }

   
}
