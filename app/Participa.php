<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Participa extends Model
{
    protected $table = 'participantes';

    public function especializacion()
   {
      return $this->hasOne('App\Especializacion','id','especializacion');
   }
     public function solicitante()
   {
      return $this->hasOne('App\User','id','id_solicitante');
   }
}
