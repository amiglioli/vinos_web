<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CataCatadores extends Model
{
     protected $table = 'cata_catadores';
     public $incrementing = false;

     public function catador(){
     	return $this->hasOne('App\Catador','id','id_catador');
     }

     public function cata(){
     	return $this->hasOne('App\Cata','id','id_cata');
     }
}
