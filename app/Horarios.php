<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Horarios extends Model
{
   protected $table = 'horarios_vinotecas';
    protected $primaryKey = 'id_vinoteca';
}
