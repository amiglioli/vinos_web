<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PedidosAdmin extends Model
{
    protected $table = 'pedidos_admin';
    protected $primaryKey = 'id_pedido';

       public function vinoteca()
    {
       return $this->hasOne('App\User','id','vinoteca_id');
    }
}
