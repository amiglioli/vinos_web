<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FichaTranquilo extends Model
{
	protected $table = 'ficha_tranquilos_datos';

	public function vista()
	{
		return $this->hasOne('App\Puntuacion','id','vista');
	}

	public function nariz()
	{
		return $this->hasOne('App\Puntuacion','id','nariz');
	}
	public function gusto()
	{
		return $this->hasOne('App\Puntuacion','id','gusto');
	}
	public function marca()
	{
		return $this->hasOne('App\Puntuacion','id','marca');
	}
    public function cata()
	{
		return $this->hasOne('App\Cata','id','id_cata');
	}
	public function catador()
	{
		return $this->hasOne('App\Catador','id','id_catador');
	}
	public function producto()
	{
		return $this->hasOne('App\Productos','id','id_producto');
	}
	public function especializacion()
	{
		return $this->hasOne('App\Especializacion','id','id_especializacion');
	}
}
