<?php

namespace App;
use Auth;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Laravel\Passport\HasApiTokens;

class User extends Authenticatable
{
    use Notifiable, HasApiTokens;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'email', 'password','verified','completado','aprobado','usuario_real'];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function roles(){
        return $this->belongsToMany(Role::class);
    }
    public function assignRole($role){
        $this->roles()->save(
            Role::whereName($role)->firstOrFail()
        );
    }

    public function hasRole($role){
        if(is_string($role)){
            return $this->roles->contains('name', $role);
        }
        return !!$role->intersect($this->roles)->count();
    }

    public function informacion_adicional(){
      return $this->hasOne('App\InformacionAdicional','user_id')->with('provincias','municipio');
    }
    public function informacion_contacto()
   {
        return $this->hasOne('App\InformacionContacto','user_id')->with('provincias');
   }

   public function zonas_distribucion(){
        return $this->hasMany('App\ZonaDistribucion','distribuidora_id','id');
   }

   public function bodega_provincias_distribucion(){
        return $this->hasMany('App\ZonaDistribucion','bodega_id','id')
                          ->select('provincia_id','bodega_id')
                          ->groupBy('provincia_id','bodega_id')
                          ->with('provincias');
   }
    public function bodega_municipios_distribucion(){
        return $this->hasMany('App\ZonaDistribucion','bodega_id','id');
   }




   public function bodega_distribucion(){
        return $this->hasMany('App\ZonaDistribucion','bodega_id','id')->select('distribuidora_id','bodega_id')->groupBy('distribuidora_id','bodega_id')->with('distribuidora');
   }

   public function imagen(){
        return $this->hasMany('App\UsuarioImagen','user_id','id')->where('perfil',true);
   }


   public function productos_vinoteca()
    {
      return $this->belongsToMany('App\Productos','vinoteca_producto','vinoteca_id','producto_id')->with('producto')
            ->with('producto_tipo')
            ->with('varietales');
    }

}
