<?php

function get_dias_semana(){
	return ["LU","MA","MI","JU","VI","SA","DO"];
}

function solicitudes_bodegas(){
	$data = DB::table('solicitudes_bodegas')->where('leido',0)->count();
	return $data;
}

function dayweek($dia){
    $data = $dia;
    if($data == 0){
        return 'Domingo';
     }

     if($data == 1){
        return 'Lunes';
     }
     if($data == 2){
        return 'Martes';
     }
     if($data == 3){
        return 'Miércoles';
     }
     if($data == 4){
        return 'Jueves';
     }
     if($data == 5){
        return 'Viernes';
     }
     if($data == 6){
        return 'Sábado';
     }
     
}

///////////ESTO ES SOLO APRA DISTRIBUIDORAS//////////
function cantidad_pedidos(){
    $data = DB::table('vinoteca_quiere_productos')->where('distribuidora_id',Auth::user()->id)->where('atendido',false)->count();
    return $data;
}

function cantidad_pedidos_no_resueltos(){
    $data = DB::table('pedidos_admin')->where('resuelto',false)->count();
    return $data;
}


function decimal($numero)
{
    $valor = number_format($numero, 2, '.', ' ');
    return $valor;
}

function edad_momento_cata($edad,$cata_fecha)
{
    $d1 = new DateTime($edad);
    $d2 = new DateTime($cata_fecha);

    $diff = $d2->diff($d1);

    return $diff->y;
}



function GuardarFoto($imagen, $ruta){

    //tomo el archivo y lo guardo
		//dd($imagen);
    $fecha = date("d-m-Y H:i:s");

    //para que no joda cuando quiero crear el arcivo, no me deja poner :

    $fecha = str_replace(':', '', $fecha);
    $fecha = str_replace(' ', '', $fecha);
    $fecha = str_replace('-', '', $fecha);
    $file = $imagen;

    $FileName = $fecha;
		//dd($FileName);
    move_uploaded_file($file, $ruta.'/'.$FileName);

    return $FileName;

}



function fechaCastellano ($fecha) {
     if($fecha == null)
       return "-";
    $fecha1 =  explode(" ",$fecha);
     $tiempohora = null;
     if(!empty($fecha1[1]))
         $tiempohora = $fecha1[1];

    $fecha = substr($fecha, 0, 10);
    $numeroDia = date('d', strtotime($fecha));
    $dia = date('l', strtotime($fecha));
    $mes = date('F', strtotime($fecha));
    $anio = date('Y', strtotime($fecha));
    $dias_ES = array("Lunes", "Martes", "Miércoles", "Jueves", "Viernes", "Sábado", "Domingo");
    $dias_EN = array("Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday");
    $nombredia = str_replace($dias_EN, $dias_ES, $dia);
   $meses_ES = array("Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre");
    $meses_EN = array("January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December");
    $nombreMes = str_replace($meses_EN, $meses_ES, $mes);

     if($tiempohora != null)
       return $nombredia." ".$numeroDia." de ".$nombreMes." de ".$anio." a las ".$tiempohora;
     else
       return $nombredia." ".$numeroDia." de ".$nombreMes." de ".$anio;

    //return $nombreMes;
}
