<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class VinotecaQuiereProducto extends Model
{
    protected $table = 'vinoteca_quiere_productos';
    protected $primaryKey = 'id_promocion';

    public $incrementing = false;

    public function producto()
   {
    return $this->hasOne('App\Productos','id','producto_id');
   }

    public function vinoteca()
   {
    return $this->hasOne('App\InformacionAdicional','user_id','vinoteca_id');
   }


}
