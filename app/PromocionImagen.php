<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PromocionImagen extends Model
{
    protected $table = 'promocion_imagen';
    public $incrementing = false; //para que no intente retornar el id en el save()
    protected $primaryKey = 'promocion_id';


}
