<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PromocionFija extends Model
{
    protected $table = 'promocion_fija';
    public $incrementing = false; //para que no intente retornar el id en el save()
    protected $primaryKey = 'promocion_id';


}
