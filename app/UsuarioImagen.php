<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UsuarioImagen extends Model
{
   protected $table = 'usuario_imagenes';
   public $incrementing = false;
   public $timestamps = false;

}
