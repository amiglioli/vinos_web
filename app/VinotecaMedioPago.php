<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class VinotecaMedioPago extends Model
{
    protected $table = 'vinoteca_mediopago';
    public $incrementing = false;

}
