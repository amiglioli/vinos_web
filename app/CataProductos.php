<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CataProductos extends Model
{
    protected $table = 'cata_productos';
    public $incrementing = false;

       public function producto()
   {
        return $this->hasOne('App\Productos','id','id_producto');
   }
}
