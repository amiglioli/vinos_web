<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cata extends Model
{

    protected $table = 'catas';

   public function catadores()
   {
        return $this->hasMany('App\CataCatadores','id_cata','id')->with('catador');
   }

  public function catadores_cata(){
      return $this->belongsToMany('App\Catador','cata_catadores','id_cata','id_catador')->with('especializacion');
   }
   
   
  public function productos_cata(){
      return $this->belongsToMany('App\Productos','cata_productos','id_cata','id_producto')->with('bodega')->with('varietales');
   }

   public function ranking()
   {
     return $this->hasOne('App\Ranking','cata_id','id')->with('resultado');
   }


   public function productos()
   {
        return $this->hasMany('App\CataProductos','id_cata','id')->with('producto');
   }

   public function cantidad_productos()
   {
        return $this->hasMany('App\CataProductos','id_cata','id');
   }


}
