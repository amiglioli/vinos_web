<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SolicitudBodega extends Model
{
   protected $table = 'solicitudes_bodegas';

   public function solicitante()
  {
     return $this->belongsTo('App\User','id_solicitante','id');
  }
}
