<?php

namespace App;
use App\User;
use DB;
use Illuminate\Database\Eloquent\Model;

class InformacionAdicional extends Model
{
   protected $table = 'informacion_adicional';
   protected $primaryKey = 'user_id';

   public function user()
   {
        return $this->belongsTo('App\User', 'user_id')->with('productos_vinoteca')->with('roles');
   }

   public function provincias()
   {
   	return $this->hasOne('App\Provincia','provincia_id','provincia');
   }

    public function municipio()
   {
        return $this->hasOne('App\Municipio','municipio_id','partido')->with('informacion_adicional');
   }

}
