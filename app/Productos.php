<?php

namespace App;
use Auth;
use Illuminate\Database\Eloquent\Model;
use App\User;
class Productos extends Model
{
    protected $table = 'productos';

    public function producto()
    {
       return $this->hasOne('App\Producto','id','producto');
    }



    public function distribuidoras_que_reparten_mi_zona()
    {

        $usuario = User::whereId(Auth::user()->id)->with('informacion_adicional')->first();

        $informacion_adicional = $usuario->informacion_adicional;
        $mi_provincia = $informacion_adicional->provincia;
        $mi_partido = $informacion_adicional->partido;
        return $this->hasMany('App\ZonaDistribucion','bodega_id','bodega_id')
                          ->with('distribuidora')->select('distribuidora_id','bodega_id')
                          ->groupBy('distribuidora_id','bodega_id')
                          ->where('provincia_id', $mi_provincia )
                          ->where('municipio_id', $mi_partido );
    }

    public function pedido_admin()
    {
      return $this->hasMany('App\PedidosAdmin','bodega_id','bodega_id')->where('vinoteca_id',Auth::user()->id)->with('vinoteca');
    }

    public function distribuyo_el_producto()
    {
      return $this->hasMany('App\ZonaDistribucion','bodega_id','bodega_id')
                          ->where('distribuidora_id', Auth::user()->id );
    }


    public function tengo_el_producto()
    {
      return $this->belongsToMany('App\User','vinoteca_producto','producto_id','vinoteca_id')->where('id', Auth::user()->id)->where('vinoteca_producto.estado',1);
    }
    public function no_tengo_el_producto()
    {
      return $this->belongsToMany('App\User','vinoteca_producto','producto_id','vinoteca_id')->where('id', Auth::user()->id)->where('vinoteca_producto.estado',0);
    }

    public function quiero_producto()
    {
      return $this->belongsToMany('App\User','vinoteca_quiere_productos','producto_id','vinoteca_id')->where('id', Auth::user()->id);
    }

    public function producto_tipo()
    {
       return $this->hasOne('App\ProductoTipo','id','tipo');
    }
    public function varietales()
    {
       return $this->hasOne('App\Varietal','id','varietal');
    }
    public function bodega()
    {
       return $this->hasOne('App\User','id','bodega_id');
    }
    public function pais()
    {
       return $this->hasOne('App\Pais','id','origen_pais');
    }
    public function provincia()
    {
       return $this->hasOne('App\Provincia','provincia_id','origen_provincia');
    }
    public function region()
    {
       return $this->hasOne('App\Region','id','origen_region');
    }
    public function ciudad()
    {
       return $this->hasOne('App\Ciudades','id','origen_ciudad');
    }
    public function resultado()
    {
      return $this->hasOne('App\Resultado','producto_id','id');
    }

    public function cata_cerrada()
    {
      return $this->belongsToMany('App\Cata','cata_productos','id_producto','id_cata')->where('cerrada',true);
    }

    public function distribuidora_vino()
    {
      return $this->hasMany('App\ZonaDistribucion','bodega_id','bodega_id')->select('bodega_id','distribuidora_id')->groupBy('distribuidora_id','bodega_id')->with('distribuidora');
    }
}
