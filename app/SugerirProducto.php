<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SugerirProducto extends Model
{
    protected $table = 'sugerencia_producto';

    public function varietal()
   {
      return $this->hasOne('App\Varietal','id','varietal');
   }
}
