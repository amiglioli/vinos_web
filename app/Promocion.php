<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Promocion extends Model
{
    protected $table = 'promociones';

   public function promocion_fija()
   {
   	return $this->hasOne('App\PromocionFija','promocion_id','id');	
   }

   public function promocion_recurrente()
   {
   	return $this->hasOne('App\PromocionRecurrente','promocion_id','id');	
   }


   public function promocion_imagen()
   {
   	return $this->hasOne('App\PromocionImagen','promocion_id','id');	
   }
}
