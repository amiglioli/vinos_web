<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Auth;
class MediosPago extends Model
{
   
    protected $table = 'medios_pago';

    public function lo_tengo(){
    	$mi_id = Auth::user()->id;
    	return $this->hasOne('App\VinotecaMedioPago','medio_pago_id','id')->where('vinoteca_id', $mi_id);
    }
}
