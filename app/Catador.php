<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Catador extends Model
{
    protected $table = 'catadores';

   public function especializacion()
   {
        return $this->hasOne('App\Especializacion','id','especializacion_id');
   }

   public function catas_cerradas(){
   		return $this->belongsToMany('App\Cata','cata_catadores','id_catador','id_cata')->where('cerrada',true);
   }
}
