<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
/*
Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
*/

Route::get('auth/logout', function(){
	Auth::logout();
	return view('auth/login');
});

Route::get('map', 		'SearchMapController@index');
Route::get('map/data', 	'SearchMapController@data');

Route::auth();



///////////////////REGISTRO///////////////////////////

Route::get('/', function(){

	return view('auth/login');
});

Route::get('/registro/{tipo}'		, 'Auth\RegisterController@index');
Route::get('/user/verify/{token}'	, 'Auth\RegisterController@verifyUser');
Route::post('/cambiarContrasena'	, 'Auth\RegisterController@cambiarContrasena');


Route::group(['middleware' => ['auth']], function() {


Route::get('/inicio/mapa'	, 'HomeController@mapa');
Route::get('cargar_bodegas'	, 'HomeController@cargar_bodegas');
	////////////////////////Muestras Pendientes/////////////////////////////////
Route::get('ficha_tranquilo/muestras_pendientes/{id_catador}/{id_cata}/','FichaTranquiloController@muestras');
Route::get('cata/cerrar/{id}/','CataController@cerrarCata');
Route::get('perfil/setperfil/{id}','PerfilController@establecer_imagen_perfil');
	


	Route::resource('admin/usuarios'				,'UsersController');
	Route::resource('admin/roles'					,'RolesController');
	Route::resource('bodegas_secundarias'			,'BodegasSecundariasController');
	Route::resource('producto'	            		,'ProductoController');
	Route::resource('producto_tipo'	        		,'ProductoTipoController');
	Route::resource('especializaciones'	    		,'EspecializacionesController');
	Route::resource('varietales'	        		,'VarietalesController');
	Route::resource('paises'	           			,'PaisesController');
	Route::resource('provincias'	        		,'ProvinciaController');
	Route::resource('regiones'	            		,'RegionController');
	Route::resource('ciudades'	            		,'CiudadesController');
	Route::resource('puntuacion_tranquilos'	   		,'PuntuacionTranquilosController');
	Route::resource('catas'                 	    ,'CataController');
	Route::resource('cata_ranking'                  ,'CataRankingController');
	Route::resource('catadores'             		,'CatadorController');
	Route::resource('listado'               		,'ListadoController');
	Route::resource('solicitud_bodega'      		,'SolicitudBodegaController');
	Route::resource('aprobacion_bodega'     		,'AprobacionBodegaController');
	Route::resource('pedido_bodega_distribuidora'   ,'PedidoBodegaDistribuidoraController');
	Route::resource('productos'            			,'ProductosController');
	Route::resource('perfil'                		,'PerfilController');
	Route::resource('sommelier'                		,'SommelierController');

	Route::resource('permission'            		,'PermissionController');
	Route::resource('ficha_tranquilo'       		,'FichaTranquiloController');
	Route::get('promociones/activar_promocion'		,'PromocionesController@activar_promocion');
  	Route::resource('promociones'             		,'PromocionesController');
  	Route::resource('promociones_imagenes'    		,'PromocionesImagenesController');
  	Route::resource('ranking'    					,'RankingController');
  	Route::resource('portfolio'    					,'PortfolioController');
  	Route::resource('participa'    					,'ParticipaController');
  	Route::resource('pedidos'    					,'PedidosController');
  	Route::resource('pedidos_admin'    				,'PedidosAdminController');
  	Route::resource('horarios'    					,'HorariosController');
  	Route::resource('mediospago'    			    ,'MediosPagoController');
  	Route::resource('sugerir_producto'    			,'SugerirProductoController');
  	Route::resource('eventos'    					,'EventosController');

	Route::get('/activaciones'						,'InformacionAdicionalController@index');

	Route::get('pedidos/{id}/atendido'    			, 'PedidosController@atendido');
	Route::get('pedidos_admin/{id}/resuelto'    			, 'PedidosAdminController@resuelto');
	Route::get('pedidos_imprimir/{id}'    			, 'PedidosController@print');

	Route::get('cata_ranking/{id}/vervino'			, 'CataRankingController@vervino');
	Route::get('cata_ranking/{id}/ver_ranking'		, 'CataRankingController@ver_ranking');
	Route::get('cata_ranking/{id}/lotengo'			, 'CataRankingController@lotengo');
	Route::get('cata_ranking/{id}/loquiero'			, 'CataRankingController@loquiero');
	Route::get('cata_ranking/{id}/quien_distribuye'	, 'CataRankingController@ver_quien_distribuye');
	Route::get('cata_ranking/{id_cata}/ver_vinotecas_zona', 'CataRankingController@ver_vinotecas_zona');
	Route::get('ver_ficha/{id_cata}/{id_producto}/{id_ranking}', 'CataRankingController@verficha');//

	Route::post('/completar_registro/{id}'	,'InformacionAdicionalController@completar_registro');
	Route::get('/localidades/{id}'			,'InformacionAdicionalController@localidades');


	Route::get('buscar', function(){
		return view('buscador.index');
	});

	Route::group(['middleware' => ['verificaciondedatos']], function() {
		Route::get('/inicio'	, 'HomeController@index');
	});
});




Route::group(array('prefix' => 'api'), function(){
	Route::get('usuarios', 'UsersController@api');
	Route::get('perfil', 'PerfilController@api_img_perfil');

	Route::get('bodegas_secundarias', 'BodegasSecundariasController@api');
	Route::get('producto', 'ProductoController@api');
	Route::get('producto_tipo', 'ProductoTipoController@api');
	Route::get('especializaciones', 'EspecializacionesController@api');
	Route::get('varietales', 'VarietalesController@api');
	Route::get('paises', 'PaisesController@api');
	Route::get('provincias', 'ProvinciaController@api');
	Route::get('regiones', 'RegionController@api');
	Route::get('ciudades', 'CiudadesController@api');
	Route::get('puntuacion_tranquilos', 'PuntuacionTranquilosController@api');
	Route::get('catas', 'CataController@api');

	Route::get('catadores', 'CatadorController@api');

	Route::get('catadores/vercerradas', 'CatadorController@apiver_cerradas');

	Route::get('listado', 'ListadoController@api');
	Route::get('solicitud_bodega', 'SolicitudBodegaController@api');
	Route::get('aprobacion_bodega', 'AprobacionBodegaController@api');
	Route::get('productos', 'ProductosController@api');
	Route::get('permission', 'PermissionController@api');
	Route::get('ficha_tranquilo', 'FichaTranquiloController@api');
	Route::get('promociones', 'PromocionesController@api');
  	Route::get('promociones_imagenes', 'PromocionesImagenesController@api');
  	Route::get('ranking', 'RankingController@api');
  	Route::get('portfolio', 'PortfolioController@api');
  	Route::get('portfolio/localidades', 'PortfolioController@apilocalidades');
  	Route::get('pedidos', 'PedidosController@api');
  	Route::get('pedidos_admin', 'PedidosAdminController@api');
  	Route::get('horarios', 'HorariosController@api');
  	Route::get('eventos', 'EventosController@api');
  	Route::get('participa', 'ParticipaController@api');
  	Route::get('sugerir_producto', 'SugerirProductoController@api');

	/////////////////////////////////////////////////////////////////////////////////
	Route::get('cata_ranking', 'CataRankingController@api');//
	Route::get('vervino', 'CataRankingController@apivervino');//
	Route::get('ver_ranking', 'CataRankingController@apiver_ranking');//
	Route::get('ver_ranking_mobile', 'CataRankingController@apiver_ranking_mobile');//
	Route::get('apiverficha', 'CataRankingController@apiverficha');//
	
});


route::get('viewclear', function(){
	Artisan::call('view:clear');
	return Redirect::to('');
});
