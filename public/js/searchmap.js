var tmpListado = [];
var infowindowOpened = null;

var vm = function(){
	self				= this;
	self.resultado		= ko.observableArray([]);
	self.verMapa = function(marker,infowindow,latitud,longitud) {
		if (infowindowOpened != null) infowindowOpened.close();
		clickBtnMapa();
		setTimeout(function() {
			window.scrollTo(0, 0);
			map.panTo(new google.maps.LatLng(latitud, longitud));
			map.setZoom(15);
			infowindow.open(map, marker);
			infowindowOpened = infowindow;
		},1000);
	}
};
vm = new vm();
ko.applyBindings(vm);

var map;
var btnBuscarArea;
var map_ini_pos = { };
var map_curr_pos = { };
var allMarkers = [];
var markerMap = function(filterTipo, filterZona) {
	$.each(allMarkers, function(k,m) {
		m.marker.setMap(null);
    });
    vm.listado = [];
    var icon1 = {
        path: "M10.808 19.464c4.776 0 8.647-3.873 8.647-8.65 0-4.778-3.87-8.65-8.647-8.65-4.775 0-8.646 3.872-8.646 8.65 0 4.777 3.87 8.65 8.646 8.65z",
        fillColor: '#FD8131',
        fillOpacity: 1,
        anchor: new google.maps.Point(0,0),
        strokeWeight: 0.4,
        strokeColor: '#333333',
        scale: 0.6
    };
    var icon2 = {
        path: "M10.808 19.464c4.776 0 8.647-3.873 8.647-8.65 0-4.778-3.87-8.65-8.647-8.65-4.775 0-8.646 3.872-8.646 8.65 0 4.777 3.87 8.65 8.646 8.65z",
        fillColor: '#31d4fd',
        fillOpacity: 1,
        anchor: new google.maps.Point(0,0),
        strokeWeight: 0.4,
        strokeColor: '#333333',
        scale: 0.6
    };
    var icon3 = {
        path: "M10.808 19.464c4.776 0 8.647-3.873 8.647-8.65 0-4.778-3.87-8.65-8.647-8.65-4.775 0-8.646 3.872-8.646 8.65 0 4.777 3.87 8.65 8.646 8.65z",
        fillColor: '#50e422',
        fillOpacity: 1,
        anchor: new google.maps.Point(0,0),
        strokeWeight: 0.4,
        strokeColor: '#333333',
        scale: 0.6
    };
    $.getJSON( url_data+'?tipo='+filterTipo+'&zona='+filterZona+'&lat1='+map_curr_pos.p1.lat()+'&lon1='+map_curr_pos.p1.lng()+'&lat2='+map_curr_pos.p2.lat()+'&lon2='+map_curr_pos.p2.lng(), function( markers ) {
		tmpListado = [];
		$.each(markers, function(k,marker) {
			if (marker.tipo == 1) tmpIcon = icon1;
			if (marker.tipo == 2) tmpIcon = icon2;
			if (marker.tipo == 3) tmpIcon = icon3;
			var m = new google.maps.Marker({
				position: {lat: marker.latitud, lng: marker.longitud},
			  	map: map,
			  	icon: tmpIcon,
			});
			//var contentInfoWindow = '<b>'+marker.nombre+'</b><hr>Domicilio: Av Ejemplo '+(marker.id*2)+'<br>'+marker.zona_nombre+'<br><br>Horario: Lun-Vie 8h a 20h<br><font color="green">Abierto ahora</font><hr><img src="http://www.reservandovinos.com/uploads/l_72_vinoteca-fratelli-vinos-cordoba-argentina-2.jpg" width="80"><img src="https://media-cdn.tripadvisor.com/media/photo-s/0e/07/0b/ac/photo7jpg.jpg" width="80">';

			console.log(marker);
			var contentInfoWindow = '<table class="infowindow"><td><td style="width: 200px;"><img src="'+url_imgperfil+'/'+marker.imagen+'" height="200"></td><td style="width: 300px; vertical-align: top;"><span class="titulo">'+marker.nombre+'</span><hr><span class="detalle">'+marker.domicilio+'<br>'+marker.localidad+'<br><br><span class="abierto">'+marker.web+'</span><br></span><hr><a href="#">Detalle</a>&nbsp;-&nbsp;<a href="#">Horarios</a>&nbsp;-&nbsp;<a href="#">Promociones</a></td></tr></table>';
			var infowindow = new google.maps.InfoWindow({
				content: contentInfoWindow
			});
			m.addListener('click', function() {
				if (infowindowOpened != null) infowindowOpened.close();
				infowindow.open(map, m);
				infowindowOpened = infowindow;
			});
			marker.marker = m;
			marker.contentInfoWindow = contentInfoWindow;
			marker.infowindow = infowindow;
			tmpListado.push(marker);
			allMarkers.push(marker);
      	});
    	vm.resultado(tmpListado);
    });
}

function initMap() {
	$.getJSON( url_mapstyle, function( mapStyle ) {
		map = new google.maps.Map(document.getElementById('map'), {
			center: {lat: -35.2, lng: -58.2520396},
			zoom: 9,
			styles: mapStyle,
			gestureHandling: 'greedy',
			mapTypeControl: false,
			fullscreenControl: false,
		});
		google.maps.event.addListener(map, 'dragend', function() {
			map_move();
		});
		google.maps.event.addListener(map, 'zoom_changed', function() {
			map_move();
		});
		google.maps.event.addListenerOnce(map, 'bounds_changed', function() {
			map_ini_pos.p1 = new google.maps.LatLng(map.getBounds().getNorthEast().lat(),map.getBounds().getNorthEast().lng());
			map_ini_pos.p2 = new google.maps.LatLng(map.getBounds().getSouthWest().lat(),map.getBounds().getSouthWest().lng());
			map_curr_pos.p1 = new google.maps.LatLng(map_ini_pos.p1.lat(),map_ini_pos.p1.lng());
			map_curr_pos.p2 = new google.maps.LatLng(map_ini_pos.p2.lat(),map_ini_pos.p2.lng());
			markerMap([1,2,3], [1,2]);
		});
		btnBuscarArea = document.createElement('div');
        var centerControl = new CenterControl(btnBuscarArea, map);
        btnBuscarArea.index = 1;
        map.controls[google.maps.ControlPosition.TOP_CENTER].push(btnBuscarArea);

		
	});
}

function map_move() {
	map_curr_pos.p1 = new google.maps.LatLng(map.getBounds().getNorthEast().lat(),map.getBounds().getNorthEast().lng());
	map_curr_pos.p2 = new google.maps.LatLng(map.getBounds().getSouthWest().lat(),map.getBounds().getSouthWest().lng());
	var d1 = calcDistance(map_ini_pos.p1, map_curr_pos.p1);
	var d2 = calcDistance(map_ini_pos.p2, map_curr_pos.p2);
	var d3 = calcDistance(map_ini_pos.p1, map_ini_pos.p2)/5;
	if (d1 > d3 || d2 > d3) 	$('#btnBuscarArea').fadeIn();
	else 						$('#btnBuscarArea').fadeOut();
}

function calcDistance(p1, p2) {
	return (google.maps.geometry.spherical.computeDistanceBetween(p1, p2) / 1000).toFixed(2);
}

function CenterControl(controlDiv, map) {
	// Set CSS for the control border.
	var controlUI = document.createElement('div');
	controlUI.setAttribute("id", "btnBuscarArea");
	controlUI.style.display = 'none';
	controlUI.style.backgroundColor = '#D02026';
	controlUI.style.border = '2px solid #fff';
	controlUI.style.borderRadius = '3px';
	controlUI.style.boxShadow = '0 2px 6px rgba(0,0,0,.3)';
	controlUI.style.cursor = 'pointer';
	controlUI.style.marginBottom = '22px';
	controlUI.style.textAlign = 'center';
	controlUI.title = 'Buscar en esta zona';
	controlDiv.appendChild(controlUI);

	// Set CSS for the control interior.
	var controlText = document.createElement('div');
	controlText.style.color = 'rgb(255,255,255)';
	controlText.style.fontFamily = 'Roboto,Arial,sans-serif';
	controlText.style.fontSize = '16px';
	controlText.style.lineHeight = '38px';
	controlText.style.paddingLeft = '5px';
	controlText.style.paddingRight = '5px';
	controlText.innerHTML = 'Buscar en esta zona';
	controlUI.appendChild(controlText);

	controlUI.addEventListener('click', function() {
		filtrar();
		map_ini_pos.p1 = new google.maps.LatLng(map.getBounds().getNorthEast().lat(),map.getBounds().getNorthEast().lng());
		map_ini_pos.p2 = new google.maps.LatLng(map.getBounds().getSouthWest().lat(),map.getBounds().getSouthWest().lng());
		map_curr_pos.p1 = new google.maps.LatLng(map_ini_pos.p1.lat(),map_ini_pos.p1.lng());
		map_curr_pos.p2 = new google.maps.LatLng(map_ini_pos.p2.lat(),map_ini_pos.p2.lng());
		$('#btnBuscarArea').fadeOut();
	});

}

var filtrar = function() {
	var tipo = [];
	if ($('#filter_tipoVinotecas').is(':checked')) tipo.push(1);
	if ($('#filter_tipoDistribuidoras').is(':checked')) tipo.push(2);
	if ($('#filter_tipoBodegas').is(':checked')) tipo.push(3);

	var zona = [];
	if ($('#filter_zona1').is(':checked')) zona.push(1);
	if ($('#filter_zona2').is(':checked')) zona.push(2);
	markerMap(tipo, zona);
}
$('.filtrar').on("click", function(e) {
	filtrar();
});
$('#btnFiltros').on("click", function(e) {
	clickBtnFiltros();
});
$('#btnMapa').on("click", function(e) {
	clickBtnMapa();
});
$('#btnListado').on("click", function(e) {
	clickBtnListado();
})
$('#btnCerrarFiltros').on("click", function(e) {
	clickBtnFiltros();
});

var clickBtnMapa = function() {
	window.scrollTo(0, 0);
	$('#listado').fadeOut();
	$('#map').fadeIn();
	$('#btnListado').removeClass('btn-danger');
	$('#btnListado').addClass('btn-default');
	$('#btnMapa').removeClass('btn-default');
	$('#btnMapa').addClass('btn-danger');
}
var clickBtnListado = function() {
	window.scrollTo(0, 0);
	$('#listado').fadeIn();
	$('#map').fadeOut();
	$('#btnMapa').removeClass('btn-danger');
	$('#btnMapa').addClass('btn-default');
	$('#btnListado').removeClass('btn-default');
	$('#btnListado').addClass('btn-danger');
}
var clickBtnFiltros = function() {
	window.scrollTo(0, 0);
	if ($('#div_filtros').css('display') == 'block') {
		$('#div_filtros').css("display","none");
		$('#btnFiltros').removeClass('btn-danger');
		$('#btnFiltros').addClass('btn-default');
		
		$('#div_derecho').removeClass('col-xs-12 col-sm-12 col-md-12 col-lg-10');
		$('#div_derecho').addClass('col-xs-12 col-sm-12 col-md-12 col-lg-12');

	}
	else {
		$('#div_filtros').css("display","block");
		$('#btnFiltros').removeClass('btn-default');
		$('#btnFiltros').addClass('btn-danger');

		$('#div_derecho').removeClass('col-xs-12 col-sm-12 col-md-12 col-lg-12');
		$('#div_derecho').addClass('col-xs-12 col-sm-12 col-md-12 col-lg-10');
	}
}