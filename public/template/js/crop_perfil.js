function crop_perfil(){
var $image = $(".image-crop > img")
$($image).cropper({
  aspectRatio: 16/9,
  preview: ".img-preview",
  autoCropArea: 1,
  done: function(data) {
    //console.log(data);
  }
});

var $inputImage = $("#inputImage");
if (window.FileReader) {
  $inputImage.change(function() {
    var fileReader = new FileReader(),
    files = this.files,
    file;

    if (!files.length) {
      return;
    }

    file = files[0];

    if (/^image\/\w+$/.test(file.type)) {
      fileReader.readAsDataURL(file);
      fileReader.onload = function () {
        $inputImage.val("");
        $image.cropper("reset", true).cropper("replace", this.result);
      };
    } else {
      showMessage("Please choose an image file.");
    }
  });
} else {
  $inputImage.addClass("hide");
}

$("#download").click(function() {
  //toastr.success("Su imagen ha sido guardada correctamente");
  base64 = $image.cropper("getDataURL");
  $('#imagen').val(base64);
});

$("#zoomIn").click(function() {
  $image.cropper("zoom", 0.1);
});

$("#zoomOut").click(function() {
  $image.cropper("zoom", -0.1);
});

$("#rotateLeft").click(function() {
  $image.cropper("rotate", 45);
});

$("#rotateRight").click(function() {
  $image.cropper("rotate", -45);
});

$("#setDrag").click(function() {
  $image.cropper("setDragMode", "crop");
});
}
