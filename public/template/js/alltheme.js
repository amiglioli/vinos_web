$(".eliminar-registro").on("click", function() {
    var url_href = $(this).data("href")
    swal({
        title: "¿Estás seguro?",
        text: "Una vez ejecutada la acción no se podrá recurar el registro!",
        icon: "warning",
        buttons: true,
        dangerMode: true,
    }).then((willDelete) => {
        if (willDelete) {
            window.location.href = url_href;
        }
    });
});


function fecha_format(fecha)
{   
  var dia = moment(fecha).format('l');
  return dia;

}


function dayweek(dia){
 var data = dia;
     if(data == 0){
        return 'Lunes';
     }
     if(data == 1){
        return 'Martes';
     }
     if(data == 2){
        return 'Miércoles';
     }
     if(data == 3){
        return 'Jueves';
     }
     if(data == 4){
        return 'Viernes';
     }
     if(data == 5){
        return 'Sábado';
     }
     if(data == 6){
        return 'Domingo';
     }
}


function sumarDias(fecha, dias) {
    fecha.setDate(fecha.getDate() + dias);
    return fecha;
}

function calculateAge(birthday) { 

        var mdate = birthday;
        var yearThen = parseInt(mdate.substring(0,4), 10);
        var monthThen = parseInt(mdate.substring(5,7), 10);
        var dayThen = parseInt(mdate.substring(8,10), 10);
        
        var today = new Date();
        var birthday = new Date(yearThen, monthThen-1, dayThen);
        
        var differenceInMilisecond = today.valueOf() - birthday.valueOf();
        
        var year_age = Math.floor(differenceInMilisecond / 31536000000);
        var day_age = Math.floor((differenceInMilisecond % 31536000000) / 86400000);
        
        if ((today.getMonth() == birthday.getMonth()) && (today.getDate() == birthday.getDate())) {
            //alert("Happy B'day!!!");
        }
        
        var month_age = Math.floor(day_age/30);
        
        day_age = day_age % 30;
        
        if (isNaN(year_age) || isNaN(month_age) || isNaN(day_age)) {
            $("#exact_age").text("Invalid birthday - Please try again!");
        }
        else {
            return  year_age;
        }
   
}


 function decimal(number)
 {
    var value = parseFloat(number).toFixed(2);

    return value;
 }


function spanishDate(d) {
    var localLocale = moment(d);
    moment.locale('es');
    localLocale.locale(false);
    return localLocale.format('LL');
}

function diferencia_dias(d1, d2) {
    var fechaI = new Date(d2)
    var fechaF = new Date(d1)
    var difM = fechaF - fechaI // diferencia en milisegundos
    var difD = difM / (1000 * 60 * 60 * 24) // diferencia en dias
    return difD;
}


function formatearFecha(fecha){
    var res = fecha.split("-");
    var fecha =  res[2]+"-"+res[1]+"-"+res[0];
    //alert(fecha);
  var date = new Date(fecha.replace( /(\d{2})-(\d{2})-(\d{4})/, "$3/$2/$1") );


    var year = date.getFullYear();

  var month = (1 + date.getMonth()).toString();
  month = month.length > 1 ? month : '0' + month;

  var day = date.getDate().toString();
  day = day.length > 1 ? day : '0' + day;
  
  return day+"/"+month+"/"+year;
    
}