@extends('template/searchmap')
@section('linkscss')
<link href="{{asset('template/css/searchmap.css')}}" rel="stylesheet">
@endsection
@section('content')
<div class="div_header">
  <img src="http://redvin.com.ar/web14/img/logo.png" height="20">
  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
  <button type="button" class="btn js-switch-vista btn-default selected" id="btnFiltros" rel="nofollow" title="Filtros">
    <i class="fa fa-sliders-h" style="padding: 0px 3px" aria-hidden="true"></i></button>
  &nbsp;&nbsp;|&nbsp;&nbsp;
  <button type="button" class="btn js-switch-vista btn-danger selected" id="btnMapa" rel="nofollow" title="Mapa">
    <i class="fa fa-map" style="padding: 0px 3px" aria-hidden="true"></i></button>
  <button type="button" class="btn js-switch-vista btn-default" id="btnListado" title="Listado">
    <i class="fa fa-th-list" style="padding: 0px 3px" aria-hidden="true"></i></button>
</div>
<div class="div_content">
  <div class="row" style="padding: 0px; margin: 0px;">
    <div id="div_filtros" class="col-xs-12 col-sm-12 col-md-12 col-lg-2" style="display: none; padding: 0px; margin: 0px;">
      <div class="row" style="padding: 0px; margin: 0px;">
        <div class="col-12" style="padding: 0px; margin: 0px; background: #D02026; font-weight: bold; color: white; text-align: center; padding: 10px;">
          Filtrar por:
        </div>
        <div class="col-12" style="padding: 0px; margin: 0px; border: 2px solid #D02026; padding: 10px">
          <span style="font-weight: bold">Categoría</span>
          <label class="container">Vinotecas
            <input type="checkbox" class="filtrar" id="filter_tipoVinotecas" value="1">
            <span class="checkmark"></span>
          </label>
          <label class="container">Distribuidoras
            <input type="checkbox" class="filtrar" id="filter_tipoDistribuidoras" value="2">
            <span class="checkmark"></span>
          </label>
          <label class="container">Bodegas
            <input type="checkbox" class="filtrar" id="filter_tipoBodegas" value="3">
            <span class="checkmark"></span>
          </label>
          <hr>
          <span style="font-weight: bold">Zona</span>
          <label class="container">Capital Federal
            <input type="checkbox" class="filtrar" id="filter_zona1" value="1">
            <span class="checkmark"></span>
          </label>
          <label class="container">La Plata
            <input type="checkbox" class="filtrar" id="filter_zona2" value="2">
            <span class="checkmark"></span>
          </label>
          <hr>
          <span style="font-weight: bold">Medio de pago</span>
          <label class="container">Efectivo
            <input type="checkbox" class="filtrar" id="" value="">
            <span class="checkmark"></span>
          </label>
          <label class="container">Tarjeta de Débito
            <input type="checkbox" class="filtrar" id="" value="">
            <span class="checkmark"></span>
          </label>
          <label class="container">Tarjeta de Crédito
            <input type="checkbox" class="filtrar" id="" value="">
            <span class="checkmark"></span>
          </label>
          <hr>
          <span style="font-weight: bold">Otros filtros</span>
          <label class="container">Abierto hoy
            <input type="checkbox" class="filtrar" id="" value="">
            <span class="checkmark"></span>
          </label>
          <label class="container">Con promociones hoy
            <input type="checkbox" class="filtrar" id="" value="">
            <span class="checkmark"></span>
          </label>

        </div>
      </div>
    </div>
    <div id="div_derecho" class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="padding: 0px; margin: 0px; height: 800px">
      <div id="map"></div>
      <div id="listado">
        <table class="table table-striped">
          <thead>
            <tr class="thead_default" style="background-color: #D02026; color: #fff">
              <td class="text-left"></td>
              <td class="text-left">TIPO</td>
              <td class="text-left">NOMBRE</td>
              <td class="text-left">ZONA</td>
              <td class="text-center"></td>
            </tr>
          </thead>
          <tbody data-bind="foreach: resultado">
            <tr>
              <td class="text-left"><img src="http://www.reservandovinos.com/uploads/l_72_vinoteca-fratelli-vinos-cordoba-argentina-2.jpg" height="75"></td>
              <td class="text-left" data-bind="text: tipo_descripcion"></td>
              <td class="text-left" data-bind="text: nombre"></td>
              <td class="text-left" data-bind="text: localidad"></td>
              <td class="text-center">
                <button type="button" class="btn js-switch-vista btn-danger selected" rel="nofollow" title="Ver en Mapa" data-bind="click: function() { vm.verMapa(marker,infowindow,latitud,longitud) }">
                  <i class="fa fa-map" style="padding: 0px 3px" aria-hidden="true"></i>
                </button>
              </td>
             </tr>
          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>
@endsection
@section('js')
	<script>
		var url_mapstyle = '{{url('assets/mapstyle/mapstyle.json')}}';
    var url_data = '{{url('/map/data')}}';
		var url_imgperfil = '{{url('/imgusuario')}}';
	</script>
    <script src="{{asset('js/searchmap.js')}}"></script>
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyD9FZU87YCCSGovLg57u9dvi2jLDDz3uyw&callback=initMap&libraries=geometry" async defer></script>
@endsection
