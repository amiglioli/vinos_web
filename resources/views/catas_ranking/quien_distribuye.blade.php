@extends('template.template')
@section('linkscss')
<link type="text/css" rel="stylesheet" href="{{ asset('/assets/select2/select2.css') }}">
<link type="text/css" rel="stylesheet" href="{{ asset('/assets/toastr/toastr.min.css') }}">
<link type="text/css" rel="stylesheet" href="{{ asset('/assets/bootstrap-datepicker/css/datepicker.css') }}">
<link type="text/css" rel="stylesheet" href="{{ asset('/assets/sweetalert/sweet-alert.css') }}">
<link href="{{asset('template/css/estilo_tabla.css')}}" rel="stylesheet">
@endsection

@section('content')
 <a href="{{url()->previous()}}" type="button" class="btn btn-primary" style="margin-left:2.5%">Volver</a>

<section class="bf-table" id="data" style="padding-bottom: 20px ">
   <div class="card card-cascade narrower">
      <!--Card image-->
      <div class="view view-cascade gradient-card-header blue-gradient narrower py-2 mx-4 mb-3 d-flex justify-content-between align-items-center">
         <div>
            <!--<button type="button" class="btn btn-outline-white btn-rounded btn-sm px-2 waves-effect waves-light"><i class="fa fa-th-large mt-0"></i></button>
               <button type="button" class="btn btn-outline-white btn-rounded btn-sm px-2 waves-effect waves-light"><i class="fa fa-columns mt-0"></i></button> -->
         </div>
         <span class="white-text">Distribuidoras</span>
         <div>
         </div>
      </div>
      <!--/Card image-->
      <div class="px-4">
        
         <div class="table-wrapper table-responsive">
            <table class="table table-bordered tabla_custom table-hover mb-0">
        <thead>
          <tr>
            <th class="col_1" style="padding:10px;">Nombre</th>
            <th class="col_4" style="padding:10px;">Partido</th>
            <th class="col_4" style="padding:10px;">Localidad</th>
            <th class="col_4" style="padding:10px;">Calle</th>
            <th class="col_4" style="padding:10px;">Número</th>
            <th class="col_4" style="padding:10px;">E-mail</th>
            <th class="col_4" style="padding:10px;">Teléfono</th>
          </tr>
        </thead>
        <tbody>
         @foreach($producto->distribuidoras_que_reparten_mi_zona as $dist)
            <tr>
              <td class="col_1 text-left">{{$dist->distribuidora->name}}</td>
              <td class="col_1 text-left">{{$dist->distribuidora->informacion_adicional->municipio->municipio}}</td>
              <td class="col_1 text-left">{{$dist->distribuidora->informacion_adicional->localidad}}</td>
              <td class="col_1 text-left">{{$dist->distribuidora->informacion_adicional->domicilio}}</td>
              <td class="col_1 text-left">{{$dist->distribuidora->informacion_adicional->numero}}</td>
              <td class="col_1 text-left">{{$dist->distribuidora->email}}</td>
              <td class="col_1 text-left">{{$dist->distribuidora->informacion_adicional->telefono}}</td>
            </tr>
          @endforeach
        </tbody>
      </table>
    <div class="d-flex justify-content-between">
            <div style="padding-top: 40px">
                
            </div>
            
         </div>

      </div>
   </div>
</section>
@endsection
