@extends('template.template')
<style>
.letra{
  font-size:14px;
}
#colorth{
  background-color:#e5e5e5;
  font-weight: bold;
}

</style>
<link href="{{asset('template/css/estilo_tabla.css')}}" rel="stylesheet">
@section('content')
<div id="tabla_vue_js">
  <div class="row">
  <div class="row wrapper red-bg">
  <div class="col-lg-10">				
<h2>Catas REDVIN</h2>	  </div>
  <div class="col-lg-2">
  </div>
</div>	
    <div class="col-lg-12">
        @include('catas_ranking.catas_realizadas')
        @include('catas_ranking.catas_a_realizarse')
    </div>
  </div>
</div>
@endsection

@section('js')
<script>
    moment.locale('es');
    var url = "{{ url('api/cata_ranking') }}";
    var url_realizarse = "{{ url('api/cata_ranking') }}";
    var url_normal = "{{ url('cata_ranking') }}";
</script>
@yield('js_a_realizarse')
@yield('js_realizadas')
@endsection
