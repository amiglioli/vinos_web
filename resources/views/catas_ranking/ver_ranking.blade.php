@extends('template.template')
@section('linkscss')
<link type="text/css" rel="stylesheet" href="{{ asset('/assets/select2/select2.css') }}">
<link type="text/css" rel="stylesheet" href="{{ asset('/assets/toastr/toastr.min.css') }}">
<link type="text/css" rel="stylesheet" href="{{ asset('/assets/bootstrap-datepicker/css/datepicker.css') }}">
<link type="text/css" rel="stylesheet" href="{{ asset('/assets/sweetalert/sweet-alert.css') }}">
<link href="{{asset('template/css/estilo_tabla.css')}}" rel="stylesheet">
<style>
.flash-button{
background:purple;
padding:3px 3px;
color:#fff;
border:none;
border-radius:2px;
animation-name: flash;
animation-duration: 1s;
animation-timing-function: linear;
animation-iteration-count: infinite;
//Firefox 1+
-webkit-animation-name: flash;
-webkit-animation-duration: 1s;
-webkit-animation-timing-function: linear;
-webkit-animation-iteration-count: infinite;
//Safari 3-4
-moz-animation-name: flash;
-moz-animation-duration: 1s;
-moz-animation-timing-function: linear;
-moz-animation-iteration-count: infinite;
}
@keyframes flash {
0% { opacity: 1.0; }
50% { opacity: 0.5; }
100% { opacity: 1.0; }
}
//Firefox 1+
@-webkit-keyframes flash {
0% { opacity: 1.0; }
50% { opacity: 0.5; }
100% { opacity: 1.0; }
}
//Safari 3-4
@-moz-keyframes flash {
0% { opacity: 1.0; }
50% { opacity: 0.5; }
100% { opacity: 1.0; }
}
</style>
@endsection
@section('content')
<div class="row wrapper red-bg">
  <div class="col-lg-10">
  <h2>Catas</h2>    </div>
  <div class="col-lg-2">
  </div>
</div>
<section class="bf-table" id="data" style="display: none; padding-bottom: 20px ">
  <div class="card card-cascade narrower">
    <!--Card image-->
    <div class="view view-cascade gradient-card-header blue-gradient narrower py-2 mx-4 mb-3 d-flex justify-content-between align-items-center">
      <div>
        <!--<button type="button" class="btn btn-outline-white btn-rounded btn-sm px-2 waves-effect waves-light"><i class="fa fa-th-large mt-0"></i></button>
        <button type="button" class="btn btn-outline-white btn-rounded btn-sm px-2 waves-effect waves-light"><i class="fa fa-columns mt-0"></i></button> -->
      </div>
      <span class="white-text" ><b>{{$cata->descripcion}}</b> realizada en {{$cata->lugar}} el día <b>{{fechaCastellano($cata->fecha)}}</b></span>
      <div>
        @if(Auth::user()->hasRole('distribuidora'))
        <!-- <a type="button" class="btn btn-outline-white btn-rounded btn-sm px-2 waves-effect waves-light" href="{{ URL::to('cata_ranking/'.$id.'/ver_vinotecas_zona') }}">VINOTECAS QUE NO TIENEN MIS PRODUCTOS</a> -->
        @endif
      </div>
    </div>
    <!--/Card image-->
    <div class="px-4">
      <div class="table-wrapper table-responsive">
        <!--Table-->
        <div class="btn-group pull-left" data-bind="foreach: filters">
          <span data-bind="click: function() { vm.remove_filters(name) }" title="Click para quitar este filtro" class="label label_table"><i class="fa fa-times-circle" aria-hidden="true"></i>&nbsp;&nbsp;<a style="color: white;" data-bind="text: value"></a></span>
        </div>
        <table class="table table-bordered tabla_custom table-hover mb-0">
          <thead>
            <tr>
              <th class="col_1" style="padding: 0px;">
                <input id="puesto" type="text" placeholder="Puesto" style="text-align: center;" class="form-control" data-bind="event: { change: function(){vm.filterPuesto($element)} }">
              </th>
              <th class="col_1" style="padding: 0px;">
                <input id="puntaje" type="text" placeholder="Puntaje" style="text-align: center;" class="form-control" data-bind="event: { change: function(){vm.filterPuntaje($element)} }">
              </th>
              <th class="col_1" style="padding: 0px;">
                <input id="bodega" type="text" placeholder="Bodega" style="text-align: center;" class="form-control" data-bind="event: { change: function(){vm.filterBodega($element)} }">
              </th>
              <th class="col_1" style="padding: 0px;">
                <input id="marca" type="text" placeholder="Marca" style="text-align: center;" class="form-control" data-bind="event: { change: function(){vm.filterMarca($element)} }">
              </th>
              <th class="col_1" style="padding: 0px;">
                <select id="especializacion" class="form-control select2" data-bind="event:{ change: function(){ vm.filterVarietal($element) } }" style="width:100%">
                  <option value="">Varietal</option>
                  @foreach($varietales as $v)
                  <option value="{{ $v->id }}">{{ $v->varietal }}</option>
                  @endforeach
                </select>
              </th>
              <th class="col_1" style="padding: 0px;width:50px;">
                <input id="anio" type="text" placeholder="Año" style="text-align: center;" class="form-control" data-bind="event: { change: function(){vm.filterAnio($element)} }">
              </th>
              <th class="text-left" style="width:200px;">Acciones</th>
            </tr>
          </thead>
          <tbody data-bind="foreach: data().data">
            <tr>
              <td style=''width='50px:'''   class="col_1 text-center"   data-bind="text: puesto"></td>
              <td style=''width='10px:'''   class="col_1 text-center"   data-bind="text: decimal(puntaje)"></td>
              <td style=''width='50px:'''  class="col_1 text-left"   data-bind="text: producto.bodega.name"></td>
              <td style=''width='50px:'''  class="col_1 text-left"   data-bind="text: producto.marca"></td>

              
                <td style=''width='50px:''  class="col_1 text-left" data-bind="text: producto.varietales != null ? producto.varietales.varietal : '-'"></td>
              

              <td style=''width='80px:'''   class="col_1 text-center"   data-bind="text: producto.anio"></td>
              <td style=''width='250px:'''>
                <a type="button" title="Ver producto" data-bind="event:{ click: function(){ vm.ver_producto(producto.id) } }" class="btn btn-outline btn-success btn-xs"><i class="ti-eye"></i></a>
                @if(Auth::user()->hasRole('vinoteca'))
                <select id="especializacion"  data-bind="event:{ change: function(){ vm.filterEstado($element) } }" style="width:r0%;display: inline;border:1px solid; margin: 0;">
                  
                  <!-- ko if: producto.tengo_el_producto.length == 0 && producto.no_tengo_el_producto.length == 0 -->
                  <option  selected>SIN INFORMAR</option>
                  <!-- /ko -->
                  <!-- ko if: producto.tengo_el_producto.length > 0 -->
                  <option data-bind="value: producto.id+'-1'" selected>LO TENGO</option>
                  <option data-bind="value: producto.id+'-3'"> SIN INFORMAR</option>
                  <!-- /ko -->
                  <!-- ko if: producto.tengo_el_producto.length == 0 -->
                  <option data-bind="value: producto.id+'-1'">LO TENGO</option>
                  <!-- /ko -->
                  <!-- ko if: producto.no_tengo_el_producto.length > 0. -->
                  <option data-bind="value: producto.id+'-0'" selected="">NO LO TENGO</option>
                  <option data-bind="value: producto.id+'-3'"> SIN INFORMAR</option>
                  <!-- /ko -->
                  <!-- ko if: producto.no_tengo_el_producto.length == 0. -->
                  <option data-bind="value: producto.id+'-0'">NO LO TENGO</option>
                  <!-- /ko -->
                </select>
                
                <a type="button" title="Ver quien distribuye este producto" data-bind="attr: { href: '{{ url("cata_ranking") }}' +'/'+producto_id+'/quien_distribuye',class: producto.distribuidoras_que_reparten_mi_zona == 0 ? 'btn btn-outline btn-warning btn-xs' : 'btn btn-outline btn-warning active btn-xs ' }">Envios</i></a>
                <!-- ko if: producto.quiero_producto.length == 0 && producto.pedido_admin == 0. -->
                <a  class="btn btn-outline btn-warning btn-xs" data-bind="event:{ click: function(){ vm.lo_quiero(producto.id,producto.bodega_id,0,producto.distribuidoras_que_reparten_mi_zona.length) } }" title="Quiero este producto">Quiero</a>
                <!-- /ko -->
                <!-- ko if: producto.quiero_producto.length > 0 || producto.pedido_admin.length > 0. -->
                <a  class="btn btn-outline btn-warning active btn-xs" data-bind="event:{ click: function(){ vm.lo_quiero(producto.id,producto.bodega_id,1,producto.distribuidoras_que_reparten_mi_zona.length) } }" title="Quiero este producto">Quiero</a>
                <!-- /ko -->
                @endif
                @if(Auth::user()->hasRole('bodega'))
                <a type="button" title="Ver Ficha" data-bind="attr: { href: '{{ url("ver_ficha/".$id) }}' +'/'+producto_id+'/'+ranking_id, }" class="btn btn-outline btn-warning btn-sm">Fichas de Cata</a>
                @endif
                @can('admin')
                <a type="button" title="Ver Ficha" data-bind="attr: { href: '{{ url("ver_ficha/".$id) }}' +'/'+producto_id+'/'+ranking_id, }" class="btn btn-outline btn-warning btn-sm">Ficha de Cata</a>
                @endcan
                @if(Auth::user()->hasRole('distribuidora'))
                <!-- ko if: producto.distribuyo_el_producto.length > 0. -->
                <a type="button" class="btn btn-outline btn-primary btn" title="Si no distribuye este producto, debe eliminarlo en su Portfolio">Lo distribuyo</a>
                <!-- /ko -->
                <!-- ko if: producto.distribuyo_el_producto.length == 0. -->
                <a type="button" class="btn btn-outline btn-danger btn" title="Si distribuye este producto, debe darlo de alta en su Portfolio">No lo distribuyo</a>
                <!-- /ko -->
                @endif
              </td>
            </tr>
          </tbody>
        </table>
      </div>
      <div class="d-flex justify-content-between">
        <div style="padding-top: 40px">
          <p>Mostrando del <span data-bind="text: data().from == null ? '0' : data().from"></span> al <span data-bind="text: data().to == null ? '0' : data().to"></span> de <span data-bind="text: data().total == null ? '0' : data().total"></span> registros</p>
        </div>
        <nav class="my-4">
          <ul class="pagination pagination-circle pg-blue mb-0">
            <li class="page-item" data-bind="css: data().prev_page_url == null ? 'disabled' : ''"><a class="page-link waves-effect waves-effect" data-bind="click: function() { ir(1) }">« Primera</a></li>
            <li class="page-item" data-bind="if: data().current_page-100 > 0, click: function() { ir(data().current_page-100) } "><span class="page-link waves-effect waves-effect" data-bind="text: data().current_page-100"></span></li>
            <li class="page-item" data-bind="if: data().current_page-50 > 0, click: function() { ir(data().current_page-50) } "><span class="page-link waves-effect waves-effect" data-bind="text: data().current_page-50"></span></li>
            <li class="page-item" data-bind="if: data().current_page-10 > 0, click: function() { ir(data().current_page-10) } "><span class="page-link waves-effect waves-effect" data-bind="text: data().current_page-10"></span></li>
            <li class="page-item" data-bind="if: data().current_page-5 > 0, click: function() { ir(data().current_page-5) } "><span class="page-link waves-effect waves-effect" data-bind="text: data().current_page-5"></span></li>
            <li class="page-item" data-bind="if: data().current_page-2 > 0, click: function() { ir(data().current_page-2) } "><span class="page-link waves-effect waves-effect" data-bind="text: data().current_page-2"></span></li>
            <li class="page-item" data-bind="if: data().current_page-1 > 0, click: function() { ir(data().current_page-1) } "><span class="page-link waves-effect waves-effect" data-bind="text: data().current_page-1"></span></li>
            <li class="page-item active" ><span class="page-link waves-effect waves-effect" data-bind="text: data().current_page"></span></li>
            <li class="page-item" data-bind="if: data().current_page+1 <= data().last_page, click: function() { ir(data().current_page+1) } "><span class="page-link waves-effect waves-effect" data-bind="text: data().current_page+1"></span></li>
            <li class="page-item" data-bind="if: data().current_page+2 <= data().last_page, click: function() { ir(data().current_page+2) } "><span class="page-link waves-effect waves-effect" data-bind="text: data().current_page+2"></span></li>
            <li class="page-item" data-bind="if: data().current_page+5 <= data().last_page, click: function() { ir(data().current_page+5) } "><span class="page-link waves-effect waves-effect" data-bind="text: data().current_page+5"></span></li>
            <li class="page-item" data-bind="if: data().current_page+10 <= data().last_page, click: function() { ir(data().current_page+10) } "><span class="page-link waves-effect waves-effect" data-bind="text: data().current_page+10"></span></li>
            <li class="page-item" data-bind="if: data().current_page+50 <= data().last_page, click: function() { ir(data().current_page+50) } "><span class="page-link waves-effect waves-effect" data-bind="text: data().current_page+50"></span></li>
            <li class="page-item" data-bind="if: data().current_page+100 <= data().last_page, click: function() { ir(data().current_page+100) } "><span class="page-link waves-effect waves-effect" data-bind="text: data().current_page+100"></span></li>
            <li class="page-item" data-bind="css: data().last_page == null || data().current_page >= data().last_page ? 'disabled' : ''"><a class="page-link waves-effect waves-effect" data-bind="click: function() { ir(data().last_page) }">Última »</a></li>
          </ul>
        </nav>
      </div>
    </div>
  </div>
</section>
@include('catas_ranking.vervinodetalle')
@endsection

@section('js')
<script src='{{ asset("assets/toastr/toastr.min.js") }}'></script>
<script src="{{ asset('assets/select2/select2.full.js') }}"></script>
<script src="{{ asset('assets/moment/moment-with-locales.min.js')}}"></script>
<script src="{{ asset('assets/bootstrap-datepicker/js/bootstrap-datepicker.js') }}"></script>
<script src="{{ asset('assets/bootstrap-datepicker/js/locales/bootstrap-datepicker.es.js') }}"></script>

<script>
moment.locale('es');
var url = "{{ url('api/ver_ranking') }}";
var url_normal = "{{ url('ver_ranking') }}";
var urlproducto = "{{ url('api/productos') }}";
var url_lotengo =  "{{ url('cata_ranking') }}";
var url_loquiero =  "{{ url('cata_ranking') }}";
var vm = function() {
	self = this;
	self.data = ko.observableArray([]);
	self.puesto = '';
  self.puntaje = '';
  self.bodega = '';
  self.marca = '';
  self.varietal = '';
  self.varietal_text = '';
  self.estado = '';
  self.estado_text = '';
  self.anio = '';
	self.page = '1';
	self.filters = ko.observableArray([]);
	self.init = function() {
		self.ajax();
		$('.select2').select2();
	}
	self.filterPuesto = function(element) {
		self.page = 1;
		self.puesto = $(element).val()
		self.update_filters();
		self.ajax();
	};
  self.filterPuntaje = function(element) {
    self.page = 1;
    self.puntaje = $(element).val()
    self.update_filters();
    self.ajax();
  };
  self.filterBodega = function(element) {
    self.page = 1;
    self.bodega = $(element).val()
    self.update_filters();
    self.ajax();
  };
  self.filterMarca = function(element) {
    self.page = 1;
    self.marca = $(element).val()
    self.update_filters();
    self.ajax();
  };
  self.filterVarietal = function(element) {
    self.page = 1;
    self.varietal = $(element).val()
    self.varietal_text = $(element).find("option:selected").text();
    self.update_filters();
    self.ajax();
  };
  self.filterAnio = function(element) {
    self.page = 1;
    self.varietal = $(element).val()
    self.update_filters();
    self.ajax();
  };
  self.filterEstado = function(element) {
    self.estado = $(element).val()
    self.estado_text = $(element).find("option:selected").text();
    var resultado = self.estado.split('-');
    $.ajax({
      url: url_lotengo+'/'+resultado[0]+'/lotengo',
      type: 'GET',
      data: {id:resultado[0],estado:resultado[1]},
      success: function(result) {
        toastr.success(result);
        self.ajax();
      }
    });

  };

	self.update_filters = function() {
		self.filters([]);
		if (self.puesto != '') self.filters.push({
			key: 'puesto',
			value: self.puesto,
			name: 'puesto'
		});
    if (self.puntaje != '') self.filters.push({
      key: 'puntaje',
      value: self.puntaje,
      name: 'puntaje'
    });
    if (self.bodega != '') self.filters.push({
      key: 'bodega',
      value: self.bodega,
      name: 'bodega'
    });
    if (self.marca != '') self.filters.push({
      key: 'marca',
      value: self.marca,
      name: 'marca'
    });
    if (self.varietal != '') self.filters.push({
      key: 'varietal',
      value: self.varietal_text,
      name: 'varietal'
    });
    if (self.anio != '') self.filters.push({
      key: 'anio',
      value: self.anio,
      name: 'anio'
    });
	};
	self.remove_filters = function(name) {
		if (name == 'puesto') {
			self.puesto = '';
		}
    if (name == 'puntaje') {
      self.puntaje = '';
    }
    if (name == 'bodega') {
      self.bodega = '';
    }
    if (name == 'marca') {
      self.marca = '';
    }
    if (name == 'varietal') {
      self.varietal = '';
      self.varietal_text = '';
    }
    if (name == 'anio') {
      self.anio = '';
    }
		$('#' + name).val("").trigger("change");
		self.update_filters();
		self.ajax();
	};
	self.ir = function(page) {
		self.page = page;
		self.ajax();
		$('html, body').animate({
			scrollTop: 0
		}, 'fast');
	};
  self.lo_tengo = function(id) {
    $.ajax({
      url: url_lotengo+'/'+id+'/lotengo',
      type: 'GET',
      data    :   {producto_id:id},
      success: function(result) {
        toastr.success(result);
        self.ajax();
      }
    });
  };

  self.lo_quiero = function(id, bodega_id, estado,hay_distribuidora) {
      if(hay_distribuidora == 0)
        var titulo = 'Intentaremos localizar un distribuidor de este producto en su zona';
      else
        var titulo = 'Se lo informaremos al distribuidor de su zona';

      if (estado == 0) {
          swal({
                  title: "¿Quiere este producto?",
                  text: titulo,
                  icon: "warning",
                  buttons: true,
                  dangerMode: true,
              })
              .then((willDelete) => {
                  if (willDelete) {
                      $.ajax({
                          url: url_loquiero + '/' + id + '/loquiero',
                          type: 'GET',
                          data: {
                              producto_id: id,
                              bodega: bodega_id
                          },
                          success: function(result) {
                              swal("Su pedido ha sido informado", {
                                  icon: "success",
                              });
                              self.ajax();
                          }
                      });

                  }
              });
      } else {

          $.ajax({
              url: url_loquiero + '/' + id + '/loquiero',
              type: 'GET',
              data: {
                  producto_id: id,
                  bodega: bodega_id
              },
              success: function(result) {
                  swal("Su pedido se ha cancelado");
                  self.ajax();
              }
          });
      }

  };

  self.ver_producto = function(id) {
  $("#p_modal").css('display','block');
  $("#table_productos").css('display','none');
  $("#modal_producto").modal('show');
    $.getJSON(urlproducto, {
      page: self.page,
      producto_id: id
    })
    .done(function(data) {
        data = data.data[0];
        console.log(data)
        if(data.img_etiqueta == null)
          $("#m_imagen").attr('src',"{{asset('template/img/fondo-usuario.jpg')}}");
        else
          $("#m_imagen").attr('src',"{{asset('')}}"+"/"+data.img_etiqueta);

        $("#m_producto").html(data.producto.nombre);
        $("#m_tipo").html(data.producto_tipo.tipo);
        if(data.varietales != null)
          $("#m_varietal").html(data.varietales.varietal);
        else
          $("#m_varietal").html('-');
        
        $("#m_marca").html(data.marca);
        $("#m_bodega").html(data.bodega.name);
        $("#m_anio").html(data.anio);
        $("#m_pais").html(data.pais.nombre);
        $("#m_provincia").html(data.provincia.provincia);
        $("#m_region").html(data.region.nombre);
        $("#m_ciudad").html(data.ciudad.nombre);


        $("#p_modal").css('display','none');
        $("#table_productos").css('display','table');


    })
    .error(function(d) {
      toastr.error('Se encontro un error intente nuevamente');
    });
  };
	self.eliminar = function(id) {
		$.ajax({
			url: url_normal+'/'+id,
			type: 'DELETE',
			data    :   {
				'_token': "{{ csrf_token() }}"
			},
			success: function(result) {
				toastr.success(result);
				self.ajax();
			}
		});
	};
	self.ajax = function() {
		$.getJSON(url, {
			page: self.page,
			id_cata: '{{$id}}',
      puesto: self.puesto,
      puntaje:self.puntaje,
      bodega:self.bodega,
      marca:self.marca,
      varietal:self.varietal,
      anio:self.anio
		})
		.done(function(data) {
			console.log(data.data);
			Pace.restart()
			vm.data(data);
			$("#data").fadeIn();
			$(".loading").remove();
		})
		.error(function(d) {
			toastr.error('Se encontro un error intente nuevamente');
		});
	};
};
vm = new vm();
vm.init();
ko.applyBindings(vm, document.getElementById("data"));

@if(session()->has('message'))
toastr.success('{{ session()->get('message ') }}');
@endif
</script>
@endsection
