
<style>
  .letra{
    font-size:14px;
  }
  #colorth{
    background-color:#e5e5e5;
    font-weight: bold;
  }

  #table_productos {
    font-family: Poppins, Helvetica, sans-serif;
    border-collapse: collapse;
    width: 100%;
  }

  #table_productos td, #table_productos th {
    border: 1px solid #ddd;
    padding: 8px;
  }

  #table_productos tr:nth-child(even){background-color: #f2f2f2;}

  #table_productos tr:hover {background-color: #ddd;}

  #table_productos th {
    padding-top: 12px;
    padding-bottom: 12px;
    text-align: left;
    background-color: #d02026;
    color: white;
  }

</style>


<div class="modal fade" id="modal_producto" role="dialog">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <!--<button type="button" class="close" data-dismiss="modal">&times;</button> -->
          <div class="row wrapper red-bg">
  <div class="col-lg-10">				
<h2>Información del producto</h2>	  </div>
  <div class="col-lg-2">
  </div>
</div>	
      </div>
      <div class="modal-body">
        <center> <p id="p_modal">Estamos cargando sus datos..</p></center>
        <div class="row">
          <div class="col-md-6">
            <table id="table_productos">
             <tr>
              <th class="h4">Producto</th>
              <th class="h4">Detalle</th>
            </tr>
            <tr>
              <td class="informacion">Producto</td>
              <td class="detalle"><span id="m_producto"></span></td>
            </tr>
            <tr>
              <td class="informacion">Tipo</td>
              <td class="detalle"><span id="m_tipo"></span>  </td>
            </tr>
            <tr>
              <td class="informacion">Varietal</td>
              <td class="detalle"><span id="m_varietal"></span>  </td>
            </tr>
            <tr>
              <td class="informacion">Marca</td>
              <td class="detalle"><span id="m_marca"></span>  </td>
            </tr>
            <tr>
              <td class="informacion">Bodega</td>
              <td class="detalle"><span id="m_bodega"></span> </td>
            </tr>
            <tr>
              <td class="informacion">Año</td>
              <td class="detalle"> <span id="m_anio"></span> </td>
            </tr>
            <tr>
              <td class="informacion">País</td>
              <td class="detalle"> <span id="m_pais"></span>   </td>
            </tr>
            <tr>
              <td class="informacion">Provincia</td>
              <td class="detalle"> <span id="m_provincia"></span> </td>
            </tr>
            <tr>
              <td class="informacion">Región</td>
              <td class="detalle"> <span id="m_region"></span> </td>
            </tr>
            <tr>
              <td class="informacion">Ciudad</td>
              <td class="detalle"> <span id="m_ciudad"></span> </td>
            </tr>

          </table>
        </div>
        <div class="col-md-6">
          <img id="m_imagen" alt="image" type="file" style="max-width::500px;max-height:500px" id="img" src="">
        </div>
      </div>
    </div>
    <div class="modal-footer">
      <button type="button" class="btn btn-danger" data-dismiss="modal">Cerrar</button>
    </div>
  </div>
</div>
</div>
