@extends('template.template')
@section('linkscss')
<link type="text/css" rel="stylesheet" href="{{ asset('/assets/select2/select2.css') }}">
<link type="text/css" rel="stylesheet" href="{{ asset('/assets/toastr/toastr.min.css') }}">
<link type="text/css" rel="stylesheet" href="{{ asset('/assets/bootstrap-datepicker/css/datepicker.css') }}">
<link type="text/css" rel="stylesheet" href="{{ asset('/assets/sweetalert/sweet-alert.css') }}">
<link href="{{asset('template/css/estilo_tabla.css')}}" rel="stylesheet">
<link href="{{asset('template/css/home.css')}}" rel="stylesheet">
@endsection
@section('content')
<div class="col-sm-12">
  <div class="ibox">
    <div class="ibox-content">
      <h2>Productos</h2>
      <p>
        Muestra si los vinos lo tienen la vinoteca
      </p>
      <div class="">
        <ul class="nav nav-tabs">
          @foreach($productos_cata as $indice => $pc)
          @if($indice == 0 )
          <li class="active"><a data-toggle="tab" href="#tab-{{$pc->id_producto}}"><i class="fa fa-glass"></i> {{$pc->marca}}</a></li>
          @else
          <li><a data-toggle="tab" href="#tab-{{$pc->id_producto}}"><i class="fa fa-glass"></i>{{$pc->marca}}</a></li>
          @endif
          @endforeach
        </ul>
        <div class="tab-content">
          @foreach($productos_cata as $indice => $pc)
          @if($indice == 0)
          <div id="tab-{{$pc->id_producto}}" class="tab-pane active">
            @else
            <div id="tab-{{$pc->id_producto}}" class="tab-pane">
              @endif
              <div class="slimScrollDiv" style="position: relative; overflow: hidden; width: auto;"><div>
                <div class="table-responsive">
                  <table class="table table-striped table-hover">
                    <thead>
                      <th>Vinoteca</th>
                      <th>Provincia</th>
                      <th>Localidad</th>
                      <th>Municipio</th>
                      <th>Domicilio</th>
                      <th>Numero</th>
                      <th>Teléfono</th>
                      <th>Detalle</th>
                    </thead>
                    <tbody>
                      @if(!empty($pc->vinotecas))
                      @foreach($pc->vinotecas as $vinoteca)
                      <tr>
                        <td>{{$vinoteca->vionteca_nombre}}</td>
                        <td>{{$vinoteca->nombre_provincia}}</td>
                        <td>{{$vinoteca->municipio}}</td>
                        <td>{{$vinoteca->localidad}}</td>
                        <td>{{$vinoteca->domicilio}}</td>
                        <td>{{$vinoteca->numero}}</td>
                        <td>{{$vinoteca->telefono}}</td>
                        @if($vinoteca->lo_tiene)
                        <td class="client-status"><span class="label label-primary">Lo tiene</span></td>
                        @else
                        <td class="client-status"><span class="label label-danger">No lo tiene</span></td>
                        @endif
                      </tr>
                      @endforeach
                      @else
                      <tr>
                        <td style="text-align: center" colspan="9">Usted no distribuye este producto</td>
                      </tr>
                      @endif
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>
          @endforeach
          <div class="googleMap" style="width:100%;height:400px;"></div>
        </div>
      </div>
    </div>
  </div>
</div>
<input type="hidden" id="vinotecas" value="{{$vinotecas_zona}}">
@endsection

@section('js')

<script>
$( document ).ready(function() {
  initialize();
  var data  = $("#vinotecas").val();
  var myJSON = JSON.parse(data);
  var  uvinoteca = '{{asset('/iconosmapa/vinoteca.png')}}';
  $.each(myJSON, function (index, value) {
    //console.log(value);

    



    var latlng = new google.maps.LatLng(value.geo_latitud, value.geo_longitud);
     //var latLng = new google.maps.LatLng(json[i].lat, json[i].lng);
            var marker = new google.maps.Marker({
                position: latlng,
                map: map
            });
             marker.setIcon(icon(uvinoteca));

  });
});


function initialize() {
    var myLat = "{{Auth::user()->informacion_adicional->geo_latitud}}";
    var myLong = "{{Auth::user()->informacion_adicional->geo_longitud}}";
    var latlng = new google.maps.LatLng(myLat,myLong);
    var options = {
        zoom: 8,
        center: latlng,
        mapTypeId: google.maps.MapTypeId.ROADMAP
    };

    map = new google.maps.Map($('.googleMap')[0], options);


}


function icon(param){
  var icon = {
    url: param,
    scaledSize: new google.maps.Size(40, 40), // scaled size
    origin: new google.maps.Point(0,0), // origin
    anchor: new google.maps.Point(0, 0) // anchor
  };
  return icon;
}

</script>

<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyD9FZU87YCCSGovLg57u9dvi2jLDDz3uyw"></script>



@endsection
