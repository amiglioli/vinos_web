<link href="{{asset('template/css/estilo_tabla.css')}}" rel="stylesheet">
<div class="col-lg-12" id='catas_realizadas'>
  <div class="card card-cascade narrower">
      <!--Card image-->
      <div class="view view-cascade gradient-card-header blue-gradient narrower py-2 mx-4 mb-3 d-flex justify-content-between align-items-center" style="background-color: #000000 !important">
         <div>
            <!--<button type="button" class="btn btn-outline-white btn-rounded btn-sm px-2 waves-effect waves-light"><i class="fa fa-th-large mt-0"></i></button>
               <button type="button" class="btn btn-outline-white btn-rounded btn-sm px-2 waves-effect waves-light"><i class="fa fa-columns mt-0"></i></button> -->
         </div>
         <span class="white-text">CATAS REALIZADAS</span>
         <div>
         </div>
      </div>
      <!--/Card image-->
      <div class="px-4">
        
         <div class="table-wrapper table-responsive">
            <!--Table-->
            <div class="btn-group pull-left" data-bind="foreach: filters">
          <span data-bind="click: function() { vm.remove_filters(name) }" title="Click para quitar este filtro" class="label label_table"><i class="fa fa-times-circle" aria-hidden="true"></i>&nbsp;&nbsp;<a style="color: white;" data-bind="text: value"></a></span>
        </div>
            <table class="table table-bordered tabla_custom table-hover mb-0">
          <thead>
            <tr>
              <th class="text-left">FECHA</th>
              <th class="text-left">LUGAR</th>
              <th class="text-left">DESCRIPCIÓN</th>
              <th class="text-left">RANKING</th>
            </tr>
          </thead>
          <tbody data-bind="foreach: data().data">
            <tr>
              <td class="col_1 text-left"   data-bind="text: spanishDate(fecha)"></td>
              <td class="col_1 text-left"   data-bind="text: lugar"></td>
              <td class="col_1 text-left"   data-bind="text: descripcion"></td>
              <td class="col_1 text-left"> <a class="btn btn-outline btn-warning" data-bind="attr: { href: '{{ url("cata_ranking") }}' +'/'+id+'/ver_ranking', }">VER</a></td>
            </tr>
          </tbody>
        </table>
    </div>
         
         <div class="d-flex justify-content-between">
            <div style="padding-top: 40px">
                <p>Mostrando del <span data-bind="text: data().from == null ? '0' : data().from"></span> al <span data-bind="text: data().to == null ? '0' : data().to"></span> de <span data-bind="text: data().total == null ? '0' : data().total"></span> registros</p>               
            </div>
            <nav class="my-4">
               <ul class="pagination pagination-circle pg-blue mb-0">
                  <li class="page-item" data-bind="css: data().prev_page_url == null ? 'disabled' : ''"><a class="page-link waves-effect waves-effect" data-bind="click: function() { ir(1) }">« Primera</a></li>
                  <li class="page-item" data-bind="if: data().current_page-100 > 0, click: function() { ir(data().current_page-100) } "><span class="page-link waves-effect waves-effect" data-bind="text: data().current_page-100"></span></li>
                  <li class="page-item" data-bind="if: data().current_page-50 > 0, click: function() { ir(data().current_page-50) } "><span class="page-link waves-effect waves-effect" data-bind="text: data().current_page-50"></span></li>
                  <li class="page-item" data-bind="if: data().current_page-10 > 0, click: function() { ir(data().current_page-10) } "><span class="page-link waves-effect waves-effect" data-bind="text: data().current_page-10"></span></li>
                  <li class="page-item" data-bind="if: data().current_page-5 > 0, click: function() { ir(data().current_page-5) } "><span class="page-link waves-effect waves-effect" data-bind="text: data().current_page-5"></span></li>
                  <li class="page-item" data-bind="if: data().current_page-2 > 0, click: function() { ir(data().current_page-2) } "><span class="page-link waves-effect waves-effect" data-bind="text: data().current_page-2"></span></li>
                  <li class="page-item" data-bind="if: data().current_page-1 > 0, click: function() { ir(data().current_page-1) } "><span class="page-link waves-effect waves-effect" data-bind="text: data().current_page-1"></span></li>
                  <li class="page-item active" ><span class="page-link waves-effect waves-effect" data-bind="text: data().current_page"></span></li>
                  <li class="page-item" data-bind="if: data().current_page+1 <= data().last_page, click: function() { ir(data().current_page+1) } "><span class="page-link waves-effect waves-effect" data-bind="text: data().current_page+1"></span></li>
                  <li class="page-item" data-bind="if: data().current_page+2 <= data().last_page, click: function() { ir(data().current_page+2) } "><span class="page-link waves-effect waves-effect" data-bind="text: data().current_page+2"></span></li>
                  <li class="page-item" data-bind="if: data().current_page+5 <= data().last_page, click: function() { ir(data().current_page+5) } "><span class="page-link waves-effect waves-effect" data-bind="text: data().current_page+5"></span></li>
                  <li class="page-item" data-bind="if: data().current_page+10 <= data().last_page, click: function() { ir(data().current_page+10) } "><span class="page-link waves-effect waves-effect" data-bind="text: data().current_page+10"></span></li>
                  <li class="page-item" data-bind="if: data().current_page+50 <= data().last_page, click: function() { ir(data().current_page+50) } "><span class="page-link waves-effect waves-effect" data-bind="text: data().current_page+50"></span></li>
                  <li class="page-item" data-bind="if: data().current_page+100 <= data().last_page, click: function() { ir(data().current_page+100) } "><span class="page-link waves-effect waves-effect" data-bind="text: data().current_page+100"></span></li>
                  <li class="page-item" data-bind="css: data().last_page == null || data().current_page >= data().last_page ? 'disabled' : ''"><a class="page-link waves-effect waves-effect" data-bind="click: function() { ir(data().last_page) }">Última »</a></li>
               </ul>

            </nav>

         </div>

      </div>
   </div>
  </div>


  @section('js_realizadas')
  <script>
  moment.locale('es');
  var vm = function() {
    self = this;
    self.data = ko.observableArray([]);
    self.producto = '';
    self.producto_text = '';
    self.page = '1';
    self.filters = ko.observableArray([]);
    self.init = function() {
      self.ajax();
      $('.select2').select2();
    }
    self.filterProducto = function(element) {
      self.page = 1;
      self.producto = $(element).val()
      self.producto_text = $(element).find("option:selected").text();
      self.update_filters();
      self.ajax();
    };

    self.update_filters = function() {
      self.filters([]);
      if (self.producto != '') self.filters.push({
        key: 'producto',
        value: self.producto_text,
        name: 'producto'
      });
    };
    self.remove_filters = function(name) {
      if (name == 'producto') {
        self.producto = '';
        self.producto_text = '';
      }
      $('#' + name).val("").trigger("change");
      self.update_filters();
      self.ajax();
    };
    self.ir = function(page) {
      self.page = page;
      self.ajax();
      $('html, body').animate({
        scrollTop: 0
      }, 'fast');
    };
    self.eliminar = function(id) {
      $.ajax({
        url: url_normal+'/'+id,
        type: 'DELETE',
        data    :   {
          '_token': "{{ csrf_token() }}"
        },
        success: function(result) {
          toastr.success(result);
          self.ajax();
        }
      });
    };
    self.ajax = function() {
      $.getJSON(url, {
        page: self.page,
        realizadas: '{{date("Y-m-d")}}'
      })
      .done(function(data) {
        console.log(data);
        Pace.restart()
        vm.data(data);
        $("#data").fadeIn();
        $(".loading").remove();
      })
      .error(function(d) {
        toastr.error('Se encontro un error intente nuevamente');
      });
    };
  };
  vm = new vm();
  vm.init();
  ko.applyBindings(vm, document.getElementById("catas_realizadas"));

  @if(session()->has('message'))
  toastr.success('{{ session()->get('message ') }}');
  @endif
  </script>
  @endsection
