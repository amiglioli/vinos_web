@extends('template.template')
@section('linkscss')
<link type="text/css" rel="stylesheet" href="{{ asset('/assets/select2/select2.css') }}">
<link type="text/css" rel="stylesheet" href="{{ asset('/assets/toastr/toastr.min.css') }}">
<link type="text/css" rel="stylesheet" href="{{ asset('/assets/bootstrap-datepicker/css/datepicker.css') }}">
<link type="text/css" rel="stylesheet" href="{{ asset('/assets/sweetalert/sweet-alert.css') }}">
<link href="{{asset('template/css/estilo_tabla.css')}}" rel="stylesheet">
@endsection
@section('content')
<div class="row wrapper red-bg">
  <div class="col-lg-10">
    <h2>Productos</h2>
  </div>
  <div class="col-lg-2">
  </div>
</div>
<section class="bf-table" id="data" style="display: none; padding-bottom: 20px ">
  <div class="card card-cascade narrower">
    <!--Card image-->
    <div class="view view-cascade gradient-card-header blue-gradient narrower py-2 mx-4 mb-3 d-flex justify-content-between align-items-center">
     
      <span class="white-text">Listado de productos</span>
       <div style="text-align:right">
	   <a type="button" class="btn btn-outline btn-danger"  href="{{ url('productos/create') }}">Agregar producto</a>
      </div>
    </div>
    <!--/Card image-->
    <div class="px-4">
      
      <div class="table-wrapper table-responsive">
        <!--Table-->
        <div class="btn-group pull-left" data-bind="foreach: filters">
          <span data-bind="click: function() { vm.remove_filters(name) }" title="Click para quitar este filtro" class="label label_table"><i class="fa fa-times-circle" aria-hidden="true"></i>&nbsp;&nbsp;<a style="color: white;" data-bind="text: value"></a></span>
        </div>
        <table class="table table-striped tabla_custom table-hover mb-0">
          <thead>
            <tr>
              <th class="col_1" style="padding: 0px;">
                <select id="producto" class="form-control select2" data-bind="event:{ change: function(){ vm.filterProducto($element) } }" style="width:100%">
                  <option value="">Producto</option>
                  @foreach($producto as $p)
                  <option value="{{ $p->id }}">{{ $p->nombre }}</option>
                  @endforeach
                </select>
              </th>
              <th class="col_1" style="padding: 0px;">
                <select id="producto_tipo" class="form-control select2" data-bind="event:{ change: function(){ vm.filterProductoTipo($element) } }" style="width:100%">
                  <option value="">Tipo</option>
                  @foreach($producto_tipo as $pt)
                  <option value="{{ $pt->id }}">{{ $pt->tipo }}</option>
                  @endforeach
                </select>
              </th>

               <th class="col_1" style="padding: 0px;">
                <select id="bodega" class="form-control select2" data-bind="event:{ change: function(){ vm.filterBodega($element) } }" style="width:100%">
                  <option value="">Bodega</option>
                  @foreach($bodegas as $b)
                  <option value="{{ $b->id }}">{{ $b->name }}</option>
                  @endforeach
                </select>
              </th>


              <th class="col_1" style="padding: 0px;">
                <select id="marca" class="form-control select2" data-bind="event:{ change: function(){ vm.filterMarca($element) } }" style="width:100%">
                  <option value="">Marca</option>
                  @foreach($productos as $p)
                  <option value="{{ $p->marca }}">{{ $p->marca }}</option>
                  @endforeach
                </select>
              </th>

             <!-- <th class="col_1" style="padding: 0px;">
                <input id="marca" type="text" placeholder="Marca" class="form-control" data-bind="event: { change: function(){vm.filterMarca($element)} }">
              </th> -->

               <th class="col_1" style="padding: 0px;">
                <select id="varietal" class="form-control select2" data-bind="event:{ change: function(){ vm.filterVarietal($element) } }" style="width:100%">
                  <option value="">Varietal</option>
                  @foreach($varietales as $v)
                  <option value="{{ $v->id }}">{{ $v->varietal }}</option>
                  @endforeach
                </select>
              </th>
              <th class="col_1" style="padding: 0px;">
                <input id="anio" type="text" placeholder="Año" class="form-control" data-bind="event: { change: function(){vm.filterAnio($element)} }">
              </th>
              <th class="text-left">Puntaje</th>
              <th class="text-left">Acciones</th>
            </tr>
          </thead>
          <tbody data-bind="foreach: data().data">
            <tr>
              <td class="col_1 text-left" style="width:30px"    data-bind="text: producto.nombre"></td>
              <td class="col_1 text-left" style="width:120px"    data-bind="text: producto_tipo.tipo"></td>
              <td class="col_1 text-left" style="width:180px"    data-bind="text: bodega.name"></td>
              <td class="col_1 text-left" style="width:180px"   data-bind="text: marca"></td>
              <!-- ko if: varietales != null -->
                <td class="col_1 text-left" style="width:100px"   data-bind="text: varietales.varietal"></td>
              <!-- /ko -->
              <!-- ko if: varietales == null -->
                <td class="col_1 text-left" style="width:150px"> - </td>
              <!-- /ko -->
              
              <td style="width:55px"   data-bind="text: anio"></td>

              <td style="width:80px"><a href="javascript:void(0)" data-bind="text:cata_cerrada.length,attr: { href: '{{ url("productos") }}' +'/'+id}"  class="btn btn-outline btn-success btn-xs"></a></td>
              <td class="col_1 ">
                
                <a type="button" title="Ver producto" data-bind="event:{ click: function(){ vm.ver_producto(id) } }" class="btn btn-outline btn-success btn-xs">VER</i></a>
                <!-- ko if: cata_cerrada.length == 0 -->
                <a href="javascript:void(0)" data-bind="attr: { href: '{{ url("productos") }}' +'/'+id+'/edit', }"  class="btn btn-outline btn-warning btn-xs">Editar</a>
                <a href="javascript:void(0)" data-bind="event:{ click: function(){ vm.eliminar(id) } }" class="btn btn-outline btn-danger btn-sm">Eliminar</a>
                <!-- /ko -->
              </td>
            </tr>
          </tbody>
        </table>
      </div>
      
      <div class="d-flex justify-content-between">
        <div style="padding-top: 40px">
          <p>Mostrando del <span data-bind="text: data().from == null ? '0' : data().from"></span> al <span data-bind="text: data().to == null ? '0' : data().to"></span> de <span data-bind="text: data().total == null ? '0' : data().total"></span> registros</p>
        </div>
        <nav class="my-4">
          <ul class="pagination pagination-circle pg-blue mb-0">
            <li class="page-item" data-bind="css: data().prev_page_url == null ? 'disabled' : ''"><a class="page-link waves-effect waves-effect" data-bind="click: function() { ir(1) }">« Primera</a></li>
            <li class="page-item" data-bind="if: data().current_page-100 > 0, click: function() { ir(data().current_page-100) } "><span class="page-link waves-effect waves-effect" data-bind="text: data().current_page-100"></span></li>
            <li class="page-item" data-bind="if: data().current_page-50 > 0, click: function() { ir(data().current_page-50) } "><span class="page-link waves-effect waves-effect" data-bind="text: data().current_page-50"></span></li>
            <li class="page-item" data-bind="if: data().current_page-10 > 0, click: function() { ir(data().current_page-10) } "><span class="page-link waves-effect waves-effect" data-bind="text: data().current_page-10"></span></li>
            <li class="page-item" data-bind="if: data().current_page-5 > 0, click: function() { ir(data().current_page-5) } "><span class="page-link waves-effect waves-effect" data-bind="text: data().current_page-5"></span></li>
            <li class="page-item" data-bind="if: data().current_page-2 > 0, click: function() { ir(data().current_page-2) } "><span class="page-link waves-effect waves-effect" data-bind="text: data().current_page-2"></span></li>
            <li class="page-item" data-bind="if: data().current_page-1 > 0, click: function() { ir(data().current_page-1) } "><span class="page-link waves-effect waves-effect" data-bind="text: data().current_page-1"></span></li>
            <li class="page-item active" ><span class="page-link waves-effect waves-effect" data-bind="text: data().current_page"></span></li>
            <li class="page-item" data-bind="if: data().current_page+1 <= data().last_page, click: function() { ir(data().current_page+1) } "><span class="page-link waves-effect waves-effect" data-bind="text: data().current_page+1"></span></li>
            <li class="page-item" data-bind="if: data().current_page+2 <= data().last_page, click: function() { ir(data().current_page+2) } "><span class="page-link waves-effect waves-effect" data-bind="text: data().current_page+2"></span></li>
            <li class="page-item" data-bind="if: data().current_page+5 <= data().last_page, click: function() { ir(data().current_page+5) } "><span class="page-link waves-effect waves-effect" data-bind="text: data().current_page+5"></span></li>
            <li class="page-item" data-bind="if: data().current_page+10 <= data().last_page, click: function() { ir(data().current_page+10) } "><span class="page-link waves-effect waves-effect" data-bind="text: data().current_page+10"></span></li>
            <li class="page-item" data-bind="if: data().current_page+50 <= data().last_page, click: function() { ir(data().current_page+50) } "><span class="page-link waves-effect waves-effect" data-bind="text: data().current_page+50"></span></li>
            <li class="page-item" data-bind="if: data().current_page+100 <= data().last_page, click: function() { ir(data().current_page+100) } "><span class="page-link waves-effect waves-effect" data-bind="text: data().current_page+100"></span></li>
            <li class="page-item" data-bind="css: data().last_page == null || data().current_page >= data().last_page ? 'disabled' : ''"><a class="page-link waves-effect waves-effect" data-bind="click: function() { ir(data().last_page) }">Última »</a></li>
          </ul>
        </nav>
      </div>
    </div>
  </div>
</section>
@include('catas_ranking.vervinodetalle')
@endsection

@section('js')
<script src='{{ asset("assets/toastr/toastr.min.js") }}'></script>
<script src="{{ asset('assets/select2/select2.full.js') }}"></script>
<script src="{{ asset('assets/moment/moment-with-locales.min.js')}}"></script>
<script src="{{ asset('assets/bootstrap-datepicker/js/bootstrap-datepicker.js') }}"></script>
<script src="{{ asset('assets/bootstrap-datepicker/js/locales/bootstrap-datepicker.es.js') }}"></script>
<script>
moment.locale('es');
var url = "{{ url('api/productos') }}";
var url_normal = "{{ url('productos') }}";
var urlproducto = "{{ url('api/productos') }}";
var vm = function() {
  self = this;
  self.data = ko.observableArray([]);
  self.producto = '';
  self.producto_text = '';
  self.producto_tipo = '';
  self.producto_tipo_text = '';
  self.varietal = '';
  self.varietal_text = '';
  self.marca = '';
  self.marca_text = '';
  self.bodega = '';
  self.bodega_text = '';

  self.anio = '';
  self.page = '1';
  self.filters = ko.observableArray([]);
  self.init = function() {
    self.ajax();
    $('.select2').select2();
  }
  self.filterProducto = function(element) {
    self.page = 1;
    self.producto = $(element).val()
    self.producto_text = $(element).find("option:selected").text();
    self.update_filters();
    self.ajax();
  };
  self.filterProductoTipo= function(element) {
    self.page = 1;
    self.producto_tipo = $(element).val()
    self.producto_tipo_text = $(element).find("option:selected").text();
    self.update_filters();
    self.ajax();
  };
  self.filterVarietal= function(element) {
    self.page = 1;
    self.varietal = $(element).val()
    self.varietal_text = $(element).find("option:selected").text();
    self.update_filters();
    self.ajax();
  };
  self.filterMarca= function(element) {
    self.page = 1;
    self.marca = $(element).val()
    self.marca_text = $(element).find("option:selected").text();
    self.update_filters();
    self.ajax();
  };
  self.filterBodega= function(element) {
    self.page = 1;
    self.bodega = $(element).val()
    self.bodega_text = $(element).find("option:selected").text();
    self.update_filters();
    self.ajax();
  };
  self.filterAnio= function(element) {
    self.page = 1;
    self.anio = $(element).val()
    self.update_filters();
    self.ajax();
  };

  self.update_filters = function() {
    self.filters([]);
    if (self.producto != '') self.filters.push({
      key: 'producto',
      value: self.producto_text,
      name: 'producto'
    });
    if (self.producto_tipo != '') self.filters.push({
      key: 'producto_tipo',
      value: self.producto_tipo_text,
      name: 'producto_tipo'
    });
    if (self.varietal != '') self.filters.push({
      key: 'varietal',
      value: self.varietal_text,
      name: 'varietal'
    });
    if (self.marca != '') self.filters.push({
      key: 'marca',
      value: self.marca_text,
      name: 'marca'
    });
    if (self.bodega != '') self.filters.push({
      key: 'bodega',
      value: self.bodega_text,
      name: 'bodega'
    });
    if (self.anio != '') self.filters.push({
      key: 'anio',
      value: self.anio,
      name: 'anio'
    });
  };
  self.ver_producto = function(id) {
  $("#p_modal").css('display','block');
  $("#table_productos").css('display','none');
  $("#modal_producto").modal('show');
    $.getJSON(urlproducto, {
      page: self.page,
      producto_id: id
    })
    .done(function(data) {
        data = data.data[0];
        console.log(data)
        if(data.img_etiqueta == null)
          $("#m_imagen").attr('src',"{{asset('template/img/fondo-usuario.jpg')}}");
        else
          $("#m_imagen").attr('src',"{{asset('')}}"+"/"+data.img_etiqueta);

        $("#m_producto").html(data.producto.nombre);
        $("#m_tipo").html(data.producto_tipo.tipo);
        if(data.varietales != null)
          $("#m_varietal").html(data.varietales.varietal);
        else
          $("#m_varietal").html('-');
        $("#m_marca").html(data.marca);
        $("#m_bodega").html(data.bodega.name);
        $("#m_anio").html(data.anio);
        $("#m_pais").html(data.pais.nombre);
        $("#m_provincia").html(data.provincia.provincia);
        $("#m_region").html(data.region.nombre);
        $("#m_ciudad").html(data.ciudad.nombre);


        $("#p_modal").css('display','none');
        $("#table_productos").css('display','table');


    })
    .error(function(d) {
      toastr.error('Se encontro un error intente nuevamente');
    });
  };
  self.remove_filters = function(name) {
    if (name == 'producto') {
      self.producto = '';
      self.producto_text = '';
    }
    if (name == 'producto_tipo') {
      self.producto_tipo = '';
      self.producto_tipo_text = '';
    }
    if (name == 'varietal') {
      self.varietal = '';
      self.varietal_text = '';
    }
    if (name == 'marca') {
      self.marca = '';
      self.marca_text = '';
    }
    if (name == 'bodega') {
      self.bodega = '';
      self.bodega_text = '';
    }
    if (name == 'anio') {
      self.anio = '';
    }
    $('#' + name).val("").trigger("change");
    self.update_filters();
    self.ajax();
  };
  self.ir = function(page) {
    self.page = page;
    self.ajax();
    $('html, body').animate({
      scrollTop: 0
    }, 'fast');
  };
  self.eliminar = function(id) {
    $.ajax({
      url: url_normal+'/'+id,
      type: 'DELETE',
      data    :   {
        '_token': "{{ csrf_token() }}"
      },
      success: function(result) {
        toastr.success(result);
        self.ajax();
      }
    });
  };
  self.ajax = function() {
    $.getJSON(url, {
      page: self.page,
      producto: self.producto,
      producto_tipo:self.producto_tipo,
      marca:self.marca,
      varietal:self.varietal,
      bodega:self.bodega,
      anio:self.anio
    })
    .done(function(data) {
      console.log(data);
      Pace.restart()
      vm.data(data);
      $("#data").fadeIn();
      $(".loading").remove();
    })
    .error(function(d) {
      toastr.error('Se encontro un error intente nuevamente');
    });
  };
};
vm = new vm();
vm.init();
ko.applyBindings(vm, document.getElementById("data"));

@if(session()->has('message'))
toastr.success('{{ session()->get('message ') }}');
@endif
</script>
@endsection
