@extends('template.template')
@section('linkscss')
<link href="{{asset('template/css/cropper.min.css')}}" rel="stylesheet">
@endsection
@section('head_content')
@endsection
@section('content')
<form id="form_productos" class="validator form-horizontal" action="{{ url('productos',$producto->id) }}" method="post" enctype="multipart/form-data">
  @method('PUT')
  {{ csrf_field() }}
  <div class="row">
    <div class="col-lg-6 text-center">
      <div class="ibox float-e-margins">
        <div class="ibox-title">
          <h5>Edición del Producto <span style="color:green">{{$producto->marca}}</span></h5>
        </div>
        <div class="ibox-content">
          {{ csrf_field() }}
          <div class="form-group"><label class="col-lg-3 control-label">Producto</label>
            <div class="col-lg-6">
              <select class="form-control select2" name="producto" id="producto" value="{{ old('producto') }}">
                @foreach($productos as $p)
                <option value="{{$p->id}}" @if($producto->producto == $p->id) selected @endif>{{$p->nombre}}</option>
                @endforeach
              </select>
            </div>
          </div>

          <div class="form-group"><label class="col-lg-3 control-label">Tipo</label>
            <div class="col-lg-6">
              <select class="form-control select2" name="tipo" id="tipo">
              </select>
            </div>
          </div>

          <div class="form-group" id="varietal_div"><label class="col-lg-3 control-label">Varietal</label>
            <div class="col-lg-6">
              <select class="form-control select2" name="varietal" id="varietal" value="{{ old('varietal', $producto->varietal)}}">
              </select>
            </div>
          </div>

          <div class="form-group" id="corte_div"><label class="col-lg-3 control-label">Corte</label>
            <div class="col-lg-6">
              <input type="text" id="corte" name="corte" value="{{ old('corte', $producto->corte)}}"  placeholder="Tipo  de Corte" class="form-control">
            </div>
          </div>

          <div class="form-group"><label class="col-lg-3 control-label">Marca</label>
            <div class="col-lg-6">
              <input type="text" name="marca" value="{{ old('marca', $producto->marca)}}" placeholder="Ingrese la marca del vino" class="form-control" required>
            </div>
          </div>

          <div class="form-group"><label class="col-lg-3 control-label">Bodega</label>
            <div class="col-lg-6">
              <select class="form-control select2" name="bodega_id" value="{{ old('bodega_id') }}">
                @foreach($bodegas as $b)
                <option @if($b->id == $producto->bodega_id) selected @endif value="{{$b->id}}">{{$b->name}}</option>
                @endforeach
              </select>
            </div>
          </div>

          <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}" id="data_1">
            <label for="anio" class="col-md-3 control-label">Año</label>

            <div class="col-md-6 input-group date" >
              <span style="display:none; " class="input-group-addon"></span>
              <input name="anio" style="padding: 0px;width: 89%;margin: left; margin-left: 5%;" id="anio" type="text" class="form-control" value="{{$producto->anio}}">
            </div>
          </div>


          <div class="form-group"><label class="col-lg-3 control-label">País de Origen</label>
            <div class="col-lg-6">
              <select class="form-control select2" id="origen_pais" name="origen_pais" placeholder="Ingrese País de Origen" class="form-control" required>
                <option></option>
                @foreach($paises as $p)
                <option @if($p->id == $producto->origen_pais) selected @endif value="{{$p->id}}">{{$p->nombre}}</option>
                @endforeach
              </select>
            </div>
          </div>

          <div class="form-group"><label class="col-lg-3 control-label">Provincia</label>
            <div class="col-lg-6">
              <select class="form-control select2" name="origen_provincia" id="origen_provincia" class="form-control" required="">
              </select>
            </div>
          </div>

          <div class="form-group"><label class="col-lg-3 control-label">Region</label>
            <div class="col-lg-6">
              <select class="form-control select2" name="region" id="region" class="form-control" required="">
                <option></option>
                @foreach($regiones as $r)
                <option  @if($r->id == $producto->origen_region) selected @endif value="{{$r->id}}">{{$r->nombre}}</option>
                @endforeach
              </select>
            </div>
          </div>

          <div class="form-group"><label class="col-lg-3 control-label">Ciudad</label>
            <div class="col-lg-6">
              <select class="form-control select2" name="ciudad" id="ciudad" class="form-control" required>
              </select>
            </div>
          </div>

          <div class="form-group">
            <div class="col-lg-offset-1 col-lg-10">
              <button class="btn btn-sm btn-danger" id="guardar_form" type="button">Guardar Cambios</button>
            </div>
          </div>

        </div>
      </div>
    </div>
    <div class="col-md-6">
      <div class="ibox-content">
        <div class="row">
          <div class="col-md-12">
            <div class="image-crop">
              @if($producto->img_etiqueta != null)
              <img src="{{asset($producto->img_etiqueta)}}">
              @else
              <img src="{{asset('template/img/fondo_cropic.png')}}">
              @endif
            </div>
          </div>
          <div class="col-md-12">
            <h4>Principales métodos</h4>
            <p>
              Seleccione una imagen para su producto
            </p>
            <div class="btn-group">
              <label title="Subir Imagen" for="inputImage" class="btn btn-primary">
                <input type="file" accept="image/*" name="file" id="inputImage" class="hide">
                Subir Imagen
              </label>
              <button style="display:none" id="download"></button>
              <input type="hidden" name="imagen" id="imagen">
            </div>

            <h4>Otros métodos</h4>
            <p>
              Estas son alguna de las opciones para recortar su imagen
            </p>
            <div class="btn-group">
              <button class="btn btn-white" id="zoomIn" type="button">Zoom +</button>
              <button class="btn btn-white" id="zoomOut" type="button">Zoom -</button>
              <button class="btn btn-white" id="rotateLeft" type="button">Rotar Izquierda</button>
              <button class="btn btn-white" id="rotateRight" type="button">Rotar Derecha  </button>
            </div>
          </div>
        </div>
      </div>
    </div>
  </form>
</div>
@endsection
@section('js')
<script src='{{ asset("assets/toastr/toastr.min.js") }}'></script>
<script src="{{asset('template/js/cropper.min.js')}}" type="text/javascript" ></script>
<script src="{{asset('template/js/crop_producto.js')}}" type="text/javascript" ></script>

<script>
var producto_selected = "{{old('producto',$producto->producto)}}";
var tipo_selected = "{{old('tipo',$producto->tipo)}}";
var varietal_selected = "{{old('varietal',$producto->varietal)}}";

var pais_origen_selected = "{{old('origen_pais',$producto->origen_pais)}}";
var origen_provincia_selected = "{{old('origen_provincia',$producto->origen_provincia)}}";
var origen_region_selected = "{{old('origen_region',$producto->origen_region)}}";
var origen_ciudad_selected = "{{old('origen_ciudad',$producto->origen_ciudad)}}";


$(document).ready(function() {
  $('.select2').select2();
  crop_producto();
  $('#producto').trigger('change');
  $('#origen_pais').trigger('change');

  $('#data_1 .input-group.date').datepicker({
    format: "yyyy",
    viewMode: "years",
    minViewMode: "years",
    autoclose: true

  });
});

$("#guardar_form").click(function(){
  //base64 = $image.cropper("getDataURL");
  $('#download').trigger('click');
  $("#form_productos").submit();
});

$('#producto').change(function() {
  $('#corte_div').hide();
  ocultar_varietal();
  var valor = $('#producto').val()
  var url = "{{ url('api/producto_tipo') }}";

  $.getJSON(url, {
    page: 1,
    producto: valor
  })
  .done(function(data) {
    txt = '<option></option>';

    $.each(data.data, function(index, value) {
      if (tipo_selected == value.id)
      txt += '<option value="' + value.id + '" selected >' + value.tipo + '</option>'
      else
      txt += '<option value="' + value.id + '" >' + value.tipo + '</option>'
    });

    $("#tipo").html(txt).trigger("change.select2");
    $('#tipo').trigger('change');
  })
  .error(function(d) {
    alert('Se encontro un error intente nuevamente');
  });

});

$('#tipo').change(function() {
  $('#corte_div').hide();
  //$('#corte').val(null);
  var valor = $('#tipo').val()
  var url = "{{url('api/varietales')}}"

  $.getJSON(url, {
    page: 1,
    tipo: valor,
  })
  .done(function(data) {
    //console.log(data.data);

    txt = '<option></option>';

    $.each(data.data, function(index, value) {
      if (value.id == varietal_selected)
      txt += '<option value="' + value.id + '" selected >' + value.varietal + '</option>'
      else
      txt += '<option value="' + value.id + '" >' + value.varietal + '</option>'
    });

    $("#varietal").html(txt).trigger("change.select2");
    $('#varietal').trigger('change');

  })
  .error(function(d) {
    alert('Se encontro un error intente nuevamente');
  });

});
$('#varietal').change(function() {
  var valor = $('#varietal option:selected').text();
  valor = valor.toLowerCase().replace(/\s/g, '');
  if (valor == 'blend') {
    $('#corte_div').show();
  } else {
    $('#corte_div').hide();
    $('#corte').val(null);
  }
});


function ocultar_varietal() {
  var valor = $('#producto option:selected').text();
  valor = valor.toLowerCase().replace(/\s/g, '');
  if (valor == 'espumante') {
    $('#varietal_div').hide();
    $('#varietal').val(null);
  } else {
    $('#varietal_div').show();

  }
}


$('#origen_pais').change(function() {
  var valor = $(this).val();
  var url = "{{url('api/provincias')}}";
  $.getJSON(url, {
    pais: valor,
  })
  .done(function(data) {
    //console.log(data.data);
    txt = '<option></option>';
    $.each(data.data, function(index, value) {
      if (value.provincia_id == origen_provincia_selected)
      txt += '<option value="' + value.provincia_id + '" selected >' + value.provincia + '</option>'
      else
      txt += '<option value="' + value.provincia_id + '" >' + value.provincia + '</option>'
    });

    $("#origen_provincia").html(txt).trigger("change.select2");
    $('#origen_provincia').trigger('change');

  })
  .error(function(d) {
    alert('Se encontro un error intente nuevamente');
  });
});


$('#origen_provincia').change(function() {
  var valor = $(this).val();

  var url = "{{url('api/ciudades')}}";
  $.getJSON(url, {
    provincia: valor,
  })
  .done(function(data) {
    //console.log(data.data);
    txt = '<option></option>';
    $.each(data.data, function(index, value) {
      if (value.id == origen_ciudad_selected)
      txt += '<option value="' + value.id + '" selected >' + value.nombre + '</option>'
      else
      txt += '<option value="' + value.id + '" >' + value.nombre + '</option>'

    });

    $("#ciudad").html(txt).trigger("change.select2");
    $('#ciudad').trigger('change');

  })
  .error(function(d) {
    alert('Se encontro un error intente nuevamente');
  });

});
</script>
@endsection
