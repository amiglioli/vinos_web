@extends('template.template')
@section('linkscss')
<link type="text/css" rel="stylesheet" href="{{ asset('/assets/select2/select2.css') }}">
<link type="text/css" rel="stylesheet" href="{{ asset('/assets/toastr/toastr.min.css') }}">
<link type="text/css" rel="stylesheet" href="{{ asset('/assets/bootstrap-datepicker/css/datepicker.css') }}">
<link type="text/css" rel="stylesheet" href="{{ asset('/assets/sweetalert/sweet-alert.css') }}">
<link href="{{asset('template/css/estilo_tabla.css')}}" rel="stylesheet">
@endsection
@section('content')
   <a href="{{url('productos')}}" type="button" class="btn btn-primary btn-xs">VOLVER</A>
   <br>
   <br>
<section class="bf-table" id="data" style="display: none; padding-bottom: 20px ">
   <div class="card card-cascade narrower">
      <!--Card image-->
   

      <div class="view view-cascade gradient-card-header blue-gradient narrower py-2 mx-4 mb-3 d-flex justify-content-between align-items-center">
         <div>
            <!--<button type="button" class="btn btn-outline-white btn-rounded btn-sm px-2 waves-effect waves-light"><i class="fa fa-th-large mt-0"></i></button>
               <button type="button" class="btn btn-outline-white btn-rounded btn-sm px-2 waves-effect waves-light"><i class="fa fa-columns mt-0"></i></button> -->
         </div>
         <span class="white-text">Historial del producto</span>
         <div>
            <!-- boton -->
         </div>
      </div>
      <!--/Card image-->
      <div class="px-4">

         <div class="table-wrapper table-responsive">
            <!--Table-->
            <div class="btn-group pull-left" data-bind="foreach: filters">
          <span data-bind="click: function() { vm.remove_filters(name) }" title="Click para quitar este filtro" class="label label_table"><i class="fa fa-times-circle" aria-hidden="true"></i>&nbsp;&nbsp;<a style="color: white;" data-bind="text: value"></a></span>
        </div>
            <table class="table table-bordered tabla_custom table-hover mb-0">
					<thead>
						<tr>
							<th class="col_1" style="padding: 0px;">
								<input id="cata" type="text" placeholder="CATA" style="text-align: center;" class="form-control" data-bind="event: { change: function(){vm.filterCata($element)} }">
							</th>
							<th class="col_1" style="padding: 0px;">
								<input id="fecha" type="text" placeholder="FECHA" style="text-align: center;" class="form-control" data-bind="event: { change: function(){vm.filterFecha($element)} }">
							</th>
							<th class="col_1" style="padding: 0px;">
								<input id="ṕuesto" type="text" placeholder="PUESTO" style="text-align: center;" class="form-control" data-bind="event: { change: function(){vm.filterPuesto($element)} }">
							</th>
							<th class="col_1" style="padding: 0px;">
								<input id="puntaje" type="text" placeholder="PUNTAJE" style="text-align: center;" class="form-control" data-bind="event: { change: function(){vm.filterPuntaje($element)} }">
							</th>
							<th class="text-left"></th>
						</tr>
					</thead>
					<tbody data-bind="foreach: data().data">
						<tr>
							<td class="col_1 text-left" 	data-bind="text: ranking.cata.descripcion"></td>
							<td class="col_1 text-left" 	data-bind="text: fecha_format(ranking.cata.fecha)"></td>
							<td class="col_1 text-left" 	data-bind="text: puesto"></td>
							<td class="col_1 text-left" 	data-bind="text: decimal(puntaje)"></td>

						</tr>
					</tbody>
				</table>
			</div>

					 <div class="d-flex justify-content-between">
							<div style="padding-top: 40px">
									<p>Mostrando del <span data-bind="text: data().from == null ? '0' : data().from"></span> al <span data-bind="text: data().to == null ? '0' : data().to"></span> de <span data-bind="text: data().total == null ? '0' : data().total"></span> registros</p>
							</div>
							<nav class="my-4">
								 <ul class="pagination pagination-circle pg-blue mb-0">
										<li class="page-item" data-bind="css: data().prev_page_url == null ? 'disabled' : ''"><a class="page-link waves-effect waves-effect" data-bind="click: function() { ir(1) }">« Primera</a></li>
										<li class="page-item" data-bind="if: data().current_page-100 > 0, click: function() { ir(data().current_page-100) } "><span class="page-link waves-effect waves-effect" data-bind="text: data().current_page-100"></span></li>
										<li class="page-item" data-bind="if: data().current_page-50 > 0, click: function() { ir(data().current_page-50) } "><span class="page-link waves-effect waves-effect" data-bind="text: data().current_page-50"></span></li>
										<li class="page-item" data-bind="if: data().current_page-10 > 0, click: function() { ir(data().current_page-10) } "><span class="page-link waves-effect waves-effect" data-bind="text: data().current_page-10"></span></li>
										<li class="page-item" data-bind="if: data().current_page-5 > 0, click: function() { ir(data().current_page-5) } "><span class="page-link waves-effect waves-effect" data-bind="text: data().current_page-5"></span></li>
										<li class="page-item" data-bind="if: data().current_page-2 > 0, click: function() { ir(data().current_page-2) } "><span class="page-link waves-effect waves-effect" data-bind="text: data().current_page-2"></span></li>
										<li class="page-item" data-bind="if: data().current_page-1 > 0, click: function() { ir(data().current_page-1) } "><span class="page-link waves-effect waves-effect" data-bind="text: data().current_page-1"></span></li>
										<li class="page-item active" ><span class="page-link waves-effect waves-effect" data-bind="text: data().current_page"></span></li>
										<li class="page-item" data-bind="if: data().current_page+1 <= data().last_page, click: function() { ir(data().current_page+1) } "><span class="page-link waves-effect waves-effect" data-bind="text: data().current_page+1"></span></li>
										<li class="page-item" data-bind="if: data().current_page+2 <= data().last_page, click: function() { ir(data().current_page+2) } "><span class="page-link waves-effect waves-effect" data-bind="text: data().current_page+2"></span></li>
										<li class="page-item" data-bind="if: data().current_page+5 <= data().last_page, click: function() { ir(data().current_page+5) } "><span class="page-link waves-effect waves-effect" data-bind="text: data().current_page+5"></span></li>
										<li class="page-item" data-bind="if: data().current_page+10 <= data().last_page, click: function() { ir(data().current_page+10) } "><span class="page-link waves-effect waves-effect" data-bind="text: data().current_page+10"></span></li>
										<li class="page-item" data-bind="if: data().current_page+50 <= data().last_page, click: function() { ir(data().current_page+50) } "><span class="page-link waves-effect waves-effect" data-bind="text: data().current_page+50"></span></li>
										<li class="page-item" data-bind="if: data().current_page+100 <= data().last_page, click: function() { ir(data().current_page+100) } "><span class="page-link waves-effect waves-effect" data-bind="text: data().current_page+100"></span></li>
										<li class="page-item" data-bind="css: data().last_page == null || data().current_page >= data().last_page ? 'disabled' : ''"><a class="page-link waves-effect waves-effect" data-bind="click: function() { ir(data().last_page) }">Última »</a></li>
								 </ul>

							</nav>

					 </div>

				</div>
		 </div>
	</section>
@endsection

@section('js')
<script src='{{ asset("assets/toastr/toastr.min.js") }}'></script>
<script src="{{ asset('assets/select2/select2.full.js') }}"></script>
<script src="{{ asset('assets/moment/moment-with-locales.min.js')}}"></script>
<script src="{{ asset('assets/bootstrap-datepicker/js/bootstrap-datepicker.js') }}"></script>
<script src="{{ asset('assets/bootstrap-datepicker/js/locales/bootstrap-datepicker.es.js') }}"></script>
<script>
moment.locale('es');
var url = "{{ url('api/ranking') }}";
var url_normal = "{{ url('ranking') }}";
var vm = function() {
	self = this;
	self.data = ko.observableArray([]);
	self.cata = '';
	self.fecha = '';
	self.puesto = '';
	self.puntaje = '';
	self.page = '1';
	self.filters = ko.observableArray([]);
	self.init = function() {
		self.ajax();
		$('.select2').select2();
	}
	self.filterCata = function(element) {
		self.page = 1;
		self.cata = $(element).val()
		self.update_filters();
		self.ajax();
	};
	self.filterFecha = function(element) {
		self.page = 1;
		self.fecha = $(element).val()
		self.update_filters();
		self.ajax();
	};
	self.filterPuesto = function(element) {
		self.page = 1;
		self.puesto = $(element).val()
		self.update_filters();
		self.ajax();
	};
	self.filterPuntaje = function(element) {
		self.page = 1;
		self.puntaje = $(element).val()
		self.update_filters();
		self.ajax();
	};

	self.update_filters = function() {
		self.filters([]);
		if (self.cata != '') self.filters.push({
			key: 'cata',
			value: self.cata,
			name: 'cata'
		});
		if (self.fecha != '') self.filters.push({
			key: 'fecha',
			value: self.fecha,
			name: 'fecha'
		});
		if (self.puesto != '') self.filters.push({
			key: 'puesto',
			value: self.puesto,
			name: 'puesto'
		});
		if (self.puntaje != '') self.filters.push({
			key: 'puntaje',
			value: self.puntaje,
			name: 'puntaje'
		});
	};
	self.remove_filters = function(name) {
		if (name == 'cata') {
			self.cata = '';
		}
		if (name == 'fecha') {
			self.fecha = '';
		}
		if (name == 'puesto') {
			self.puesto = '';
		}
		if (name == 'puntaje') {
			self.puntaje = '';
		}

		$('#' + name).val("").trigger("change");
		self.update_filters();
		self.ajax();
	};
	self.ir = function(page) {
		self.page = page;
		self.ajax();
		$('html, body').animate({
			scrollTop: 0
		}, 'fast');
	};
	self.eliminar = function(id) {
		$.ajax({
			url: url_normal+'/'+id,
			type: 'DELETE',
			data    :   {
				'_token': "{{ csrf_token() }}"
			},
			success: function(result) {
				toastr.success(result);
				self.ajax();
			}
		});
	};
	self.ajax = function() {
		$.getJSON(url, {
			page: self.page,
			cata: self.cata,
			fecha:self.fecha,
			puesto:self.puesto,
			puntaje:self.puntaje,
			id_producto: "{{$producto_id}}"
		})
		.done(function(data) {
			console.log(data);
			Pace.restart()
			vm.data(data);
			$("#data").fadeIn();
			$(".loading").remove();
		})
		.error(function(d) {
			toastr.error('Se encontro un error intente nuevamente');
		});
	};
};
vm = new vm();
vm.init();
ko.applyBindings(vm, document.getElementById("data"));

@if(session()->has('message'))
toastr.success('{{ session()->get('message ') }}');
@endif
</script>
@endsection
