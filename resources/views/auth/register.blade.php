@extends('layouts.app')

@section('content')
<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Formulario de Registro') }}</div>

                <div class="card-body">
                    <form method="POST" action="{{ route('register') }}">
                        @csrf
                     <input  type="hidden" name="tipo" value="{{$tipo}}" required>

                    <div class="form-group">
                        <input id="entidad" type="text" class="form-control transparent" name="entidad" value="{{ old('entidad') }}" required autofocus placeholder="Nombre {{ucfirst($tipo)}}">
                    </div>
                    @if ($errors->has('entidad'))
                    <span class="help-block blank">
                        <strong>{{ $errors->first('entidad') }}</strong>
                    </span>
                    @endif
                    <div class="form-group">
                        <input id="persona_contacto" type="text" class="form-control transparent" name="persona_contacto" value="{{ old('persona_contacto') }}" required  placeholder="Persona de Contacto">
                    </div>
                    @if ($errors->has('persona_contacto'))
                    <span class="help-block" >
                        <strong>{{ $errors->first('persona_contacto') }}</strong>
                    </span>
                    @endif
                    <div class="form-group">
                        <input id="email" type="email" class="form-control transparent" name="email" value="{{ old('email') }}" required placeholder="E-mail de Contacto ">
                    </div>
                    @if ($errors->has('email'))
                    <span class="help-block" >
                        <strong>{{ $errors->first('email') }}</strong>
                    </span>
                    @endif
                    <div class="form-group">
                        <!--    <input id="provincia" type="text" class="form-control transparent" name="provincia" value="{{ old('provincia') }}" required placeholder="Provincia"> -->
              <select class="form-control transparent" name="provincia"  id="select_id"  tabindex="1" required placeholder="Seleccione su Provincia ..." >
                @foreach($provincias as $p)
                       <option value="{{$p->provincia_id}}">{{$p->provincia}}</option>
                @endforeach
              </select>
                    </div>

                       <!-- <div class="form-group row">
                            <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('Name') }}</label>

                            <div class="col-md-6">
                                <input id="name" type="text" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" value="{{ old('name') }}" required autofocus>

                                @if ($errors->has('name'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required>

                                @if ($errors->has('email'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password" class="col-md-4 col-form-label text-md-right">{{ __('Password') }}</label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required>

                                @if ($errors->has('password'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                    

                        <div class="form-group row">
                            <label for="password-confirm" class="col-md-4 col-form-label text-md-right">{{ __('Confirm Password') }}</label>

                            <div class="col-md-6">
                                <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required>
                            </div>
                        </div>
                            -->
                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-danger">
                                    {{ __('Registrarme') }}
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

<script src="{{asset('template/js/jquery-2.1.1.js')}}" type="text/javascript" ></script>
<script src="{{asset('template/js/bootstrap.min.js')}}" type="text/javascript" ></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
<script src="{{asset('template/js/plugins/chosen/chosen.jquery.js')}}" type="text/javascript" ></script>
<script>

</script>
