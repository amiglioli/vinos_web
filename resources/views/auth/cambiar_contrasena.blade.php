<style>
  .transparent {
  background:rgba(0,0,0,0.5) !important;
  }
  .transparent_violet {
    border-color: rgba(45, 0, 79,0.1) !important;
    background:rgba(68, 0, 117,0.6) !important;
  }


  .blank {
   color:#fff !important;
  }
  .img-center {
    background-repeat: no-repeat;
    background-attachment: fixed;
    background-position: center;
  }
</style>

<link href="{{asset('template/css/bootstrap.min.css')}}" rel="stylesheet">
<link href="{{asset('template/font-awesome/css/font-awesome.css')}}" rel="stylesheet">
<link href="{{asset('template/css/animate.css')}}" rel="stylesheet">
<link href="{{asset('template/css/style.css')}}" rel="stylesheet">
<body class="gray-bg img-center" style="background-image: url('http://cdn.shopify.com/s/files/1/1666/2579/t/2/assets/slide1_image.jpg?999213287192304216');">
    <div class="middle-box text-center loginscreen animated fadeInDown">
        <div>
            <div>
                <h1 class="logo-name">W+</h1>
            </div>
            <p style="color:#fff">
                Genere una contraseña que desea para acceder al sitio
            </p>
            <form class="m-t" method="POST" action="{{ URL::to('cambiarContrasena') }}">
              {{ csrf_field() }}
              @if(Session::has('error_verify'))
               <div class="alert alert-danger">
                  {{Session::get('error_verify')}}
                </div>
              @endif
              <input type="hidden" name="token_verify" value="{{$token}}">

              <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                  <input id="password" type="password" class="form-control" name="password" style="background:rgba(0,0,0,0.5);" placeholder="Ingrese una contraseña" required>
              </div>
              <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                  <input id="password2" type="password" class="form-control" name="password2" placeholder="Repita su contraseña" style="background:rgba(0,0,0,0.5);" required>
              </div>
              <button type="submit" class="btn btn-primary block full-width m-b transparent_violet">Aceptar</button>
            </form>
        </div>
    </div>

  <script src="{{asset('template/js/jquery-2.1.1.js')}}" type="text/javascript" ></script>
  <script src="{{asset('template/js/bootstrap.min.js')}}" type="text/javascript" ></script>
</body>


