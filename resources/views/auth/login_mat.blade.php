<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">
  
  <meta name="description" content="">
  <meta name="author" content="">
  	<link rel="shortcut icon" href="{{ asset('images/favicon.ico') }}">

	<title>{{ env('APP_NAME') }}</title>
	<link href="{{ asset('/css/app.css') }}" rel="stylesheet">
	<link href="{{ asset('/css/style.css') }}" rel="stylesheet">
	<link href="{{ asset('/assets/jquery-toggles/toggles-full.css') }}" rel="stylesheet">
	<link href="{{ asset('/assets/icheck/skins/all.css') }}" rel="stylesheet">
	<link href='//fonts.googleapis.com/css?family=Roboto:400,300' rel='stylesheet' type='text/css'>
	<style>
	.btn-info {
    	color: #ffffff;
    	background-color: #6eafe5;
    	border-color: transparent;
	}
	@media only screen and (max-width: 500px) {
		.signin{
			width:90%;
		}
    }
	
	.form-control {
    	padding: 17px 12px;
	}
	</style>
  <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!--[if lt IE 9]>
  <script src="../lib/html5shiv/html5shiv.js"></script>
  <script src="../lib/respond/respond.src.js"></script>
  <![endif]-->
</head>

<body class="signwrapper" style="background-image: url({{ asset('images/login.jpg') }});background-size: cover;">
	<div class="sign-overlay"></div>
	<div class="signpanel">
		@if (env('APP_ENV') != 'production')
		<div style="width: 100%; background-color: #DD0000; color: #FFF; font-family: Verdana; font-size: 13px; text-align: center; padding: 4px;">Ambiente de Desarrollo [Si ve este mensaje en el ambiente de produccion, ejecute el bash setup_produccion.sh en el raiz del proyecto]</div>
		@endif
		
	</div>
	<div class="panel signin" style="border-radius: 10px; padding: 0px!important; background-color: transparent; ">
		<div style="background-color: rgba(255, 255, 255, 0.8); border-top-left-radius: 20px; border-top-right-radius: 20px; padding: 5px;">
			<div class="panel-heading">
			      <h1 style="color:white"><img src="{{ asset('images/logo_gba.svg') }}" alt="" class="img-responsive" style="width:50%;margin:auto"></h1>
			</div>
		</div>
		<div style="background-color: rgba(180, 182, 185, 0.5); padding: 0px;">
			<div class="panel-heading text-center">
			      <h2 style="color:white">PARTIDAS</h2>
			</div>
		</div>
		<div style="background-color: rgba(180, 182, 185, 0.5); border-bottom-left-radius: 20px; border-bottom-right-radius: 20px; ">
		    <div class="panel-body" style="padding: 30px; padding-top: 0px">
				@if (count($errors) > 0)
					<div class="alert alert-danger">
						Hubo un problema con las credenciales intenta de nuevo por favor.<br><br>
						<ul>
							@foreach ($errors->all() as $error)
								<li>{{ $error }}</li>
							@endforeach
						</ul>
					</div>
				@endif
		      	<form class="form-horizontal" role="form" method="POST" action="{{ url('/login') }}">
					<input type="hidden" name="_token" value="{{ csrf_token() }}">
			        <div class="form-group mb10">
						<div class="input-group">
			            	<span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
			            	<input type="text" class="form-control" name="email" value="{{ old('email') }}" placeholder="Usuario">
			          	</div>
			        </div>
			        <div class="form-group nomargin">
						<div class="input-group">
			            	<span class="input-group-addon"><i class="glyphicon glyphicon-lock"></i></span>
			            	<input type="password" class="form-control" name="password" placeholder="Contraseña">
			          	</div>
			        </div>
				    <input type="hidden" name="ad" id="ad" value="on"/>
				    <br />
					<div class="form-group" style="margin:0 auto">
			        	<div class="g-recaptcha" data-sitekey="6LfvNSwUAAAAAHQp8QISxBS4d07oaCJAFXglTCsp"></div>
					</div>
					<br />
			        <div class="form-group">
						<button class="btn btn-info btn-quirk btn-block" style="background-color: #9bca47">Iniciar Sesión</button>
			        </div>
		      </form>		      
		    </div>
	    </div>
	</div>
	<div class="slide-tools" style="height: auto;position: fixed; width: 100%; z-index: 14; bottom: 0;background-color: #9bca47; color: #ffffff; font-size: 14px; font-family: 'Roboto Condensed', sans-serif">
      	<div class="col-xs-12 text-center" style="margin-top:10px">
			<p>Buenos Aires Provincia</p>
		</div>
	</div>
	<script src="{{asset('/assets/jquery/jquery-2.2.3.min.js')}}"></script>
	<script src="{{asset('/assets/bootstrap/bootstrap.min.js')}}"></script>
	<!-- <script src='https://www.google.com/recaptcha/api.js'></script> -->
</body>
</html>
