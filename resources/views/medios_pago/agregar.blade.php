@extends('template.template')
@section('linkscss')
<style>
@keyframes hover-color {
from {
border-color: #c0c0c0;
}
to {
border-color: #d02026;
}
}
.magic-radio, .magic-checkbox {
position: absolute;
display: none;
}
.magic-radio[disabled], .magic-checkbox[disabled] {
cursor: not-allowed;
}
.magic-radio + label, .magic-checkbox + label {
position: relative;
display: block;
padding-left: 30px;
cursor: pointer;
vertical-align: middle;
}
.magic-radio + label:hover:before, .magic-checkbox + label:hover:before {
animation-duration: 0.4s;
animation-fill-mode: both;
animation-name: hover-color;
}
.magic-radio + label:before, .magic-checkbox + label:before {
position: absolute;
top: 0;
left: 0;
display: inline-block;
width: 20px;
height: 20px;
content: '';
border: 1px solid #c0c0c0;
}
.magic-radio + label:after, .magic-checkbox + label:after {
position: absolute;
display: none;
content: '';
}
.magic-radio[disabled] + label, .magic-checkbox[disabled] + label {
cursor: not-allowed;
color: #e4e4e4;
}
.magic-radio[disabled] + label:hover, .magic-radio[disabled] + label:before, .magic-radio[disabled] + label:after, .magic-checkbox[disabled] + label:hover, .magic-checkbox[disabled] + label:before, .magic-checkbox[disabled] + label:after {
cursor: not-allowed;
}
.magic-radio[disabled] + label:hover:before, .magic-checkbox[disabled] + label:hover:before {
border: 1px solid #e4e4e4;
animation-name: none;
}
.magic-radio[disabled] + label:before, .magic-checkbox[disabled] + label:before {
border-color: #e4e4e4;
}
.magic-radio:checked + label:before, .magic-checkbox:checked + label:before {
animation-name: none;
}
.magic-radio:checked + label:after, .magic-checkbox:checked + label:after {
display: block;
}
.magic-radio + label:before {
border-radius: 50%;
}
.magic-radio + label:after {
top: 6px;
left: 6px;
width: 8px;
height: 8px;
border-radius: 50%;
background: #d02026;
}
.magic-radio:checked + label:before {
border: 1px solid #d02026;
}
.magic-radio:checked[disabled] + label:before {
border: 1px solid #c9e2f9;
}
.magic-radio:checked[disabled] + label:after {
background: #c9e2f9;
}
.magic-checkbox + label:before {
border-radius: 3px;
}
.magic-checkbox + label:after {
top: 2px;
left: 7px;
box-sizing: border-box;
width: 6px;
height: 12px;
transform: rotate(45deg);
border-width: 2px;
border-style: solid;
border-color: #fff;
border-top: 0;
border-left: 0;
}
.magic-checkbox:checked + label:before {
border: #d02026;
background: #d02026;
}
.magic-checkbox:checked[disabled] + label:before {
border: #c9e2f9;
background: #c9e2f9;
}
</style>
@endsection
@section('head_content')
@endsection
@section('content')
  <div class="row wrapper red-bg">
  <div class="col-lg-10">				
<h2>Seleccione los medios de pago disponibles en su Vinoteca</h2>	  </div>
  <div class="col-lg-2">
  </div>
</div><div class="row">
  <div class="col-lg-12 text-center">
    <div class="ibox float-e-margins">
      
      
      <div class="ibox-content">
        <div class="row">
          <div class="ibox-title" style="border-style:none;">
            <h5>Efectivo</h5>
          </div>
          @foreach($efectivo as $e)
          <div class="col-md-3">
            <div class="media-body ">
              <input  @if($e->lo_tengo != null) checked @endif class="magic-checkbox pull-left" type="checkbox" value="{{$e->id}}" name="{{$e->nombre}}" id="{{$e->nombre}}"/>
              <label class="pull-left" for="{{$e->nombre}}">
              <img src="{{asset('tarjetas/'.$e->imagen)}}" style="height: 25px;width: 45px">
              {{$e->nombre}}
              </label>

            </div>
          </div>
          @endforeach
        </div>
      </div>
      <div class="ibox-content">
        
        <div class="row">
          <div class="ibox-title" style="border-style:none;">
            <h5>Crédito</h5>
          </div>
          @foreach($credito as $c)
          <div class="col-md-3" style="padding-bottom: 5px;">
            <div class="media-body ">
              <input  @if($c->lo_tengo != null)  checked @endif class="magic-checkbox pull-left" type="checkbox" value="{{$c->id}}" name="{{$c->nombre}}" id="{{$c->nombre}}"/>
              <label class="pull-left" for="{{$c->nombre}}">
                <img src="{{asset('tarjetas/'.$c->imagen)}}" style="height: 25px;width: 45px">
                {{$c->nombre}}
              </label>
            </div>
          </div>
          @endforeach
        </div>
      </div>
      <div class="ibox-content">
        <div class="row">
          <div class="ibox-title" style="border-style:none;">
            <h5>Débito</h5>
          </div>
          @foreach($debito as $d)
          <div class="col-md-3">
            <div class="media-body ">
              <input  @if($d->lo_tengo != null)  checked @endif class="magic-checkbox pull-left" type="checkbox" value="{{$d->id}}" name="{{$d->nombre}}" id="{{$d->nombre}}"/>
              <label class="pull-left" for="{{$d->nombre}}">
                <img src="{{asset('tarjetas/'.$d->imagen)}}" style="height: 25px;width: 45px">
                {{$d->nombre}}
              </label>
            </div>
          </div>
          @endforeach
        </div>
      </div>
    </div>
  </div>
</div>
@endsection
@section('js')
<script>
var url = "{{url('mediospago')}}";

$(".magic-checkbox").click(function(){
    var valor = $(this).val();
    var estado = false;
    if ($(this).is(':checked'))
      var estado = 1;
    else
      var estado = 0;

    cambiar_estado(valor,estado);
});


  function cambiar_estado(valor,estado) {
    $.ajax({
      url: url,
      type: 'POST',
      data    :   {
        '_token': "{{ csrf_token() }}",
        tarjeta:valor,
        estado:estado,
      },
      success: function(result) {
        if(result.estado)
          toastr.success(result.mensaje);
        else
          toastr.error(result.mensaje);
      }
    });
  };
</script>
@endsection