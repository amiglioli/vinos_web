@extends('template.template')
@section('linkscss')

<link href="{{asset('template/css/estilo_tabla.css')}}" rel="stylesheet">
@endsection

@section('content')

<section class="bf-table" id="data" style="padding-bottom: 20px ">
   <div class="card card-cascade narrower">
      <!--Card image-->
      <div class="view view-cascade gradient-card-header blue-gradient narrower py-2 mx-4 mb-3 d-flex justify-content-between align-items-center">
        
         <span class="white-text">Mis Medios de Pago</span>
          <div style="text-align:right">
            <a type="button" class="btn btn-outline btn-danger" href="{{ url('mediospago/create') }}">Editar</a>
         </div>
      </div>
      <!--/Card image-->
      <div class="px-4">
        
         <div class="table-wrapper table-responsive">
            <table class="table table-bordered tabla_custom table-hover mb-0">
        <thead>
          <tr>
            <th class="col_1" style="padding:10px;">Medios de Pago habilitados</th>
            
          </tr>
        </thead>
        <tbody>
          @foreach($efectivo as $e)
            <tr>
              @if($e->lo_tengo != null)
                <td><img src="{{asset('tarjetas/'.$e->imagen)}}" style="height: 25px;"> {{$e->nombre}}</td>
              @endif
            </tr>
          @endforeach
          @foreach($credito as $c)
            <tr>
              @if($c->lo_tengo != null)
                <td><img src="{{asset('tarjetas/'.$c->imagen)}}" style="height: 25px;"> {{$c->nombre}}</td>
              @endif
            </tr>
          @endforeach

          @foreach($debito as $d)
            <tr>
              @if($d->lo_tengo != null)
                <td><img src="{{asset('tarjetas/'.$d->imagen)}}" style="height: 25px;"> {{$d->nombre}}</td>
              @endif
            </tr>
          @endforeach


        </tbody>
      </table>
        </div>  
        

      </div>
   </div>
</section>


@endsection
