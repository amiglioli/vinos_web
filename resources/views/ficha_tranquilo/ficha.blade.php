@extends('template.template')
<style>
.letra{
font-size:14px;
}
#colorth{
background-color:#e5e5e5;
font-weight: bold;
}
/* The container */
.radiop {
display: block;
position: relative;
padding-left: 35px;
margin-bottom: 12px;
cursor: pointer;
font-size: 22px;
-webkit-user-select: none;
-moz-user-select: none;
-ms-user-select: none;
user-select: none;
}
/* Hide the browser's default radio button */
.radiop input {
position: absolute;
opacity: 0;
cursor: pointer;
}
/* Create a custom radio button */
.checkmark {
position: absolute;
top: 0;
left: 0;
height: 25px;
width: 25px;
background-color: #aaa;
border-radius: 50%;
}
/* On mouse-over, add a grey background color */
.radiop:hover input ~ .checkmark {
background-color: #ccc;
}
/* When the radio button is checked, add a blue background */
.radiop input:checked ~ .checkmark {
background-color: #d02026;
}
/* Create the indicator (the dot/circle - hidden when not checked) */
.checkmark:after {
content: "";
position: absolute;
display: none;
}
/* Show the indicator (dot/circle) when checked */
.radiop input:checked ~ .checkmark:after {
display: block;
}
/* Style the indicator (dot/circle) */
.radiop .checkmark:after {
top: 9px;
left: 9px;
width: 8px;
height: 8px;
border-radius: 50%;
background: white;
}
.detalles{
font-size:17px;
}
</style>
@section('content')
<div class="ibox float-e-margins">
  <div class="ibox-title text-center">
    <center><p class="detalles">Cata correspondiente al <b>{{fechaCastellano($cata->fecha)}}</b> realizada en <i>{{$cata->lugar}}</i>.</p></center>
  </div>
  <form class="validator form-horizontal" action="{{url('ficha_tranquilo')}}" method="post">
    <input type="hidden" name="cata" value="{{$cata->id}}">
    <input type="hidden" id="id_prod" name="id_prod" value="">
    <div class="ibox-content">
      <div class="row">
        <div class="col-md-12">
          <center><span  id="descripcion">{{$cata->descripcion}}</span></center>
        </div>
      </div>
      <br>
      <div class="row">
        <div class="col-md-4">
          <div class="form-group{{ $errors->has('catadores') ? ' has-error' : '' }}">
            <label for="catadores" class="col-md-3 control-label">Jurado</label>
            <div class="col-md-8">
              <select data-placeholder="Seleccione el catador" class="form-control select2" name="catadores" value=""  id="catadores"  tabindex="1">
                <option></option>
                @foreach($cata->catadores as $c)
                <option value="{{$c->catador->id}}">{{$c->catador->nombre}} {{$c->catador->apellido}}</option>
                @endforeach
              </select>
            </div>
          </div>
        </div>
        <div class="col-md-4">
          <p class="detalles">Especialización :<span id="especializacion"></span></p>
          <input  id="id_especializacion" name="id_especializacion" value="" type="hidden">
        </div>
        <div class="col-md-4">
          <p class="detalles">Puntaje : <span id="resultado"></span></p>
          <input  id="resultado_final" name="puntaje" value="" type="hidden">
        </div>
        <input type="hidden" id="edad" name="edad" value=""/>
      </div>
      <span id="catas_pendientes"></span>
      <br>
      {{ csrf_field() }}
      <table class="table table-bordered">
        <thead>
          <tr>
            <th style="width:15%;">
              <select class="form-control"  required id="muestra_nro" name="muestra_nro">
              </select>
            </th>
            @foreach($puntos as $p)
            <th>{{$p->nombre}}</th>
            @endforeach
          </tr>
        </thead>
        <tbody>
          @foreach($campos as $c)
          <tr>
            <td>{{strtoupper($c)}}</td>
            @foreach($puntos as $p)
            <td>
              <label class="radiop">
                <input required class="calc radiop" type="radio" data-valor="{{$p->puntuacion}}" name="{{$c}}" id="{{$c}}" value="{{$p->id}}" />
                <span class="checkmark"></span>
              </label>
            </td>
            @endforeach
          </tr>
          @endforeach
        </tbody>
        <!-- <p>Total: PHP <span id="resultado">0</span> -->
      </table>
      <button id="enviar_ficha" class="btn btn-md btn-primary" style="margin-left:42%" type="submit">Completar Registro</button>
    </div>
  </form>
</div>
@endsection
@section('js')
<script>
  var id_cata = "{{$cata->id}}";
  $(document).ready(function() {
      $('.select2').select2();
      $('.i-checks').iCheck({
          checkboxClass: 'icheckbox_square-green',
          radioClass: 'iradio_square-green',
      });

      $(".calc").change(function() {
          calcscore()
      });


  });

  $('#catadores').change(function() {
      var id_catador = $(this).val();

      $.ajax({
          url: "{{url('')}}"+'/ficha_tranquilo/muestras_pendientes/'+id_catador+'/'+id_cata,
          type: 'GET',
          beforeSend: function() {
              //$("#resultado").html("Procesando, espere por favor...");
          },
          success: function(response) {

              $('#especializacion').html(response.catadores.especializacion.especializacion);
              $('#id_especializacion').val(response.catadores.especializacion.id);
              $('#edad').val(calculateAge(response.catadores.edad));

              var muestras_pendientes = response.muestras_pendientes;
              //console.log(muestras_pendientes);
              if (muestras_pendientes.length == 0) {
                  $("#catas_pendientes").html('<span style="color:red">Este catador no tiene muestras pendientes</span>');
                  $("#enviar_ficha").prop("disabled", true)
                  $("#muestra_nro").html("<option>Sin muestras pendientes</option>");
              } else {
                  var txt = 'Muestras pendientes: ';
                  var select = '<option></option>';
                  $.each(muestras_pendientes, function(index, value) {
                      txt += "<button type='button' class='btn btn-danger btn-circle'>" + value.muestra_nro + "</button>";
                      select += "<option  data-idprod='"+value.id_producto+"' value='" + value.muestra_nro + "'>" + value.muestra_nro + "</option>";

                  });

                  $('#catas_pendientes').html(txt);
                  $("#muestra_nro").html(select);
                  $("#enviar_ficha").prop("disabled", false);
              }


          }
      });
  });

  $("#muestra_nro").change(function(){
    var id_producto = $(this).find(':selected').data('idprod')
    $("#id_prod").val(id_producto);
  });
  function calcscore() {
      console.log("entro")
      score = 0;
      $(".calc:checked").each(function() {
          score += Number($(this).data('valor'));
      });
      //$("#price").text(score.toFixed(2));  
      var resultado = score / 4;
      $("#resultado").text(resultado);
      $("#resultado_final").val(resultado);
  }
  $().ready(function() {
      $(".calc").change(function() {
          calcscore()
      });
  });
</script>
@endsection