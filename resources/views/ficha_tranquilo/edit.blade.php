@extends('template.template')
<style>
.letra{
font-size:14px;
}
#colorth{
background-color:#e5e5e5;
font-weight: bold;
}
/* The container */
.radiop {
display: block;
position: relative;
padding-left: 35px;
margin-bottom: 12px;
cursor: pointer;
font-size: 22px;
-webkit-user-select: none;
-moz-user-select: none;
-ms-user-select: none;
user-select: none;
}
/* Hide the browser's default radio button */
.radiop input {
position: absolute;
opacity: 0;
cursor: pointer;
}
/* Create a custom radio button */
.checkmark {
position: absolute;
top: 0;
left: 0;
height: 25px;
width: 25px;
background-color: #aaa;
border-radius: 50%;
}
/* On mouse-over, add a grey background color */
.radiop:hover input ~ .checkmark {
background-color: #ccc;
}
/* When the radio button is checked, add a blue background */
.radiop input:checked ~ .checkmark {
background-color: #1ab394;
}
/* Create the indicator (the dot/circle - hidden when not checked) */
.checkmark:after {
content: "";
position: absolute;
display: none;
}
/* Show the indicator (dot/circle) when checked */
.radiop input:checked ~ .checkmark:after {
display: block;
}
/* Style the indicator (dot/circle) */
.radiop .checkmark:after {
top: 9px;
left: 9px;
width: 8px;
height: 8px;
border-radius: 50%;
background: white;
}
.detalles{
font-size:17px;
}
</style>
@section('content')
<div class="ibox float-e-margins">
  <div class="ibox-title text-center">
    <center><p class="detalles">Cata correspondiente al <b>{{fechaCastellano($data->cata->fecha)}}</b> realizada en <i>{{$data->cata->lugar}}</i>.</p></center>
  </div>
  <form class="validator form-horizontal" action="{{ url('ficha_tranquilo',$data->id) }}" method="post">
    @method('PUT')
    {{ csrf_field() }}
    <input type="hidden" id="id_prod" name="id_prod" value="">
    <div class="ibox-content">
      <div class="row">
        <div class="col-md-12">
          <center><span  id="descripcion">{{$data->cata->descripcion}}</span></center>
        </div>
      </div>
      <br>
      <div class="row">
        <div class="col-md-4">
          <div class="form-group{{ $errors->has('catadores') ? ' has-error' : '' }}">
            <input type="hidden" name="catadores" value="{{$data->catador->id}}">
            <p class="detalles">Jurado :<span id="especializacion"> {{$data->catador->nombre}} {{$data->catador->apellido}} </span>
          </div>
        </div>
        <div class="col-md-4">
          <p class="detalles">Especialización :<span id="especializacion">{{$data->especializacion->especializacion}}</span>
          <input  id="id_especializacion" name="id_especializacion" value="" type="hidden">
        </div>
        <div class="col-md-4">
          <p class="detalles">Puntaje : <span id="resultado"></span></p>
          <input  id="resultado_final" name="puntaje" value="" type="hidden">
        </div>
        <input type="hidden" id="edad" name="edad" value=""/>
      </div>
      <span id="catas_pendientes"></span>
      <br>
      {{ csrf_field() }}
      <table class="table table-bordered">
        <thead>
          <tr>
          <th style="width:15%;"><input style="height:32px;" class="form-control" placeholder="Numero de Muestra" id="muestra_nro" name="muestra_nro"; value="{{$data->nro_muestra}}" readonly type="text"></th>
            @foreach($puntos as $p)
            <th>{{$p->nombre}}</th>
            @endforeach
          </tr>
        </thead>
        <tbody>
          @foreach($campos as $c)
          <tr>
            <td>{{strtoupper($c)}}</td>
            @foreach($puntos as $p)
            <td>
              <label class="radiop">
                <input required class="calc radiop" @if($data->$c == $p->id) checked  @endif data-valor="{{$p->puntuacion}}" type="radio" name="{{$c}}" id="{{$c}}" value="{{$p->id}}" />
                <span class="checkmark"></span>
              </label>
            </td>
            @endforeach
          </tr>
          @endforeach
        </tbody>
        <!-- <p>Total: PHP <span id="resultado">0</span> -->
      </table>
      <button id="enviar_ficha" class="btn btn-md btn-primary" style="margin-left:42%" type="submit">Completar Registro</button>
    </div>
  </form>
</div>
@endsection
@section('js')
<script>
  var id_cata = "{{$data->cata->id}}";
  $(document).ready(function() {
      $('.select2').select2();
       calcscore();
      $(".calc").change(function() {
          calcscore()
      });


  });

  $("#muestra_nro").change(function(){
    var id_producto = $(this).find(':selected').data('idprod')
    $("#id_prod").val(id_producto);
  });
  function calcscore() {
      console.log("entro")
      score = 0;
      $(".calc:checked").each(function() {
          score += Number($(this).data('valor'));
      });
      //$("#price").text(score.toFixed(2));  
      var resultado = score / 4;
      $("#resultado").text(resultado);
      $("#resultado_final").val(resultado);
  }
  $().ready(function() {
      $(".calc").change(function() {
          calcscore()
      });
  });
</script>
@endsection