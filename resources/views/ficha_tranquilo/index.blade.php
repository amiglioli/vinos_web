@extends('template.template')
@section('linkscss')
<link type="text/css" rel="stylesheet" href="{{ asset('/assets/select2/select2.css') }}">
<link type="text/css" rel="stylesheet" href="{{ asset('/assets/toastr/toastr.min.css') }}">
<link type="text/css" rel="stylesheet" href="{{ asset('/assets/bootstrap-datepicker/css/datepicker.css') }}">
<link type="text/css" rel="stylesheet" href="{{ asset('/assets/sweetalert/sweet-alert.css') }}">
<link href="{{asset('template/css/estilo_tabla.css')}}" rel="stylesheet">
@endsection

@section('content')
<section class="bf-table" id="data" style="display: none; padding-bottom: 20px ">
   <div class="card card-cascade narrower">
      <!--Card image-->
      <div class="view view-cascade gradient-card-header blue-gradient narrower py-2 mx-4 mb-3 d-flex justify-content-between align-items-center">
         <div>
            <!--<button type="button" class="btn btn-outline-white btn-rounded btn-sm px-2 waves-effect waves-light"><i class="fa fa-th-large mt-0"></i></button>
               <button type="button" class="btn btn-outline-white btn-rounded btn-sm px-2 waves-effect waves-light"><i class="fa fa-columns mt-0"></i></button> -->
         </div>
         <span class="white-text">Ficha de Puntuaciones</span>
         <div>
            <!-- boton -->
         </div>
      </div>
      <!--/Card image-->
      <div class="px-4">

         <div class="table-wrapper table-responsive">
            <!--Table-->
            <div class="btn-group pull-left" data-bind="foreach: filters">
          <span data-bind="click: function() { vm.remove_filters(name) }" title="Click para quitar este filtro" class="label label_table"><i class="fa fa-times-circle" aria-hidden="true"></i>&nbsp;&nbsp;<a style="color: white;" data-bind="text: value"></a></span>
        </div>
            <table class="table table-bordered tabla_custom table-hover mb-0">
          <thead>
            <tr>
              <th class="col_1" style="padding: 0px;">
                <input id="descripcion" type="text" placeholder="Descripcion" style="text-align: center;" class="form-control" data-bind="event: { change: function(){vm.filterDescripcion($element)} }">
              </th>
              <th class="col_1" style="padding: 0px;">
                <input id="lugar" type="text" placeholder="Lugar" style="text-align: center;" class="form-control" data-bind="event: { change: function(){vm.filterLugar($element)} }">
              </th>
              <th class="col_1" style="padding: 0px;">
                  <input id="anio" type="text" placeholder="Fecha" class="form-control encabezado" data-bind="">
                </th>
               <th class="col_1" style="padding: 0px;">
                <input id="catador" type="text" placeholder="Catador" style="text-align: center;" class="form-control" data-bind="event: { change: function(){vm.filterCatador($element)} }">
              </th>
              <th class="col_1" style="padding: 0px;">
                <select id="vista" class="form-control select2" data-bind="event:{ change: function(){ vm.filterVista($element) } }" style="width:100%">
                  <option value="">Vista</option>
                  @foreach($data as $d)
                  <option value="{{ $d->id }}">{{ $d->nombre }}</option>
                  @endforeach
                </select>
              </th>
              <th class="col_1" style="padding: 0px;">
                <select id="nariz" class="form-control select2" data-bind="event:{ change: function(){ vm.filterNariz($element) } }" style="width:100%">
                  <option value="">Nariz</option>
                  @foreach($data as $d)
                  <option value="{{ $d->id }}">{{ $d->nombre }}</option>
                  @endforeach
                </select>
              </th>
              <th class="col_1" style="padding: 0px;">
                <select id="gusto" class="form-control select2" data-bind="event:{ change: function(){ vm.filterGusto($element) } }" style="width:100%">
                  <option value="">Gusto</option>
                  @foreach($data as $d)
                  <option value="{{ $d->id }}">{{ $d->nombre }}</option>
                  @endforeach
                </select>
              </th>
              <th class="col_1" style="padding: 0px;">
                <select id="marca" class="form-control select2" data-bind="event:{ change: function(){ vm.filterMarca($element) } }" style="width:100%">
                  <option value="">Marca</option>
                  @foreach($data as $d)
                  <option value="{{ $d->id }}">{{ $d->nombre }}</option>
                  @endforeach
                </select>
              </th>
              <th class="col_1" style="padding: 0px;">
                <input id="vino" type="text" placeholder="Vino" style="text-align: center;" class="form-control" data-bind="event: { change: function(){vm.filterVino($element)} }">
              </th>
              <th class="col_1" style="padding: 0px;">
                <input id="muestra" type="text" placeholder="Muestra" style="text-align: center;" class="form-control" data-bind="event: { change: function(){vm.filterMuestra($element)} }">
              </th>
              <th class="text-left"></th>
            </tr>
          </thead>
          <tbody data-bind="foreach: data().data">
            <tr>
              <td class="col_1 text-left"   data-bind="text: cata.descripcion"></td>
              <td class="col_1 text-left"       data-bind="text: cata.lugar"></td>
              <td class="col_1 text-left"   data-bind="text: formatearFecha(cata.fecha)"></td>
              <td class="col_1 text-left"   data-bind="text: catador.nombre+' '+catador.apellido"></td>
              <td class="col_1 text-left"   data-bind="text: vista.nombre"></td>
              <td class="col_1 text-left"   data-bind="text: nariz.nombre"></td>
              <td class="col_1 text-left"   data-bind="text: gusto.nombre"></td>
              <td class="col_1 text-left"   data-bind="text: marca.nombre"></td>
              <td class="col_1 text-left"   data-bind="text: producto.marca"></td>
              <td class="col_1 text-left"   data-bind="text: nro_muestra"></td>

              <td class="col_1 text-center">
               <!-- ko if: cata.cerrada == false -->
                <a href="javascript:void(0)" data-bind="attr: { href: '{{ url("ficha_tranquilo") }}' +'/'+id+'/edit', }"  class="btn btn-outline btn-warning btn-xs">Editar</a>
                <a href="javascript:void(0)" data-bind="event:{ click: function(){ vm.eliminar(id) } }" class="btn btn-outline btn-danger btn-xs">Eliminar</a>
                              <!-- /ko -->
              </td>

            </tr>
          </tbody>
        </table>
         </div>

         <div class="d-flex justify-content-between">
            <!--Name-->
            <div style="padding-top: 40px">
                <p>Mostrando del <span data-bind="text: data().from == null ? '0' : data().from"></span> al <span data-bind="text: data().to == null ? '0' : data().to"></span> de <span data-bind="text: data().total == null ? '0' : data().total"></span> registros</p>
            </div>
            <nav class="my-4">
               <ul class="pagination pagination-circle pg-blue mb-0">
                  <li class="page-item" data-bind="css: data().prev_page_url == null ? 'disabled' : ''"><a class="page-link waves-effect waves-effect" data-bind="click: function() { ir(1) }">« Primera</a></li>
                  <li class="page-item" data-bind="if: data().current_page-100 > 0, click: function() { ir(data().current_page-100) } "><span class="page-link waves-effect waves-effect" data-bind="text: data().current_page-100"></span></li>
                  <li class="page-item" data-bind="if: data().current_page-50 > 0, click: function() { ir(data().current_page-50) } "><span class="page-link waves-effect waves-effect" data-bind="text: data().current_page-50"></span></li>
                  <li class="page-item" data-bind="if: data().current_page-10 > 0, click: function() { ir(data().current_page-10) } "><span class="page-link waves-effect waves-effect" data-bind="text: data().current_page-10"></span></li>
                  <li class="page-item" data-bind="if: data().current_page-5 > 0, click: function() { ir(data().current_page-5) } "><span class="page-link waves-effect waves-effect" data-bind="text: data().current_page-5"></span></li>
                  <li class="page-item" data-bind="if: data().current_page-2 > 0, click: function() { ir(data().current_page-2) } "><span class="page-link waves-effect waves-effect" data-bind="text: data().current_page-2"></span></li>
                  <li class="page-item" data-bind="if: data().current_page-1 > 0, click: function() { ir(data().current_page-1) } "><span class="page-link waves-effect waves-effect" data-bind="text: data().current_page-1"></span></li>
                  <li class="page-item active" ><span class="page-link waves-effect waves-effect" data-bind="text: data().current_page"></span></li>
                  <li class="page-item" data-bind="if: data().current_page+1 <= data().last_page, click: function() { ir(data().current_page+1) } "><span class="page-link waves-effect waves-effect" data-bind="text: data().current_page+1"></span></li>
                  <li class="page-item" data-bind="if: data().current_page+2 <= data().last_page, click: function() { ir(data().current_page+2) } "><span class="page-link waves-effect waves-effect" data-bind="text: data().current_page+2"></span></li>
                  <li class="page-item" data-bind="if: data().current_page+5 <= data().last_page, click: function() { ir(data().current_page+5) } "><span class="page-link waves-effect waves-effect" data-bind="text: data().current_page+5"></span></li>
                  <li class="page-item" data-bind="if: data().current_page+10 <= data().last_page, click: function() { ir(data().current_page+10) } "><span class="page-link waves-effect waves-effect" data-bind="text: data().current_page+10"></span></li>
                  <li class="page-item" data-bind="if: data().current_page+50 <= data().last_page, click: function() { ir(data().current_page+50) } "><span class="page-link waves-effect waves-effect" data-bind="text: data().current_page+50"></span></li>
                  <li class="page-item" data-bind="if: data().current_page+100 <= data().last_page, click: function() { ir(data().current_page+100) } "><span class="page-link waves-effect waves-effect" data-bind="text: data().current_page+100"></span></li>
                  <li class="page-item" data-bind="css: data().last_page == null || data().current_page >= data().last_page ? 'disabled' : ''"><a class="page-link waves-effect waves-effect" data-bind="click: function() { ir(data().last_page) }">Última »</a></li>
               </ul>

            </nav>
         </div>

      </div>
   </div>
</section>
@endsection

@section('js')
<script src='{{ asset("assets/toastr/toastr.min.js") }}'></script>
<script src="{{ asset('assets/select2/select2.full.js') }}"></script>
<script src="{{ asset('assets/moment/moment-with-locales.min.js')}}"></script>
<script src="{{ asset('assets/bootstrap-datepicker/js/bootstrap-datepicker.js') }}"></script>
<script src="{{ asset('assets/bootstrap-datepicker/js/locales/bootstrap-datepicker.es.js') }}"></script>
<script>
moment.locale('es');
var url = "{{ url('api/ficha_tranquilo') }}";
var url_normal = "{{ url('ficha_tranquilo') }}";
var vm = function() {
  self = this;
  self.data = ko.observableArray([]);
  self.descripcion = '';
  self.lugar = '';
  self.catador = '';
  self.muestra = '';
  self.vista = '';
  self.vino = '';
  self.vista_text = '';
  self.nariz = '';
  self.nariz_text = '';
  self.gusto = '';
  self.gusto_text = '';
  self.marca = '';
  self.marca_text = '';
  self.anio            = '';
  self.anio_text       = '';
  self.page = '1';
  self.filters = ko.observableArray([]);
  self.init = function() {
    self.ajax();
    $('.select2').select2();
  }
  self.filterDescripcion = function(element) {
    self.page = 1;
    self.descripcion = $(element).val()
    self.update_filters();
    self.ajax();
  };
  self.filterMuestra = function(element) {
    self.page = 1;
    self.muestra = $(element).val()
    self.update_filters();
    self.ajax();
  };
  self.filterLugar = function(element) {
    self.page = 1;
    self.lugar = $(element).val()
    self.update_filters();
    self.ajax();
  };
  self.filterCatador = function(element) {
    self.page = 1;
    self.catador = $(element).val()
    self.update_filters();
    self.ajax();
  };
  self.filterAnio = function(id,tipo, f){
    self.page = 1;
    var d = new Date(f);
    self.anio = d.getFullYear() + "-" + ("0"+(d.getMonth()+1)).slice(-2) + "-" + ("0" + d.getDate()).slice(-2) ;;
    self.anio_text = ("0" + d.getDate()).slice(-2) + "/" + ("0"+(d.getMonth()+1)).slice(-2) + "/" + d.getFullYear();
    self.ajax();
    self.update_filters();
};
  self.filterNariz = function(element) {
      self.page = 1;
      self.nariz = $(element).val()
      self.nariz_text = $(element).find("option:selected").text();
      self.update_filters();
      self.ajax();
    };
    self.filterVista = function(element) {
      self.page = 1;
      self.vista = $(element).val()
      self.vista_text = $(element).find("option:selected").text();
      self.update_filters();
      self.ajax();
    };
    self.filterMarca = function(element) {
      self.page = 1;
      self.marca = $(element).val()
      self.marca_text = $(element).find("option:selected").text();
      self.update_filters();
      self.ajax();
    };
    self.filterGusto = function(element) {
      self.page = 1;
      self.gusto = $(element).val()
      self.gusto_text = $(element).find("option:selected").text();
      self.update_filters();
      self.ajax();
    };
  self.filterVino = function(element) {
    self.page = 1;
    self.vino = $(element).val()
    self.update_filters();
    self.ajax();
  };

  self.update_filters = function() {
    self.filters([]);
    console.log(self.filters([]));
    if (self.descripcion != '') self.filters.push({
      key: 'descripcion',
      value: self.descripcion,
      name: 'descripcion'
    });
    if (self.muestra != '') self.filters.push({
      key: 'muestra',
      value: self.muestra,
      name: 'muestra'
    });
    if (self.nariz != '') self.filters.push({
      key: 'nariz',
      value: self.nariz_text,
      name: 'nariz'
    });
    if (self.vista != '') self.filters.push({
      key: 'vista',
      value: self.vista_text,
      name: 'vista'
    });
    if (self.marca != '') self.filters.push({
      key: 'marca',
      value: self.marca_text,
      name: 'marca'
    });
    if (self.gusto != '') self.filters.push({
      key: 'gusto',
      value: self.gusto_text,
      name: 'gusto'
    });
    if (self.lugar != '') self.filters.push({
      key: 'lugar',
      value: self.lugar,
      name: 'lugar'
    });
    if (self.anio != '') self.filters.push({
      key: 'anio',
      value: self.anio_text,
      name: 'anio'
    });
    if (self.catador != '') self.filters.push({
      key: 'catador',
      value: self.catador,
      name: 'catador'
    });
    if (self.vino != '') self.filters.push({
      key: 'vino',
      value: self.vino,
      name: 'vino'
    });


      console.log(self.filters())
  };
  self.remove_filters = function(name) {
    if (name == 'descripcion') {
      self.descripcion = '';
    }
    if (name == 'nariz') {
      self.nariz = '';
      self.nariz_text = '';
    }
    if (name == 'vista') {
      self.vista = '';
      self.vista_text = '';
    }
    if (name == 'gusto') {
      self.gusto = '';
      self.gusto_text = '';
    }
    if (name == 'muestra') {
      self.muestra = '';
      self.muestra_text = '';
    }
    if (name == 'marca') {
      self.marca = '';
      self.marca_text = '';
    }
    if (name == 'lugar') {
      self.lugar = '';
    }
    if (name == 'anio') {
      self.anio = '';
      self.anio_text = '';
    }
    if (name == 'catador') {
      self.catador = '';
    }
    if (name == 'vino') {
      self.vino = '';
    }
    if (name == 'bodega') {
      self.bodega = '';
    }
    $('#' + name).val("").trigger("change");
     if (name=='anio') $('#anio').val("");
    self.update_filters();
    self.ajax();
  };
  self.ir = function(page) {
    self.page = page;
    self.ajax();
    $('html, body').animate({
      scrollTop: 0
    }, 'fast');
  };
  self.eliminar = function(id) {
    $.ajax({
      url: url_normal+'/'+id,
      type: 'DELETE',
      data    :   {
        '_token': "{{ csrf_token() }}"
      },
      success: function(result) {
        toastr.success(result);
        self.ajax();
      }
    });
  };
  self.ajax = function() {
    $.getJSON(url, {
      page: self.page,
      descripcion: self.descripcion,
      lugar:self.lugar,
      catador:self.catador,
      nariz:self.nariz,
      gusto:self.gusto,
      anio:self.anio,
      marca:self.marca,
      muestra:self.muestra,
      vista:self.vista,
      vino:self.vino
    })
    .done(function(data) {
      console.log(data);
      Pace.restart()
      vm.data(data);
      $("#data").fadeIn();
      $(".loading").remove();
    })
    .error(function(d) {
      toastr.error('Se encontro un error intente nuevamente');
    });
  };
};

vm = new vm();
vm.init();
ko.applyBindings(vm, document.getElementById("data"));
    $('#anio').datepicker({
        language: 'es',
        format: 'dd/mm/yyyy',
        autoclose: true
    }).on('changeDate', function(ev) {
        vm.filterAnio(null, null, ev.date);
    });
@if(session()->has('message'))
toastr.success('{{ session()->get('message ') }}');
@endif
</script>
@endsection
