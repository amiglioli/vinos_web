@extends('template.template')
@section('linkscss')
<link type="text/css" rel="stylesheet" href="{{ asset('/assets/select2/select2.css') }}">
<link type="text/css" rel="stylesheet" href="{{ asset('/assets/toastr/toastr.min.css') }}">
<link type="text/css" rel="stylesheet" href="{{ asset('/assets/bootstrap-datepicker/css/datepicker.css') }}">
<link type="text/css" rel="stylesheet" href="{{ asset('/assets/sweetalert/sweet-alert.css') }}">
<link href="{{asset('template/css/estilo_tabla.css')}}" rel="stylesheet">
@endsection

@section('content')
<section class="bf-table" id="data" style="display: none; padding-bottom: 20px ">
   <div class="card card-cascade narrower">
      <!--Card image-->
      <div class="view view-cascade gradient-card-header blue-gradient narrower py-2 mx-4 mb-3 d-flex justify-content-between align-items-center">
         <div>
            <!--<button type="button" class="btn btn-outline-white btn-rounded btn-sm px-2 waves-effect waves-light"><i class="fa fa-th-large mt-0"></i></button>
               <button type="button" class="btn btn-outline-white btn-rounded btn-sm px-2 waves-effect waves-light"><i class="fa fa-columns mt-0"></i></button> -->
         </div>
         <span class="white-text">Listado de {{ucfirst($perfil.'s')}}</span>
         <div>
            <!-- boton -->
         </div>
      </div>
      <!--/Card image-->
      <div class="px-4">
        
         <div class="table-wrapper table-responsive">
            <!--Table-->
            <div class="btn-group pull-left" data-bind="foreach: filters">
          <span data-bind="click: function() { vm.remove_filters(name) }" title="Click para quitar este filtro" class="label label_table"><i class="fa fa-times-circle" aria-hidden="true"></i>&nbsp;&nbsp;<a style="color: white;" data-bind="text: value"></a></span>
        </div>
            <table class="table table-bordered tabla_custom table-hover mb-0">
					<thead>
						<tr>
							<th class="col_1" style="padding: 0px;">
								<select id="nombre" class="form-control select2" data-bind="event:{ change: function(){ vm.filterUsuario($element) } }" style="width:100%">
									<option value="">Nombre</option>
									@foreach($usuarios as $p)
									<option value="{{ $p->id }}">{{ $p->name }}</option>
									@endforeach
								</select>
							</th>
							<th class="col_1" style="padding: 0px;">
								<input id="email" type="text" placeholder="E-mail" style="text-align: center;" class="form-control" data-bind="event: { change: function(){vm.filterEmail($element)} }">
							</th>
							<th class="col_1" style="padding: 0px;">
								<select id="provincia" class="form-control select2" data-bind="event:{ change: function(){ vm.filterProvincia($element) } }" style="width:100%">
									<option value="">PROVINCIA</option>
									@foreach($provincias as $p)
									<option value="{{ $p->provincia_id }}">{{ $p->provincia }}</option>
									@endforeach
								</select>
							</th>
							<th class="col_1" style="padding: 0px;">
								<select id="municipio" class="form-control select2" data-bind="event:{ change: function(){ vm.filterMunicipio($element) } }" style="width:100%">
									<option value="">MUNICIPIO</option>
									@foreach($municipios as $m)
									<option value="{{ $m->municipio_id }}">{{ $m->municipio }}</option>
									@endforeach
								</select>
							</th>
							<th class="col_1" style="padding: 0px;">
								<input id="localidad" type="text" placeholder="Localidad" style="text-align: center;" class="form-control" data-bind="event: { change: function(){vm.filterLocalidad($element)} }">
							</th>
							<th class="col_1" style="padding: 0px;">
								<select id="activa" class="form-control select2" data-bind="event:{ change: function(){ vm.filterActiva($element) } }" style="width:100%">
									<option value="">ACTIVA</option>
									<option value="t">SI</option>
									<option value="f">NO</option>
								</select>
							</th>
							<th class="text-left"></th>
						</tr>
					</thead>
					<tbody data-bind="foreach: data().data">
						<tr>
							<td class="col_1 text-left" 	data-bind="text: name"></td>
							<td class="col_1 text-left" 	data-bind="text: email"></td>
							<td class="col_1 text-left" 	data-bind="text: provincia"></td>
							<td class="col_1 text-left" 	data-bind="text: municipio"></td>
							<td class="col_1 text-left" 	data-bind="text: localidad"></td>
							<td class="col_1 text-left" 	data-bind="visible: aprobado  == true ">SI</td>
							<td class="col_1 text-left" 	data-bind="visible: aprobado  == false ">NO</td>


							<td class="col_1 text-center">
								<a href="javascript:void(0)" data-bind="attr: { href: '{{ url("listado") }}' +'/'+id+'/edit', }"  class="btn btn-outline btn-warning btn-xs">Editar</a>
							</td>
						</tr>
					</tbody>
				</table>
				</div>
         
         <div class="d-flex justify-content-between">
            <div style="padding-top: 40px">
                <p>Mostrando del <span data-bind="text: data().from == null ? '0' : data().from"></span> al <span data-bind="text: data().to == null ? '0' : data().to"></span> de <span data-bind="text: data().total == null ? '0' : data().total"></span> registros</p>               
            </div>
            <nav class="my-4">
               <ul class="pagination pagination-circle pg-blue mb-0">
                  <li class="page-item" data-bind="css: data().prev_page_url == null ? 'disabled' : ''"><a class="page-link waves-effect waves-effect" data-bind="click: function() { ir(1) }">« Primera</a></li>
                  <li class="page-item" data-bind="if: data().current_page-100 > 0, click: function() { ir(data().current_page-100) } "><span class="page-link waves-effect waves-effect" data-bind="text: data().current_page-100"></span></li>
                  <li class="page-item" data-bind="if: data().current_page-50 > 0, click: function() { ir(data().current_page-50) } "><span class="page-link waves-effect waves-effect" data-bind="text: data().current_page-50"></span></li>
                  <li class="page-item" data-bind="if: data().current_page-10 > 0, click: function() { ir(data().current_page-10) } "><span class="page-link waves-effect waves-effect" data-bind="text: data().current_page-10"></span></li>
                  <li class="page-item" data-bind="if: data().current_page-5 > 0, click: function() { ir(data().current_page-5) } "><span class="page-link waves-effect waves-effect" data-bind="text: data().current_page-5"></span></li>
                  <li class="page-item" data-bind="if: data().current_page-2 > 0, click: function() { ir(data().current_page-2) } "><span class="page-link waves-effect waves-effect" data-bind="text: data().current_page-2"></span></li>
                  <li class="page-item" data-bind="if: data().current_page-1 > 0, click: function() { ir(data().current_page-1) } "><span class="page-link waves-effect waves-effect" data-bind="text: data().current_page-1"></span></li>
                  <li class="page-item active" ><span class="page-link waves-effect waves-effect" data-bind="text: data().current_page"></span></li>
                  <li class="page-item" data-bind="if: data().current_page+1 <= data().last_page, click: function() { ir(data().current_page+1) } "><span class="page-link waves-effect waves-effect" data-bind="text: data().current_page+1"></span></li>
                  <li class="page-item" data-bind="if: data().current_page+2 <= data().last_page, click: function() { ir(data().current_page+2) } "><span class="page-link waves-effect waves-effect" data-bind="text: data().current_page+2"></span></li>
                  <li class="page-item" data-bind="if: data().current_page+5 <= data().last_page, click: function() { ir(data().current_page+5) } "><span class="page-link waves-effect waves-effect" data-bind="text: data().current_page+5"></span></li>
                  <li class="page-item" data-bind="if: data().current_page+10 <= data().last_page, click: function() { ir(data().current_page+10) } "><span class="page-link waves-effect waves-effect" data-bind="text: data().current_page+10"></span></li>
                  <li class="page-item" data-bind="if: data().current_page+50 <= data().last_page, click: function() { ir(data().current_page+50) } "><span class="page-link waves-effect waves-effect" data-bind="text: data().current_page+50"></span></li>
                  <li class="page-item" data-bind="if: data().current_page+100 <= data().last_page, click: function() { ir(data().current_page+100) } "><span class="page-link waves-effect waves-effect" data-bind="text: data().current_page+100"></span></li>
                  <li class="page-item" data-bind="css: data().last_page == null || data().current_page >= data().last_page ? 'disabled' : ''"><a class="page-link waves-effect waves-effect" data-bind="click: function() { ir(data().last_page) }">Última »</a></li>
               </ul>

            </nav>

         </div>

      </div>
   </div>
</section>
@endsection

@section('js')
<script src='{{ asset("assets/toastr/toastr.min.js") }}'></script>
<script src="{{ asset('assets/select2/select2.full.js') }}"></script>
<script src="{{ asset('assets/moment/moment-with-locales.min.js')}}"></script>
<script src="{{ asset('assets/bootstrap-datepicker/js/bootstrap-datepicker.js') }}"></script>
<script src="{{ asset('assets/bootstrap-datepicker/js/locales/bootstrap-datepicker.es.js') }}"></script>
<script>
moment.locale('es');
var url = "{{ url('api/listado') }}";
var url_normal = "{{ url('listado') }}";
var vm = function() {
	self = this;
	self.data = ko.observableArray([]);
	self.nombre = '';
	self.nombre_text = '';
	self.email = '';
	self.provincia = '';
	self.provincia_text = '';
	self.municipio = '';
	self.municipio_text = '';
	self.localidad = '';
	self.activa = '';
	self.activa_text = '';
	self.page = '1';
	self.filters = ko.observableArray([]);
	self.init = function() {
		self.ajax();
		$('.select2').select2();
	}
	self.filterUsuario = function(element) {
		self.page = 1;
		self.nombre = $(element).val()
		self.nombre_text = $(element).find("option:selected").text();
		self.update_filters();
		self.ajax();
	};
	self.filterEmail = function(element) {
		self.page = 1;
		self.email = $(element).val()
		self.update_filters();
		self.ajax();
	};
	self.filterProvincia = function(element) {
		self.page = 1;
		self.provincia = $(element).val()
		self.provincia_text = $(element).find("option:selected").text();
		self.update_filters();
		self.ajax();
	};
	self.filterLocalidad = function(element) {
		self.page = 1;
		self.localidad = $(element).val()
		self.update_filters();
		self.ajax();
	};
	self.filterMunicipio = function(element) {
		self.page = 1;
		self.municipio = $(element).val()
		self.municipio_text = $(element).find("option:selected").text();
		self.update_filters();
		self.ajax();
	};
	self.filterActiva = function(element) {
		self.page = 1;
		self.activa = $(element).val()
		self.activa_text = $(element).find("option:selected").text();
		self.update_filters();
		self.ajax();
	};

	self.update_filters = function() {
		self.filters([]);
		if (self.nombre != '') self.filters.push({
			key: 'nombre',
			value: self.nombre_text,
			name: 'nombre'
		});
		if (self.email != '') self.filters.push({
			key: 'email',
			value: self.email,
			name: 'email'
		});
		if (self.provincia != '') self.filters.push({
			key: 'provincia',
			value: self.provincia_text,
			name: 'provincia'
		});

		if (self.municipio != '') self.filters.push({
			key: 'municipio',
			value: self.municipio_text,
			name: 'municipio'
		});
		if (self.localidad != '') self.filters.push({
			key: 'localidad',
			value: self.localidad,
			name: 'localidad'
		});
		if (self.activa != '') self.filters.push({
			key: 'activa',
			value: self.activa_text,
			name: 'activa'
		});
	};
	self.remove_filters = function(name) {
		if (name == 'nombre') {
			self.nombre = '';
			self.nombre_text = '';
		}
		if (name == 'email') {
			self.email = '';
		}
		if (name == 'provincia') {
			self.provincia = '';
			self.provincia_text = '';
		}
		if (name == 'municipio') {
			self.municipio = '';
			self.municipio_text = '';
		}
		if (name == 'localidad') {
			self.localidad = '';
			self.localidad = '';
		}
		if (name == 'activa') {
			self.activa = '';
			self.activa_text = '';
		}
		$('#' + name).val("").trigger("change");
		self.update_filters();
		self.ajax();
	};
	self.ir = function(page) {
		self.page = page;
		self.ajax();
		$('html, body').animate({
			scrollTop: 0
		}, 'fast');
	};
	self.ajax = function() {
		$.getJSON(url, {
			page: self.page,
			nombre: self.nombre,
			email: self.email,
			provincia: self.provincia,
			municipio:self.municipio,
			localidad:self.localidad,
			activa:self.activa,
			perfil:'{{$perfil}}'
		})
		.done(function(data) {
			console.log(data);
			Pace.restart()
			vm.data(data);
			$("#data").fadeIn();
			$(".loading").remove();
		})
		.error(function(d) {
			toastr.error('Se encontro un error intente nuevamente');
		});
	};
};
vm = new vm();
vm.init();
ko.applyBindings(vm, document.getElementById("data"));

@if(session()->has('message'))
toastr.success('{{ session()->get('message ') }}');
@endif
</script>
@endsection
