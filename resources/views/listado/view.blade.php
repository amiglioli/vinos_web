@extends('template.template')
@section('content')
<div class="row wrapper border-bottom white-bg page-heading">
  <div class="col-lg-10">
    <h2>Detalles de  {{ $data->user->name }}</h2>
    <ol class="breadcrumb">
      <li>

      </li>
    </ol>
  </div>
  <div class="col-lg-2">

  </div>
</div>

<div class="row">
  <br>
  <div class="col-lg-12 text-center">
    <div class="ibox float-e-margins">

      <div class="ibox-content">
        <form class="validator form-horizontal">
          {{ csrf_field() }}
          <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
            <label for="name" class="col-md-4 control-label">Denominación</label>
            <div class="col-md-6">
              <input disabled id="entidad" type="text" class="form-control" name="entidad" value="{{ $data->user->name }}" required autofocus>
            </div>
          </div>
          <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
            <label for="name" class="col-md-4 control-label">Provincia</label>

            <div class="col-md-6">
              <input disabled id="localidad" type="text" class="form-control" name="localidad" value="{{ $data->provincias->provincia}}" required autofocus>
            </div>
          </div>


          <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
            <label  for="name" class="col-md-4 control-label">Partido</label>
            <div class="col-md-6">
              <input disabled id="localidad" type="text" class="form-control" name="localidad" value="{{$data->municipio}}" required autofocus>
            </div>
          </div>


          <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
            <label for="name" class="col-md-4 control-label">Localidad</label>
            <div class="col-md-6">
              <input disabled id="localidad" type="text" class="form-control" name="localidad" value="{{$data->localidad}}" required autofocus>
            </div>
          </div>


          <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
            <label for="name" class="col-md-4 control-label">Código Postal</label>
            <div class="col-md-6">
              <input disabled id="codigo_postal" type="text" class="form-control" name="codigo_postal" value="{{ $data->codigo_postal}}" required autofocus>
            </div>
          </div>

          <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
            <label for="name" class="col-md-4 control-label">Domicilio</label>
            <div class="col-md-6">
              <input disabled id="domicilio" type="text" class="form-control" name="domicilio" value="{{$data->domicilio}}" required autofocus>
            </div>
          </div>

          <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
            <label for="name" class="col-md-4 control-label">Número </label>
            <div class="col-md-6">
              <input disabled id="numero" type="text" class="form-control" name="numero" value="{{$data->numero}}" required autofocus>
            </div>
          </div>


          <div class="form-group{{ $errors->has('telefono') ? ' has-error' : '' }}">
            <label for="name" class="col-md-4 control-label">Teléfono </label>
            <div class="col-md-6">
              <input disabled id="telefono" type="text" class="form-control" name="telefono" value="{{$data->telefono}}" required autofocus>
            </div>
          </div>

          <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
            <label for="name" class="col-md-4 control-label">Página Web</label>
            <div class="col-md-6">
              <input disabled id="web" type="text" class="form-control" name="web" value="{{$data->web}}" autofocus>
            </div>
          </div>

        </form>
      </div>
    </div>
  </div>
</div>
@endsection
