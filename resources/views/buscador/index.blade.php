@extends('template.template')
@section('linkscss')
<link type="text/css" rel="stylesheet" href="{{ asset('/assets/select2/select2.css') }}">
<link type="text/css" rel="stylesheet" href="{{ asset('/assets/toastr/toastr.min.css') }}">
<style>
#mapCanvas {
width: 100%;
height: 400px;
border:1px solid;
}
</style>
@endsection
@section('content')
<div class="row wrapper red-bg">
    <div class="col-lg-10">
        <h2>Buscá dentro de REDVIN</h2>
    </div>
    <div class="col-lg-2">
    </div>
</div>
<div class="row" id="app">
    <div class="col-lg-12 text-center">
        <div class="ibox float-e-margins">
            <div class="ibox-content">
                <div class="row">
                    <div class="col-md-6">
                        <select name="entidad" id="entidad" class="form-control select2" v-model="entidad"  @change="fetchEntidad">
                            <option value="">Seleccione el contenido a buscar</option>
                            <option v-for="en in entidades" :value="en">@{{en.toUpperCase()}}</option>
                        </select>
                    </div>
                    <div class="col-md-6" v-show="entidad != ''">
                        <select style="width: 100%" name="nombre" id="select_ajax" class="form-control select2_ajax">
                            <option value="">Seleccione el contenido a buscar</option>
                        </select>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div v-if="item_selected.id">
        <div class="col-lg-12"  v-if="entidad != 'vino'">

            <div class="col-lg-6">
                <div class="row wrapper red-bg">
                    <div class="col-lg-10">
                        <h2>Información</h2>
                    </div>
                    <div class="col-lg-2">
                    </div>
                </div>
                <div class="ibox" >
                    <div class="ibox-content">
                        <div class="table-responsive" style="max-height: 440px !important">
                            <table class="table table-striped">
                                <tbody>
                                    <tr>
                                        <th>Denominación</t>
                                            <td> @{{item_selected.name}}</td>
                                        </tr>
                                        <th>Email</t>
                                            <td> @{{item_selected.email}}</td>
                                        </tr>
                                        <tr>
                                            <th>Telefono</th>
                                            <td> @{{item_selected.informacion_adicional.telefono}} </td>
                                        </tr>
                                        <tr>
                                            <th>Domicilio</th>
                                            <td> @{{item_selected.informacion_adicional.domicilio}} @{{item_selected.informacion_adicional.numero}}</td>
                                        </tr>
                                        <tr>
                                            <th>Provincia</th>
                                            <td>@{{item_selected.informacion_adicional.provincias.provincia}}</td>
                                        </tr>
                                        <tr>
                                            <th>Municipio</th>
                                            <td>@{{item_selected.informacion_adicional.municipio.municipio}}</td>
                                        </tr>
                                        <tr>
                                            <th>Localidad</th>
                                            <td>@{{item_selected.informacion_adicional.localidad}}</td>
                                        </tr>
                                        <tr>
                                            <th>Codigo Postal</th>
                                            <td>@{{item_selected.informacion_adicional.codigo_postal}}</td>
                                        </tr>
                                    </tr>
                                    <tr>
                                        <th>Pagina web</th>
                                        <td>@{{item_selected.informacion_adicional.web}}</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="row wrapper red-bg">
                    <div class="col-lg-10">
                        <h2>Imagen de la @{{entidad}}</h2>
                    </div>
                    <div class="col-lg-2">
                    </div>
                </div>
                <div class="ibox text-center" >
                    <div class="ibox-content">
                        <img v-if="item_selected.imagen[0]" alt="image" class="rounded-circle m-t-xs img-fluid" style="max-width: 100%" v-bind:src="url_asset+'imgusuario/'+item_selected.imagen[0].path">
                        <img v-else alt="image" class="rounded-circle m-t-xs img-fluid" style="max-width: 100%"  v-bind:src="url_asset+'/template/img/user.png'">
                    </div>
                </div>
                
                
            </div>
        </div>
        <div class="col-lg-12"  v-if="entidad == 'vino'">
            <div class="col-lg-6">
                <div class="row wrapper red-bg">
                    <div class="col-lg-10">
                        <h2>Información</h2>
                    </div>
                    <div class="col-lg-2">
                    </div>
                </div>
                <div class="ibox" >
                    <div class="ibox-content">
                        <div class="table-responsive" style="max-height: 440px !important">
                            <table class="table table-striped">
                                <tbody>
                                    
                                    <tr>
                                        <th>Marca</th>
                                        <td> @{{item_selected.marca}}</td>
                                    </tr>
                                    <tr>
                                        <th>Año</th>
                                        <td> @{{item_selected.anio}}</td>
                                    </tr>
                                    <tr>
                                        <th>Bodega</th>
                                        <td>@{{item_selected.bodega.name}} </td>
                                    </tr>
                                    <tr>
                                        <th>Producto</th>
                                        <td>@{{item_selected.producto.nombre}}</td>
                                    </tr>
                                    <tr>
                                        <th>Tipo</th>
                                        <td>@{{item_selected.producto_tipo.tipo}}</td>
                                    </tr>
                                    <tr v-if="item_selected.varietales">
                                        <th>Varietal</th>
                                        <td>@{{item_selected.varietales.varietal}}</td>
                                    </tr>
                                    <tr>
                                        <th>Ciudad</th>
                                        <td>@{{item_selected.ciudad.nombre}}</td>
                                    </tr>
                                    <tr>
                                        <th>Región</th>
                                        <td>@{{item_selected.region.nombre}}</td>
                                    </tr>
                                    <tr>
                                        <th>Provincia</th>
                                        <td>@{{item_selected.provincia.provincia}}</td>
                                    </tr>
                                    <tr>
                                        <th>País</th>
                                        <td>@{{item_selected.pais.nombre}}</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="row wrapper red-bg">
                    <div class="col-lg-10">
                        <h2 v-show="entidad == 'vino'">Imagen del Producto</h2>
                    </div>
                    <div class="col-lg-2">
                    </div>
                </div>
                <div class="ibox text-center" >
                    <div class="ibox-content">
                        <img v-if="item_selected.img_etiqueta" alt="image" class="rounded-circle text-center m-t-xs img-fluid" width=225 height=360 v-bind:src="url_asset+item_selected.img_etiqueta">
                        <img v-else alt="image" class="rounded-circle m-t-xs text-center img-fluid" width=400 height=225 v-bind:src="url_asset+'/template/img/user.png'">
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-12" v-show="entidad != 'vinoteca'">
            <div class="ibox" >
                <div class="row wrapper red-bg">
                    <div class="col-lg-10">
                        <h2 v-show="entidad == 'bodega'">Distribuidores</h2>
                       <!-- <h2 v-show="entidad == 'vinoteca'">Productos de la Vinoteca</h2> -->
                        <h2 v-show="entidad == 'distribuidora'">Portfolio</h2>
                        <h2 v-show="entidad == 'vino'">Distribuidores de este vino</h2>
                    </div>
                    <div class="col-lg-2">
                    </div>
                </div>
                <div class="ibox-content">
                    <div class="table-responsive" style="max-height: 440px !important">
                        <table class="table table-striped">
                            <thead>
                                <tr>
                                    <th v-for="header in headers">@{{header}}</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr  v-if="entidad == 'bodega' && item_selected.bodega_distribucion.length > 0"   v-for="distribuidora in item_selected.bodega_distribucion">
                                    <td>@{{distribuidora.distribuidora.name}}</td>
                                    <td>@{{distribuidora.distribuidora.email}}</td>
                                    <td>@{{distribuidora.distribuidora.informacion_adicional.telefono}}</td>
                                </tr>
                                <!--
                                <tr  v-if="entidad == 'vinoteca' && item_selected.productos_vinoteca.length > 0"   v-for="producto in item_selected.productos_vinoteca">
                                    <td>@{{producto.marca}}</td>
                                    <td>@{{producto.anio}}</td>
                                    <td>@{{producto.producto_tipo.tipo}}</td>
                                    <td>@{{producto.producto.nombre}}</td>
                                    <td v-if="producto.varietales">@{{producto.varietales.varietal}}</td>
                                    <td v-else> - </td>
                                </tr>
                            -->
                                <tr  v-if="entidad == 'distribuidora' && datos_portfolio.length > 0"   v-for="bodega in datos_portfolio">
                                    <td>@{{bodega.name}}</td>
                                    <td>
                                        <span class="btn btn-success btn-xs" style="    margin-left: 4px;" v-if="bodega.bodega_provincias_distribucion.length > 0" v-for="provincia in bodega.bodega_provincias_distribucion"> @{{provincia.provincias.provincia}} </span>
                                    </td>
                                    <td>
                                        @{{bodega.bodega_municipios_distribucion.length}}
                                        <a  @click="ver_localidades(bodega.id)" class="btn btn-success btn-xs">Ver</a>
                                    </td>
                                </tr>
                                <tr  v-if="entidad == 'vino' && item_selected.distribuidora_vino.length > 0" v-for="distribuidora in item_selected.distribuidora_vino">
                                    <td>@{{distribuidora.distribuidora.name}}</td>
                                    <td v-if="distribuidora.distribuidora.informacion_adicional">@{{distribuidora.distribuidora.informacion_adicional.provincias.provincia}}</td>
                                    <td v-if="distribuidora.distribuidora.informacion_adicional">@{{distribuidora.distribuidora.informacion_adicional.municipio.municipio}}</td>
                                    <td v-if="distribuidora.distribuidora.informacion_adicional">@{{distribuidora.distribuidora.email}}</td>


                                     <td v-if="distribuidora.distribuidora.informacion_adicional.telefono != null">@{{distribuidora.distribuidora.informacion_adicional.telefono}}</td>
                                    <td v-else> - </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @include('buscador.localidades')
    <div class="col-md-12 " v-show="( entidad == 'bodega' ||  entidad == 'vinoteca') && item_selected.informacion_adicional" >

        <div class="row wrapper red-bg">
            <div class="col-lg-10">
                <h2>Ubicación de la @{{entidad}} <b><i>@{{item_selected.name}}</i></b></h2>
            </div>
            <div class="col-lg-2">
            </div>
        </div>
        <div>
            <div  id="mapCanvas"></div>
        </div>
    </div>
</div>

@endsection
@section('js')
<script src='{{ asset("assets/toastr/toastr.min.js") }}'></script>
<script src="{{ asset('assets/select2/select2.full.js') }}"></script>
<script src="https://cdn.jsdelivr.net/npm/vue@2.5.16/dist/vue.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/axios/0.18.0/axios.js"></script>
<script type="text/javascript" src="http://maps.google.com/maps/api/js?&libraries=places&key=AIzaSyD9FZU87YCCSGovLg57u9dvi2jLDDz3uyw"></script>
<script>
var url_productos = "{{url('api/productos')}}";
var url_perfiles = "{{url('api/usuarios')}}"
var app = new Vue({
    el: '#app',

    data: {
        url_asset: "{{asset('')}}",
        item_selected: {},
        alldata: [],
        headers: [],
        entidad: '',
        localidades:[],
        markers: [],
        datos_portfolio: [],
        map: {},
        entidades: ['bodega', 'distribuidora', 'vinoteca', 'vino']
    },
    mounted() {
        let app = this;
        $('#entidad').select2().on('change', function() {
            app.entidad = $(this).val();
            app.fetchEntidad();
        });

        var mapstyle = {};
        $.ajax({
          url: "{{url('assets/mapstyle/mapstyle.json')}}",
          dataType: 'json',
          async: false,
          success: function(data) {
            mapstyle = data;
          }
        });
        const element = document.getElementById('mapCanvas')
        const options = {
          zoom: 14,
          center: new google.maps.LatLng(51.501527,-0.1921837),
          styles:mapstyle
        }
        this.map = new google.maps.Map(element, options);        

    },
    methods: {
        ver_localidades(bodega_id){
            this.localidades = []
            var valor = $("#select_ajax").val();
                 axios.get("{{url('api/portfolio/localidades')}}", {
                    params: {
                        id_distribiudora: valor,
                        bodega_id:bodega_id
                    }
                    }).then(response => {
                    this.localidades = response.data;
                    $("#exampleModal").modal()
                }, function(){
                    alert('Error!');
                });  
        },
        fetchEntidad() {
            this.item_selected = {};
            if (this.entidad == 'vino')
                var url = url_productos
            else if (this.entidad != 'vino' && this.entidad != '')
                var url = url_perfiles

            if (this.entidad != '') {
                $('#select_ajax').select2({
                    ajax: {
                        url: url,
                        data: function(params) {
                            var query = {
                                nombre: params.term,
                                nombre_rol: app.entidad,
                                user_real: 1,
                                perPage: 40
                            }
                            return query;
                        },
                        processResults: function(data, params) {
                            this.alldata = data.data;
                            return {
                                results: $.map(data.data, function(obj) {

                                    if (app.entidad == 'vino')
                                        return {
                                            id: obj.id,
                                            text: obj.marca
                                        };
                                    else
                                        return {
                                            id: obj.id,
                                            text: obj.name
                                        };
                                })
                            };
                        }.bind(this),
                    }
                }).on('change', function() {
                    var valor = $("#select_ajax").val();
                    app.fetchInformacion(valor);
                });
            }

        },
        fetchInformacion(val) {
            //console.log(this.alldata)

            this.alldata.forEach((item) => {
                if (item.id == val) {
                    this.item_selected = item;
                }
            });
            
            if (this.entidad == 'distribuidora'){
                 //var distribuidora_id = $('#select_ajax :selected').val();
                 axios.get("{{url('api/portfolio')}}", {
                    params: {
                        id_distribiudora: val
                    }
                    }).then(response => {
                        let items = response.data.data;
                        this.datos_portfolio = items;
                        //this.prueba();
                    }, function() {
                        alert('Error!');
                    });
            }

            
            this.set_header(this.entidad)

            //console.log(this.item_selected.informacion_adicional );
            if(this.item_selected.informacion_adicional !== null && this.entidad != 'vino'){
                if(this.markers.length > 0){
                    this.markers[0].setMap(null);
                    this.markers = [];
                }
                const position = new google.maps.LatLng(this.item_selected.informacion_adicional.geo_latitud,this.item_selected.informacion_adicional.geo_longitud);
                const marker = new google.maps.Marker({
                    position: position,
                    map: this.map
                });
                this.markers.push(marker);

                this.map.panTo(position);
            }

            

        },
        set_header(val) {
            if (val == 'bodega')
                this.headers = ['Nombre', 'Email', 'Teléfono']
            if (val == 'distribuidora')
                this.headers = ['Bodega', 'Provincias', 'Localidades']
            //if (val == 'vinoteca')
               // this.headers = ['Marca','Año','Tipo','Producto','Varietal']
            if (val == 'vino')
                this.headers = ['Distribuidora','Provincia', 'Partido','Email', 'Teléfono']
        }
    }
})







</script>
@endsection
