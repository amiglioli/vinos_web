@extends('template.template')
<meta name="csrf-token" content="{{ csrf_token() }}">
<style>
.img-container {
  /* Never limit the container height here */
  max-width: 100%;
}
</style>
@section('linkscss')
<link href="{{asset('template/css/cropper.min.css')}}" rel="stylesheet">
@endsection
@section('head_content')

@endsection
@section('content')
@if (Session::has('message_error'))
<div id="message-alert" class="alert alert-danger">
  <button type="button" class="close"
  data-target="#message-alert"
  data-dismiss="alert">
  <span aria-hidden="true">&times;</span><span class="sr-only">Close</span>
</button>
@foreach (Session::pull('message_error') as  $error )
{{$error[0]}} <br>
@endforeach
</div>
@elseif (Session::has('message_success'))
<div id="message-alert" class="alert alert-success">
  <button type="button" class="close"
  data-target="#message-alert"
  data-dismiss="alert">
  <span aria-hidden="true">&times;</span><span class="sr-only">Close</span>
</button>
@foreach (Session::pull('message_success') as  $success )
{{$success[0]}} <br>
@endforeach
</div>
@elseif (Session::has('message_warning'))
<div id="message-alert" class="alert alert-warning ">
  <button type="button" class="close"
  data-target="#message-alert"
  data-dismiss="alert">
  <span aria-hidden="true">&times;</span><span class="sr-only">Close</span>
</button>
@foreach (Session::pull('message_warning') as  $warning )
{{$warning[0]}} <br>
@endforeach
</div>
@endif
<div class="row">
  <div class="col-lg-12">
    <div class="ibox float-e-margins">
      <div class="ibox-title  back-change">
        <h5>Imagen para promoción</h5>
        <div class="ibox-tools">
          <a class="collapse-link">
            <i class="fa fa-chevron-up"></i>
          </a>
          <a class="dropdown-toimgggle" data-toggle="dropdown" href="#">
            <i class="fa fa-wrench"></i>
          </a>
          <ul class="dropdown-menu dropdown-user">
            <li><a href="#">Config option 1</a>
            </li>
            <li><a href="#">Config option 2</a>
            </li>
          </ul>
          <a class="close-link">
            <i class="fa fa-times"></i>
          </a>
        </div>
      </div>
      <div class="ibox-content">
        <p>
          Recorte las dimenciones que desee para su imagen
        </p>
        <div class="row">
          <div class="col-md-6">
            <div class="image-crop">
              <img src="{{URL::asset($promocion->promocion_imagen->ruta_imagen)}}">
            </div>
          </div>
          <div class="col-md-6">
            <h4>Previsualización de imagen</h4>
            <div class="img-preview img-preview-sm"></div>
            <h4>Principales métodos</h4>
            <p>
              Puede subir una nueva imagen al recuadro y cargarla facilmente como promoción.
            </p>
            <div class="btn-group">
              <label title="Subir Imagen" for="inputImage" class="btn btn-primary">
                <input type="file" accept="image/*" name="file" id="inputImage" class="hide">
                Subir imagen
              </label>
              <label title="Guardar" id="download" class="btn btn-info">Guardar</label>
            </div>
            <div class="form-group" id="data_5">
              <label class="font-normal">Seleccionar período vigencia</label>
              <div class="input-daterange input-group" id="datepicker">
                <input type="text" class="input-sm form-control" id="start" name="start" value="{{$promocion->promocion_imagen->fecha_desde}}"/>
                <span class="input-group-addon">Hasta</span>
                <input type="text" class="input-sm form-control" id="end" name="end" value="{{$promocion->promocion_imagen->fecha_hasta}}" />
              </div>
            </div>

            <h4>Otros métodos</h4>
            <p>
              Estas son alguna de las opciones para recortar su imagen
            </p>
            <div class="btn-group">
              <button class="btn btn-white" id="zoomIn" type="button">Zoom +</button>
              <button class="btn btn-white" id="zoomOut" type="button">Zoom -</button>
              <button class="btn btn-white" id="rotateLeft" type="button">Rotar Izquierda</button>
              <button class="btn btn-white" id="rotateRight" type="button">Rotar Derecha  </button>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

@endsection

@section('js')
<script src="{{asset('template/js/cropper.min.js')}}" type="text/javascript" ></script>
<script src="{{asset('template/js/datees.js')}}" type="text/javascript" ></script>
<script>

$(document).ready(function(){
  var base_url = "{{ url('') }}";
  var i = 0
  var startDate = new Date();
  var FromEndDate = new Date();
  var ToEndDate = new Date();

  ToEndDate.setDate(ToEndDate.getDate()+365);

  $('#start').datepicker({
   // keyboardNavigation: false,
    //forceParse: false,
    autoclose: true,
    startDate: $('#start').val(),
    language: "es",
    format: "yyyy-mm-dd"
  })
      .on('changeDate', function(selected){
         if(selected.date != null){
          startDate = new Date(selected.date.valueOf());
          var endDate = addDays(startDate,7);
          startDate.setDate(startDate.getDate(new Date(selected.date.valueOf())));
          $('#end').datepicker('setEndDate', endDate);
          $('#end').datepicker('setStartDate', startDate);
          $('#end').datepicker('setDate', endDate);
        }else{
          $('#start').datepicker('setStartDate', startDate);
        }
      });
  $('#end')
      .datepicker({
        autoclose: true,
        startDate: $('#start').val(),
        language: "es",
        endDate:addDays($('#start').val(),8),
        format: "yyyy-mm-dd"
      });


  function addDays(date, days) {
    var result = new Date(date);
    result.setDate(result.getDate() + days);
    return result;
  }
  var $image = $(".image-crop > img")
  $($image).cropper({
    aspectRatio: 1.618,
    preview: ".img-preview",
    done: function(data) {
      console.log(i++)
      //console.log(data);
    }
  });

  var $inputImage = $("#inputImage");
  if (window.FileReader) {
    $inputImage.change(function() {
      var fileReader = new FileReader(),
      files = this.files,
      file;

      if (!files.length) {
        return;
      }

      file = files[0];

      if (/^image\/\w+$/.test(file.type)) {
        fileReader.readAsDataURL(file);
        fileReader.onload = function () {
          $inputImage.val("");
          $image.cropper("reset", true).cropper("replace", this.result);
        };
      } else {
        showMessage("Please choose an image file.");
      }
    });
  } else {
    $inputImage.addClass("hide");
  }

  $("#download").click(function() {
    if(i == 1){
      console.log("paso");
     var base64 = null;
     var start = $('#start').val();
     var end = $('#end').val();
     save_img(base64,start,end);

    }else{
      console.log("murio");
      var start = $('#start').val();
      var end = $('#end').val();
      base64 = $image.cropper("getDataURL");
      save_img(base64,start,end);
    }
  });

  $("#zoomIn").click(function() {
    $image.cropper("zoom", 0.1);
  });

  $("#zoomOut").click(function() {
    $image.cropper("zoom", -0.1);
  });

  $("#rotateLeft").click(function() {
    $image.cropper("rotate", 45);
  });

  $("#rotateRight").click(function() {
    $image.cropper("rotate", -45);
  });

  $("#setDrag").click(function() {
    $image.cropper("setDragMode", "crop");
  });



  function save_img(base64,start,end)
  {
    //console.log(base64,start,end);
    var id_promo = "{{$promocion->id}}";
    $.ajax({
      headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
      //url: base_url+"/promociones_imagenes",
      url: "{{ route('promociones_imagenes.update', $promocion->id) }}",
      type: 'PUT',
      data:{
        img: base64,
        start: start,
        end: end,
        id: id_promo
      },
      success:function(data){
        //toastr.success('OK');
        window.location.href = base_url+"/promociones";
      },
      error:function(){
        toastr.error('Se encontro un error intente nuevamente');
      }
    });
  }


});

</script>






@endsection
