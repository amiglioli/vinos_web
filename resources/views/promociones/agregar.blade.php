  @extends('template.template')
  @section('linkscss')
  <link href="{{asset('template/css/ion.rangeSlider.css')}}" rel="stylesheet">
  <link href="{{asset('template/css/ion.rangeSlider.skinFlat.css')}}" rel="stylesheet">
  <link href="{{asset('template/css/jquery.bootstrap-touchspin.min.css')}}" rel="stylesheet">

  @endsection
  @section('content')

<div class="row">
  <div class="col-lg-12 text-center">
    <div class="ibox float-e-margins">
      <div class="ibox-title">
        <h5>Alta de Promociones</h5>
      </div>

      <div class="ibox-content">
       <form class="validator form-horizontal" id="form" action="{{url('promociones')}}" method="post">
          {{ csrf_field() }}
          <div class="form-group">
              <label for="name" class="col-md-4 control-label">Tipo</label>
              <div class="col-md-6">
                <select  class="form-control" name="tipo" value=""  id="select_id"  tabindex="1">
                 <option @if(old('tipo') == 'fija') selected @endif value="fija">PROMOCIÓN ÚNICA</option>
                 <option @if(old('tipo') == 'recurrente') selected @endif value="recurrente">PROMOCIÓN RECURRENTE</option>
               </select>
            </div>
          </div>
          <div class="form-group" id="data_1">
            <label for="fecha" class="col-md-4 control-label">Fecha</label>
            <div class="col-md-6 input-group date" >
              <span class="input-group-addon"><i class="fa fa-calendar"></i></span><input name="fecha" id="fecha" type="text" class="form-control" value="{{ old('fecha')}}">
            </div>
          </div>
          <div class="form-group" id="data_2">
           <label class="col-sm-4 control-label">Dias</label>
             <div class="col-sm-6 i-checks">
              <label class="checkbox-inline">
                @foreach(get_dias_semana() as $num_dia => $d)
                <input type="checkbox" value="1"  @if( !empty(old('dias')[$num_dia]) ) checked @endif name="dias[{{$num_dia}}]" id="inlineCheckbox1">{{$d}}</label> <label class="checkbox-inline custom">
                @endforeach
            </div>
          </div>
          <div class="form-group">
            <label for="descripcion" class="col-md-4 control-label">Descuento</label>
            <div class="col-sm-6">
              <input id="ionrange_1"  type="hidden" name="descuento" value="{{old('descuento',5)}}"><br>
            </div>
          </div>
          <div class="form-group">
            <label for="descripcion" class="col-md-4 control-label">Descripción</label>
            <div class="col-md-6">
              <input id="descripcion" type="descripcion" class="form-control" name="descripcion" value="{{ old('descripcion') }}" required>
            </div>
          </div>
          <div class="form-group">
            <div class="col-md-6 col-md-offset-4">
              <button type="submit" class="btn btn-primary" onclick="check_date()">
                Agregar Promoción
              </button>
            </div>
          </div>
      </form>
    </div>
  </div>


</div>
</div>
@endsection

@section('js')
<script src="{{asset('template/js/jquery.bootstrap-touchspin.min.js')}}" type="text/javascript" ></script>
<script src="{{asset('template/js/ion.rangeSlider.min.js')}}" type="text/javascript" ></script>
<script src="{{asset('template/js/datees.js')}}" type="text/javascript" ></script>
<script>

  $( document ).ready(function() {
   $('.i-checks').iCheck({
      checkboxClass: 'icheckbox_square-green',
      radioClass: 'iradio_square-green',
    });
    $("#data_2").hide();

     $('.i-checks').change();
     $('#select_id').change();
  });

  $('#data_1 .input-group.date').datepicker({
   todayBtn: "linked",
   language: "es",
   startDate: "today",
   keyboardNavigation: false,
   forceParse: false,
   calendarWeeks: true,
   autoclose: true,
   format: "yyyy-mm-dd"

 });

  $("#ionrange_1").ionRangeSlider({
    grid: true,
    min: 0,
    max: 100,
    from: $("#ionrange_1").val(),
    step: 5,
    from_shadow: true,
    postfix: " %",
    hasGrid: true,
    onFinish: function(data){
      //var value = data.fromNumber;
      //console.log(data.from);
      $("#ionrange_1").prop("value", data.from);
       //$("#SomeValue").prop("value",data.from_value)
    }
  });



  function check_date(){

    tipo = $('select[name=tipo]').val();

    if($("#fecha").val() == "" && tipo == "fija") {
      toastr.error("Ingrese una fecha");
      $("#form").submit(function(e){
        return false;
      });
    }else{
      $("#form").submit(function(e){
        return true;
      });
    }
  }

  $('#select_id').change(function(){
    var valor = $('#select_id').val()
    if(valor == 'recurrente'){
      $("#data_1").hide();
      $("#data_2").show();
    }else{
     $("#data_1").show();
     $("#data_2").hide();
     $("input").iCheck('uncheck');
   }

 });






</script>


@endsection
