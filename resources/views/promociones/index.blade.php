@extends('template.template')

@section('linkscss')
<link type="text/css" rel="stylesheet" href="{{ asset('/assets/select2/select2.css') }}">
<link type="text/css" rel="stylesheet" href="{{ asset('/assets/toastr/toastr.min.css') }}">
<link type="text/css" rel="stylesheet" href="{{ asset('/assets/bootstrap-datepicker/css/datepicker.css') }}">
<link type="text/css" rel="stylesheet" href="{{ asset('/assets/sweetalert/sweet-alert.css') }}">
<link href="{{asset('template/css/estilo_tabla.css')}}" rel="stylesheet">

@endsection

@section('content')

  <div class="row wrapper red-bg">
  <div class="col-lg-10">				
<h2>Mis Promociones</h2>	  </div>
  <div class="col-lg-2">
  </div>
</div>	
@include('promociones.tabla_promociones')
@include('promociones.tabla_promociones_imagenes')
@endsection
@section('js')
<script src='{{ asset("assets/toastr/toastr.min.js") }}'></script>
<script src="{{ asset('assets/select2/select2.full.js') }}"></script>

<script src="{{ asset('assets/bootstrap-datepicker/js/bootstrap-datepicker.js') }}"></script>
<script src="{{ asset('assets/bootstrap-datepicker/js/locales/bootstrap-datepicker.es.js') }}"></script>
<script src="{{ asset('assets/moment/moment-with-locales.min.js')}}"></script>
<script>

    moment.locale('es');
    var url = "{{ url('api/promociones') }}";
    var url_normal = "{{ url('promociones') }}";

    var url_img = "{{ url('api/promociones_imagenes') }}";
    var url_normal_img = "{{ url('promociones_imagenes') }}";
    
    
</script>
@yield('js_promocion')
@yield('js_promocion_imagenes')

@endsection


