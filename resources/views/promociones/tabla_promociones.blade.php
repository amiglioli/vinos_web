<section class="bf-table" id="data" style="display: none; padding-bottom: 20px ">
   <div class="card card-cascade narrower">
      <!--Card image-->
     <div class="view view-cascade gradient-card-header blue-gradient narrower py-2 mx-4 mb-3 d-flex justify-content-between align-items-center">
              <span class="white-text">Promociones</span><br>
		 <div style="text-align:right">
	   <a type="button" class="btn btn-danger" href="{{ URL::to('promociones/create') }}">Agregar promoción</a></div>
	     </div>
      <!--/Card image-->
      <div class="px-4">
        
         <div class="table-wrapper table-responsive">
            <!--Table-->
            <div class="btn-group pull-left" data-bind="foreach: filters">
          <span data-bind="click: function() { vm.remove_filters(name) }" title="Click para quitar este filtro" class="label label_table"><i class="fa fa-times-circle" aria-hidden="true"></i>&nbsp;&nbsp;<a style="color: white;" data-bind="text: value"></a></span>
        </div>
            <table class="table table-bordered tabla_custom table-hover mb-0">
               <!--Table head-->
               <thead>
                  <tr>
                     <th class="col_1" style="padding: 0px;width:150px;">
                        <select id="dias" name="dias" class="form-control select2" data-bind="event:{ change: function(){ vm.filterDia($element) } }" style="width:100%">
                          <option value="">Día</option>
                          <option value="fija">Promoción única</option>
                          <option value="recurrente">Promoción recurrente</option>
                        </select>
                      </th>
                      <th class="col_1" style="padding: 0px;width:150px;">
                        <input class="form-control" placeholder="Descripción" id="descripcion"  name="descripcion" data-bind="event:{ change: function(){ vm.filterDescripcion($element) } }" autocomplete="off">
                      </th>
                      <th class="col_1" style="padding: 0px;width:150px;">
                        
                        <select id="descuento" name="descuento" class="form-control select2" data-bind="event:{ change: function(){ vm.filterDescuento($element) } }" style="width:100%">
                          <option value="">Descuento (%)</option>
                          @for($i = 5; $i<=75; $i= $i+5)
                            <option value="{{$i}}">{{$i}} %</option>
                          @endfor
                        </select>
                      </th>
                      <th class="col_1" style="padding: 0px;width:150px;">
                        <select id="estado" name="estado" class="form-control select2" data-bind="event:{ change: function(){ vm.filterEstado($element) } }" style="width:100%">
                          <option value="">Estado</option>
                          
                            <option value="true">Activa</option>
                            <option value="false">No activa</option>
                          
                        </select>
                      </th>
                      <th class="text-left" style="width:250px;">Acciones</th>
                  </tr>
               </thead>
               <tbody data-bind="foreach: data().data">
                  <tr>
                     <!-- ko if: tipo == 'recurrente' -->
                      <td>
                        <button data-bind="visible: promocion_recurrente.lunes     == true " class="btn btn-success btn-circle" type="button"><b>LU</b></button>
                        <button data-bind="visible: promocion_recurrente.martes    == true " class="btn btn-success btn-circle" type="button"><b>MA</b></button>
                        <button data-bind="visible: promocion_recurrente.miercoles == true " class="btn btn-success btn-circle" type="button"><b>MI</b></button>
                        <button data-bind="visible: promocion_recurrente.jueves    == true " class="btn btn-success btn-circle" type="button"><b>JU</b></button>
                        <button data-bind="visible: promocion_recurrente.viernes   == true " class="btn btn-success btn-circle" type="button"><b>VI</b></button>
                        <button data-bind="visible: promocion_recurrente.sabado    == true " class="btn btn-success btn-circle" type="button"><b>SA</b></button>
                        <button data-bind="visible: promocion_recurrente.domingo   == true " class="btn btn-success btn-circle" type="button"><b>DO</b></button>
                      </td>
                      <td style="min-width: 600px;max-width: 600px" data-bind="text:promocion_recurrente.descripcion"></td> 
                      <td>
                        <button class="btn btn-warning btn-circle" style="" type="button"><b data-bind="text: promocion_recurrente.porcentaje_descuento"></b></button>
                      </td> 
                     <!-- /ko -->
                     <!-- ko if: tipo == 'fija'-->
                        <td data-bind="text: spanishDate(promocion_fija.fecha)">Fija</td>
                        <td data-bind="text:promocion_fija.descripcion"></td> 
                        <td>
                          <button class="btn btn-warning btn-circle" style="" type="button"><b data-bind="text: promocion_fija.porcentaje_descuento"></b></button>
                        </td> 
                     <!-- /ko -->
                    <td>
                      <a data-bind="visible: estado == true , click: function(){ vm.activarPromocion(false,id) }" type="button"   class="btn btn-outline btn-warning">Activa</a>
                      <a data-bind="visible: estado == false, click: function(){ vm.activarPromocion(true,id) } " type="button"  class="btn btn-outline btn-warning">No activa</a>
                    </td>
                    <td class="col_1 text-center">
                          <a href="javascript:void(0)" data-bind="attr: { href: '{{ url("promociones") }}' +'/'+id+'/edit', }"  class="btn btn-outline btn-warning">Editar</a>
                          <a href="javascript:void(0)" data-bind="event:{ click: function(){ vm.eliminar(id) } }" class="btn btn-outline btn-warning">Eliminar</a>
                    </td>
                  </tr>
               </tbody>
            </table>
         </div>
         
         <div class="d-flex justify-content-between">
            <div style="padding-top: 40px">
                <p>Mostrando del <span data-bind="text: data().from == null ? '0' : data().from"></span> al <span data-bind="text: data().to == null ? '0' : data().to"></span> de <span data-bind="text: data().total == null ? '0' : data().total"></span> registros</p>               
            </div>
            <nav class="my-4">
               <ul class="pagination pagination-circle pg-blue mb-0">
                  <li class="page-item" data-bind="css: data().prev_page_url == null ? 'disabled' : ''"><a class="page-link waves-effect waves-effect" data-bind="click: function() { ir(1) }">« Primera</a></li>
                  <li class="page-item" data-bind="if: data().current_page-100 > 0, click: function() { ir(data().current_page-100) } "><span class="page-link waves-effect waves-effect" data-bind="text: data().current_page-100"></span></li>
                  <li class="page-item" data-bind="if: data().current_page-50 > 0, click: function() { ir(data().current_page-50) } "><span class="page-link waves-effect waves-effect" data-bind="text: data().current_page-50"></span></li>
                  <li class="page-item" data-bind="if: data().current_page-10 > 0, click: function() { ir(data().current_page-10) } "><span class="page-link waves-effect waves-effect" data-bind="text: data().current_page-10"></span></li>
                  <li class="page-item" data-bind="if: data().current_page-5 > 0, click: function() { ir(data().current_page-5) } "><span class="page-link waves-effect waves-effect" data-bind="text: data().current_page-5"></span></li>
                  <li class="page-item" data-bind="if: data().current_page-2 > 0, click: function() { ir(data().current_page-2) } "><span class="page-link waves-effect waves-effect" data-bind="text: data().current_page-2"></span></li>
                  <li class="page-item" data-bind="if: data().current_page-1 > 0, click: function() { ir(data().current_page-1) } "><span class="page-link waves-effect waves-effect" data-bind="text: data().current_page-1"></span></li>
                  <li class="page-item active" ><span class="page-link waves-effect waves-effect" data-bind="text: data().current_page"></span></li>
                  <li class="page-item" data-bind="if: data().current_page+1 <= data().last_page, click: function() { ir(data().current_page+1) } "><span class="page-link waves-effect waves-effect" data-bind="text: data().current_page+1"></span></li>
                  <li class="page-item" data-bind="if: data().current_page+2 <= data().last_page, click: function() { ir(data().current_page+2) } "><span class="page-link waves-effect waves-effect" data-bind="text: data().current_page+2"></span></li>
                  <li class="page-item" data-bind="if: data().current_page+5 <= data().last_page, click: function() { ir(data().current_page+5) } "><span class="page-link waves-effect waves-effect" data-bind="text: data().current_page+5"></span></li>
                  <li class="page-item" data-bind="if: data().current_page+10 <= data().last_page, click: function() { ir(data().current_page+10) } "><span class="page-link waves-effect waves-effect" data-bind="text: data().current_page+10"></span></li>
                  <li class="page-item" data-bind="if: data().current_page+50 <= data().last_page, click: function() { ir(data().current_page+50) } "><span class="page-link waves-effect waves-effect" data-bind="text: data().current_page+50"></span></li>
                  <li class="page-item" data-bind="if: data().current_page+100 <= data().last_page, click: function() { ir(data().current_page+100) } "><span class="page-link waves-effect waves-effect" data-bind="text: data().current_page+100"></span></li>
                  <li class="page-item" data-bind="css: data().last_page == null || data().current_page >= data().last_page ? 'disabled' : ''"><a class="page-link waves-effect waves-effect" data-bind="click: function() { ir(data().last_page) }">Última »</a></li>
               </ul>

            </nav>

         </div>

      </div>
   </div>
</section>

@section('js_promocion')
<script>
  var vm = function() {
        self = this;
        self.data           = ko.observableArray([]);
        self.dias           = '';
        self.dias_text      = '';
        self.descripcion    = '';
        self.descuento      = '';
        self.descuento_text = '';
        self.estado         = '';
        self.estado_text    = '';
        self.page           = '1';
        self.filters        = ko.observableArray([]);
        self.init = function() {
            self.ajax();
            $('.select2').select2();
        }
        self.filterDia = function(element) {
            self.page = 1;
            self.dias = $(element).val()
            self.dias_text = $(element).find("option:selected").text();
            self.update_filters();
            self.ajax();
        };
        self.filterDescuento = function(element) {
            self.page = 1;
            self.descuento = $(element).val()
            self.descuento_text = $(element).find("option:selected").text();
            self.update_filters();
            self.ajax();
        };
        self.filterEstado = function(element) {
            self.page = 1;
            self.estado = $(element).val()
            self.estado_text = $(element).find("option:selected").text();
            self.update_filters();
            self.ajax();
        };
        self.filterDescripcion = function(element) {
            self.page = 1;
            self.descripcion = $(element).val()
            self.update_filters();
            self.ajax();
        };
        
        self.activarPromocion = function(estado,id){

          $.ajax({
            url: url_normal+"/activar_promocion",
            type: 'GET',
            data    :   {
              '_token': "{{ csrf_token() }}",
              estado: estado,
              id_promocion: id
            },
            success: function(result) {
              if(estado == true)
               toastr.success('Promoción Activa');
              else
               toastr.error('Promoción no Activa');
              self.ajax();
            }
          });

        };
        self.update_filters = function() {
            self.filters([]);
            if (self.dias != '') self.filters.push({
                key: 'dias',
                value: self.dias_text,
                name: 'dias'
            });
            if (self.descuento != '') self.filters.push({
                key: 'descuento',
                value: self.descuento_text,
                name: 'descuento'
            });
            if (self.descripcion != '') self.filters.push({
                key: 'descripcion',
                value: self.descripcion,
                name: 'descripcion'
            });
            if (self.estado != '') self.filters.push({
                key: 'estado',
                value: self.estado_text,
                name: 'estado'
            });
        
        };
        self.remove_filters = function(name) {
            if (name == 'dias') {
                self.dias = '';
                self.dias_text = '';
            }
            if (name == 'descuento') {
                self.descuento = '';
                self.descuento_text = '';
            }
            if (name == 'descripcion') {
                self.descripcion = '';
            }
            if (name == 'estado') {
                self.estado = '';
                self.estado_text = '';
            }

             
            $('#' + name).val("").trigger("change");
            self.update_filters();
            self.ajax();
        };
        self.ir = function(page) {
            self.page = page;
            self.ajax();
            $('html, body').animate({
                scrollTop: 0
            }, 'fast');
        };

          self.eliminar = function(id) {
    swal({
      title: "Está seguro que desea eliminar esta promoción?",
      text: "",
      icon: "warning",
      buttons: true,
      dangerMode: true,
    })
    .then((willDelete) => {
      if (willDelete) {
          $.ajax({
            url: url_normal+'/'+id,
            type: 'DELETE',
            data    :   {
              '_token': "{{ csrf_token() }}"
            },
            success: function(result) {
              toastr.success(result);
              self.ajax();
            }
          });
        self.ajax();
      }
    });
  };

        self.ajax = function() {
            $.getJSON(url, {
                    page                  : self.page,
                    dia                   : self.dias,
                    porcentaje_descuento  : self.descuento,
                    descripcion           : self.descripcion,
                    estado                : self.estado
                })
                .done(function(data) {
                    //console.log(data);
                    Pace.restart()
                    vm.data(data);
                    $("#data").fadeIn();
                    $(".loading").remove();
                })
                .error(function(d) {
                    //toastr.error('Se encontro un error intente nuevamente');
                });
        };
    };
    vm = new vm();
    vm.init();
    ko.applyBindings(vm, document.getElementById("data"));
</script>
@endsection