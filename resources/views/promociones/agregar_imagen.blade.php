@extends('template.template')
<meta name="csrf-token" content="{{ csrf_token() }}">
<style>
.img-container {
  /* Never limit the container height here */
  max-width: 100%;
}
</style>
@section('linkscss')
<link href="{{asset('template/css/cropper.min.css')}}" rel="stylesheet">
<link type="text/css" rel="stylesheet" href="{{ asset('/assets/toastr/toastr.min.css') }}">
@endsection
@section('head_content')

@endsection
@section('content')
@if (Session::has('message_error'))
<div id="message-alert" class="alert alert-danger">
  <button type="button" class="close"
  data-target="#message-alert"
  data-dismiss="alert">
  <span aria-hidden="true">&times;</span><span class="sr-only">Close</span>
</button>
@foreach (Session::pull('message_error') as  $error )
{{$error[0]}} <br>
@endforeach
</div>
@elseif (Session::has('message_success'))
<div id="message-alert" class="alert alert-success">
<button type="button" class="close"
data-target="#message-alert"
data-dismiss="alert">
<span aria-hidden="true">&times;</span><span class="sr-only">Close</span>
</button>
@foreach (Session::pull('message_success') as  $success )
{{$success[0]}} <br>
@endforeach
</div>
@elseif (Session::has('message_warning'))
<div id="message-alert" class="alert alert-warning ">
<button type="button" class="close"
data-target="#message-alert"
data-dismiss="alert">
<span aria-hidden="true">&times;</span><span class="sr-only">Close</span>
</button>
@foreach (Session::pull('message_warning') as  $warning )
{{$warning[0]}} <br>
@endforeach
</div>
@endif
<div class="row">
            <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-title  back-change">
                        <h5>Las promociones en imágenes permanecerán como máximo 7 días, a partir de la fecha de inicio solicitada y podrá ser renovada tantas veces como lo desee.</h5>
                    </div>
                    <div class="ibox-content">
                        <p>
                           Recorte las dimenciones que desee para su imagen
                        </p>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="image-crop">
                                    <img src="{{asset('template/img/fondo_cropic.png')}}">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <h4>Previsualización de imagen</h4>
                                <div class="img-preview img-preview-sm"></div>
                               <br>
                                <div class="btn-group">
                                    <label title="Subir Imagen" for="inputImage" class="btn btn-primary">
                                        <input type="file" accept="image/*" name="file" id="inputImage" class="hide">
                                        Subir Imagen
                                    </label>
                                    <label title="Guardar" id="download" class="btn btn-primary">Guardar</label>
                                </div>
                                <div class="form-group" id="data_5">
                        <label class="font-normal">Seleccionar período vigencia</label>
                        <div class="input-daterange input-group" id="datepicker">
                            <input type="text" class="input-sm form-control" id="start" name="start" value="<?php echo date('Y-m-d') ?>"/>
                            <span class="input-group-addon">Hasta</span>
                            <input type="text" class="input-sm form-control" id="end" name="end" value="<?php echo  date('Y-m-d', strtotime("+7 day")) ?>" />
                        </div>
                    </div>

                                <div class="btn-group">
                                    <button class="btn btn-white" id="zoomIn" type="button">Zoom +</button>
                                    <button class="btn btn-white" id="zoomOut" type="button">Zoom -</button>
                                    <button class="btn btn-white" id="rotateLeft" type="button">Rotar Derecha</button>
                                    <button class="btn btn-white" id="rotateRight" type="button">Rotar Izquierda  </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

@endsection

@section('js')
<script src="{{asset('template/js/cropper.min.js')}}" type="text/javascript" ></script>
<script src="{{asset('template/js/datees.js')}}" type="text/javascript" ></script>
<script src='{{ asset("assets/toastr/toastr.min.js") }}'></script>
<script>

$(document).ready(function(){
  var base_url = "{{ url('') }}";
   var startDate = new Date();
   var FromEndDate = new Date();
   var ToEndDate = new Date();
   ToEndDate.setDate(ToEndDate.getDate()+365);
   $('#start').datepicker({
    // keyboardNavigation: false,
     //forceParse: false,
     autoclose: true,
     startDate: new Date(),
     language: "es",
     format: "yyyy-mm-dd"
   })
       .on('changeDate', function(selected){
          if(selected.date != null){
           startDate = new Date(selected.date.valueOf());
           var endDate = addDays(startDate,7);
           startDate.setDate(startDate.getDate(new Date(selected.date.valueOf())));
           $('#end').datepicker('setEndDate', endDate);
           $('#end').datepicker('setStartDate', startDate);
           $('#end').datepicker('setDate', endDate);
         }else{
           $('#start').datepicker('setStartDate', startDate);
         }
       });
   $('#end')
       .datepicker({
         autoclose: true,
         startDate: new Date(),
         language: "es",
         endDate:'+7d',
         format: "yyyy-mm-dd"
       });


   var $image = $(".image-crop > img")
   $($image).cropper({
       aspectRatio: 1.618,
       preview: ".img-preview",
       done: function(data) {
          //console.log(data);
       }
   });

    var $inputImage = $("#inputImage");
    if (window.FileReader) {
       $inputImage.change(function() {
           var fileReader = new FileReader(),
                   files = this.files,
                   file;

           if (!files.length) {
               return;
           }

           file = files[0];

           if (/^image\/\w+$/.test(file.type)) {
               fileReader.readAsDataURL(file);
               fileReader.onload = function () {
                   $inputImage.val("");
                   $image.cropper("reset", true).cropper("replace", this.result);
               };
           } else {
               showMessage("Please choose an image file.");
           }
       });
     } else {
        $inputImage.addClass("hide");
     }

     $("#download").click(function() {
         base64 = $image.cropper("getDataURL");
         save_img(base64);
     });

     $("#zoomIn").click(function() {
         $image.cropper("zoom", 0.1);
     });

     $("#zoomOut").click(function() {
         $image.cropper("zoom", -0.1);
     });

     $("#rotateLeft").click(function() {
         $image.cropper("rotate", 45);
     });

     $("#rotateRight").click(function() {
         $image.cropper("rotate", -45);
     });

     $("#setDrag").click(function() {
         $image.cropper("setDragMode", "crop");
     });

    function save_img(base54)
    {
        $.ajax({
          headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
          url: base_url+"/promociones_imagenes",
          data:{
            img: base64,
            start: $('#start').val(),
            end: $('#end').val(),
            tipo: 'imagen'
          },
          type:"post",
          success:function(data){
            //toastr.success('OK');
            window.location.href = base_url+"/promociones";
          },
          error:function(){
            toastr.error('Se encontro un error intente nuevamente');
          }
        });
    }


});

function addDays(date, days) {
  var result = new Date(date);
  result.setDate(result.getDate() + days);
  return result;
}

 </script>






@endsection
