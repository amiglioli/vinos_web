  @extends('template.template')
  @section('linkscss')
  <link href="{{asset('template/css/ion.rangeSlider.css')}}" rel="stylesheet">
  <link href="{{asset('template/css/ion.rangeSlider.skinFlat.css')}}" rel="stylesheet">
  <link href="{{asset('template/css/jquery.bootstrap-touchspin.min.css')}}" rel="stylesheet">

  @endsection

  @section('content')
<div class="row">
  <div class="col-lg-12 text-center">
    <div class="ibox float-e-margins">
      <div class="ibox-title">
        <h5>Editar su promoción</h5>
      </div>

      <div class="ibox-content">
      <form class="validator form-horizontal" action="{{ url('promociones',$promocion->id) }}" method="POST">
          @method('PUT')
        {{ csrf_field() }}
        <input class="form-control" type="hidden" name="tipo" value="{{$promocion->tipo}}">
        <input class="form-control" type="hidden" name="id" value="{{$promocion->id}}">
        <div class="form-group">
          <label for="name" class="col-md-4 control-label">Tipo</label>
          <div class="col-md-6">
            @if($promocion->tipo == 'recurrente')
              <input class="form-control" readonly type="text" value="PROMOCIÓN RECURRENTE">
            @else
              <input class="form-control" readonly type="text" value="PROMOCIÓN ÚNICA">
            @endif
           
          </div>
        </div>

        @if($promocion->tipo == 'fija')
          <div class="form-group" id="data_1">
            <label for="fecha" class="col-md-4 control-label">Fecha</label>
            <div class="col-md-6 input-group date" >
              <span class="input-group-addon"><i class="fa fa-calendar"></i></span><input name="fecha" id="fecha" type="text" class="form-control" value="{{ old('fecha', !empty($promocion->promocion_fija) ? $promocion->promocion_fija->fecha : '') }}">
            </div>
          </div>
        @else
            <div class="form-group" id="data_2">
             <label class="col-sm-4 control-label">Dias</label>
             <div class="col-sm-6 i-checks">
              <label class="checkbox-inline">
                  <input type="checkbox" value="1" @if($promocion->promocion_recurrente->lunes) checked @endif name="dias[0]" id="inlineCheckbox1"> LU</label> <label class="checkbox-inline custom">
                  <input type="checkbox" value="1" @if($promocion->promocion_recurrente->martes ) checked @endif name="dias[1]" id="inlineCheckbox1"> MA</label> <label class="checkbox-inline custom">
                  <input type="checkbox" value="1" @if($promocion->promocion_recurrente->miercoles ) checked @endif name="dias[2]" id="inlineCheckbox1"> MI</label> <label class="checkbox-inline custom">
                  <input type="checkbox" value="1" @if($promocion->promocion_recurrente->jueves) checked @endif name="dias[3]" id="inlineCheckbox1"> JU</label> <label class="checkbox-inline custom">
                  <input type="checkbox" value="1" @if($promocion->promocion_recurrente->viernes) checked @endif name="dias[4]" id="inlineCheckbox1"> VI</label> <label class="checkbox-inline custom">
                  <input type="checkbox" value="1" @if($promocion->promocion_recurrente->sabado ) checked @endif name="dias[5]" id="inlineCheckbox1"> SA</label> <label class="checkbox-inline custom">
                  <input type="checkbox" value="1" @if($promocion->promocion_recurrente->domingo) checked @endif name="dias[6]" id="inlineCheckbox1"> DO</label> <label class="checkbox-inline custom">
               
              </div>
            </div>
        @endif


        <div class="form-group">
          <label for="descripcion" class="col-md-4 control-label">Descuento</label>
          <div class="col-sm-6">
            @if($promocion->tipo == 'fija')
              <input id="ionrange_1" type="hidden" name="descuento" value="{{$promocion->promocion_fija->porcentaje_descuento}}"><br>
            @else
              <input id="ionrange_1" type="hidden" name="descuento" value="{{$promocion->promocion_recurrente->porcentaje_descuento}}"><br>
            @endif
          </div>
        </div>


        <div class="form-group">
          <label for="descripcion" class="col-md-4 control-label">Descripción</label>
          <div class="col-md-6">
            @if($promocion->tipo == 'fija')
              <input id="descripcion" type="descripcion" class="form-control" name="descripcion" value="{{ old('descripcion',$promocion->promocion_fija->descripcion) }}" required>
            @else
              <input id="descripcion" type="descripcion" class="form-control" name="descripcion" value="{{ old('descripcion',$promocion->promocion_recurrente->descripcion) }}" required>
            @endif
            
          </div>
        </div>


        <div class="form-group">
          <div class="col-md-6 col-md-offset-4">
            <button type="submit" class="btn btn-primary" onclick="check_date()">
              Editar promoción
            </button>
          </div>
        </div>
      </form>
    </div>
  </div>


</div>
</div>
@endsection
@section('js')
<script src="{{asset('template/js/jquery.bootstrap-touchspin.min.js')}}" type="text/javascript" ></script>
<script src="{{asset('template/js/ion.rangeSlider.min.js')}}" type="text/javascript" ></script>
<script src="{{asset('template/js/datees.js')}}" type="text/javascript" ></script>
<script>

  $( document ).ready(function() {
   $('.i-checks').iCheck({
      checkboxClass: 'icheckbox_square-green',
      radioClass: 'iradio_square-green',
    });
    //$("#data_2").hide();

     $('.i-checks').change();
     //$('#select_id').change();
  });

  $('#data_1 .input-group.date').datepicker({
   todayBtn: "linked",
   language: "es",
   keyboardNavigation: false,
   forceParse: false,
   calendarWeeks: true,
   autoclose: true,
   format: "yyyy-mm-dd"

 });

  $("#ionrange_1").ionRangeSlider({
    grid: true,
    min: 5,
    max: 75,
    from: $("#ionrange_1").val(),
    step: 5,
    from_shadow: true,
    postfix: " %",
    hasGrid: true,
    onFinish: function(data){
      //var value = data.fromNumber;
      //console.log(data.from);
      $("#ionrange_1").prop("value", data.from);
       //$("#SomeValue").prop("value",data.from_value)
    }
  });



  function check_date(){

    tipo = $('select[name=tipo]').val();

    if($("#fecha").val() == "" && tipo == "fija") {
      toastr.error("Ingrese una fecha");
      $("#form").submit(function(e){
        return false;
      });
    }else{
      $("#form").submit(function(e){
        return true;
      });
    }
  }







</script>


@endsection