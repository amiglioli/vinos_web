<section class="bf-table" id="data_imagenes" style="display: none; padding-bottom: 20px ">
   <div class="card card-cascade narrower">
      <!--Card image-->
       <div class="view view-cascade gradient-card-header blue-gradient narrower py-2 mx-4 mb-3 d-flex justify-content-between align-items-center">
             <span class="white-text">Promociones con Imágenes</span><br>
		 <div style="text-align:right">
	   <a type="button" class="btn btn-danger" href="{{ URL::to('promociones_imagenes/create') }}">Agregar promoción con imágenes</a></div>
      
    
      </div>
	 
      <!--/Card image-->
      <div class="px-4">
        
         <div class="table-wrapper table-responsive">
            <!--Table-->
            <div class="btn-group pull-left" data-bind="foreach: filters">
          <span data-bind="click: function() { vmi.remove_filters(name) }" title="Click para quitar este filtro" class="label label_table"><i class="fa fa-times-circle" aria-hidden="true"></i>&nbsp;&nbsp;<a style="color: white;" data-bind="text: value"></a></span>
        </div>
            <table class="table table-bordered tabla_custom table-hover mb-0">
               <!--Table head-->
               <thead>
                  <tr>
                    <th class="col_1" style="padding: 0px;width:150px;">
                      <input id="datepicker_desde" type="text" placeholder="Desde" class="form-control encabezado" data-bind="">
                    </th>
                    <th class="col_1" style="padding: 0px;width:150px;">
                      <input id="datepicker_hasta" type="text" placeholder="Hasta" class="form-control encabezado" data-bind="">
                    </th>
                    <th class="text-left" style="width:150px;" >
                      Imagen
                    </th>

                    <th class="col_1" style="padding: 0px;width:150px;">
                      <select id="estado_imagen" name="estado_imagen" class="form-control select2" data-bind="event:{ change: function(){ vmi.filterEstado($element) } }" style="width:100%">
                        <option value="">Estado</option>
                        
                          <option value="true">Activa</option>
                          <option value="false">No activa</option>
                        
                      </select>
                    </th>
                    <th class="text-left" style="width:250px;">Acciones</th>
                  </tr>
                  
               </thead>
               <tbody data-bind="foreach: data().data">
                    <tr>
                        <td data-bind="text: spanishDate(promocion_imagen.fecha_desde)"></td>
                        <td data-bind="text: spanishDate(promocion_imagen.fecha_hasta)"></td>
                        <td><img data-bind="attr: { src: '{{url(asset(''))}}'+promocion_imagen.ruta_imagen }"  alt="" style="border:1px solid;" border=3 height=90 width=160></td> 
                        <td>
                          <a data-bind="visible: estado == true , click: function(){ vmi.activarPromocion(false,id) }" type="button"   class="btn btn-outline btn-warning">Activa</a>
                          <a data-bind="visible: estado == false, click: function(){ vmi.activarPromocion(true,id) } " type="button"  class="btn btn-outline btn-warning">No activa</a>
                        </td>
                        <td class="col_1 text-center">
                            <a href="javascript:void(0)" data-bind="attr: { href: '{{ url("promociones_imagenes") }}' +'/'+id+'/edit', }"  class="btn btn-outline btn-warning">Editar</a>
                            <a href="javascript:void(0)" data-bind="event:{ click: function(){ vmi.eliminar(id) } }" class="btn btn-outline btn-warning">Eliminar</a>
                        </td>
                    </tr>

               </tbody>
            </table>
         </div>
         
         <div class="d-flex justify-content-between">
            <!--Name-->
            <div style="padding-top: 40px">
                <p>Mostrando del <span data-bind="text: data().from == null ? '0' : data().from"></span> al <span data-bind="text: data().to == null ? '0' : data().to"></span> de <span data-bind="text: data().total == null ? '0' : data().total"></span> registros</p>               
            </div>
            <nav class="my-4">
               <ul class="pagination pagination-circle pg-blue mb-0">
                  <li class="page-item" data-bind="css: data().prev_page_url == null ? 'disabled' : ''"><a class="page-link waves-effect waves-effect" data-bind="click: function() { ir(1) }">« Primera</a></li>
                  <li class="page-item" data-bind="if: data().current_page-100 > 0, click: function() { ir(data().current_page-100) } "><span class="page-link waves-effect waves-effect" data-bind="text: data().current_page-100"></span></li>
                  <li class="page-item" data-bind="if: data().current_page-50 > 0, click: function() { ir(data().current_page-50) } "><span class="page-link waves-effect waves-effect" data-bind="text: data().current_page-50"></span></li>
                  <li class="page-item" data-bind="if: data().current_page-10 > 0, click: function() { ir(data().current_page-10) } "><span class="page-link waves-effect waves-effect" data-bind="text: data().current_page-10"></span></li>
                  <li class="page-item" data-bind="if: data().current_page-5 > 0, click: function() { ir(data().current_page-5) } "><span class="page-link waves-effect waves-effect" data-bind="text: data().current_page-5"></span></li>
                  <li class="page-item" data-bind="if: data().current_page-2 > 0, click: function() { ir(data().current_page-2) } "><span class="page-link waves-effect waves-effect" data-bind="text: data().current_page-2"></span></li>
                  <li class="page-item" data-bind="if: data().current_page-1 > 0, click: function() { ir(data().current_page-1) } "><span class="page-link waves-effect waves-effect" data-bind="text: data().current_page-1"></span></li>
                  <li class="page-item active" ><span class="page-link waves-effect waves-effect" data-bind="text: data().current_page"></span></li>
                  <li class="page-item" data-bind="if: data().current_page+1 <= data().last_page, click: function() { ir(data().current_page+1) } "><span class="page-link waves-effect waves-effect" data-bind="text: data().current_page+1"></span></li>
                  <li class="page-item" data-bind="if: data().current_page+2 <= data().last_page, click: function() { ir(data().current_page+2) } "><span class="page-link waves-effect waves-effect" data-bind="text: data().current_page+2"></span></li>
                  <li class="page-item" data-bind="if: data().current_page+5 <= data().last_page, click: function() { ir(data().current_page+5) } "><span class="page-link waves-effect waves-effect" data-bind="text: data().current_page+5"></span></li>
                  <li class="page-item" data-bind="if: data().current_page+10 <= data().last_page, click: function() { ir(data().current_page+10) } "><span class="page-link waves-effect waves-effect" data-bind="text: data().current_page+10"></span></li>
                  <li class="page-item" data-bind="if: data().current_page+50 <= data().last_page, click: function() { ir(data().current_page+50) } "><span class="page-link waves-effect waves-effect" data-bind="text: data().current_page+50"></span></li>
                  <li class="page-item" data-bind="if: data().current_page+100 <= data().last_page, click: function() { ir(data().current_page+100) } "><span class="page-link waves-effect waves-effect" data-bind="text: data().current_page+100"></span></li>
                  <li class="page-item" data-bind="css: data().last_page == null || data().current_page >= data().last_page ? 'disabled' : ''"><a class="page-link waves-effect waves-effect" data-bind="click: function() { ir(data().last_page) }">Última »</a></li>
               </ul>

            </nav>

            
               
              
            

         </div>

      </div>
   </div>
</section>



@section('js_promocion_imagenes')
<script>
  var vmi = function() {
        self2 = this;
        self2.data                   = ko.observableArray([]);
        self2.page                   = '1';
        self2.fecha_desde            = '';
        self2.fecha_desde_text       = '';
        self2.fecha_hasta            = '';
        self2.fecha_hasta_text       = '';
        self2.estado                 = '';
        self2.estado_text            = '';
        self2.filters                = ko.observableArray([]);
        self2.init = function() {
            self2.ajax();
            $('.select2').select2();
        }
        self2.activarPromocion = function(estado,id){
          $.ajax({
            url: url_normal+"/activar_promocion",
            type: 'GET',
            data    :   {
              '_token': "{{ csrf_token() }}",
              estado: estado,
              id_promocion: id
            },
            success: function(result) {
                            if(estado == true)
               toastr.success('Promoción  Activa');
              else
               toastr.error('Promoción No Activa');
              self2.ajax();
            }
          });

        };
        self2.filterEstado = function(element) {
            self2.page = 1;
            self2.estado = $(element).val()
            self2.estado_text = $(element).find("option:selected").text();
            self2.update_filters();
            self2.ajax();
        };
        self2.fechas_desde = function(id,tipo, f){
            self2.page = 1;
            var d = new Date(f);
            self2.fecha_desde = d.getFullYear() + "-" + ("0"+(d.getMonth()+1)).slice(-2) + "-" + ("0" + d.getDate()).slice(-2) ;;
            self2.fecha_desde_text = ("0" + d.getDate()).slice(-2) + "/" + ("0"+(d.getMonth()+1)).slice(-2) + "/" + d.getFullYear();
            self2.ajax();
            self2.update_filters();  
        };

        self2.fechas_hasta = function(id,tipo, f){
            self2.page = 1;
            var d = new Date(f);
            self2.fecha_hasta = d.getFullYear() + "-" + ("0"+(d.getMonth()+1)).slice(-2) + "-" + ("0" + d.getDate()).slice(-2) ;;
            self2.fecha_hasta_text =  ("0" + d.getDate()).slice(-2) + "/" + ("0"+(d.getMonth()+1)).slice(-2) + "/" + d.getFullYear();
            self2.ajax();
            self2.update_filters();  
        };
        self2.update_filters = function() {
            self2.filters([]);
            if (self2.fecha_desde != '') self2.filters.push({ key: 'Fecha desde',  value: self2.fecha_desde_text, name: 'fecha_desde' });
            if (self2.fecha_hasta != '') self2.filters.push({ key: 'Fecha hasta',  value: self2.fecha_hasta_text, name: 'fecha_hasta' });
            if (self2.estado != '') self2.filters.push({
                key: 'estado_imagen',
                value: self2.estado_text,
                name: 'estado_imagen'
            });

        
        };
        self2.remove_filters = function(name) {
            
            if (name=='fecha_desde') self2.fecha_desde = '', self2.fecha_desde_text = '';
            if (name=='fecha_hasta') self2.fecha_hasta = '', self2.fecha_hasta_text = '';
            if (name == 'estado') {
                self2.estado = '';
                self2.estado_text = '';
            }



            $('#' + name).val("").trigger("change");
            if (name=='fecha_desde') $('#datepicker_desde').val("");
            if (name=='fecha_hasta') $('#datepicker_hasta').val("");
            self2.update_filters();
            self2.ajax();
        };
        self2.ir = function(page) {
            self2.page = page;
            self2.ajax();
            $('html, body').animate({
                scrollTop: 0
            }, 'fast');
        };


            self2.eliminar = function(id) {
    swal({
      title: "Está seguro que desea eliminar esta promoción?",
      text: "",
      icon: "warning",
      buttons: true,
      dangerMode: true,
    })
    .then((willDelete) => {
      if (willDelete) {
          $.ajax({
            url: url_normal_img+'/'+id,
            type: 'DELETE',
            data    :   {
              '_token': "{{ csrf_token() }}"
            },
            success: function(result) {
              toastr.success(result);
              self2.ajax();
            }
          });
        self2.ajax();
      }
    });
  };


        self2.ajax = function() {
            $.getJSON(url_img, {
                    page                  : self2.page,
                    fecha_desde           : self2.fecha_desde,
                    fecha_hasta           : self2.fecha_hasta,
                    estado                : self2.estado
                })
                .done(function(data) {
                    //console.log(data);
                    Pace.restart()
                    vmi.data(data);
                    $("#data_imagenes").fadeIn();
                    $(".loading").remove();
                })
                .error(function(d) {
                    toastr.error('Se encontro un error intente nuevamente');
                });
        };
    };
    $('#datepicker_desde').datepicker({
        language: 'es',
        format: 'dd/mm/yyyy',
        autoclose: true
    }).on('changeDate', function(ev) {
        vmi.fechas_desde(null, null, ev.date);
    });
    $('#datepicker_hasta').datepicker({
        language: 'es',
        format: 'dd/mm/yyyy',
        autoclose: true
    }).on('changeDate', function(ev) {
        vmi.fechas_hasta(null, null, ev.date);
    });
    vmi = new vmi();
    vmi.init();
    ko.applyBindings(vmi, document.getElementById("data_imagenes"));
</script>
@endsection