@extends('template/template')
@section('linkscss')
<link href='{{ asset("assets/sweetalert2/sweetalert2.min.css") }}' type="text/css"     rel="stylesheet"></link>
<link href="{{asset('template/css/home.css')}}" rel="stylesheet">
@endsection
@section('content')
<div class="row wrapper red-bg">
  <div class="col-lg-10">
    <h2>Bienvenido</h2>
  </div>
  <div class="col-lg-2">
  </div>
</div>
<meta name="csrf-token" content="{{ csrf_token() }}">
@can('ver-estadisticas-inicio')
<div class="error-desc">

 <div class="row">
    <div class="col-lg-3">
      <div class="ibox float-e-margins">
        <div class="ibox-title">
          <span class="label label-success pull-right">VINOTECAS</span>
          <h5>CANTIDAD</h5>
        </div>
        <div class="ibox-content">
          @if($data['vinotecas'])
          <h1 class="no-margins">{{$data['vinotecas']}}</h1>
          @else
          <h1 class="no-margins">0</h1>
          <div class="stat-percent font-bold text-success"></div>
          @endif
          <small class="total">TOTAL</small><br>
          @if (count($ultimos["vinotecas"]) > 0)
          <small>Última registrada {{fechaCastellano($ultimos["vinotecas"]->created_at)}}</small>
          @endif
        </div>
      </div>
    </div>
    <div class="col-lg-3">
      <div class="ibox float-e-margins">
        <div class="ibox-title">
          <span class="label label-info pull-right">BODEGAS</span>
          <h5>CANTIDAD</h5>
        </div>
        <div class="ibox-content">
          <h1 class="no-margins">{{$data['bodegas']}}</h1>
          <div class="stat-percent font-bold text-info"></div>
          <small class="total">TOTAL</small><br>
          @if (count($ultimos["bodega"]) > 0)
          <small>Última registrada {{fechaCastellano($ultimos["bodega"]->created_at)}}</small>
          @endif
        </div>
      </div>
    </div>
    <div class="col-lg-3">
      <div class="ibox float-e-margins">
        <div class="ibox-title">
          <span class="label label-primary pull-right">DISTRIBUIDORAS</span>
          <h5>CANTIDAD</h5>
        </div>
        <div class="ibox-content">
          <h1 class="no-margins">{{$data['distribuidoras']}}</h1>
          <div class="stat-percent font-bold text-navy"></div>
          <small class="total">TOTAL</small><br>
          @if (count($ultimos["distribuidoras"]) > 0)
          <small>Última registrada {{fechaCastellano($ultimos["distribuidoras"]->created_at)}}</small>
          @endif
        </div>
      </div>
    </div>
    <div class="col-lg-3">
      <div class="ibox float-e-margins">
        <div class="ibox-title">
          <span class="label label-danger pull-right">PROMOCIONES ACTIVAS</span>
          <h5>CANTIDAD</h5>
        </div>
        <div class="ibox-content">
          <h1 class="no-margins">{{$promociones_activas}}</h1>
          <div class="stat-percent font-bold text-navy"></div>
          <small class="total">TOTAL</small><br>
          <small>Cantidad de promociones que se encuentran activas en el sistema</small>
        </div>
      </div>
    </div>
  </div>
</div>
@endcan
@can('ver-mapa-inicio')
<div class="row">
  <div class="col-md-12">
    <div class="col-md-4">
      <div class="ibox-content">
        <div>
          <div class="feed-activity-list">
            <div class="feed-element">
              <a href="" class="pull-left">
                <img alt="image" class="img-md" src="{{asset('/iconosmapa/bodega.png')}}">
              </a>
              <div class="media-body ">
                <input  class="pull-right rol magic-checkbox" type="checkbox" value="bodega" name="bodega" id="bodega"/>
                <label class="text rightc" for="bodega"></label>
                <div class="alineacionletras">
                  <span class="letraiconos">Bodegas</span>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="col-md-4">
      <div class="ibox-content">
        <div>
          <div class="feed-activity-list">
            <div class="feed-element">
              <a href="" class="pull-left">
                <img alt="image" class="img-md" src="{{asset('/iconosmapa/distribuidora.png')}}">
              </a>
              <div class="media-body ">
                <input  class="pull-right rol magic-checkbox" type="checkbox" value="distribuidora" name="distribuidora" id="distribuidora"/>
                <label class="text rightc" for="distribuidora"></label>
                <div class="alineacionletras">
                  <span class="letraiconos">Distribuidoras</span>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="col-md-4">
      <div class="ibox-content">
        <div>
          <div class="feed-activity-list">
            <div class="feed-element">
              <a href="" class="pull-left">
                <img alt="image" class="img-md" src="{{asset('/iconosmapa/vinoteca.png')}}">
              </a>
              <div class="media-body ">
                <input  class="pull-right rol magic-checkbox" type="checkbox" value="vinoteca" name="vinoteca" id="vinoteca"/>
                <label class="text rightc" for="vinoteca"></label>
                <div class="alineacionletras">
                  <span class="letraiconos">Vinotecas</span>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<br>

<div id="wrapper">
  <div id="map" style="width: 100%; height: 400px;border: 1px solid;"></div>
  <div id="over_map">
    <div class="form-group">
      <select class="select2_demo_2 form-control" name="perfiles" id="perfiles" required>
        <?php $tit = ''; ?>
        <option value=""></option>
        @foreach($perfiles as $index => $al)
        <!-- agrego el titlo al optfroup -->
        @if($al->roles[0]->label != $tit)
        <?php
        //seteo el nuevo valor del titulo
        $tit = $al->roles[0]->label
        ?>
        <optgroup class="opt-{{$al->roles[0]->label}}" label = {{$al->roles[0]->label}}>
          <!-- Si es la primera vez que entra al for no tengo que cerrarlo, pero si ya paso mas de una vez, tengo que cerar el optfgroup viejo -->
          @if($index > 0)
        </optgroup>
        @endif
        @endif
        @if(!empty($al->informacion_adicional))
        <option value="{{$al->informacion_adicional->geo_latitud}}/{{$al->informacion_adicional->geo_longitud}}">{{$al->name}}</option>
        @endif
        @endforeach
        <!-- Cierro siempre el ultimo. -->
      </optgroup>
    </select>
  </div>
</div>
</div>
@endcan
@endsection
@section('js')

<script src="http://maps.google.com/maps/api/js?&key=AIzaSyD9FZU87YCCSGovLg57u9dvi2jLDDz3uyw" type="text/javascript"></script>
<!-- importo la libreria moments -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.15.0/moment.min.js"></script>
<!-- importo todos los idiomas -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.15.0/moment-with-locales.min.js"></script>
<script>
$(document).ready(function() {
  initialize();
  var role = ["vinoteca","bodega","distribuidora"];
  $( "#vinoteca" ).prop( "checked", true );
  $( "#bodega" ).prop( "checked", true );
  $( "#distribuidora" ).prop( "checked", true );
  api(role);

});
var beaches = [];
var map;



function api(role) {
  
    $.ajax({
        url: base_url + '/inicio/mapa/',
        type: 'GET',
        data: {perfiles: role},
        beforeSend: function() {
        },
        success: function(response) {
            var usuarios = response.usuarios;
            beaches = [];
            $.each(usuarios, function(index, value) {
                if (value.informacion_adicional != null) {
                    beaches.push([
                    value.name,
                    value.informacion_adicional.geo_latitud,
                    value.informacion_adicional.geo_longitud,
                    value.informacion_adicional.user_id,
                    value.roles[0].name
                   ])
                }
            });
            console.log(beaches)
            setMarkers(beaches)        
        }
    });

}




function initialize() {
  $.getJSON( "{{url('assets/mapstyle/mapstyle.json')}}", function( mapStyle ) {
    map = new google.maps.Map(document.getElementById('map'), {
      center: {lat: -35.2, lng: -58.2520396},
      zoom: 9,
      styles: mapStyle,
      gestureHandling: 'greedy',
      mapTypeControl: false,
      fullscreenControl: false,
    });
  });
}

function setMarkers(locations) {
  console.log(locations)

    var bounds = new google.maps.LatLngBounds();

    for (var i = 0; i < locations.length; i++) {
        var beach = locations[i];
        var coords = new google.maps.LatLng(beach[1], beach[2]);
        var contentString = '';
        var infowindow = new google.maps.InfoWindow({content: contentString});

        var markerImage = new google.maps.MarkerImage(seticon(beach[4]),
            new google.maps.Size(44, 44, "px", "px"),
            new google.maps.Point(0, 0),
            new google.maps.Point(0, 0),
            new google.maps.Size(40, 40, "px", "px")
        );
        var marker = new google.maps.Marker({
            position: coords,
            map: map,
            icon: markerImage,
            title: beach[0],
            zIndex: beach[3]
        });
        google.maps.event.addListener(marker, 'click',
            function(infowindow, marker) {
                return function() {
                    infowindow.open(map, marker);
                };
            }(infowindow, marker)
        );
        bounds.extend(coords);
        map.fitBounds(bounds);
    }
}



$(".rol").click(function () {
    roles_a_mostrar();
});

function roles_a_mostrar(){
  var role = [];
  $(".rol:checked").each(function() {
    role.push($(this).val());
  });
   api(role);
}


function seticon(val) {
switch (val) {
    case 'distribuidora':
        day = "{{asset('/iconosmapa/distribuidora.png')}}";
        return day;
    case 'vinoteca':
        day = "{{asset('/iconosmapa/vinoteca.png')}}";
        return day;
    case 'bodega':
        day = "{{asset('/iconosmapa/bodega.png')}}";
        return day;
}
}
</script>
@endsection
