@extends('template.template')
@section('linkscss')
<link type="text/css" rel="stylesheet" href="{{ asset('/assets/select2/select2.css') }}">
<link type="text/css" rel="stylesheet" href="{{ asset('/assets/toastr/toastr.min.css') }}">
<link type="text/css" rel="stylesheet" href="{{ asset('/assets/bootstrap-datepicker/css/datepicker.css') }}">
<link type="text/css" rel="stylesheet" href="{{ asset('/assets/sweetalert/sweet-alert.css') }}">
<link href="{{asset('template/css/estilo_tabla.css')}}" rel="stylesheet">
@endsection
@section('content')
<section class="bf-table" id="data" style="display: none; padding-bottom: 20px ">
	<div class="card card-cascade narrower">
		<!--Card image-->
		<div class="view view-cascade gradient-card-header blue-gradient narrower py-2 mx-4 mb-3 d-flex justify-content-between align-items-center">
			<div>
				<!--<button type="button" class="btn btn-outline-white btn-rounded btn-sm px-2 waves-effect waves-light"><i class="fa fa-th-large mt-0"></i></button>
				<button type="button" class="btn btn-outline-white btn-rounded btn-sm px-2 waves-effect waves-light"><i class="fa fa-columns mt-0"></i></button> -->
			</div>
			<span class="white-text">Listado de pedidos</span>
			<div>
				<!-- <a type="button" class="btn btn-outline-white btn-rounded btn-sm px-2 waves-effect waves-light" href="{{ URL::to('producto/create') }}">Nuevo</a> -->
			</div>
		</div>
		<!--/Card image-->
		<div class="px-4">
			
			<div class="table-wrapper table-responsive">
				<!--Table-->
				<div class="btn-group pull-left" data-bind="foreach: filters">
					<span data-bind="click: function() { vm.remove_filters(name) }" title="Click para quitar este filtro" class="label label_table"><i class="fa fa-times-circle" aria-hidden="true"></i>&nbsp;&nbsp;<a style="color: white;" data-bind="text: value"></a></span>
				</div>
				<table class="table table-bordered tabla_custom table-hover mb-0">
					<thead>
						<tr>
							<th class="col_1" style="padding: 0px;">
								<input id="vinoteca" type="text" placeholder="Vinoteca" style="text-align: center;" class="form-control" data-bind="event: { change: function(){vm.filterVinoteca($element)} }">
							</th>
							<th class="col_1" style="padding: 0px;">
								<input id="nombre" type="text" placeholder="Provincia" style="text-align: center;" class="form-control" data-bind="event: { change: function(){vm.filterNombre($element)} }">
							</th>
							<th class="col_1" style="padding: 0px;">
								<input id="municipio" type="text" placeholder="Municipio" style="text-align: center;" class="form-control" data-bind="event: { change: function(){vm.filterMunicipio($element)} }">
							</th>
							<th class="col_1" style="padding: 0px;">
								<input id="localidad" type="text" placeholder="Localidad" style="text-align: center;" class="form-control" data-bind="event: { change: function(){vm.filterLocalidad($element)} }">
							</th>
							<th class="col_1" style="padding: 0px;">
								<input id="direccion" type="text" placeholder="Calle" style="text-align: center;" class="form-control" data-bind="event: { change: function(){vm.filterDireccion($element)} }">
							</th>
							<th class="col_1" style="padding: 0px;">
								<input id="numero" type="text" placeholder="Número" style="text-align: center;" class="form-control" data-bind="event: { change: function(){vm.filterNumero($element)} }">
							</th>
							<th class="text-center" >
								Teléfono
							</th>
							<th class="col_1" style="padding: 0px;">
								<input id="nombre_vino" type="text" placeholder="Producto" style="text-align: center;" class="form-control" data-bind="event: { change: function(){vm.filterNombreVino($element)} }">
							</th>
							<th class="col_1" style="padding: 0px;">
								<input id="bodega" type="text" placeholder="Bodega" style="text-align: center;" class="form-control" data-bind="event: { change: function(){vm.filterBodega($element)} }">
							</th>
							<th class="col_1" style="padding: 0px;">
								<select id="varietal" class="form-control select2" data-bind="event:{ change: function(){ vm.filterVarietal($element) } }" style="width:100%">
									<option value="">Varietal</option>
									@foreach($varietales as $v)
									<option value="{{ $v->id }}">{{ $v->varietal }}</option>
									@endforeach
								</select>
							</th>
							<th class="text-center">
								Año
							</th>
							<!-- <th class="text-left">Acciones</th>-->
						</tr>
					</thead>
					<tbody data-bind="foreach: data().data">
						<tr>
							<td class="col_1 text-center" 	data-bind="text: nombre_vinoteca"></td>
							<td class="col_1 text-center" 	data-bind="text: nombre_provincia"></td>
							<td class="col_1 text-center" 	data-bind="text: nombre_municipio"></td>
							<td class="col_1 text-center" 	data-bind="text: localidad"></td>
							<td class="col_1 text-center" 	data-bind="text: domicilio"></td>
							<td class="col_1 text-center" 	data-bind="text: numero"></td>
							<td class="col_1 text-center" 	data-bind="text: telefono"></td>
							<td class="col_1 text-center" 	data-bind="text: producto_marca"></td>
							<td class="col_1 text-center" 	data-bind="text: nombre_bodega"></td>
							<td class="col_1 text-center" 	data-bind="text: varietal"></td>
							<td class="col_1 text-center" 	data-bind="text: producto_anio"></td>
							<td class="col_1 text-center">
								<!-- ko if: resuelto == false -->
								<a href="javascript:void(0)" data-bind="event:{ click: function(){ vm.atendido(record_id,true) } }"  class="btn  btn-danger  btn-xs">NO RESUELTO</a>
								<!-- /ko -->
								<!-- ko if: resuelto == true -->
								<a href="javascript:void(0)" data-bind="event:{ click: function(){ vm.atendido(record_id,false) } }"  class="btn btn-primary btn-xs">RESUELTO</a>
								<!-- /ko -->
								<a href="javascript:void(0)" data-bind="event:{ click: function(){ vm.eliminar(record_id) } }"  class="btn btn-outline btn-danger btn-xs">ELIMINAR</a>
							</td>
						</tr>
					</tbody>
				</table>
			</div>
			
			<div class="d-flex justify-content-between">
				<div style="padding-top: 40px">
					<p>Mostrando del <span data-bind="text: data().from == null ? '0' : data().from"></span> al <span data-bind="text: data().to == null ? '0' : data().to"></span> de <span data-bind="text: data().total == null ? '0' : data().total"></span> registros</p>
				</div>
				<nav class="my-4">
					<ul class="pagination pagination-circle pg-blue mb-0">
						<li class="page-item" data-bind="css: data().prev_page_url == null ? 'disabled' : ''"><a class="page-link waves-effect waves-effect" data-bind="click: function() { ir(1) }">« Primera</a></li>
						<li class="page-item" data-bind="if: data().current_page-100 > 0, click: function() { ir(data().current_page-100) } "><span class="page-link waves-effect waves-effect" data-bind="text: data().current_page-100"></span></li>
						<li class="page-item" data-bind="if: data().current_page-50 > 0, click: function() { ir(data().current_page-50) } "><span class="page-link waves-effect waves-effect" data-bind="text: data().current_page-50"></span></li>
						<li class="page-item" data-bind="if: data().current_page-10 > 0, click: function() { ir(data().current_page-10) } "><span class="page-link waves-effect waves-effect" data-bind="text: data().current_page-10"></span></li>
						<li class="page-item" data-bind="if: data().current_page-5 > 0, click: function() { ir(data().current_page-5) } "><span class="page-link waves-effect waves-effect" data-bind="text: data().current_page-5"></span></li>
						<li class="page-item" data-bind="if: data().current_page-2 > 0, click: function() { ir(data().current_page-2) } "><span class="page-link waves-effect waves-effect" data-bind="text: data().current_page-2"></span></li>
						<li class="page-item" data-bind="if: data().current_page-1 > 0, click: function() { ir(data().current_page-1) } "><span class="page-link waves-effect waves-effect" data-bind="text: data().current_page-1"></span></li>
						<li class="page-item active" ><span class="page-link waves-effect waves-effect" data-bind="text: data().current_page"></span></li>
						<li class="page-item" data-bind="if: data().current_page+1 <= data().last_page, click: function() { ir(data().current_page+1) } "><span class="page-link waves-effect waves-effect" data-bind="text: data().current_page+1"></span></li>
						<li class="page-item" data-bind="if: data().current_page+2 <= data().last_page, click: function() { ir(data().current_page+2) } "><span class="page-link waves-effect waves-effect" data-bind="text: data().current_page+2"></span></li>
						<li class="page-item" data-bind="if: data().current_page+5 <= data().last_page, click: function() { ir(data().current_page+5) } "><span class="page-link waves-effect waves-effect" data-bind="text: data().current_page+5"></span></li>
						<li class="page-item" data-bind="if: data().current_page+10 <= data().last_page, click: function() { ir(data().current_page+10) } "><span class="page-link waves-effect waves-effect" data-bind="text: data().current_page+10"></span></li>
						<li class="page-item" data-bind="if: data().current_page+50 <= data().last_page, click: function() { ir(data().current_page+50) } "><span class="page-link waves-effect waves-effect" data-bind="text: data().current_page+50"></span></li>
						<li class="page-item" data-bind="if: data().current_page+100 <= data().last_page, click: function() { ir(data().current_page+100) } "><span class="page-link waves-effect waves-effect" data-bind="text: data().current_page+100"></span></li>
						<li class="page-item" data-bind="css: data().last_page == null || data().current_page >= data().last_page ? 'disabled' : ''"><a class="page-link waves-effect waves-effect" data-bind="click: function() { ir(data().last_page) }">Última »</a></li>
					</ul>
				</nav>
			</div>
		</div>
	</div>
</section>
@endsection

@section('js')
<script src='{{ asset("assets/toastr/toastr.min.js") }}'></script>
<script src="{{ asset('assets/select2/select2.full.js') }}"></script>
<script src="{{ asset('assets/moment/moment-with-locales.min.js')}}"></script>
<script src="{{ asset('assets/bootstrap-datepicker/js/bootstrap-datepicker.js') }}"></script>
<script src="{{ asset('assets/bootstrap-datepicker/js/locales/bootstrap-datepicker.es.js') }}"></script>

<script>
moment.locale('es');
var url = "{{ url('api/pedidos_admin') }}";
var url_normal = "{{ url('pedidos_admin') }}";
var vm = function() {
	self = this;
	self.data = ko.observableArray([]);
	self.vinoteca = '';
	self.municipio = '';
	self.localidad = '';
	self.direccion = '';
	self.numero = '';
	self.nombre_vino = '';
	self.bodega = '';
	self.varietal = '';
    self.varietal_text = '';
	self.page = '1';
	self.filters = ko.observableArray([]);
	self.init = function() {
		self.ajax();
		$('.select2').select2();
	}
	self.filterVinoteca = function(element) {
		self.page = 1;
		self.vinoteca = $(element).val()
		self.update_filters();
		self.ajax();
	};
	self.filterMunicipio = function(element) {
		self.page = 1;
		self.municipio = $(element).val()
		self.update_filters();
		self.ajax();
	};
	self.filterLocalidad = function(element) {
		self.page = 1;
		self.localidad = $(element).val()
		self.update_filters();
		self.ajax();
	};
	self.filterDireccion = function(element) {
		self.page = 1;
		self.direccion = $(element).val()
		self.update_filters();
		self.ajax();
	};
	self.filterNumero = function(element) {
		self.page = 1;
		self.numero = $(element).val()
		self.update_filters();
		self.ajax();
	};
	self.filterNombreVino = function(element) {
		self.page = 1;
		self.nombre_vino = $(element).val()
		self.update_filters();
		self.ajax();
	};
	self.filterBodega = function(element) {
		self.page = 1;
		self.bodega = $(element).val()
		self.update_filters();
		self.ajax();
	};
	 self.filterVarietal = function(element) {
	    self.page = 1;
	    self.varietal = $(element).val()
	    self.varietal_text = $(element).find("option:selected").text();
	    self.update_filters();
	    self.ajax();
      };

	self.update_filters = function() {
		self.filters([]);
		if (self.vinoteca != '') self.filters.push({
			key: 'vinoteca',
			value: self.vinoteca,
			name: 'vinoteca'
		});
		if (self.municipio != '') self.filters.push({
			key: 'municipio',
			value: self.municipio,
			name: 'municipio'
		});
		if (self.localidad != '') self.filters.push({
			key: 'localidad',
			value: self.localidad,
			name: 'localidad'
		});
		if (self.direccion != '') self.filters.push({
			key: 'direccion',
			value: self.direccion,
			name: 'direccion'
		});
		if (self.numero != '') self.filters.push({
			key: 'numero',
			value: self.numero,
			name: 'numero'
		});
		if (self.nombre_vino != '') self.filters.push({
			key: 'nombre_vino',
			value: self.nombre_vino,
			name: 'nombre_vino'
		});
		if (self.bodega != '') self.filters.push({
			key: 'bodega',
			value: self.bodega,
			name: 'bodega'
		});
		 if (self.varietal != '') self.filters.push({
                key: 'varietal',
                value: self.varietal_text,
                name: 'varietal'
           });


			
	};
	self.remove_filters = function(name) {
		if (name == 'vinoteca') {
			self.vinoteca = '';
		}
		if (name == 'municipio') {
			self.municipio = '';
		}
		if (name == 'localidad') {
			self.localidad = '';
		}
		if (name == 'direccion') {
			self.direccion = '';
		}
		if (name == 'numero') {
			self.numero = '';
		}
		if (name == 'nombre_vino') {
			self.nombre_vino = '';
		}
		if (name == 'bodega') {
			self.bodega = '';
		}
		if (name == 'varietal') {
			self.varietal = '';
			self.varietal_text = '';
		}
		
		$('#' + name).val("").trigger("change");
		self.update_filters();
		self.ajax();
	};
	self.ir = function(page) {
		self.page = page;
		self.ajax();
		$('html, body').animate({
			scrollTop: 0
		}, 'fast');
	};
	self.eliminar = function(id) {
		swal({
		  title: "Estas seguro?",
		  text: "Una vez que el pedido se elimine,no podra volver ha ser visto!",
		  icon: "warning",
		  buttons: true,
		  dangerMode: true,
		})
		.then((willDelete) => {
		  if (willDelete) {
		  			$.ajax({
			url: url_normal+'/'+id,
			type: 'DELETE',
			data    :   {
				'_token': "{{ csrf_token() }}"
			},
			success: function(result) {
				toastr.success(result);
				self.ajax();
			}
		});
		    swal("Hecho! Su pedido fue eliminado correctamente!", {
		      icon: "success",
		    });
		    self.ajax();
		  } else {
		    swal("Su pedido fue salvado!");
		  }
		});
	};
	self.atendido = function(id,estado) {
		$.ajax({
			url: url_normal+'/'+id+'/resuelto',
			type: 'GET',
			data:{'estado': estado},
			success: function(result) {
				toastr.success(result);
				self.ajax();
				//location.reload();
			}
		});
	};
	self.ajax = function() {
		$.getJSON(url, {
			page: self.page,
			vinoteca: self.vinoteca,
			municipio: self.municipio,
			localidad : self.localidad,
			direccion : self.direccion,
			numero: self.numero,
			nombre_vino : self.nombre_vino,
			bodega: self.bodega,
			varietal: self.varietal

		})
		.done(function(data) {
			console.log(data);
			Pace.restart()
			vm.data(data);
			$("#data").fadeIn();
			$(".loading").remove();
		})
		.error(function(d) {
			toastr.error('Se encontro un error intente nuevamente');
		});
	};
};
vm = new vm();
vm.init();
ko.applyBindings(vm, document.getElementById("data"));

@if(session()->has('message'))
toastr.success('{{ session()->get('message ') }}');
@endif
</script>
@endsection
