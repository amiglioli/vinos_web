@extends('template.template')

@section('content')
<div class="row">
  <div class="col-lg-12 text-center">
    <div class="ibox float-e-margins">
      <div class="ibox-title">
        <h5>Asociar Bodegas</h5>
      </div>
      <div class="ibox-content">
        <form class="validator form-horizontal" action="{{ url('portfolio',$bodega->id) }}" method="post">
          @method('PUT')
          {{ csrf_field() }}
          <div class="form-group">
            <label for="name" class="col-md-4 control-label">Bodegas</label>
            <div class="col-md-6">
              <select class="select2_demo_2 form-control" name="bodegas[]" multiple="multiple" required>
                
                <option value="{{$bodega->id}}" selected>{{$bodega->name}}</option>
                
              </select>
            </div>
          </div>
          <div class="form-group">
            <label for="name" class="col-md-4 control-label">Provincias</label>
            <div class="col-md-6">
              <select class="select2_demo_2 form-control" name="provincia[]" id="provincia"  multiple="multiple" required>
                @foreach($provincias as $sp)
                <option @if($sp->selected) selected @endif value="{{$sp->provincia_id}}">{{$sp->provincia}}</option>
                @endforeach
              </select>
            </div>
          </div>
          <div class="row">
            <div class="col-lg-12">
              <div class="ibox">
                <div class="ibox-title">
                  <h5>Listado de municipios</h5>
                  <div class="ibox-tools">
                    <a class="collapse-link">
                      <i class="fa fa-chevron-up"></i>
                    </a>
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                      <i class="fa fa-wrench"></i>
                    </a>
                    <ul class="dropdown-menu dropdown-user">
                      <li><a href="#">Config option 1</a>
                    </li>
                    <li><a href="#">Config option 2</a>
                  </li>
                </ul>
                <a class="close-link">
                  <i class="fa fa-times"></i>
                </a>
              </div>
            </div>
            <div class="ibox-content">
              <p style="color:red">
                Porfavor seleccione los municipios correspondientes a sus zonas de distribucion
              </p>
              <select class="form-control dual_select" name="municipios[]" id="municipios" multiple>
                @foreach($municipios as $m)
                <option selected="" value="{{$m->provincia_id}}-{{$m->municipio_id}}">{{$m->municipio_id}}</option>
                @endforeach
              </select>
            </div>
          </div>
        </div>
      </div>
      <div class="form-group">
        <div class="col-lg-offset-1 col-lg-10">
          <button class="btn btn-sm btn-primary " type="submit">Asociar</button>
        </div>
      </div>
    </form>
  </div>
</div>
</div>
</div>
@endsection

@section('js')

<script>

$( document ).ready(function() {
     var html = '';
    $(".select2_demo_2").select2();
    var seleccionados = [];
    cargar_array_municipios();
    getValueUsingClass();
    //cargar_array_localidades();
   $('.dual_select').bootstrapDualListbox({
       selectorMinimalHeight: 160
   });

 });

$("#provincia").change(function () {
  getValueUsingClass();
});

function getValueUsingClass(){
  var chkArray = [];
  $("#provincia").each(function() {
    chkArray.push($(this).val());
  });

  var selected;
  selected = chkArray.join(',') ;
  if(selected.length >= 1){
    $.each(chkArray[0], function( index, value ) {
      html = '';
      ajax_municipios(value);
    });
    refrecarSeleccionados();
  }else{
    html = '';
    $('select[id="municipios"]').html(html);
    $('select[id=municipios]').bootstrapDualListbox('refresh', true);
    refrecarSeleccionados();
  }
}

$("#municipios").change(function(){
  refrecarSeleccionados();
});

function ajax_municipios(provincia_id){
    var base_url = "<?= URL::to('') ?>";
    base_url = base_url.trim();
   $.ajax({
      url:  base_url+'/api/provincias/',
      type:  'GET',
      dataType : 'json',
      data : { provincia : provincia_id },
      beforeSend: function () {
        $("#resultado").html("Procesando, espere por favor...");
      },
      success:  function (response) {
        $.each(response.data[0].municipio, function( index, value ) {
          
          if(jQuery.inArray(parseInt(value.municipio_id), seleccionados[value.provincia_id]) !== -1){
            html+= "<option  selected value="+value.provincia_id+"-"+value.municipio_id+">"+value.municipio+"</option>"

          }
          else{
            html+= "<option value="+value.provincia_id+"-"+value.municipio_id+">"+value.municipio+"</option>"
          }
        });
        $('select[id="municipios"]').html(html);
        $('select[id=municipios]').bootstrapDualListbox('refresh', true);
      }
    });
}



function cargar_array_municipios()
{
  var select = $("#municipios").val();
  seleccionados = [];
  if(select != null){
    for (var i = 0; i < select.length; ++i) {
      var j = select[i].split('-');
      var provincia = parseInt(j[0]);
      var municipio = parseInt(j[1]);
      if(seleccionados[provincia] != null){
        seleccionados[provincia].push(municipio);
      }else{
        seleccionados[provincia] = [];
        seleccionados[provincia].push(municipio);
      }
    }
  }

}

function refrecarSeleccionados(){
  select = $("#municipios").val();
  cargar_array_municipios();
}

</script>
@endsection
