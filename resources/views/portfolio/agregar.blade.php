@extends('template.template')
@section('head_content')
@section('content')
<div class="row">
  <div class="col-lg-12 text-center">
    <div class="ibox float-e-margins">
      <div class="ibox-title">
        <h5>Ingrese las bodegas que representa, con su correspondiente zona de distribución</h5>
      </div>
      <div class="ibox-title">
        <p style="float:left">Las Bodegas que se distribuyen en la misma zona, podrán ser cargadas en forma simultánea. Ver ejemplo” (a futuro mostraremos una imagen)
        </p>
      </div>
      <div class="ibox-content">
        <form class="validator form-horizontal" action="{{ URL::to('portfolio') }}" method="post">
          {{ csrf_field() }}
          <div class="form-group">
            <label for="name" class="col-md-4 control-label" style="margin-top:2%;">Seleccione Bodegas</label>
            <div class="col-md-6">
              <a type="button"  data-toggle="modal" data-target="#myModal2">
                No encuentro la Bodega
              </a>
              <select class="form-control" id="selectbodega" name="bodegas[]" multiple="multiple" required>
                @foreach($bodegas as $b)
                <option value="{{$b->id}}">{{$b->name}}</option>
                @endforeach
              </select>
            </div>
          </div>
          <div class="form-group">
            <label for="name" class="col-md-4 control-label" style="margin-top:0%;">Seleccione Provincias</label>
            <div class="col-md-6">
              <select class="select2_demo_2 form-control" name="provincia[]" id="provincia"  multiple="multiple" required>
                @foreach($provincias as $p)
                <option value="{{$p->provincia_id}}">{{$p->provincia}}</option>
                @endforeach
              </select>
            </div>
          </div>
          <div class="row">
            <div class="col-lg-12">
              <div class="ibox">
                <div class="ibox-title">
                  <h5>Marque en el cuadro inferior izquierdo, el/los Partidos en los que distribuye las Bodegas seleccionadas</h5>
                </div>
                <div class="col-md-12">
                  <div class="col-md-6">
                    Partidos de la Provincia
                  </div>
                  <div class="col-md-6">
                    Partidos que distribuye
                  </div>
                </div>
                <div class="ibox-content">
                  <select class="form-control dual_select" name="municipios[]" id="municipios" multiple>
                  </select>
                </div>
              </div>
            </div>
          </div>
          <div class="form-group">
            <div class="col-lg-offset-1 col-lg-10">
              <button class="btn btn-sm btn-primary " type="submit">Guardar</button>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>
@include('portfolio.modal')

@endsection

@section('js')
<script src="{{asset('js/baseurl.js')}}" type="text/javascript" ></script>

<script>

$( document ).ready(function() {
  var html = '';
  $('.dual_select').bootstrapDualListbox({
    selectorMinimalHeight: 160
  });
  $(".select2_demo_2").select2({
    placeholder: 'Buscar Provincias',
  });
    $('#selectbodega').select2({
    width: '100%',
    placeholder: 'Buscar Bodegas',
    language: {
      noResults: function() {
        return '<a id="no-results-btn">No hay resultados, seleccione: No encuentro la Bodega</a>';
      },
    },
    escapeMarkup: function(markup) {
      return markup;
    },
  });
});

var seleccionados = [];
$("#provincia").change(function () {
  getValueUsingClass();
});



function getValueUsingClass(){
  var chkArray = [];
  $("#provincia").each(function() {
    chkArray.push($(this).val());
  });

  var selected;
  selected = chkArray.join(',') ;
  if(selected.length >= 1){
    $.each(chkArray[0], function( index, value ) {
      html = '';
      ajax_municipios(value);
    });
    refrecarSeleccionados();
  }else{
    html = '';
    $('select[id="municipios"]').html(html);
    $('select[id=municipios]').bootstrapDualListbox('refresh', true);
    refrecarSeleccionados();
  }
}

$("#municipios").change(function(){
  refrecarSeleccionados();
});

function ajax_municipios(provincia_id){
    var base_url = "<?= URL::to('') ?>";
    base_url = base_url.trim();
   $.ajax({
      url:  base_url+'/api/provincias/',
      type:  'GET',
      dataType : 'json',
      data : { provincia : provincia_id },
      beforeSend: function () {
        $("#resultado").html("Procesando, espere por favor...");
      },
      success:  function (response) {
        $.each(response.data[0].municipio, function( index, value ) {
          
          if(jQuery.inArray(parseInt(value.municipio_id), seleccionados[value.provincia_id]) !== -1){
            html+= "<option  selected value="+value.provincia_id+"-"+value.municipio_id+">"+value.municipio+"</option>"

          }
          else{
            html+= "<option value="+value.provincia_id+"-"+value.municipio_id+">"+value.municipio+"</option>"
          }
        });
        $('select[id="municipios"]').html(html);
        $('select[id=municipios]').bootstrapDualListbox('refresh', true);
      }
    });
}



function cargar_array_municipios()
{
  var select = $("#municipios").val();
  seleccionados = [];
  if(select != null){
    for (var i = 0; i < select.length; ++i) {
      var j = select[i].split('-');
      var provincia = parseInt(j[0]);
      var municipio = parseInt(j[1]);
      if(seleccionados[provincia] != null){
        seleccionados[provincia].push(municipio);
      }else{
        seleccionados[provincia] = [];
        seleccionados[provincia].push(municipio);
      }
    }
  }

}

function refrecarSeleccionados(){
  select = $("#municipios").val();
  cargar_array_municipios();
}
</script>
@endsection
