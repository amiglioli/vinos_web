<div class="modal inmodal" id="myModal2" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog">
    <form class="validator form-horizontal" action="{{ URL::to('pedido_bodega_distribuidora') }}" method="post">
      {{ csrf_field() }}
      <div class="modal-content animated flipInY">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
          <h4 class="modal-title">Solicitud de requerimiento</h4>
          <small class="font-bold">Ingrese la Bodega deseada y recibirá una notificación cuando esté incorporada al sistema.</small>
        </div>
        <div class="modal-body">
          <div class="form-group{{ $errors->has('nombre_bodega') ? ' has-error' : '' }} has-success">
            <label for="bodega" class="col-md-4 control-label">Nombre Bodega</label>
            <div class="col-md-6">
              <input id="nombre_bodega" type="text" class="form-control " name="nombre_bodega" value="{{ old('nombre_bodega') }}" required>
              @if ($errors->has('descripcion'))
              <span class="help-block">
                <strong>{{ $errors->first('nombre_bodega') }}</strong>
              </span>
              @endif
            </div>
          </div>
         <!-- 
          <div class="form-group{{ $errors->has('descripcion_solicitud') ? ' has-error' : '' }} has-success">
            <label for="descripcion_solicitud" class="col-md-4 control-label">Descripción</label>
            <div class="col-md-6">
              <input id="descripcion_solicitud" type="text" class="form-control" name="descripcion_solicitud" value="{{ old('descripcion_solicitud') }}" required>
              @if ($errors->has('descripcion'))
              <span class="help-block">
                <strong>{{ $errors->first('descripcion_solicitud') }}</strong>
              </span>
              @endif
            </div>
          </div>
        </div>
        -->
        <div class="modal-footer">
          <button type="button" class="btn btn-danger" data-dismiss="modal">Cancelar</button>
          <button type="submit" class="btn btn-primary">Enviar</button>
        </div>
      </div>
    </form>
  </div>
</div>