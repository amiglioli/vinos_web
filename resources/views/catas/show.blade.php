@extends('template.template')
@section('linkscss')
<link type="text/css" rel="stylesheet" href="{{ asset('/assets/select2/select2.css') }}">
<link type="text/css" rel="stylesheet" href="{{ asset('/assets/toastr/toastr.min.css') }}">
<link type="text/css" rel="stylesheet" href="{{ asset('/assets/bootstrap-datepicker/css/datepicker.css') }}">
<link type="text/css" rel="stylesheet" href="{{ asset('/assets/sweetalert/sweet-alert.css') }}">
<link href="{{asset('template/css/estilo_tabla.css')}}" rel="stylesheet">
<link href="{{asset('template/css/home.css')}}" rel="stylesheet">
@endsection
@section('content')
<div class="col-sm-12">
  <div class="ibox">
    <div class="ibox-content">
      <h2>{{$data->descripcion}}</h2>
      <p>
        Realizada en <i>{{$data->lugar}}</i> el día <i>{{fechaCastellano($data->fecha)}}</i>
      </p>
      <div class="">
        <ul class="nav nav-tabs">
          <li class="active"><a data-toggle="tab" href="#tab-productos"><i class="fa fa-glass"></i>Productos</a></li>
          <li><a data-toggle="tab" href="#tab-catadores"><i class="fa fa-users"></i>Catadores</a></li>
        </ul>
        <div class="tab-content">

          <div id="tab-productos" class="tab-pane active">
              <div class="slimScrollDiv" style="position: relative; overflow: hidden; width: auto;"><div>
                <div class="table-responsive">
                  <table class="table table-striped table-hover">
                    <thead>
                      <th>Puesto</th>
                      <th>Puntaje</th>
                      <th>Bodega</th>
                      <th>Marca</th>
                      <th>Varietal</th>
                      <th>Año</th>
                    </thead>
                    <tbody>
                      @if(!empty($data->productos_cata))
                      @foreach($data->ranking->resultado as $p)
                      <tr>
                        <td>{{$p->puesto}}</td>
                        <td>{{decimal($p->puntaje)}}</td>
                        <td>{{$p->producto->bodega->name}}</td>
                        <td>{{$p->producto->marca}}</td>
                        <td>{{$p->producto->varietales->varietal}}</td>
                        <td>{{$p->producto->anio}}</td>
                        <td><button class="btn btn-success btn-outline btn-sm" onclick="ver_producto({{$p->producto->id}})">Ver Detalle</button></td>
                      </tr>
                      @endforeach
                      @else
                      <tr>
                        <td style="text-align: center" colspan="9">No hay vinos disponibles</td>
                      </tr>
                      @endif
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>
          <div id="tab-catadores" class="tab-pane">
              <div class="slimScrollDiv" style="position: relative; overflow: hidden; width: auto;"><div>
                <div class="table-responsive">
                  <table class="table table-striped table-hover">
                    <thead>
                      <th>Apellido</th>
                      <th>Nombre</th>
                      <th>Especialización</th>
                      <th>Edad</th>

                    </thead>
                    <tbody>
                      @if(!empty($data->catadores_cata))
                      @foreach($data->catadores_cata as $catador)
                      <tr>
                        <td>{{$catador->apellido}}</td>
                        <td>{{$catador->nombre}}</td>
                        <td>{{$catador->especializacion->especializacion}}</td>
                        <td>{{edad_momento_cata($catador->edad,$data->fecha)}}</td>

                      </tr>
                      @endforeach
                      @else
                      <tr>
                        <td style="text-align: center" colspan="9">No hay vinos disponibles</td>
                      </tr>
                      @endif
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
@include('catas_ranking.vervinodetalle')
@endsection

@section('js')
<script>
 function ver_producto(id) {
  var urlproducto = "{{ url('api/productos') }}";
  $("#p_modal").css('display','block');
  $("#table_productos").css('display','none');
  $("#modal_producto").modal('show');
    $.getJSON(urlproducto, {
      page: 1,
      producto_id: id
    })
    .done(function(data) {
        data = data.data[0];
        console.log(data)
        if(data.img_etiqueta == null)
          $("#m_imagen").attr('src',"{{asset('template/img/fondo-usuario.jpg')}}");
        else
          $("#m_imagen").attr('src',"{{asset('')}}"+"/"+data.img_etiqueta);

        $("#m_producto").html(data.producto.nombre);
        $("#m_tipo").html(data.producto_tipo.tipo);
        $("#m_varietal").html(data.varietales.varietal);
        $("#m_marca").html(data.marca);
        $("#m_bodega").html(data.bodega.name);
        $("#m_anio").html(data.anio);
        $("#m_pais").html(data.pais.nombre);
        $("#m_provincia").html(data.provincia.provincia);
        $("#m_region").html(data.region.nombre);
        $("#m_ciudad").html(data.ciudad.nombre);


        $("#p_modal").css('display','none');
        $("#table_productos").css('display','table');


    })
    .error(function(d) {
      toastr.error('Se encontro un error intente nuevamente');
    });
  };
</script>
@endsection
