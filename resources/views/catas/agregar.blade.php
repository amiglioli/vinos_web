  @extends('template.template')

  @section('head_content')
  <style>
    .classWithPad { margin:10px; padding:10px; }
  </style>
  @endsection
  @section('content')
  <div class="row">
    <div class="col-lg-12 text-center">
      <div class="ibox float-e-margins">
        <div class="ibox-title">
          <h5>Alta de Catas</h5>
        </div>
        <div class="ibox-content">
         <form class="validator form-horizontal" action="{{ url('catas') }}" method="post">
           {{ csrf_field() }}

           <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}" id="data_1">
             <label for="fecha" class="col-md-3 control-label">Fecha</label>

             <div class="col-md-6 input-group date" >
               <span class="input-group-addon"><i class="fa fa-calendar"></i></span><input name="fecha" id="fecha" type="text" class="form-control" value="{{ old('fecha',date('Y-m-d'))}}">
               @if ($errors->has('fecha'))
               <span class="help-block">
                 <strong>{{ $errors->first('fecha') }}</strong>
               </span>
               @endif
             </div>
           </div>

           <div class="form-group"><label class="col-lg-3 control-label">Lugar</label>
            <div class="col-lg-6">
              <input type="text" name="lugar" placeholder="INGRESE EL LUGAR" class="form-control" required>
            </div>
            @if ($errors->has('lugar'))
            <span class="help-block">
             <strong>{{ $errors->first('lugar') }}</strong>
           </span>
           @endif
         </div>


         <div class="form-group"><label class="col-lg-3 control-label">Descripción</label>
          <div class="col-lg-6">
            <input type="text" name="descripcion" placeholder="INGRESE DESCRIPCION" class="form-control" required>
          </div>
          @if ($errors->has('descripcion'))
          <span class="help-block">
           <strong>{{ $errors->first('descripcion') }}</strong>
         </span>
         @endif
       </div>

       <div class="form-group"><label class="col-lg-3 control-label">Productos</label>
        <div class="col-md-6">
          <select class="form-control select2" id="productos" required>
           <option value="">PRODUCTOS</option>
           @foreach($productos as $p)
           <option value="{{$p->id}}">{{$p->marca}}</option>
           @endforeach
         </select>
         @if ($errors->has('productos'))
         <span class="help-block">
           <strong>{{ $errors->first('productos') }}</strong>
         </span>
         @endif
       </div>

     </div>

     <div class="form-group"><label class="col-lg-3 control-label">Catadores</label>
      <div class="col-md-6">
        <select class="form-control select2" name="catadores[]" multiple="multiple" required>
          @foreach($catadores as $c)
          <option value="{{$c->id}}">{{$c->nombre}} {{$c->apellido}}({{$c->dni}})</option>
          @endforeach
        </select>
        @if ($errors->has('catadores'))
        <span class="help-block">
         <strong>{{ $errors->first('catadores') }}</strong>
       </span>
       @endif
     </div>

   </div>

   <div class="col-md-12" id="muestra_nro">

    @if(old('productos'))
    @php $nombre_productos =  old('nombre_productos'); @endphp
    @foreach(old('productos') as $key => $pc)
    <div id="animation_box{{$key}}" class="animated">
     <div class="col-md-2">
      <div class="form-group">
       <label><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">{{$nombre_productos[$key]}}</font></font></label>
       <a onclick="remove('{{$key}}')" class="fa fa-close" aria-hidden="true"></a>
       <input name="productos[{{$key}}]" type="text" required placeholder="Numero de Muestra" class="form-control" value="{{$pc}}">
       <input name="nombre_productos[{{$key}}]" type="hidden" value="{{$nombre_productos[$key]}}">
     </div>
   </div>
 </div>
 @endforeach
 @endif

</div>

<div class="form-group">
  <div class="col-lg-offset-1 col-lg-10">
    <button class="btn btn-sm btn-primary " type="submit">Completar Registro</button>
  </div>
</div>
</form>
</div>
</div>
</div>
</div>
@endsection
@section('js')
<script src="{{asset('template/js/datees.js')}}" type="text/javascript" ></script>
<script>
  var id_productos = [];
  var id
  $( document ).ready(function() {
   $(".select2").select2();

   $('#data_1 .input-group.date').datepicker({
     todayBtn: "linked",
     language: "es",
     startDate: new Date(),
     keyboardNavigation: false,
     forceParse: false,
     calendarWeeks: true,
     autoclose: true,
     format: "yyyy-mm-dd"
   });

   $('.dual_select').bootstrapDualListbox({
    selectorMinimalHeight: 160
  });
   

 });

  function remove(id){

    $('#animation_box'+id).removeAttr('class').attr('class', '');
    var animation = $(this).attr("data-animation");
    $('#animation_box'+id).addClass('animated');
    $('#animation_box'+id).addClass('rollOut');

    setTimeout(function(){
     $('#animation_box'+id).remove();
   }, 300);
    

    id_productos = $.grep(id_productos, function(value) {
      return value != id;
    });

    $('#productos').val("")
    
  }

  $('#productos').change(function(){
    var html = '';
    var nombre = '';

    id = $(this).val();


    if(jQuery.inArray(id, id_productos) != -1) {
      setTimeout(function() {
        toastr.options = {
          closeButton: true,
          progressBar: true,
          showMethod: 'slideDown',
          timeOut: 4000
        };
        toastr.error('Este producto ya esta seleccionado en su cata', 'Error');

      }, 500);
    } else {
     id_productos.push(id);
     nombre = $("#productos option:selected").text();
     html+='<div id="animation_box'+id+'" class="animated">'
     html+='<div class="col-md-4">';
     html+=' <div class="form-group">';
     html+='<label><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">'+nombre+'</font></font></label>';
     html+='<a onclick="remove('+id+')" class="fa fa-close" style="color:red" aria-hidden="true"></a>'
     html+='<input name="productos['+id+']" type="text" placeholder="Numero de Muestra" class="form-control" required>';
     html+='<input name="nombre_productos['+id+']" type="hidden" value="'+nombre+'" required>';
     html+='</div>'
     html+='</div>'
     $('#muestra_nro').append(html);

     $('#animation_box'+id).removeAttr('class').attr('class', '');
     var animation = $(this).attr("data-animation");
     $('#animation_box'+id).addClass('animated');
     $('#animation_box'+id).addClass('slideInRight');
   } 

 });
</script>
@endsection
