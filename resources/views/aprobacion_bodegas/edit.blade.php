@extends('template.template')

@section('content')
<div class="row">
  <div class="col-lg-12 text-center">
    <div class="ibox float-e-margins text-center">
      <div class="ibox-title">
        <h5>Vincular bodega ha un usuario existente.</h5>
      </div>

      <div class="ibox-content">
        <form class="validator form-horizontal" id="form" action="{{url('aprobacion_bodega')}}" method="post">
          {{ csrf_field() }}
          <input type="hidden" name="id" value="{{$data->id}}">
          <div class="form-group">
            <label for="name" class="col-md-4 control-label">Vincular con la Bodega</label>
            <div class="col-md-6">
              <select class="form-control select2" name="bodega" tabindex="2" required>
                <option selected value="no_quiero_vincular">No deseo asociar con ninguna..</option>
                @foreach($bodegas_vincular as $bv)
              <option  value="{{$bv->id}}">{{$bv->name}}</option>
                @endforeach

              </select>

            </div>

          </div>

          <div class="form-group">
            <label for="descripcion" class="col-md-4 control-label">Nombre</label>
            <div class="col-md-6">
              <input id="nombre" disabled type="text" class="form-control" name="nombre" value="{{$data->name}}" required>
            </div>
          </div>


          <div class="form-group">
            <label for="descripcion" class="col-md-4 control-label">Persona Contacto</label>
            <div class="col-md-6">
              <input id="persona_contacto" disabled type="text" class="form-control" name="persona_contacto" value="{{$data->informacion_contacto->persona_contacto}}" required>
            </div>
          </div>

          <div class="form-group">
            <label for="descripcion" class="col-md-4 control-label">Email</label>

            <div class="col-md-6">
              <input id="email" disabled type="text" class="form-control" name="email" value="{{ $data->email }}" required>
            </div>
          </div>

          <div class="form-group">
            <label for="descripcion" class="col-md-4 control-label">Provincia</label>

            <div class="col-md-6">
              <input id="provincia" disabled type="text" class="form-control" name="provincia" value="{{ $data->informacion_contacto->provincias->provincia }}" required>
            </div>
          </div>

          <div class="form-group">
            <div class="col-md-6 col-md-offset-4">
              <button type="submit" class="btn btn-primary">
                Dar de Alta
              </button>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>
@endsection
@section('js')
<script>
$( document ).ready(function() {
        $('.select2').select2();
});
</script>
@endsection
