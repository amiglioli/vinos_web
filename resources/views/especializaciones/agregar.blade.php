  @extends('template.template')
  @section('head_content')
  @endsection
  @section('content')
  <div class="row">
    <div class="col-lg-12 text-center">
      <div class="ibox float-e-margins">
        <div class="ibox-title">
          <h5>Alta de producto</h5>
        </div>
        <div class="ibox-content">
         <form class="validator form-horizontal" action="{{url('especializacion')}}" method="post">
           {{ csrf_field() }}

          <div class="form-group"><label class="col-lg-3 control-label">Especialización</label>
            <div class="col-lg-6">
              <input type="text" name="nombre" placeholder="INGRESE LA ESPECIALIZACIÓN" class="form-control" required>
            </div>
          </div>

          <div class="form-group">
            <div class="col-lg-offset-1 col-lg-10">
              <button class="btn btn-sm btn-primary " type="submit">Completar Registro</button>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
  </div>
  @endsection

