  @extends('template.template')
  @section('head_content')
  @endsection
  @section('content')
  <div class="row">
    <div class="col-lg-12 text-center">
      <div class="ibox float-e-margins">
        <div class="ibox-title">
          <h5>Editar Especialización : <span style="color:green">{{$especializacion->especializacion}}</span></h5>
        </div>
        <div class="ibox-content">
         <form class="validator form-horizontal" action="{{ url('especializaciones',$especializacion->id) }}" method="post">
           @method('PUT')
           {{ csrf_field() }}
          <input name="id" type="hidden" value="{{ $especializacion->id }}"/>
          <div class="form-group"><label class="col-lg-3 control-label">Especialización</label>
            <div class="col-lg-6">
              <input type="text" name="especializacion" value="{{$especializacion->especializacion}}" placeholder="INGRESE EL PODUCTO" class="form-control" required>
            </div>
          </div>
          <div class="form-group">
            <div class="col-lg-offset-1 col-lg-10">
              <button class="btn btn-sm btn-primary " type="submit">Guardad cambios</button>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
  </div>
  @endsection
