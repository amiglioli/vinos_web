@extends('template.template')
@section('linkscss')
<link type="text/css" rel="stylesheet" href="{{ asset('/assets/select2/select2.css') }}">
<link type="text/css" rel="stylesheet" href="{{ asset('/assets/toastr/toastr.min.css') }}">
<link type="text/css" rel="stylesheet" href="{{ asset('/assets/bootstrap-datepicker/css/datepicker.css') }}">
<link type="text/css" rel="stylesheet" href="{{ asset('/assets/sweetalert/sweet-alert.css') }}">
<link href="{{asset('template/css/estilo_tabla.css')}}" rel="stylesheet">
@endsection
 
@section('content')
<section class="bf-table" id="data" style="display: none; padding-bottom: 20px ">
   <div class="card card-cascade narrower">
      <!--Card image-->
      <div class="view view-cascade gradient-card-header blue-gradient narrower py-2 mx-4 mb-3 d-flex justify-content-between align-items-center">
         <div>
            <!--<button type="button" class="btn btn-outline-white btn-rounded btn-sm px-2 waves-effect waves-light"><i class="fa fa-th-large mt-0"></i></button>
               <button type="button" class="btn btn-outline-white btn-rounded btn-sm px-2 waves-effect waves-light"><i class="fa fa-columns mt-0"></i></button> -->
         </div>
         <span class="white-text">Listado de ciudades</span>
         <div>
            <a type="button" class="btn btn-outline-white btn-rounded btn-sm px-2 waves-effect waves-light" href="{{ URL::to('ciudades/create') }}">Agregar ciudad</a>
         </div>
      </div>
      <!--/Card image-->
      <div class="px-4">
        
         <div class="table-wrapper table-responsive">
            <!--Table-->
            <div class="btn-group pull-left" data-bind="foreach: filters">
          <span data-bind="click: function() { vm.remove_filters(name) }" title="Click para quitar este filtro" class="label label_table"><i class="fa fa-times-circle" aria-hidden="true"></i>&nbsp;&nbsp;<a style="color: white;" data-bind="text: value"></a></span>
        </div>
            <table class="table table-bordered tabla_custom table-hover mb-0">
                    <thead>
                        <tr>
                            <th class="col_1" style="padding: 0px;">
                                <select id="ciudad" class="form-control select2" data-bind="event:{ change: function(){ vm.filterCiudad($element) } }" style="width:100%">
                                    <option value="">Ciudades</option>
                                    @foreach($ciudades as $ciudad)
                                    <option value="{{ $ciudad->id }}">{{ $ciudad->nombre }}</option>
                                    @endforeach
                                </select>
                            </th>
                            <th class="col_1" style="padding: 0px;">
                                <select id="provincia" class="form-control select2" data-bind="event:{ change: function(){ vm.filterProvincia($element) } }" style="width:100%">
                                    <option value="">Provincia</option>
                                    @foreach($provincias as $provincia)
                                    <option value="{{ $provincia->provincia_id }}">{{ $provincia->provincia }}</option>
                                    @endforeach
                                </select>
                            </th>
                            <th class="col_1" style="padding: 0px;">
                                <select id="pais" class="form-control select2" data-bind="event:{ change: function(){ vm.filterPais($element) } }" style="width:100%">
                                    <option value="">Pais</option>
                                    @foreach($paises as $pais)
                                    <option value="{{ $pais->id }}">{{ $pais->nombre }}</option>
                                    @endforeach
                                </select>
                            </th>
                            <th class="text-left"></th>
                        </tr>
                    </thead>
                    <tbody data-bind="foreach: data().data">
                        <tr>
                            <td class="col_1 text-left"     data-bind="text: nombre"></td>
                            <td class="col_1 text-left"     data-bind="text: provincia.provincia"></td>
                            <td class="col_1 text-left"     data-bind="text: provincia.pais.nombre"></td>

                            <td class="col_1 text-center">
                                <a href="javascript:void(0)" data-bind="attr: { href: '{{ url("ciudades") }}' +'/'+id+'/edit', }"  class="btn btn-outline btn-success btn-sm">Editar</a>
                            	<a href="javascript:void(0)" data-bind="event:{ click: function(){ vm.eliminar(id) } }"  class="btn btn-outline btn-danger btn-sm">Eliminar</a>
                            </td>
                        </tr>
                    </tbody>
                </table>
               </div>
         
         <div class="d-flex justify-content-between">
            <div style="padding-top: 40px">
                <p>Mostrando del <span data-bind="text: data().from == null ? '0' : data().from"></span> al <span data-bind="text: data().to == null ? '0' : data().to"></span> de <span data-bind="text: data().total == null ? '0' : data().total"></span> registros</p>               
            </div>
            <nav class="my-4">
               <ul class="pagination pagination-circle pg-blue mb-0">
                  <li class="page-item" data-bind="css: data().prev_page_url == null ? 'disabled' : ''"><a class="page-link waves-effect waves-effect" data-bind="click: function() { ir(1) }">« Primera</a></li>
                  <li class="page-item" data-bind="if: data().current_page-100 > 0, click: function() { ir(data().current_page-100) } "><span class="page-link waves-effect waves-effect" data-bind="text: data().current_page-100"></span></li>
                  <li class="page-item" data-bind="if: data().current_page-50 > 0, click: function() { ir(data().current_page-50) } "><span class="page-link waves-effect waves-effect" data-bind="text: data().current_page-50"></span></li>
                  <li class="page-item" data-bind="if: data().current_page-10 > 0, click: function() { ir(data().current_page-10) } "><span class="page-link waves-effect waves-effect" data-bind="text: data().current_page-10"></span></li>
                  <li class="page-item" data-bind="if: data().current_page-5 > 0, click: function() { ir(data().current_page-5) } "><span class="page-link waves-effect waves-effect" data-bind="text: data().current_page-5"></span></li>
                  <li class="page-item" data-bind="if: data().current_page-2 > 0, click: function() { ir(data().current_page-2) } "><span class="page-link waves-effect waves-effect" data-bind="text: data().current_page-2"></span></li>
                  <li class="page-item" data-bind="if: data().current_page-1 > 0, click: function() { ir(data().current_page-1) } "><span class="page-link waves-effect waves-effect" data-bind="text: data().current_page-1"></span></li>
                  <li class="page-item active" ><span class="page-link waves-effect waves-effect" data-bind="text: data().current_page"></span></li>
                  <li class="page-item" data-bind="if: data().current_page+1 <= data().last_page, click: function() { ir(data().current_page+1) } "><span class="page-link waves-effect waves-effect" data-bind="text: data().current_page+1"></span></li>
                  <li class="page-item" data-bind="if: data().current_page+2 <= data().last_page, click: function() { ir(data().current_page+2) } "><span class="page-link waves-effect waves-effect" data-bind="text: data().current_page+2"></span></li>
                  <li class="page-item" data-bind="if: data().current_page+5 <= data().last_page, click: function() { ir(data().current_page+5) } "><span class="page-link waves-effect waves-effect" data-bind="text: data().current_page+5"></span></li>
                  <li class="page-item" data-bind="if: data().current_page+10 <= data().last_page, click: function() { ir(data().current_page+10) } "><span class="page-link waves-effect waves-effect" data-bind="text: data().current_page+10"></span></li>
                  <li class="page-item" data-bind="if: data().current_page+50 <= data().last_page, click: function() { ir(data().current_page+50) } "><span class="page-link waves-effect waves-effect" data-bind="text: data().current_page+50"></span></li>
                  <li class="page-item" data-bind="if: data().current_page+100 <= data().last_page, click: function() { ir(data().current_page+100) } "><span class="page-link waves-effect waves-effect" data-bind="text: data().current_page+100"></span></li>
                  <li class="page-item" data-bind="css: data().last_page == null || data().current_page >= data().last_page ? 'disabled' : ''"><a class="page-link waves-effect waves-effect" data-bind="click: function() { ir(data().last_page) }">Última »</a></li>
               </ul>

            </nav>

         </div>

      </div>
   </div>
</section>
@endsection

@section('js')
<script src='{{ asset("assets/toastr/toastr.min.js") }}'></script>
<script src="{{ asset('assets/select2/select2.full.js') }}"></script>
<script src="{{ asset('assets/moment/moment-with-locales.min.js')}}"></script>
<script src="{{ asset('assets/bootstrap-datepicker/js/bootstrap-datepicker.js') }}"></script>
<script src="{{ asset('assets/bootstrap-datepicker/js/locales/bootstrap-datepicker.es.js') }}"></script>
<script>
    moment.locale('es');
    var url = "{{ url('api/ciudades') }}";
    var url_normal = "{{ url('ciudades') }}";
    var vm = function() {
        self = this;
        self.data = ko.observableArray([]);
        self.ciudad = '';
        self.ciudad_text = '';
        self.provincia = '';
        self.provincia_text = '';
        self.pais = '';
        self.pais_text = '';
        self.page = '1';
        self.filters = ko.observableArray([]);
        self.init = function() {
            self.ajax();
            $('.select2').select2();
        }
        self.filterCiudad = function(element) {
            self.page = 1;
            self.ciudad = $(element).val()
            self.ciudad_text = $(element).find("option:selected").text();
            self.update_filters();
            self.ajax();
        };
        self.filterProvincia = function(element) {
            self.page = 1;
            self.provincia = $(element).val()
            self.provincia_text = $(element).find("option:selected").text();
            self.update_filters();
            self.ajax();
        };
        self.filterPais = function(element) {
            self.page = 1;
            self.pais = $(element).val()
            self.pais_text = $(element).find("option:selected").text();
            self.update_filters();
            self.ajax();
        };

        self.update_filters = function() {
            self.filters([]);
            if (self.provincia != '') self.filters.push({
                key: 'provincia',
                value: self.provincia_text,
                name: 'provincia'
            });
 	 if (self.pais != '') self.filters.push({
                key: 'pais',
                value: self.pais_text,
                name: 'pais'
            });
             if (self.ciudad != '') self.filters.push({
                key: 'ciudad',
                value: self.ciudad_text,
                name: 'ciudad'
            });
        };
        self.remove_filters = function(name) {
            if (name == 'provincia') {
                self.provincia = '';
                self.provincia_text = '';
            }
             if (name == 'pais') {
                self.pais = '';
                self.pais_text = '';
            }
            if (name == 'ciudad') {
                self.ciudad = '';
                self.ciudad_text = '';
            }
            $('#' + name).val("").trigger("change");
            self.update_filters();
            self.ajax();
        };
        self.ir = function(page) {
            self.page = page;
            self.ajax();
            $('html, body').animate({
                scrollTop: 0
            }, 'fast');
        };
        self.eliminar = function(id) {
          $.ajax({
            url: url_normal+'/'+id,
            type: 'DELETE',
            data    :   {
              '_token': "{{ csrf_token() }}"
            },
            success: function(result) {
              toastr.success(result);
              self.ajax();
            }
          });
        };
        self.ajax = function() {
            $.getJSON(url, {
                    page: self.page,
                    ciudad: self.ciudad,
                    provincia: self.provincia,
                    pais: self.pais
                })
                .done(function(data) {
                    console.log(data);
                    Pace.restart()
                    vm.data(data);
                    $("#data").fadeIn();
                    $(".loading").remove();
                })
                .error(function(d) {
                    toastr.error('Se encontro un error intente nuevamente');
                });
        };
    };
    vm = new vm();
    vm.init();
    ko.applyBindings(vm, document.getElementById("data"));

    @if(session()->has('message'))
    toastr.success('{{ session()->get('message ') }}');
    @endif
</script>
@endsection
