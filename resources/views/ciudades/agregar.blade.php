  @extends('template.template')
  @section('head_content')
  @endsection
  @section('content')
  <div class="row">
    <div class="col-lg-12 text-center">
      <div class="ibox float-e-margins">
        <div class="ibox-title">
          <h5>Alta de Ciudades</h5>
        </div>
        <div class="ibox-content">
         <form class="validator form-horizontal" action="{{url('ciudades')}}" method="post">
           {{ csrf_field() }}

          <div class="form-group"><label class="col-lg-3 control-label">Ciudad</label>
            <div class="col-lg-6">
              <input type="text" name="ciudad" placeholder="INGRESE LA CIUDAD " value="{{ old('ciudad') }}" class="form-control" required>
            </div>
          </div>


          <div class="form-group"><label class="col-lg-3 control-label">Paises</label>
            <div class="col-lg-6">
              <select class="select2_demo_2 form-control" name="pais" id="pais" value="{{ old('pais') }}">
                <option value="">Paises</option>
                @foreach($paises as $p)
                <option value="{{$p->id}}" >{{$p->nombre}}</option>
                @endforeach
              </select>
            </div>
          </div>
          
            <div id="select_provincias"></div>

          <div class="form-group">
            <div class="col-lg-offset-1 col-lg-10">
              <button class="btn btn-sm btn-primary " type="submit">Completar Registro</button>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
  </div>
  @endsection
  @section('js')
<script>
$(document).ready(function() {
    $(".select2_demo_2").select2();
});
$("#pais").change(function() {
    var id_pais = $(this).val();
    var url = "{{ url('api/provincias') }}";
    $.getJSON(url, {
            pais: id_pais
        })
        .done(function(data) {
            console.log(data.data);
            txt = '<div class="form-group"><label class="col-lg-3 control-label">Provincias</label>'
            txt += '<div class="col-lg-6">'
            txt += '<select class="select2_demo_2 form-control" name="provincia" id="provincias" required>'
            $.each(data.data, function(index, value) {
                txt += '<option value="' + value.provincia_id + '" >' + value.provincia + '</option>'
            });
            txt += '</select>'
            txt += '</div>'
            txt += '</div>'

            $("#select_provincias").html(txt);
            $(".select2_demo_2").select2();

        })
        .error(function(d) {
            alert('Se encontro un error intente nuevamente');
        });


});
   </script> 
  @endsection

