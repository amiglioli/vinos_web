  @extends('template.template')
  @section('head_content')
  @endsection
  @section('content')
  <div class="row">
    <div class="col-lg-12 text-center">
      <div class="ibox float-e-margins">
        <div class="ibox-title">
          <h5>Alta de Ciudades</h5>
        </div>
        <div class="ibox-content">
         <form class="validator form-horizontal" action="{{ url('ciudades',$ciudad->id) }}" method="post">
            @method('PUT')
           {{ csrf_field() }}

           <input name="id" type="hidden" value="{{ $ciudad->id }}"/>
          <div class="form-group"><label class="col-lg-3 control-label">Ciudad</label>
            <div class="col-lg-6">
              <input type="text" name="ciudad" value="{{$ciudad->nombre}}" placeholder="INGRESE LA CIUDAD " value="{{ old('ciudad') }}" class="form-control" required>
            </div>
          </div>


          <div class="form-group"><label class="col-lg-3 control-label">Paises</label>
            <div class="col-lg-6">
              <select class="select2_demo_2 form-control" name="pais" id="pais" value="{{ old('pais') }}">
                <option value="">Paises</option>
                @foreach($paises as $p)
                <option @if($ciudad->provincia->pais_id == $p->id) selected @endif value="{{$p->id}}" >{{$p->nombre}}</option>
                @endforeach
              </select>
            </div>
          </div>
          
            <div id="select_provincias"></div>

          <div class="form-group">
            <div class="col-lg-offset-1 col-lg-10">
              <button class="btn btn-sm btn-primary " type="submit">Completar Registro</button>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
  </div>
  @endsection
  @section('js')
<script>
    var pais_selected = "{{$ciudad->provincia->pais_id}}";
    var provincia_id = "{{$ciudad->provincia->provincia_id}}";
$(document).ready(function() {
    $(".select2_demo_2").select2();



    traer_provincias(pais_selected);


    $("#pais").change(function(){
      var id_pais = $(this).val();
      traer_provincias(id_pais);

    });

});

    function traer_provincias(id_pais){
       var url = "{{ url('api/provincias') }}";
      
        $.getJSON(url,{
            pais:id_pais
          })
          .done(function(data){
      
            txt = '<div class="form-group"><label class="col-lg-3 control-label">Provincias</label>'
              txt += '<div class="col-lg-6">'
                 txt += '<select class="select2_demo_2 form-control" name="provincia" id="provincias" required>'
                    $.each(data.data, function( index, value ) {
                   if(provincia_id == value.provincia_id)
                        txt+='<option selected value="'+value.provincia_id+'" >'+value.provincia+'</option>'
                      else
                        txt+='<option value="'+value.provincia_id+'" >'+value.provincia+'</option>'
                    });
                  txt +='</select>'
              txt +='</div>'
            txt +='</div>'

            $("#select_provincias").html(txt);
            $(".select2_demo_2").select2();

          })
          .error(function(d){
            alert('Se encontro un error intente nuevamente');
          });
    }


   </script> 
  @endsection

