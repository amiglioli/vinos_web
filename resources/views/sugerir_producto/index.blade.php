@extends('template.template')
@section('linkscss')
<link type="text/css" rel="stylesheet" href="{{ asset('/assets/select2/select2.css') }}">
<link type="text/css" rel="stylesheet" href="{{ asset('/assets/toastr/toastr.min.css') }}">
<link type="text/css" rel="stylesheet" href="{{ asset('/assets/bootstrap-datepicker/css/datepicker.css') }}">
<link type="text/css" rel="stylesheet" href="{{ asset('/assets/sweetalert/sweet-alert.css') }}">
<link href="{{asset('template/css/estilo_tabla.css')}}" rel="stylesheet">
@endsection
@section('content')
<div class="row wrapper red-bg">
  <div class="col-lg-10">
    <h2>Sugerencias</h2>
  </div>
  <div class="col-lg-2">
  </div>
</div>
<section class="bf-table" id="data" style="display: none; padding-bottom: 20px ">
   <div class="card card-cascade narrower">
      <!--Card image-->
      <div class="view view-cascade gradient-card-header blue-gradient narrower py-2 mx-4 mb-3 d-flex justify-content-between align-items-center">
        
         <span class="white-text">Listado de Sugerencias</span>
          <div style="text-align:right">
	   <!-- <a type="button" class="btn btn-outline btn-danger"  href="{{ URL::to('producto/create') }}">Nuevo</a> -->
         </div>
      </div>
      <!--/Card image-->
      <div class="px-4">
        
         <div class="table-wrapper table-responsive">
            <!--Table-->
            <div class="btn-group pull-left" data-bind="foreach: filters">
          <span data-bind="click: function() { vm.remove_filters(name) }" title="Click para quitar este filtro" class="label label_table"><i class="fa fa-times-circle" aria-hidden="true"></i>&nbsp;&nbsp;<a style="color: white;" data-bind="text: value"></a></span>
        </div>
            <table class="table table-bordered tabla_custom table-hover mb-0">
					<thead>
						<tr>
							<th class="col_1" style="padding: 0px;">
								<input id="bodega" type="text" placeholder="Bodega" class="form-control" data-bind="event: { change: function(){vm.filterBodega($element)} }">
							</th>
							<th class="col_1" style="padding: 0px;">
									<input id="producto" type="text" placeholder="Producto" class="form-control" data-bind="event: { change: function(){vm.filterProducto($element)} }">
								</th>
								<th class="col_1" style="padding: 0px;">
									<input id="varietal" type="text" placeholder="Varietal" class="form-control" data-bind="event: { change: function(){vm.filterVarietal($element)} }">
								</th>
							<th class="col_1" style="padding: 0px;">
									<input id="anio" type="text" placeholder="Año" class="form-control" data-bind="event: { change: function(){vm.filterAnio($element)} }">
							</th>
							<th class="col_1" style="padding: 0px;">
									<input id="persona_contacto" type="text" placeholder="Persona de Contacto" class="form-control" data-bind="event: { change: function(){vm.FilterPersona($element)} }">
							</th>
							<th class="col_1" style="padding: 0px;">
									<input id="celular" type="text" placeholder="Celular" class="form-control" data-bind="event: { change: function(){vm.filterCelular($element)} }">
							</th>
							<th class="col_1" style="padding: 0px;">
									<input id="email" type="text" placeholder="E-mail" class="form-control" data-bind="event: { change: function(){vm.filterEmail($element)} }">
							</th>
							<!--<th class="text-left">Acciones</th> -->
						</tr>
					</thead>
					<tbody data-bind="foreach: data().data">
						<tr>
							<td class="col_1 text-left"  	style="width:200px;"	data-bind="text: bodega"></td>
							<td class="col_1 text-left" 	style="width:200px;"	data-bind="text: producto"></td>
							<td class="col_1 text-left" 	style="width:150px;" 	data-bind="text: varietal"></td>
							<td class="col_1 text-left" 	style="width:100px;" 	data-bind="text: anio"></td>
							<td class="col_1 text-left" 	style="width:250px;"	data-bind="text: persona_contacto"></td>
							<td class="col_1 text-left" 	style="width:250px;"	data-bind="text: celular"></td>
							<td class="col_1 text-left" 	style="width:250px;"	data-bind="text: email"></td>
							<!--
							<td class="col_1 text-center">
							<a href="javascript:void(0)" data-bind="attr: { href: '{{ url("producto") }}' +'/'+id+'/edit', }"  class="btn btn-outline btn-warning btn-sm">Editar</a>
							<a href="javascript:void(0)" data-bind="event:{ click: function(){ vm.eliminar(id) } }"  class="btn btn-outline btn-danger btn-sm">Eliminar</a>
							</td>
						-->
						</tr>
					</tbody>
				</table>
		</div>
         
         <div class="d-flex justify-content-between">
            <div style="padding-top: 40px">
                <p>Mostrando del <span data-bind="text: data().from == null ? '0' : data().from"></span> al <span data-bind="text: data().to == null ? '0' : data().to"></span> de <span data-bind="text: data().total == null ? '0' : data().total"></span> registros</p>               
            </div>
            <nav class="my-4">
               <ul class="pagination pagination-circle pg-blue mb-0">
                  <li class="page-item" data-bind="css: data().prev_page_url == null ? 'disabled' : ''"><a class="page-link waves-effect waves-effect" data-bind="click: function() { ir(1) }">« Primera</a></li>
                  <li class="page-item" data-bind="if: data().current_page-100 > 0, click: function() { ir(data().current_page-100) } "><span class="page-link waves-effect waves-effect" data-bind="text: data().current_page-100"></span></li>
                  <li class="page-item" data-bind="if: data().current_page-50 > 0, click: function() { ir(data().current_page-50) } "><span class="page-link waves-effect waves-effect" data-bind="text: data().current_page-50"></span></li>
                  <li class="page-item" data-bind="if: data().current_page-10 > 0, click: function() { ir(data().current_page-10) } "><span class="page-link waves-effect waves-effect" data-bind="text: data().current_page-10"></span></li>
                  <li class="page-item" data-bind="if: data().current_page-5 > 0, click: function() { ir(data().current_page-5) } "><span class="page-link waves-effect waves-effect" data-bind="text: data().current_page-5"></span></li>
                  <li class="page-item" data-bind="if: data().current_page-2 > 0, click: function() { ir(data().current_page-2) } "><span class="page-link waves-effect waves-effect" data-bind="text: data().current_page-2"></span></li>
                  <li class="page-item" data-bind="if: data().current_page-1 > 0, click: function() { ir(data().current_page-1) } "><span class="page-link waves-effect waves-effect" data-bind="text: data().current_page-1"></span></li>
                  <li class="page-item active" ><span class="page-link waves-effect waves-effect" data-bind="text: data().current_page"></span></li>
                  <li class="page-item" data-bind="if: data().current_page+1 <= data().last_page, click: function() { ir(data().current_page+1) } "><span class="page-link waves-effect waves-effect" data-bind="text: data().current_page+1"></span></li>
                  <li class="page-item" data-bind="if: data().current_page+2 <= data().last_page, click: function() { ir(data().current_page+2) } "><span class="page-link waves-effect waves-effect" data-bind="text: data().current_page+2"></span></li>
                  <li class="page-item" data-bind="if: data().current_page+5 <= data().last_page, click: function() { ir(data().current_page+5) } "><span class="page-link waves-effect waves-effect" data-bind="text: data().current_page+5"></span></li>
                  <li class="page-item" data-bind="if: data().current_page+10 <= data().last_page, click: function() { ir(data().current_page+10) } "><span class="page-link waves-effect waves-effect" data-bind="text: data().current_page+10"></span></li>
                  <li class="page-item" data-bind="if: data().current_page+50 <= data().last_page, click: function() { ir(data().current_page+50) } "><span class="page-link waves-effect waves-effect" data-bind="text: data().current_page+50"></span></li>
                  <li class="page-item" data-bind="if: data().current_page+100 <= data().last_page, click: function() { ir(data().current_page+100) } "><span class="page-link waves-effect waves-effect" data-bind="text: data().current_page+100"></span></li>
                  <li class="page-item" data-bind="css: data().last_page == null || data().current_page >= data().last_page ? 'disabled' : ''"><a class="page-link waves-effect waves-effect" data-bind="click: function() { ir(data().last_page) }">Última »</a></li>
               </ul>

            </nav>

         </div>

      </div>
   </div>
</section>
@endsection

@section('js')
<script src='{{ asset("assets/toastr/toastr.min.js") }}'></script>
<script src="{{ asset('assets/select2/select2.full.js') }}"></script>
<script src="{{ asset('assets/moment/moment-with-locales.min.js')}}"></script>
<script src="{{ asset('assets/bootstrap-datepicker/js/bootstrap-datepicker.js') }}"></script>
<script src="{{ asset('assets/bootstrap-datepicker/js/locales/bootstrap-datepicker.es.js') }}"></script>
<script>
moment.locale('es');
var url = "{{ url('api/sugerir_producto') }}";
var url_normal = "{{ url('sugerir_producto') }}";
var vm = function() {
	self = this;
	self.data = ko.observableArray([]);

	self.bodega = ''
	self.producto = ''
	self.anio = ''
	self.persona_contacto = ''
	self.celular = ''
	self.email = ''
	self.varietal = ''
	
	
	self.page = '1';
	self.filters = ko.observableArray([]);
	self.init = function() {
		self.ajax();
		$('.select2').select2();
	}
	self.filterBodega = function(element) {
		self.page = 1;
		self.bodega = $(element).val()
		self.update_filters();
		self.ajax();
	};
	self.filterProducto = function(element) {
		self.page = 1;
		self.producto = $(element).val()
		self.update_filters();
		self.ajax();
	};
	self.filterAnio = function(element) {
		self.page = 1;
		self.anio = $(element).val()
		self.update_filters();
		self.ajax();
	};
	self.FilterPersona = function(element) {
		self.page = 1;
		self.persona_contacto = $(element).val()
		self.update_filters();
		self.ajax();
	};
	self.filterCelular = function(element) {
		self.page = 1;
		self.celular = $(element).val()
		self.update_filters();
		self.ajax();
	};
	self.filterEmail = function(element) {
		self.page = 1;
		self.email = $(element).val()
		self.update_filters();
		self.ajax();
	};
	self.filterVarietal = function(element) {
			self.page = 1;
			self.varietal = $(element).val()
			self.update_filters();
			self.ajax();
	};

	self.update_filters = function() {
		self.filters([]);
		if (self.bodega != '') self.filters.push({
			key: 'bodega',
			value: self.bodega,
			name: 'bodega'
		});
		if (self.producto != '') self.filters.push({
			key: 'producto',
			value: self.producto,
			name: 'producto'
		});	
		if (self.celular != '') self.filters.push({
			key: 'celular',
			value: self.celular,
			name: 'celular'
		});	
		if (self.anio != '') self.filters.push({
			key: 'anio',
			value: self.anio,
			name: 'anio'
		});	
		if (self.persona_contacto != '') self.filters.push({
			key: 'persona_contacto',
			value: self.persona_contacto,
			name: 'persona_contacto'
		});	
		if (self.email != '') self.filters.push({
			key: 'email',
			value: self.email,
			name: 'email'
		});
		if (self.varietal != '') self.filters.push({
				key: 'varietal',
				value: self.varietal,
				name: 'varietal'
		});	
	};
	self.remove_filters = function(name) {
		if (name == 'bodega') {
			self.bodega = '';
		}
		if (name == 'producto') {
			self.producto = '';
		}
		if (name == 'anio') {
			self.anio  = '';
		}
		if (name == 'persona_contacto') {
			self.persona_contacto  = '';
		}
		if (name == 'celular') {
			self.celular  = '';
		}
		if (name == 'email') {
			self.email  = '';
		}
		if (name == 'varietal') {
			self.varietal = '';
		}

		$('#' + name).val("").trigger("change");
		self.update_filters();
		self.ajax();
	};
	self.ir = function(page) {
		self.page = page;
		self.ajax();
		$('html, body').animate({
			scrollTop: 0
		}, 'fast');
	};
	self.eliminar = function(id) {
		$.ajax({
			url: url_normal+'/'+id,
			type: 'DELETE',
			data    :   {
				'_token': "{{ csrf_token() }}"
			},
			success: function(result) {
				toastr.success(result);
				self.ajax();
			}
		});
	};
	self.ajax = function() {
		$.getJSON(url, {
			page: self.page,
			bodega: self.bodega,
			producto: self.producto,
			varietal:self.varietal,
			anio: self.anio,
			persona_contacto: self.persona_contacto,
			celular: self.celular,
			email: self.email
		})
		.done(function(data) {
			console.log(data);
			Pace.restart()
			vm.data(data);
			$("#data").fadeIn();
			$(".loading").remove();
		})
		.error(function(d) {
			toastr.error('Se encontro un error intente nuevamente');
		});
	};
};
vm = new vm();
vm.init();
ko.applyBindings(vm, document.getElementById("data"));

@if(session()->has('message'))
toastr.success('{{ session()->get('message ') }}');
@endif
</script>
@endsection
