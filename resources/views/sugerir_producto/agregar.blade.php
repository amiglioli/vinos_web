@extends('template.template')
@section('head_content')
<link href="{{asset('template/css/bootstrap-datetimepicker.css')}}" rel="stylesheet">
@endsection
@section('content')
<div class="row">
  <div class="col-lg-12 text-center">
    <div class="ibox float-e-margins">
      <div class="ibox-title" style="background-color: #d02026;color:#ffffff">
        <h5>Sugerí los productos que te interesen que participen en las CATAS REDVIN  y coordiná la participación de los mismos</h5>
      </div>
      <div class="ibox-content">
        <form class="validator form-horizontal" action="{{url('sugerir_producto')}}" method="post">
          {{ csrf_field() }}

          <div class="form-group"><label class="col-lg-3 control-label">Bodega</label>
            <div class="col-lg-6">
              <input type="text" name="bodega" placeholder="INGRESE LA BODEGA" value="{{ old('bodega') }}" class="form-control" required>
            </div>
          </div>

          <div class="form-group"><label class="col-lg-3 control-label">Nombre del vino</label>
            <div class="col-lg-6">
              <input type="text" name="nombre_vino" value="{{ old('nombre_vino') }}" placeholder="INGRESE EL NOMBRE DEL VINO" class="form-control" required>
            </div>
          </div>


            <div class="form-group"><label class="col-lg-3 control-label">Varietal</label>
            <div class="col-lg-6">
               <input type="text" name="varietal" value="{{ old('varietal') }}" placeholder="INGRESE EL VARIETAL DEL VINO" class="form-control" required>
            </div>
          </div>

          <div class="form-group"><label class="col-lg-3 control-label">Año</label>
            <div class="col-lg-6">
              <input type="number" name="anio" value="{{ old('anio') }}" placeholder="INGRESE EL AÑO" class="form-control" required>
            </div>
          </div>
          @if(Auth::user()->hasRole('distribuidora') || Auth::user()->hasRole('bodega'))
          <div class="form-group"><label class="col-lg-3 control-label">Persona de Contacto</label>
            <div class="col-lg-6">
              <input type="text"  name="persona_contacto" value="{{ old('persona_contacto') }}" placeholder="INGRESE SU INFORMACIÓN" class="form-control" required>
            </div>
          </div>
          <div class="form-group"><label class="col-lg-3 control-label">Celular</label>
            <div class="col-lg-6">
              <input type="number"  name="celular" value="{{ old('celular') }}" placeholder="INGRESE SU CELULAR" class="form-control" required>
            </div>
          </div>
                    <div class="form-group"><label class="col-lg-3 control-label">Email</label>
            <div class="col-lg-6">
              <input type="text"  name="email" value="{{ old('email') }}" placeholder="INGRESE SU EMAIL" class="form-control" required>
            </div>
          </div>
          @endif


          <div class="form-group">
            <div class="col-lg-offset-1 col-lg-10">
              <button class="btn btn-sm btn-primary " type="submit">ENVIAR</button>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>
@endsection
@section('js')
<script src="//cdn.rawgit.com/Eonasdan/bootstrap-datetimepicker/e8bddc60e73c1ec2475f827be36e1957af72e2ea/src/js/bootstrap-datetimepicker.js"></script>
<script>
$(document).ready(function() {
  $(".select2_demo_2").select2();
  $('#edad').datetimepicker({
    keepOpen:true,
    showClose: true,
    viewMode: 'years',
    locale:"es",
    format: 'Y-MM-DD',
    widgetPositioning:{
      horizontal: 'auto',
      vertical: 'bottom'
    }
  });
});
</script>
@endsection
