@extends('template.template')
@section('head_content')
@endsection
@section('content')
<div class="row">
  <div class="col-lg-12 text-center">
    <div class="ibox float-e-margins">
      <div class="ibox-title">
        <h5>Alta de Permiso</h5>
      </div>
      <div class="ibox-content">
       <form class="validator form-horizontal" action="{{url('permission')}}" method="post">
         {{ csrf_field() }}

        <div class="form-group"><label class="col-lg-3 control-label">Permiso</label>
          <div class="col-lg-6">
            <input type="text" name="permiso" placeholder="INGRESE EL NOMBRE" class="form-control" required>
          </div>
        </div>

        <div class="form-group"><label class="col-lg-3 control-label">Descripcion</label>
          <div class="col-lg-6">
            <input type="text" name="descripcion" placeholder="INGRESE LA DESCRIPCION" class="form-control" required>
          </div>
        </div>

        <div class="form-group">
          <div class="col-lg-offset-1 col-lg-10">
            <button class="btn btn-sm btn-primary " type="submit">Completar Registro</button>
          </div>
        </div>
      </form>
    </div>
  </div>
</div>
</div>
@endsection
