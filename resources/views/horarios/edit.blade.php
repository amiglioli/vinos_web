@extends('template.template')
@section('head_content')
@section('linkscss')
<link type="text/css" rel="stylesheet" href="{{ asset('/template/css/clockface.css') }}">
@endsection
@endsection
@section('content')

  <div class="row wrapper red-bg">
  <div class="col-lg-10">				
<h2>Horarios</h2>	  </div>
  <div class="col-lg-2">
  </div>
</div>	
<div class="row">
  <div class="col-lg-12 text-center">
    <div class="ibox float-e-margins">
      <div class="ibox-title">
        <h5>Edición de Horarios</h5>
      </div>
      <div class="ibox-content ">
        <form class="col-lg-offset-2 validator form-horizontal" action="{{ url('horarios',$data[0]->id_vinoteca) }}" method="post">
          @method('PUT')
          {{ csrf_field() }}

          @foreach ($data as $key => $dia)
          <div class="form-group "><label class="col-lg-1 control-label">{{dayweek($dia->dia)}}</label>
          <div class="col-lg-1">
          </div>
            <div class="col-lg-2" id="d1{{$dia->dia}}">
            <select name="dia[{{$key}}][]" class="form-control select2 {{$dia->dia}}" id="{{$key}}">
            <option value="Cerrado">CERRADO</option>
            @foreach($horarios as $hora)
              <option @if($hora == $dia->hora1) selected @endif value="{{$hora}}">{{$hora}}</option>
            @endforeach
            </select>
          </div>
            <div class="col-lg-2" id="d2{{$dia->dia}}">
            <select  name="dia[{{$key}}][]"  class="form-control select2 {{$dia->dia}}">
            <option value="Cerrado">CERRADO</option>
            @foreach($horarios as $hora)
               <option @if($hora == $dia->hora1_fin) selected @endif value="{{$hora}}">{{$hora}}</option>
            @endforeach
            </select>
          </div>
            <div class="col-lg-2" id="d3{{$dia->dia}}">
            <select  name="dia[{{$key}}][]"  class="form-control select2 {{$dia->dia}}">
            <option value="Cerrado">CERRADO</option>
            @foreach($horarios as $hora)
               <option @if($hora == $dia->hora2) selected @endif value="{{$hora}}">{{$hora}}</option>
            @endforeach
            </select>
          </div>
            <div class="col-lg-2" id="d4{{$dia->dia}}">
            <select  name="dia[{{$key}}][]"  class="form-control select2 {{$dia->dia}}">
            <option value="Cerrado">CERRADO</option>
            @foreach($horarios as $hora)
               <option @if($hora == $dia->hora2_fin) selected @endif value="{{$hora}}">{{$hora}}</option>
            @endforeach
            </select>
            </div>
       </div>
       @endforeach
       <div class="form-group">
        <div class="col-lg-10">
          <button class="btn btn-sm btn-primary " type="submit">Guardar Horarios</button>
        </div>
      </div>
    </form>
  </div>
</div>
</div>
</div>
@endsection
@section('js')
<script src='{{ asset("template/js/clockface.js") }}'></script>
<script src="{{ asset('assets/select2/select2.full.js') }}"></script>

<script>
$( document ).ready(function() {
  $('.select2').select2();
});



</script>
@endsection
