@extends('template.template')
@section('head_content')
<style>

.select2{
  width: 220px !important;
}

.select2-selection__rendered{
  width: 220px !important;
  border:1px solid;
}


</style>
@section('linkscss')

<link href="{{asset('template/css/home.css')}}" rel="stylesheet">
<link href="{{asset('template/css/estilo_tabla.css')}}" rel="stylesheet">
@endsection
@section('content')
<section class="bf-table" id="data">
  <div class="card card-cascade narrower">
    <!--Card image-->
    <div class="view view-cascade gradient-card-header blue-gradient narrower py-2 mx-4 mb-3 d-flex justify-content-between align-items-center">
      
      <span class="white-text">Horarios de atencion</span>
      <div style="text-align:right">
        <!-- <a type="button" class="btn btn-outline btn-danger"  href="{{ URL::to('producto/create') }}">Nuevo</a> -->
      </div>
    </div>
    <!--/Card image-->
    <div class="px-4">
      
      <div class="table-wrapper table-responsive">
        <!--Table-->
        <form action="{{url('horarios')}}" method="post">
          <table class="table table-bordered tabla_custom table-hover mb-0 ">
            {{ csrf_field() }}
              <thead>
                <tr>
                  <th>Día</th>
                  <th>Estado</th>
                  <th>Horarios</th>
                </tr>
              </thead>
              <tbody>
                @foreach($dias as $key => $value)
                <tr>
                  <div class="form-group">
                    <td>
                      <label for="" class="title"><?= ucfirst($value) ?></label>
                    </td>
                    <td>
                      <div class="input-group estado">
                        <input type="checkbox" class="magic-checkbox" name="check-<?= $key ?>"  onchange="cambiarEstado(this,'<?= $key ?>')" id="check-<?= $key ?>"     @if($data[$key]->count() > 0) checked @endif>
                        <label class="text rightc" for="check-<?= $key ?>"></label>
                        
                      </div>
                    </td>
                    
                    <td>
                      <div id="dia-<?= $key ?>">
                        @if($data[$key]->count() > 0)
                        <div class="col-md-12">
                          <div class="col-md-4">
                            <select name="dia[{{$key}}][0][apertura]" class="form-control">
                              @foreach($horarios as $h)
                              <option @if($data[$key][0]->hora_apertura == $h.':00') selected @endif value="{{$h}}">{{$h}}</option>
                              @endforeach
                            </select>
                          </div>
                          <div class="col-md-4">
                            <select name="dia[{{$key}}][0][cierre]" class="form-control">
                              @foreach($horarios as $h)
                              <option @if($data[$key][0]->hora_cierre == $h.':00') selected @endif value="{{$h}}">{{$h}}</option>
                              @endforeach
                            </select>
                          </div>
                          <div  id="agregar_franja-<?= $key ?>">
                            @if($data[$key]->count() == 1)
                            <button class="btn btn-outline btn-warning" onclick="agregarFranja(this,'<?= $key ?>')">Agregar horario de atención</button>
                            @endif
                          </div>
                        </div>
                        <br>
                        <br>
                        <br>
                        <div class="col-md-12" id="franja-<?= $key ?>">
                          @if($data[$key]->count() == 2)
                          <div class="col-md-4">
                            <select name="dia[{{$key}}][1][apertura]" class="form-control">
                              @foreach($horarios as $h)
                              <option @if($data[$key][1]->hora_apertura == $h.':00') selected @endif value="{{$h}}">{{$h}}</option>
                              @endforeach
                            </select>
                          </div>
                          <div class="col-md-4">
                            <select name="dia[{{$key}}][1][cierre]" class="form-control">
                              @foreach($horarios as $h)
                              <option @if($data[$key][1]->hora_cierre == $h.':00') selected @endif value="{{$h}}">{{$h}}</option>
                              @endforeach
                            </select>
                          </div>
                          <button class="btn btn-outline btn-warning" onclick="borrarFranja('{{$key}}')"><i class="fa fa-times"></i></button>
                          @endif
                        </div>
                        @endif
                      </div>
                    </td>
                  </div>
                </tr>
                @endforeach
              </tbody>
            </table>
            <center><button class="btn btn-primary"  type="submit">Aplicar</button></center>
            <form>
            </div>
            
          </div>
        </div>
      </section>

@endsection
@section('js')
<script src='{{ asset("template/js/clockface.js") }}'></script>
<script src="{{ asset('assets/select2/select2.full.js') }}"></script>
<script>


  $('select').select2();
  var dias_semana = ['Domingo','Lunes','Martes','Miercoles','Jueves','Viernes','Sabado'];
  var horarios = ['00:00','00:30','01:00','01:30','02:00','02:30','03:00','03:30','04:00','04:30','05:00','05:30','06:00','06:30','07:00','07:30','08:00','08:30','09:00','09:30','10:00','10:30','11:00','11:30','12:00','12:30','13:00','13:30','14:00','14:30','15:00','15:30','16:00','16:30','17:00','17:30','18:00','18:30','19:00','19:30','20:00','20:30','21:00','21:30','22:00','22:30','23:00','23:30'];

  function cambiarEstado(elem,dia){
    if ($('#check-'+dia).is(':checked')) 
      abrirDia(dia);
    else
      cerrarDia(dia);
  }

  function agregarFranja(elem,dia){

    var nombre_dia = dias_semana[dia];
    //console.log(nombre_dia)
    var conteindo_franja = $("#franja-"+dia).html();
    elem.remove();
    var html = '    <div class="col-md-4">';

        html+=' <select name="dia['+dia+'][1][apertura]" class="form-control">';
          $.each( horarios, function( key, value ) {
              html+=' <option value="'+value+'">'+value+'</option>';
          });
        html+=' </select>';
        html+=' </div>';
        html+='<div class="col-md-4">';
        html+=' <select name="dia['+dia+'][1][cierre]" class="form-control">';
          $.each( horarios, function( key, value ) {
              html+=' <option value="'+value+'">'+value+'</option>';
          });
        html+=' </select>';
        html+='</div>';
        html+='<button class="btn btn-outline btn-warning" onclick="borrarFranja(\''+dia+'\')"><i class="fa fa-times"></i></button>';
    $("#franja-"+dia).html(html);
    $("#franja-"+dia).css('visibility','visible');     
    $('select').select2(); 
  }

  function borrarFranja(dia){
    $("#agregar_franja-"+dia).html('<button class="btn btn-outline btn-warning" onclick="agregarFranja(this,\''+dia+'\')">Agregar horario de atención</button>');
    $("#franja-"+dia).html('');
  }


  function cerrarDia(dia){
    $('#dia-'+dia).html('');
  }
 
  function abrirDia(dia){
    var html = '<div class="col-md-12">';
    html+= '<div class="col-md-4">';
    html+=' <select name="dia['+dia+'][0][apertura]" class="form-control">';
      $.each( horarios, function( key, value ) {
          html+=' <option value="'+value+'">'+value+'</option>';
      });
    html+=' </select>';
    html+= '</div>';
    html+= '<div class="col-md-4">';
    html+=' <select name="dia['+dia+'][0][cierre]" class="form-control">';
      $.each( horarios, function( key, value ) {
          html+=' <option value="'+value+'">'+value+'</option>';
      });
    html+=' </select>';
    html+= '</div>';
    html+= '<div  id="agregar_franja-'+dia+'">';
    html+= '<button class="btn btn-outline btn-warning"  onclick="agregarFranja(this,\''+dia+'\')" >Agregar horario de atención</button>';
    html+= '</div>';
    html+= '</div>';
    html+= '<br>';
    html+= '<br>';
    html+= '<br>';
    html+= '<div class="col-md-12"  id="franja-'+dia+'" ></div>';
    $('#dia-'+dia).html(html);
    $('select').select2();
  }
</script>
@endsection
