@extends('template.template')
@section('linkscss')
<link href="{{asset('template/css/estilo_tabla.css')}}" rel="stylesheet">
@endsection
@section('content')
<section class="bf-table" id="data" style="padding-bottom: 20px ">
  <div class="card card-cascade narrower">
    <!--Card image-->
    <div class="view view-cascade gradient-card-header blue-gradient narrower py-2 mx-4 mb-3 d-flex justify-content-between align-items-center">
      
      <span class="white-text">Mis Horarios</span>
      <div style="text-align:right">
        <a type="button" class="btn btn-outline btn-danger" href="{{ url('horarios/create') }}">Editar</a>
      </div>
    </div>
    <!--/Card image-->
    <div class="px-4">
      
      <div class="table-wrapper table-responsive">
        <table class="table table-bordered tabla_custom table-hover mb-0">
          <thead>
            <tr>
              <th class="col_1" style="padding:10px;">Día</th>
              <th class="col_4" style="padding:10px;">Horarios</th>
            </tr>
          </thead>
          <tbody>
            @for($i = 0; $i < 7; $i++)
            <tr>
              <td class="col_1"><span @if( $i == date('w')) class="flash-text" style="color:#d02026;font-weight: bold" @endif >{{dayweek($i)}}</span></td>
              @if($horarios[$i]->count() > 0)
              <td>
                @foreach($horarios[$i] as $h)
                <span class="pull-left">
                  @php
                  $hora_apertura = substr($h->hora_apertura, 0, strlen($h->hora_apertura)-3);
                  $hora_cierre = substr($h->hora_cierre, 0, strlen($h->hora_cierre)-3);
                  @endphp
                  {{$hora_apertura.' - '.$hora_cierre}}
                </span>
                <br>
                @endforeach
              </td>
              @else
              <td><span class="pull-left" style="color:#ba3737">Cerrado</span></td>
              @endif
            </tr>
            @endfor
          </tbody>
        </table>
      </div>
      
    </div>
  </div>
</section>
@endsection