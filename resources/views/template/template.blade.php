<!DOCTYPE html>
<html lang="en">
  <style>
  .imgprofile {
  //border-radius: 50%;
  margin-top:-32px;
  width: 218px;
  height: 130px;
  margin-left:-14%;
  }
  </style>
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="apple-mobile-web-app-capable" content="yes" />
    <meta name="description" content="">
    <meta name="keywords" content="thema bootstrap template, thema admin, bootstrap, admin template, bootstrap admin">
    <meta name="author" content="LanceCoder">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <link rel="shortcut icon" href="{{ url('img/favicon.png') }}">
    <title>SISTEMA {{ config('app.name') }}</title>
    <link href="{{asset('template/css/bootstrap.min.css')}}" rel="stylesheet">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.13/css/all.css" integrity="sha384-DNOHZ68U8hZfKXOrtjWvjxusGo9WQnrNx2sqG0tfsghAvtVlRW3tvkXWZh58N9jp" crossorigin="anonymous">
    <link href="{{asset('template/css/animate.css')}}" rel="stylesheet">
    <link href="{{asset('template/css/plugins/iCheck/custom.css')}}" rel="stylesheet">
    <link href="{{asset('template/css/bootstrap-duallistbox.min.css')}}" rel="stylesheet">
    <link href="{{asset('template/css/style.css')}}" rel="stylesheet">
    <link href="{{asset('template/css/checkbox.css')}}" rel="stylesheet">
    <link href="{{asset('template/css/select2.min.css')}}" rel="stylesheet">
    <link href="{{asset('template/css/bootstrap-chosen.css')}}" rel="stylesheet">
    <link href="{{asset('template/css/awesome-bootstrap-checkbox.css')}}" rel="stylesheet">
    <link href="{{asset('template/css/datepicker3.css')}}" rel="stylesheet">
    <link href="{{asset('template/css/toastr.min.css')}}" rel="stylesheet">
    <link href="{{asset('template/themify-icons/themify-icons.css')}}" rel="stylesheet">

    @yield('linkscss')
  </head>
  <body>
<div style="padding-bottom:17px;background-color: #000000;">
        <nav class="navbar navbar-expand-md navbar-light navbar-laravel">
            <div class="container">
                <a class="navbar-brand" href="{{ url('/') }}">
                <img src="{{ url('img/logo.png') }}" alt=""></a>
                

            </div>
        </nav>

        
    </div>
  
  
  
  
  
    <div id="wrapper">
      <nav class="navbar-default navbar-static-side" role="navigation">
        <div class="sidebar-collapse">
          <ul class="nav metismenu" id="side-menu">
            @include('template.menu')
          </ul>
        </div>
      </nav>
      <div id="page-wrapper" class="gray-bg dashbard-1">
        <div class="row border-bottom">
          <nav class="navbar navbar-static-top" role="navigation" style="margin-bottom: 0">
            <div class="navbar-header">
              <a class="navbar-minimalize minimalize-styl-2 btn btn-link " href="#"><i class="fa fa-bars"></i> </a>
              <form role="search" class="navbar-form-custom" action="search_results.html">
                <div class="form-group">
                  <input type="text" placeholder="" class="form-control" name="top-search" id="top-search">
                </div>
              </form>
            </div>
            <ul class="nav navbar-top-links navbar-right">
              <!--
              <li>
                <span class="m-r-sm text-muted welcome-message">Welcome to INSPINIA+ Admin Theme.</span>
              </li>
              -->
              <li class="dropdown" id="visto">
                <a class="dropdown-toggle count-info" data-toggle="dropdown" href="#">
                  <i class="fa fa-bell"></i>  <span class="label label-link" id="cantidad"></span>
                </a>
                <ul class="dropdown-menu dropdown-alerts" id="notificacion">
                  <span >Sin notificaciones</span>
                </ul>
              </li>
              <li>
                <a href="{{ route('logout') }}" class="fa fa-sign-out"
                  onclick="event.preventDefault();
                  document.getElementById('logout-form').submit();">
                  Cerrar Sesión
                </a>
                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                  {{ csrf_field() }}
                </form>
              </li>
            </ul>
          </nav>
        </div>
        <!--<div class="row  border-bottom white-bg dashboard-header">
        </div>-->
        @yield('head_content')
        <div class="row">
          <div class="col-lg-12">
            <div class="wrapper wrapper-content">
              @if ($errors->any() || session()->has('message'))
              <div class="panel">
                <div class="panel-body nopadding">
                  @if ($errors->any())
                  @foreach ($errors->all() as $error)
                  <div class="alert alert-danger" role="alert">
                    <!-- <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button> -->
                    <strong>Error!</strong> {{$error}}!
                  </div>
                  @endforeach
                  @endif
                  @if(session()->has('message'))
                  <div class="col-md-12">
                    <div class="alert alert-danger col-md-12">
                      <ul>
                        <li>{{ session()->get('message') }}</li>
                      </ul>
                    </div>
                  </div>
                  @endif
                </div>
              </div>
              @endif
              @yield('content')
            </div>
            <!-- <div class="footer">
              <div class="pull-right">
                10GB of <strong>250GB</strong> Free.
              </div>
              <div>
                <strong>Copyright</strong> Example Company &copy; 2014-2017
              </div>
            </div> -->
          </div>
        </div>
      </div>
    </div>
    <script src="{{asset('template/js/jquery-2.1.1.js')}}" type="text/javascript" ></script>
    <script src="{{asset('template/js/moment-with-locales.js')}}" type="text/javascript" ></script>
    <script src="{{asset('template/js/toastr.min.js')}}" type="text/javascript" ></script>
    <script src="{{asset('template/js/alltheme.js')}}" type="text/javascript" ></script>
    <script src="{{asset('template/js/bootstrap.min.js')}}" type="text/javascript" ></script>
    <script src="{{asset('template/js/plugins/metisMenu/jquery.metisMenu.js')}}" type="text/javascript" ></script>
    <script src="{{asset('template/js/plugins/slimscroll/jquery.slimscroll.min.js')}}" type="text/javascript" ></script>
    <script src="{{asset('template/js/inspinia.js')}}" type="text/javascript" ></script>
    <script src="{{asset('template/js/plugins/dualListbox/jquery.bootstrap-duallistbox.js')}}" type="text/javascript" ></script>
    <script src="{{asset('template/js/plugins/pace/pace.min.js')}}" type="text/javascript" ></script>
    <script src="{{asset('template/js/plugins/iCheck/icheck.min.js')}}" type="text/javascript" ></script>
    <script src="{{asset('template/js/select2.full.min.js')}}" type="text/javascript" ></script>
    <script src="{{asset('template/js/bootstrap-datepicker.js')}}" type="text/javascript" ></script>
    <script src="{{asset('template/js/icheck.min.js')}}" type="text/javascript" ></script>
    <script src="{{asset('assets/knockout/knockout-3.4.0.js')}}" type="text/javascript" ></script>
    <script src="{{asset('template/js/sweetalert.min.js')}}" type="text/javascript" ></script>
    <script>
      var base_url = "<?= URL::to('') ?> ";
      base_url = base_url.trim();
      </script>
      @yield('scripts')
      @yield('js')
      <script>
     var check;
     $(document).ready(function() {
        check = '{{Auth::user()->hasRole("distribuidora")}}';
        if(check == 1){
          notify();
        }
     });
     $("#visto").click(function() {
         if (check == 1) {
             $.ajax({
                 url: base_url + '/solicitud/visto',
                 type: 'GET',
                 beforeSend: function() {
                     //$("#resultado").html("Procesando, espere por favor...");
                 },
                 success: function(response) {
                     $('#cantidad').html(0);
                     setInterval(function() {
                         notify();
                     }, 15000);
                     //notify();
                 }
             });
         }
     });

     function notify() {
         var base_url = "<?= URL::to('') ?> ";
         base_url = base_url.trim();
         $.ajax({
             url: base_url + '/solicitud/notificaciones',
             type: 'GET',
             dataType: 'json',
             beforeSend: function() {
                 //$("#resultado").html("Procesando, espere por favor...");
             },
             success: function(response) {
                 $('#cantidad').html(response.nuevas_count);
                 var op = '';
                 $.each(response.notificaciones, function(index, value) {
                     op += "<li>";
                     op += "<a href=''>";
                     op += "<div>";
                     op += "<i class='fa fa-envelope fa-fw'></i>La bodega " + value.nombre_bodega + " que usted solicito ha sido aprobada ";
                     if (value.leido_solicitante == 0)
                         op += "<span style='color:white' class='pull-right text-muted small label label-primary'>Nuevo</span>";
                     op += "</div>";
                     op += "</a>";
                     op += " </li>";
                     op += "<li class='divider'></li>";

                 });
                 $('#notificacion').html(op);
                 //$('select[id=localidades]').bootstrapDualListbox('refresh', true);

             }
         });
     }
     //console.log(base_url);
     // Enable/disable fixed top navbar
     $('#fixednavbar').click(function() {
         if ($('#fixednavbar').is(':checked')) {
             $(".navbar-static-top").removeClass('navbar-static-top').addClass('navbar-fixed-top');
             $("body").removeClass('boxed-layout');
             $("body").addClass('fixed-nav');
             $('#boxedlayout').prop('checked', false);
             if (localStorageSupport) {
                 localStorage.setItem("boxedlayout", 'off');
             }
             if (localStorageSupport) {
                 localStorage.setItem("fixednavbar", 'on');
             }
         } else {
             $(".navbar-fixed-top").removeClass('navbar-fixed-top').addClass('navbar-static-top');
             $("body").removeClass('fixed-nav');
             $("body").removeClass('fixed-nav-basic');
             $('#fixednavbar2').prop('checked', false);
             if (localStorageSupport) {
                 localStorage.setItem("fixednavbar", 'off');
             }
             if (localStorageSupport) {
                 localStorage.setItem("fixednavbar2", 'off');
             }
         }
     });
     // Enable/disable fixed top navbar
     $('#fixednavbar2').click(function() {
         if ($('#fixednavbar2').is(':checked')) {
             $(".navbar-static-top").removeClass('navbar-static-top').addClass('navbar-fixed-top');
             $("body").removeClass('boxed-layout');
             $("body").addClass('fixed-nav').addClass('fixed-nav-basic');
             $('#boxedlayout').prop('checked', false);
             if (localStorageSupport) {
                 localStorage.setItem("boxedlayout", 'off');
             }
             if (localStorageSupport) {
                 localStorage.setItem("fixednavbar2", 'on');
             }
         } else {
             $(".navbar-fixed-top").removeClass('navbar-fixed-top').addClass('navbar-static-top');
             $("body").removeClass('fixed-nav').removeClass('fixed-nav-basic');
             $('#fixednavbar').prop('checked', false);
             if (localStorageSupport) {
                 localStorage.setItem("fixednavbar2", 'off');
             }
             if (localStorageSupport) {
                 localStorage.setItem("fixednavbar", 'off');
             }
         }
     });
     // Enable/disable fixed sidebar
     $('#fixedsidebar').click(function() {
         if ($('#fixedsidebar').is(':checked')) {
             $("body").addClass('fixed-sidebar');
             $('.sidebar-collapse').slimScroll({
                 height: '100%',
                 railOpacity: 0.9
             });
             if (localStorageSupport) {
                 localStorage.setItem("fixedsidebar", 'on');
             }
         } else {
             $('.sidebar-collapse').slimscroll({
                 destroy: true
             });
             $('.sidebar-collapse').attr('style', '');
             $("body").removeClass('fixed-sidebar');
             if (localStorageSupport) {
                 localStorage.setItem("fixedsidebar", 'off');
             }
         }
     });
     // Enable/disable collapse menu
     $('#collapsemenu').click(function() {
         if ($('#collapsemenu').is(':checked')) {
             $("body").addClass('mini-navbar');
             SmoothlyMenu();
             if (localStorageSupport) {
                 localStorage.setItem("collapse_menu", 'on');
             }
         } else {
             $("body").removeClass('mini-navbar');
             SmoothlyMenu();
             if (localStorageSupport) {
                 localStorage.setItem("collapse_menu", 'off');
             }
         }
     });
     // Enable/disable boxed layout
     $('#boxedlayout').click(function() {
         if ($('#boxedlayout').is(':checked')) {
             $("body").addClass('boxed-layout');
             $('#fixednavbar').prop('checked', false);
             $('#fixednavbar2').prop('checked', false);
             $(".navbar-fixed-top").removeClass('navbar-fixed-top').addClass('navbar-static-top');
             $("body").removeClass('fixed-nav');
             $("body").removeClass('fixed-nav-basic');
             $(".footer").removeClass('fixed');
             $('#fixedfooter').prop('checked', false);
             if (localStorageSupport) {
                 localStorage.setItem("fixednavbar", 'off');
             }
             if (localStorageSupport) {
                 localStorage.setItem("fixednavbar2", 'off');
             }
             if (localStorageSupport) {
                 localStorage.setItem("fixedfooter", 'off');
             }
             if (localStorageSupport) {
                 localStorage.setItem("boxedlayout", 'on');
             }
         } else {
             $("body").removeClass('boxed-layout');
             if (localStorageSupport) {
                 localStorage.setItem("boxedlayout", 'off');
             }
         }
     });
     // Enable/disable fixed footer
     $('#fixedfooter').click(function() {
         if ($('#fixedfooter').is(':checked')) {
             $('#boxedlayout').prop('checked', false);
             $("body").removeClass('boxed-layout');
             $(".footer").addClass('fixed');
             if (localStorageSupport) {
                 localStorage.setItem("boxedlayout", 'off');
             }
             if (localStorageSupport) {
                 localStorage.setItem("fixedfooter", 'on');
             }
         } else {
             $(".footer").removeClass('fixed');
             if (localStorageSupport) {
                 localStorage.setItem("fixedfooter", 'off');
             }
         }
     });
     // SKIN Select
     $('.spin-icon').click(function() {
         $(".theme-config-box").toggleClass("show");
     });
     // Default skin
     $('.s-skin-0').click(function() {
         $("body").removeClass("skin-1");
         $("body").removeClass("skin-2");
         $("body").removeClass("skin-3");
     });
     // Blue skin
     $('.s-skin-1').click(function() {
         $("body").removeClass("skin-2");
         $("body").removeClass("skin-3");
         $("body").addClass("skin-1");
     });
     // Inspinia ultra skin
     $('.s-skin-2').click(function() {
         $("body").removeClass("skin-1");
         $("body").removeClass("skin-3");
         $("body").addClass("skin-2");
     });
     // Yellow skin
     $('.s-skin-3').click(function() {
         $("body").removeClass("skin-1");
         $("body").removeClass("skin-2");
         $("body").addClass("skin-3");
     });
     if (localStorageSupport) {
         var collapse = localStorage.getItem("collapse_menu");
         var fixedsidebar = localStorage.getItem("fixedsidebar");
         var fixednavbar = localStorage.getItem("fixednavbar");
         var fixednavbar2 = localStorage.getItem("fixednavbar2");
         var boxedlayout = localStorage.getItem("boxedlayout");
         var fixedfooter = localStorage.getItem("fixedfooter");
         if (collapse == 'on') {
             $('#collapsemenu').prop('checked', 'checked')
         }
         if (fixedsidebar == 'on') {
             $('#fixedsidebar').prop('checked', 'checked')
         }
         if (fixednavbar == 'on') {
             $('#fixednavbar').prop('checked', 'checked')
         }
         if (fixednavbar2 == 'on') {
             $('#fixednavbar2').prop('checked', 'checked')
         }
         if (boxedlayout == 'on') {
             $('#boxedlayout').prop('checked', 'checked')
         }
         if (fixedfooter == 'on') {
             $('#fixedfooter').prop('checked', 'checked')
         }
     }
      </script>
    </body>
  </html>
  <!--===== Footer End ========-->-
