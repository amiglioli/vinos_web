<li class="nav-header">
   <div class="dropdown profile-element">
      <span>
         @if(count(Auth::user()->imagen) > 0)
         <img alt="image" class="imgprofile" src="{{URL::asset('/imgusuario/'.Auth::user()->imagen[0]->path)}}">
         @else
         <img alt="image" class="imgprofile" src="{{asset('template/img/user.png')}}">
         @endif
      </span>
      <a data-toggle="dropdown" class="dropdown-toggle" href="#">
         <span class="clear"> <span class="block m-t-xs"> <strong class="h3">{{Auth::user()->name}}</strong>
            </span> <span class="text-muted text-xs block">{{Auth::user()->email}}<b class="caret"></b></span> </span> </a>
            <ul class="dropdown-menu animated fadeInRight m-t-xs">
               <li><a href="{{url('perfil')}}">Perfil</a></li>
            </ul>


         </div>
         <div class="logo-element">
            <img src="http://vinosweb.sytes.net/img/logo-2.png" alt="">
         </div>
      </li>
      <li>
         <a href="{{url('inicio')}}"><i class=" ti-home"></i> <span class="nav-label">Inicio</span></a>
      </li>
      <li>
         <a href="{{url('buscar')}}"><i class=" ti-search"></i> <span class="nav-label">Buscar</span></a>
      </li>
      @if(!Auth::user()->hasRole('sommelier'))
      <li>
         <a href="{{url('perfil')}}"><i class=" ti-user"></i> <span class="nav-label">Perfil</span></a>
      </li>
      <li>
         <a href="{{url('perfil/upload')}}"><i class=" ti-image"></i> <span class="nav-label">Mis Imágenes</span></a>
      </li>
      @endif

      
      @can('admin')
      <li>
         <a href=""><i class="fa fa-user"></i> <span class="nav-label">Perfileria</span> <span class="fa arrow"></span></a>
         <ul class="nav nav-second-level collapse">
            <li><a href="{{url('admin/usuarios')}}">Usuarios</a></li>
            <li><a href="{{url('admin/roles')}}">Roles</a></li>
            <li><a href="{{url('permission')}}">Permisos</a></li>
         </ul>
      </li>
      <li>
         <a href="{{url('catas')}}"><i class="ti-settings"></i> <span class="nav-label">Admin. Catas</span> <span class="fa arrow"></span></a>
      </li>
      <li>
         <a href="{{URL::to('cata_ranking')}}"><i style="font-size:25px;" class="fa fa-wine-glass"></i> <span style="margin-left:4px;" class="nav-label">Catas REDVIN</span> <span class="fa arrow"></span></a>
      </li>
      <li>
         <a href="{{url('catadores')}}"><i class="fa fa-users"></i> <span class="nav-label">Catadores</span> <span class="fa arrow"></span></a>
      </li>
      <li>
         <a href="{{url('productos')}}"><i class="fa fa-dolly"></i> <span class="nav-label">Productos</span> <span class="fa arrow"></span></a>
      </li>
      <li>
         <a href="{{url('ficha_tranquilo')}}"><i class="fa fa-clipboard-list"></i> <span class="nav-label">Fichas de Puntuaciónes</span> <span class="fa arrow"></span></a>
      </li>
      <li>
         <a href="{{url('aprobacion_bodega')}}"><i class="ti-check-box"></i> <span class="nav-label">Aprob. Bodegas</span> <span class="fa arrow"></span></a>
      </li>
      <li>
         <a href=""><i class="ti-view-list"></i> <span class="nav-label">Listado</span> <span class="fa arrow"></span></a>
         <ul class="nav nav-second-level collapse">
            <li><a href="{{url('listado/bodega')}}">Bodegas</a></li>
            <li><a href="{{url('listado/vinoteca')}}">Vinotecas</a></li>
            <li><a href="{{url('listado/distribuidora')}}">Distribuidoras</a></li>
         </ul>
      </li>
      <li>
         <a href=""><i class="ti-view-grid"></i> <span class="nav-label">Tablas</span> <span class="fa arrow"></span></a>
         <ul class="nav nav-second-level collapse">
            <li><a href="{{url('bodegas_secundarias')}}">Bodegas Secundarias</a></li>
            <li><a href="{{url('producto')}}">Productos</a></li>
            <li><a href="{{url('producto_tipo')}}">Tipos Vino</a></li>
            <li><a href="{{url('varietales')}}">Varietales</a></li>
            <li><a href="{{url('especializaciones')}}">Especializacíon</a></li>
            <li><a href="{{url('paises')}}">Paises</a></li>
            <li><a href="{{url('provincias')}}">Provincias</a></li>
            <li><a href="{{url('regiones')}}">Regiones</a></li>
            <li><a href="{{url('ciudades')}}">Ciudades</a></li>
            <li><a href="{{url('puntuacion_tranquilos')}}">Puntuaciones</a></li>
         </ul>
      </li>
      <li>
         <a href="{{url('pedidos_admin')}}"><i class="ti-layers-alt"></i> Pedidos a resolver. @if(cantidad_pedidos_no_resueltos() != 0)<span class="label label-danger">{{cantidad_pedidos_no_resueltos()}} @endif</span></a>
      </li>
      <li>
         <a href="{{url('solicitud_bodega')}}"><i class="ti-clipboard"></i> Solicitud Bodega. @if(solicitudes_bodegas() != 0)<span class="label label-danger">{{solicitudes_bodegas()}} @endif</span></a>
      </li>
      <li>
         <a href="{{url('eventos')}}"><i class="ti-calendar"></i>Eventos</span></a>
      </li>
      <li>
         <a href="{{url('participa')}}"><i class="ti-hand-stop"></i>Participantes</span></a>
      </li>
      <li>
         <a href="{{url('sugerir_producto')}}"><i class="ti-light-bulb"></i>Sugerencias</span></a>
      </li>
      @endcan
      @if(Auth::user()->hasRole('vinoteca'))
      <li>
         <a href="{{URL::to('cata_ranking')}}"><i style="font-size:25px;" class="fa fa-wine-glass"></i> <span style="margin-left:6px;" class="nav-label">Catas REDVIN</span> <span class="fa arrow"></span></a>
      </li>
      <li>
         <a href="{{url('promociones')}}"><i class="ti-announcement"></i> <span class="nav-label">Promociones</span> <span class="fa arrow"></span></a>
      </li>
      <li>
         <a href="{{url('horarios')}}"><i class="ti-timer"></i> <span class="nav-label">Horarios</span> <span class="fa arrow"></span></a>
      </li>
      <li>
         <a href="{{url('mediospago')}}"><i class="ti-credit-card"></i> <span class="nav-label">Medios de Pago</span> <span class="fa arrow"></span></a>
      </li>
      <li>
         <a href="{{url('participa/create')}}"><i class="ti-hand-stop"></i> <span class="nav-label">Participá</span> <span class="fa arrow"></span></a>
      </li>
      <li>
         <a href="{{url('sugerir_producto/create')}}"><i class="ti-light-bulb"></i> <span class="nav-label">Sugerir Producto</span> <span class="fa arrow"></span></a>
      </li>
          <li>
         <a href="{{url('eventos')}}"><i class="ti-calendar"></i> <span class="nav-label">Eventos</span> <span class="fa arrow"></span></a>
      </li>
      @endif
      @if(Auth::user()->hasRole('distribuidora'))
      <li>
         <a href="{{url('portfolio')}}"><i class="ti-bag"></i> <span class="nav-label">Portfolio</span> <span class="fa arrow"></span></a>
      </li>
      <li>
         <a href="{{URL::to('cata_ranking')}}"><i style="font-size:25px;" class="fa fa-wine-glass"></i> <span style="margin-left:6px;" class="nav-label">Catas REDVIN</span> <span class="fa arrow"></span></a>
      </li>
      <li>
         <a href="{{url('pedidos')}}"><i class="ti-notepad"></i> Pedidos @if(cantidad_pedidos() != 0)<span class="label label-danger">{{cantidad_pedidos()}} @endif</span></a>
      </li>
      <li>
         <a href="{{url('participa/create')}}"><i class="ti-hand-stop"></i> <span class="nav-label">Participá</span> <span class="fa arrow"></span></a>
      </li>
      <li>
         <a href="{{url('sugerir_producto/create')}}"><i class="ti-light-bulb"></i> <span class="nav-label">Sugerir Producto</span> <span class="fa arrow"></span></a>
      </li>
      <li>
         <a href="{{url('eventos')}}"><i class="ti-calendar"></i> <span class="nav-label">Eventos</span> <span class="fa arrow"></span></a>
      </li>
      @endif
      @if(Auth::user()->hasRole('bodega'))
      <li>
         <a href="{{URL::to('cata_ranking')}}"><i style="font-size:25px;" class="fa fa-wine-glass"></i> <span style="margin-left:6px;" class="nav-label">Catas REDVIN</span> <span class="fa arrow"></span></a>
      </li>
      <li>
         <a href="{{url('horarios')}}"><i class="ti-timer"></i> <span class="nav-label">Horarios</span> <span class="fa arrow"></span></a>
      </li>
      <li>
         <a href="{{url('participa/create')}}"><i class="ti-hand-stop"></i> <span class="nav-label">Participá</span> <span class="fa arrow"></span></a>
      </li>
      <li>
         <a href="{{url('sugerir_producto/create')}}"><i class="ti-light-bulb"></i> <span class="nav-label">Sugerir Producto</span> <span class="fa arrow"></span></a>
      </li>
      <li>
         <a href="{{url('eventos')}}"><i class="ti-calendar"></i> <span class="nav-label">Eventos</span> <span class="fa arrow"></span></a>
      </li>
      @endif


   @if(Auth::user()->hasRole('sommelier'))
      <li>
         <a href="{{URL::to('cata_ranking')}}"><i class="fa fa-wine-glass"></i> <span class="nav-label">Catas REDVIN</span> <span class="fa arrow"></span></a>
      </li>
      <li>
         <a href="{{url('participa/create')}}"><i class="ti-hand-stop"></i> <span class="nav-label">Participá</span> <span class="fa arrow"></span></a>
      </li>
      <li>
         <a href="{{url('sugerir_producto/create')}}"><i class="ti-light-bulb"></i> <span class="nav-label">Sugerir Producto</span> <span class="fa arrow"></span></a>
      </li>
      <li>
         <a href="{{url('eventos')}}"><i class="ti-calendar"></i> <span class="nav-label">Eventos</span> <span class="fa arrow"></span></a>
      </li>
      <li>
         <a href="{{url('sommelier/create')}}"><i class="ti-id-badge"></i> <span class="nav-label">Trabajo</span> <span class="fa arrow"></span></a>
      </li>
      @endif