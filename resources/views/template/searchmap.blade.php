<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="apple-mobile-web-app-capable" content="yes" />
    <meta name="description" content="">
    <meta name="keywords" content="thema bootstrap template, thema admin, bootstrap, admin template, bootstrap admin">
    <meta name="author" content="LanceCoder">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <link rel="shortcut icon" href="{{ url('img/favicon.png') }}">
    <title>SISTEMA {{ config('app.name') }}</title>
    <link href="{{asset('template/css/bootstrap.min.css')}}" rel="stylesheet">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.13/css/all.css" integrity="sha384-DNOHZ68U8hZfKXOrtjWvjxusGo9WQnrNx2sqG0tfsghAvtVlRW3tvkXWZh58N9jp" crossorigin="anonymous">
    <link href="{{asset('template/css/animate.css')}}" rel="stylesheet">
    <link href="{{asset('template/css/plugins/iCheck/custom.css')}}" rel="stylesheet">
    <link href="{{asset('template/css/bootstrap-duallistbox.min.css')}}" rel="stylesheet">
    <link href="{{asset('template/css/checkbox.css')}}" rel="stylesheet">
    <link href="{{asset('template/css/select2.min.css')}}" rel="stylesheet">
    <link href="{{asset('template/css/bootstrap-chosen.css')}}" rel="stylesheet">
    <link href="{{asset('template/css/awesome-bootstrap-checkbox.css')}}" rel="stylesheet">
    <link href="{{asset('template/css/datepicker3.css')}}" rel="stylesheet">
    <link href="{{asset('template/css/toastr.min.css')}}" rel="stylesheet">
    <link href="{{asset('template/themify-icons/themify-icons.css')}}" rel="stylesheet">
    @yield('linkscss')
  </head>
  <body>
    @yield('content')
    <script src="{{asset('template/js/jquery-2.1.1.js')}}" type="text/javascript" ></script>
    <script src="{{asset('template/js/moment-with-locales.js')}}" type="text/javascript" ></script>
    <script src="{{asset('template/js/toastr.min.js')}}" type="text/javascript" ></script>
    <script src="{{asset('template/js/alltheme.js')}}" type="text/javascript" ></script>
    <script src="{{asset('template/js/bootstrap.min.js')}}" type="text/javascript" ></script>
    <script src="{{asset('template/js/plugins/metisMenu/jquery.metisMenu.js')}}" type="text/javascript" ></script>
    <script src="{{asset('template/js/plugins/slimscroll/jquery.slimscroll.min.js')}}" type="text/javascript" ></script>
    <script src="{{asset('template/js/inspinia.js')}}" type="text/javascript" ></script>
    <script src="{{asset('template/js/plugins/dualListbox/jquery.bootstrap-duallistbox.js')}}" type="text/javascript" ></script>
    <script src="{{asset('template/js/plugins/pace/pace.min.js')}}" type="text/javascript" ></script>
    <script src="{{asset('template/js/plugins/iCheck/icheck.min.js')}}" type="text/javascript" ></script>
    <script src="{{asset('template/js/select2.full.min.js')}}" type="text/javascript" ></script>
    <script src="{{asset('template/js/bootstrap-datepicker.js')}}" type="text/javascript" ></script>
    <script src="{{asset('template/js/icheck.min.js')}}" type="text/javascript" ></script>
    <script src="{{asset('assets/knockout/knockout-3.4.0.js')}}" type="text/javascript" ></script>
    <script src="{{asset('template/js/sweetalert.min.js')}}" type="text/javascript" ></script>
    <script>
      var base_url = "<?= URL::to('') ?> ";
      base_url = base_url.trim();
    </script>
    @yield('scripts')
    @yield('js')
    </body>
  </html>
  <!--===== Footer End ========-->
