<div class="modal inmodal" id="myModal2" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog">
    <form class="validator form-horizontal" action="{{ URL::to('solicitud_bodega') }}" method="post">
      {{ csrf_field() }}
      <input id="id_solicitud" type="hidden" class="form-control " name="id_solicitud" value="" required>
      <div class="modal-content animated flipInY">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
          <h4 class="modal-title">Alta de bodega solicitada por distribuidora </h4>
        </div>
        <div class="modal-body">
          <div class="form-group{{ $errors->has('nombre_bodega') ? ' has-error' : '' }} has-success">
            <label for="bodega" class="col-md-4 control-label">Denominación</label>

            <div class="col-md-6">
              <input id="user" type="text" class="form-control " name="user" value="{{ old('user') }}" required>
              @if ($errors->has('user'))
              <span class="help-block">
                <strong>{{ $errors->first('user') }}</strong>
              </span>
              @endif
            </div>
          </div>
              <input id="email" type="hidden" class="form-control" name="email" value="{{ old('email') }}" required>
        </div>
        <div class="modal-footer">
          <a type="button" id="boton_descartar" onclick="eliminarsolicitud()" href="" class="btn btn-danger pull-left">Descartar solicitud</i></a>
          <button type="submit" class="btn btn-primary">Enviar</button>
        </div>
      </div>
    </form>
  </div>
</div>
