<style>
.letra{
font-size:14px;
}
#colorth{
background-color:#d02026 !important;
font-weight: bold;
}
#table_pedidos {
border-collapse: collapse;
width: 100%;
}
#table_pedidos td, #table_pedidos th {
border: 1px solid #ddd;
padding: 8px;
}
#table_pedidos tr:nth-child(even){background-color: #f2f2f2;}
#table_pedidos tr:hover {background-color: #ddd;}
#table_pedidos th {
padding-top: 12px;
padding-bottom: 12px;
text-align: left;
background-color: #d02026;
color: white;
}
</style><div class="modal fade" id="modal_producto" role="dialog">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <div class="row wrapper red-bg">
          <div class="col-lg-10">
            <h2>Información del producto</h2>
          </div>
          <div class="col-lg-2">
          </div>
        </div>
      </div>
      <div class="modal-body">
        <center> <p id="p_modal">Estamos cargando sus datos..</p></center>
        <div class="row">
          <div class="col-md-12">
            <table id="table_pedidos">
              <tr>
                <th class="titulo">Vinoteca</th>
                <th class="titulo">Detalle</th>
              </tr>
              <tr>
                <td class="informacion">Nombre</td>
                <td class="detalle" id="vinoteca_m"></td>
              </tr>
              <tr>
                <td class="informacion">Provincia</td>
                <td class="detalle" id="provincia_m"></td>
              </tr>
              <tr>
                <td class="informacion">Partido</td>
                <td class="detalle" id="municipio_m"></td>
              </tr>
              <tr>
                <td class="informacion">Localidad</td>
                <td class="detalle" id="localidad_m"></td>
              </tr>
              <tr>
                <td class="informacion">Calle</td>
                <td class="detalle" id="domicilio_m"></td>
              </tr>
              <tr>
                <td class="informacion">N°</td>
                <td class="detalle" id="numero_m"></td>
              </tr>
              <tr>
                <td class="informacion">Teléfono</td>
                <td class="detalle" id="telefono_m"></td>
              </tr>
              <tr>
                <td class="informacion">Producto</td>
                <td class="detalle" id="producto_m"></td>
              </tr>
              <tr>
                <td class="informacion">Bodega</td>
                <td class="detalle" id="bodega_m"> </td>
              </tr>
              <tr>
                <td class="informacion">Varietal</td>
                <td class="detalle" id="varietal_m"> </td>
              </tr>
              <tr>
                <td class="informacion">Año</td>
                <td class="detalle" id="anio_m"></td>
              </tr>
            </table>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <a type="button" class="btn btn-warning" id="print">PDF <i class="fa fa-file-pdf"></i></a>
        <button type="button" class="btn btn-danger pull-left" data-dismiss="modal" >Cerrar</button>
      </div>
    </div>
  </div>
</div>