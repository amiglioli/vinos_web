
<style>
  .letra{
    font-size:14px;
  }
  #colorth{
    background-color:#d02026 !important;
    font-weight: bold;
  }

  #table_productos {
    font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;
    border-collapse: collapse;
    width: 100%;
  }

  #table_productos td, #table_productos th {
    border: 1px solid #ddd;
    padding: 8px;
  }

  #table_productos tr:nth-child(even){background-color: #f2f2f2;}

  #table_productos tr:hover {background-color: #ddd;}

  #table_productos th {
    padding-top: 12px;
    padding-bottom: 12px;
    text-align: left;
    background-color: #d02026 !important;
    color: white;
  }

</style>
<h1>Pedido de {{$data->nombre_vinoteca}}</h1>
<table id="table_productos">
  <tr>
    <th class="titulo">Vinoteca</th>
    <th class="titulo">Detalle</th>
  </tr>
  <tr>
    <td class="informacion">Vinoteca</td>
    <td class="detalle">{{ $data->nombre_vinoteca }}</td>
  </tr>
  <tr>
    <td class="informacion">Provincia</td>
    <td class="detalle">{{ $data->nombre_provincia }}</td>
  </tr>
  <tr>
    <td class="informacion">Municipio</td>
    <td class="detalle">{{ $data->nombre_municipio }}</td>
  </tr>
  <tr>
    <td class="informacion">Localidad</td>
    <td class="detalle">{{ $data->localidad }}</td>
  </tr>
  <tr>
    <td class="informacion">Calle</td>
    <td class="detalle">{{ $data->domicilio }}</td>
  </tr>
  <tr>
    <td class="informacion">N°</td>
    <td class="detalle"> {{ $data->numero }} </td>
  </tr>
  <tr>
    <td class="informacion">Teléfono</td>
    <td class="detalle"> {{ $data->telefono }}   </td>
  </tr>
  <tr>
    <td class="informacion">Producto</td>
    <td class="detalle"> {{ $data->producto_marca }} </td>
  </tr>
  <tr>
    <td class="informacion">Bodega</td>
    <td class="detalle"> {{ $data->nombre_bodega }} </td>
  </tr>
  <tr>
    <td class="informacion">Varietal</td>
    <td class="detalle"> {{ $data->varietal }} </td>
  </tr>
  <tr>
    <td class="informacion">Año</td>
    <td class="detalle"> {{ $data->producto_anio }} </td>
  </tr>
</table>