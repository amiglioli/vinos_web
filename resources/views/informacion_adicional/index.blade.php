<!DOCTYPE html>
<html>
<style>
body{
  font-family: arial;
  font-size: 13px;
}
#mapCanvas {
  margin-top: 3%;
  width: 100%;
  height: 360px;
}
#infoPanel {
  margin-left: 30%;
}
#infoPanel div {
}

</style>
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title></title>
  <link href="{{asset('template/css/bootstrap.min.css')}}" rel="stylesheet">
  <link href="{{asset('template/font-awesome/css/font-awesome.css')}}" rel="stylesheet">
  <link href="{{asset('template/css/plugins/iCheck/custom.css')}}" rel="stylesheet">
  <link href="{{asset('template/css/animate.css')}}" rel="stylesheet">
  <link href="{{asset('template/css/style.css')}}" rel="stylesheet">
  <link href="{{asset('template/css/bootstrap-chosen.css')}}" rel="stylesheet">
</head>
@if (Session::has('message_error'))
<div id="message-alert" class="alert alert-danger">
  <button type="button" class="close"
  data-target="#message-alert"
  data-dismiss="alert">
  <span aria-hidden="true">&times;</span><span class="sr-only">Close</span>
</button>
@foreach (Session::pull('message_error') as  $error )
{{$error[0]}} <br>
@endforeach
</div>
@elseif (Session::has('message_success'))
<div id="message-alert" class="alert alert-success">
  <button type="button" class="close"
  data-target="#message-alert"
  data-dismiss="alert">
  <span aria-hidden="true">&times;</span><span class="sr-only">Close</span>
</button>
@foreach (Session::pull('message_success') as  $success )
{{$success[0]}} <br>
@endforeach
</div>
@elseif (Session::has('message_warning'))
<div id="message-alert" class="alert alert-warning ">
  <button type="button" class="close"
  data-target="#message-alert"
  data-dismiss="alert">
  <span aria-hidden="true">&times;</span><span class="sr-only">Close</span>
</button>
@foreach (Session::pull('message_warning') as  $warning )
{{$warning[0]}} <br>
@endforeach
</div>
@endif
<body>

  <div class="text-center">
    <div class="row jumbotron">
      <h3>Registrarse  en WINES+</h3>
      <p>Estos seran los ultimos pasos, esta informacion es importante para que su local quede posicionado en nuestro sistema<br>Asegurese que todos los datos ingresados sean correctos</p>
      <div class="col-md-6">
        <!-- <input id="searchTextField" class="form-control" type="text" class="form-control"> -->
        <form class="m-t" method="POST" action="{{ URL::to('completar_registro/'.Auth::user()->id) }}">
          {{ csrf_field() }}

          <div class="form-group col-lg-10">
            <select data-placeholder="Seleccione su direccion" class="form-control chosen-select" value="{{ old('provincia', $info_contacto->provincia)}}" name="provincia"   id="provincia"  tabindex="1">
              @foreach($provincias as $p)
                @if(!old('provincia'))
                  <option @if($info_contacto->provincia == $p->provincia_id) selected @endif value="{{$p->provincia_id}}">{{$p->provincia}}</option>
                @else
                  <option @if(old('provincia') == $p->provincia_id) selected @endif value="{{$p->provincia_id}}">{{$p->provincia}}</option>
                @endif

              @endforeach
            </select>

          </div>
          <div class="form-group col-lg-10">
            <select data-placeholder="Seleccione ciudad..." class="chosen-select form-control"  name="domicilio_partido"  id="partido"  tabindex="1">
            </select>
            @if ($errors->has('partido'))
            <span class="help-block">
              <strong>{{ $errors->first('partido') }}</strong>
            </span>
            @endif
          </div>

          <div class="form-group col-lg-10">
            <input id="domicilio_localidad" type="text" class="form-control" name="domicilio_localidad" value="{{ old('domicilio_localidad') }}" required placeholder="Ingrese su localidad">
          </div>

          <div class="form-group col-lg-10">
            <input id="cp" type="text" class="form-control" name="cp" value="{{ old('cp') }}" required placeholder="Ingrese su Codigo Postal">
            @if ($errors->has('cp'))
            <span class="help-block">
              <strong>{{ $errors->first('cp','El código postal puede tener hasta 8 caracteres') }}</strong>
            </span>
            @endif
          </div>

          <div class="form-group col-lg-10">
            <input id="direccion_domicilio" type="text" class="form-control" name="direccion_domicilio" value="{{ old('direccion_domicilio') }}" required placeholder="Ingrese su Calle">
            @if ($errors->has('direccion_domicilio'))
            <span class="help-block">
              <strong>{{ $errors->first('direccion_domicilio') }}</strong>
            </span>
            @endif
          </div>

          <div class="form-group col-lg-10">
            <input id="direccion_numero" type="text" class="form-control" name="direccion_numero" value="{{ old('direccion_numero') }}" required placeholder="Ingrese su Numero de Domicilio">
          </div>

          <div class="form-group col-lg-10">
            <input id="web" type="text" class="form-control" name="telefono" value="{{ old('telefono') }}" placeholder="Ingrese su Telefono (Opcional)">
          </div>

          <div class="form-group col-lg-10">
            <input id="web" type="text" class="form-control" name="web" value="{{ old('web') }}" placeholder="Ingrese su Pagina Web (Opcional)">
          </div>

        </div>
        <div class="col-md-6">

          <div class="form-group">
            <div  id="mapCanvas" style="border:1px solid;"></div>
            <input type="hidden" id="info" type="text" size="50" value="">
          </div>

          <div class="form-group">
            <input type="hidden" id="geo_latitud" type="text" class="form-control" name="geo_latitud" value="{{ old('geo_latitud') }}" required placeholder="Ingrese su Latitud" readonly="">
          </div>
          <div class="form-group">
            <input type="hidden" id="geo_longitud" type="text" class="form-control" name="geo_longitud" value="{{ old('geo_longitud') }}" required placeholder="Ingrese su Longitud" readonly="">
          </div>
        </div>
        <div class="col-md-12 text-center">
          <button type="submit" class="btn btn-primary">Completar Registro</button>
        </div>
      </form>
      </div>
      <h4 style="color:#fff;margin-top:1%;"> Bienvenido a {{env('APP_NAME','Wines')}}</h4>
  </div>

  <script src="{{asset('template/js/jquery-2.1.1.js')}}" type="text/javascript" ></script>

  <script src="{{asset('template/js/bootstrap.min.js')}}" type="text/javascript" ></script>
  <script src="{{asset('template/js/plugins/iCheck/icheck.min.js')}}" type="text/javascript" ></script>
  <script type="text/javascript" src="http://maps.google.com/maps/api/js?&libraries=places&key=AIzaSyD9FZU87YCCSGovLg57u9dvi2jLDDz3uyw"></script>
  <script src="{{asset('template/js/plugins/chosen/chosen.jquery.js')}}" type="text/javascript" ></script>


  <script type="text/javascript">
  var base_url = "<?= URL::to('') ?> ";
  base_url = base_url.trim();

  $(document).ready(function(){
    $('.chosen-select').chosen({});
    var provincia_id = $('#provincia').val();
    carga_localidades(provincia_id);
  });

  $('#direccion_numero').focusout(function(){
    codeAddress();
  });

  var marker;
  var map;

  function codeAddress() {
    var address = 'San Diego, CA';
    var geocoder = new google.maps.Geocoder();

    var direccion = $("#direccion_domicilio").val();
    var domicilio_localidad = $("#domicilio_localidad").val();
    var numero = $("#direccion_numero").val();

    var partido = $('#partido option:selected').text();
    var provincia = $('#partido option:selected').text()

    var dir_completa = direccion+' '+numero+', '+domicilio_localidad+', '+provincia+', Argentina';

    console.log(dir_completa);

    geocoder.geocode({'address': dir_completa}, function(results, status) {
      if(status == google.maps.GeocoderStatus.OK) {
        if(status != google.maps.GeocoderStatus.ZERO_RESULTS) {
          var latitude = results[0].geometry.location.lat();
          var longitude = results[0].geometry.location.lng();
          $('#geo_latitud').val(latitude);
          $('#geo_longitud').val(longitude);

          marker.setPosition(results[0].geometry.location);
          map.setZoom(15);
          map.panTo(marker.position);
        }
      } else {
        console.log("En base a sus datos no pudimos capturar su ubicacion");
      }
    });
  }


  $('#provincia').change(function(){
    carga_localidades($(this).val());
  });

  function carga_localidades(id)
  {
    var base_url = "<?= URL::to('') ?> ";
    base_url = base_url.trim();
    $.ajax({
      url:  base_url+'/localidades/'+id,
      type:  'GET',
      dataType : 'json',
      beforeSend: function () {
        $("#resultado").html("Procesando, espere por favor...");
      },
      success:  function (response) {
        $("#partido").children().remove();
        var html = '';
        var op = "<option  value=''></option>";
        $('#partido').append(op);
        $.each(response, function( index, value ) {
          op = "<option  value='"+value.municipio_id+"'>"+value.municipio+"</option>";
          $('#partido').append(op);
        });
        $('#partido').trigger("chosen:updated");
        //$('select[id=localidades]').bootstrapDualListbox('refresh', true);
      }
    });
  }
  var geocoder = new google.maps.Geocoder();

  function geocodePosition(pos) {
    geocoder.geocode({
      latLng: pos
    }, function(responses) {
      if (responses && responses.length > 0) {
        updateMarkerAddress(responses[0].formatted_address);
        ubicacion = responses[0].formatted_address.split(",");

        if (responses[0].address_components) {
          for (var i in responses[0].address_components) {
            if (typeof(responses[0].address_components[i]) === "object" && responses[0].address_components[i].types[0] == "postal_code") {
              var result = responses[0].address_components[i].long_name;
            }
          }
        }
        if (!result) {
          //alert("Error, there is no postal code");
        } else {
          $("#cp").val(result);
        }

        direccion = ubicacion[0];
        partido  = ubicacion[1];
        provincia  = ubicacion[2];
        pais = responses[0].formatted_address.split(",").splice(-1,1);
        //$("#direccion").val(direccion);
        $("#domicilio_pais").val(pais);
        $("#web").val('');
      } else {
        updateMarkerAddress('Keine Koordinaten gefunden!');
      }
    });
  }


  function updateMarkerPosition(latLng) {
    document.getElementById('info').value = [
      latLng.lat(),
      latLng.lng()
    ].join(', ');

    $("#geo_latitud").val(latLng.lat());
    $("#geo_longitud").val(latLng.lng());

  }

  function updateMarkerAddress(str) {

    //document.getElementById('searchTextField').value = str;
    //document.getElementById("searchTextField").focus();
    //document.getElementById("searchTextField").blur();
  }

  function initialize(coord) {
    //alert(coord);
    var latLng = new google.maps.LatLng(coord.lat, coord.lng);
    map = new google.maps.Map(document.getElementById('mapCanvas'), {
      zoom: 5,
      center: latLng,
      mapTypeId: google.maps.MapTypeId.ROADMAP,
      streetViewControl: false,
      mapTypeControl: false

    });



    marker = new google.maps.Marker({
      position: latLng,
      map: map,
      draggable: true,
      title:"Sie können mich per Drag & Drop auf das gewünschte Ziel setzen. Oder im Suche feld den Ort eingeben und auswählen."
    });

    map.panTo(latLng);



    //var input = document.getElementById('searchTextField');
    //var autocomplete = new google.maps.places.Autocomplete(input);

    //autocomplete.bindTo('bounds', map);

    // Update current position info.
    updateMarkerPosition(latLng);
    //geocodePosition(latLng);

    // Add dragging event listeners.
    google.maps.event.addListener(marker, 'dragstart', function() {
      updateMarkerAddress('Dragging...');
    });

    google.maps.event.addListener(marker, 'drag', function() {

      updateMarkerPosition(marker.getPosition());
    });

    google.maps.event.addListener(marker, 'dragend', function() {
      geocodePosition(marker.getPosition());
    });

    /*  google.maps.event.addListener(autocomplete, 'place_changed', function() {
    input.className = 'form-control';
    var place = autocomplete.getPlace();
    if (!place.geometry) {
    // Inform the user that the place was not found and return.
    input.className = 'notfound';
    return;
  }

  // If the place has a geometry, then present it on a map.
  if (place.geometry.viewport) {
  map.fitBounds(place.geometry.viewport);
} else {

map.setCenter(place.geometry.location);
map.setZoom(17);

// Why 17? Because it looks good.
}
marker.setPosition(place.geometry.location);
updateMarkerPosition(marker.getPosition());



if (place.address_components) {
for (var i in place.address_components) {
if (typeof(place.address_components[i]) === "object" && place.address_components[i].types[0] == "postal_code") {
var result = place.address_components[i].long_name;
}
}
}
if (!result) {
alert("No hemos podido capturar su codigo postal");
} else {
$("#cp").val(result);
}

ubicacion = place.formatted_address.split(",");
direccion = ubicacion[0];
pais  = place.formatted_address.split(",").splice(-1,1);

if(marker != null){
lat = marker.getPosition().lat();
lng = marker.getPosition().lng();
web = place.website;
}
//$("#direccion").val(direccion);

//$("#domicilio_pais").val(pais);
//$("#web").val(web);
$("#geo_latitud").val(lat);
$("#geo_longitud").val(lng);
-65.9223088
});
*/
}

// Onload handler to fire off the app.
google.maps.event.addDomListener(window, 'load', function(){
  var np = $("#provincia option:selected").text();
  geocoder.geocode({'address': np+",Argentina"}, function(results, status) {
    if(status == google.maps.GeocoderStatus.OK) {
      if(status != google.maps.GeocoderStatus.ZERO_RESULTS) {
        var latitude = results[0].geometry.location.lat();
        var longitude = results[0].geometry.location.lng();
        $('#geo_latitud').val(latitude);
        $('#geo_longitud').val(longitude);
        initialize({'lat':latitude,'lng':longitude})
      }
    } else {
      console.log("En base a sus datos no pudimos capturar su ubicacion");
    }
  });

  ;});



</script>
</body>
</html>
