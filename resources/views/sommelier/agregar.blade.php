@extends('template.template')
@section('head_content')
<link href="{{asset('template/css/bootstrap-datetimepicker.css')}}" rel="stylesheet">
@endsection
@section('content')
<div class="row">
  <div class="col-lg-12 text-center">
    <div class="ibox float-e-margins">
      <div class="ibox-title" style="background-color: #d02026;color:#ffffff">
        <h5>Ofrecé tu servicio como Sommelier a través de REDVIN</h5>
      </div>
      <div class="ibox-content">
        <form class="validator form-horizontal" action="{{url('sommelier')}}" method="post">
          {{ csrf_field() }}

          <div class="form-group"><label class="col-lg-3 control-label">DNI</label>
            <div class="col-lg-6">
              <input type="number" name="dni" placeholder="INGRESE EL DNI" maxlength="8" value="{{ old('dni') }}" class="form-control" required>
            </div>
          </div>

          <div class="form-group"><label class="col-lg-3 control-label">APELLIDO</label>
            <div class="col-lg-6">
              <input type="text" name="apellido" value="{{ old('apellido') }}" placeholder="INGRESE EL APELLIDO" class="form-control" required>
            </div>
          </div>


          <div class="form-group"><label class="col-lg-3 control-label">NOMBRE</label>
            <div class="col-lg-6">
              <input type="text" name="nombre" value="{{ old('nombre') }}" placeholder="INGRESE EL NOMBRE" class="form-control" required>
            </div>
          </div>


          <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}" id="data_1">
            <label for="edad" class="col-md-3 control-label">FECHA DE NACIMIENTO</label>
            <div class="col-md-6 " >
              <input name="edad" id="edad" value="{{ old('edad') }}" autocomplete="off" type="text" class="form-control" required>
              @if ($errors->has('edad'))
              <span class="help-block">
                <strong>{{ $errors->first('edad') }}</strong>
              </span>
              @endif
            </div>
          </div>



          <div class="form-group"><label class="col-lg-3 control-label">CELULAR</label>
            <div class="col-lg-6">
              <input type="number" name="celular" value="{{ old('celular') }}" placeholder="INGRESE EL CELULAR" class="form-control" required>
            </div>
          </div>

          <div class="form-group"><label class="col-lg-3 control-label">EMAIL</label>
            <div class="col-lg-6">
              <input type="text"  name="email" value="{{ old('email') }}" placeholder="INGRESE EL EMAIL" class="form-control" required>
            </div>
          </div>

          <div class="form-group"><label class="col-lg-3 control-label">TITULO</label>
            <div class="col-lg-6">
              <input type="text"  name="titulo" value="{{ old('titulo') }}" placeholder="TIPO DE TITULO" class="form-control" required>
            </div>
          </div>


          <div class="form-group"><label class="col-lg-3 control-label">OBTENIDO EN</label>
            <div class="col-lg-6">
              <input type="text"  name="obtenido" value="{{ old('obtenido') }}" placeholder="DONDE OBTUVO EL TITULO?" class="form-control" required>
            </div>
          </div>


          <div class="form-group"><label class="col-lg-3 control-label">AÑO EGRESO</label>
            <div class="col-lg-6">
              <input type="text"  name="anio_egreso" value="{{ old('anio_egreso') }}" placeholder="AÑO DE EGRESO" class="form-control" required>
            </div>
          </div>

            <div class="form-group">
            <div class="col-md-12 col-md-offset-3">
              <div class="col-lg-3">
                <label>Provincia</label>
                <select name="provincia" id="provincia" class="form-control select2" required>
                  @foreach($provincias as $p)
                  <option value="{{$p->provincia_id}}">{{$p->provincia}}</option>
                  @endforeach
                </select>
              </div>
              <div class="col-lg-3">
                <label>Municipio</label>
                <select name="municipio" id="municipio" class="form-control select2">
                </select>
              </div>
            </div>
          </div>

          <div class="form-group"><label class="col-lg-3 control-label">EXPERENCIA COMO SOMMELIER</label>
            <div class="col-lg-6">
              <textarea class="form-control" name="experiencia" value="{{ old('experiencia') }}" rows="5" id="comment" required></textarea>
            </div>
          </div>


          <div class="form-group">
            <div class="col-lg-offset-1 col-lg-10">
              <button class="btn btn-sm btn-primary " type="submit">ENVIAR</button>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>
@endsection
@section('js')
<script src="//cdn.rawgit.com/Eonasdan/bootstrap-datetimepicker/e8bddc60e73c1ec2475f827be36e1957af72e2ea/src/js/bootstrap-datetimepicker.js"></script>
<script>
$(document).ready(function() {
  var dt = new Date();
  var edad = dt.setFullYear(new Date().getFullYear()-18);
  var anios =  moment(edad).format('YYYY-MM-DD');

  $('.select2').select2();
  $('#provincia').trigger("change");

  $('#edad').datetimepicker({
    keepOpen:true,
    showClose: true,
    viewMode: 'years',
    maxDate:anios,
    locale:"es",
    format: 'Y-MM-DD',
    widgetPositioning:{
      horizontal: 'auto',
      vertical: 'bottom'
    }
  });
    $('#edad').val("");
});

  $('#provincia').change(function(){
    carga_localidades($(this).val());
  });


  function carga_localidades(id)
  {
    var base_url = "{{url('')}}";
    $.ajax({
      url:  base_url+'/localidades/'+id,
      type:  'GET',
      dataType : 'json',
      beforeSend: function () {
        $("#resultado").html("Procesando, espere por favor...");
      },
      success:  function (response) {
        //$("#municipio").children().remove();
        //var html = '';
        var op = "<option  value='' disabled selected>Municipio</option>";
        
        $.each(response, function( index, value ) {
          op+= "<option  value='"+value.municipio_id+"'>"+value.municipio+"</option>";
        });

        $('#municipio').html(op);
        $('#municipio').trigger("change");
        //$('select[id=localidades]').bootstrapDualListbox('refresh', true);
      }
    });
  }
</script>
@endsection
