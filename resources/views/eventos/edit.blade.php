@extends('template.template')
@section('head_content')
<link href="{{asset('template/css/crop_eventos.css')}}" rel="stylesheet">
<link href="{{asset('template/css/magic-checkbox.css')}}" rel="stylesheet">
@endsection
<style>
#mapCanvas {
width: 100%;
height: 200px;
border:1px solid;
}
.title-crop{
background-color: white !important;
}

#avatar{
max-width:480px;
max-height:270px;
}

.modal-header{
background-color:#d02026
}
#modalLabel{
color:#ffffff; font-size:20px
}
.modal-body img {
  max-width: 100%;
}

</style>
@section('content')
<div class="row">
  <div class="col-lg-12 text-center">
    <div class="ibox float-e-margins">
      <div class="ibox-title" style="background-color: #d02026;color:#ffffff">
        <h5>Publicá tu Evento</h5>
      </div>
      <div class="ibox-content">
        <form class="validator form-horizontal" action="{{ url('eventos',$data->id) }}" method="post">
          @method('PUT')
          {{ csrf_field() }}
          <div class="form-group">
            <div class="col-lg-8 col-md-offset-2">
              <label CLASS="pull-left">Nombre del Evento</label>
              <input type="text" name="nombre_evento" placeholder="INGRESE EL NOMBRE DEL EVENTO" value="{{$data->nombre}}" class="form-control" required>
            </div>
          </div>
          <div class="form-group">
            <div class="col-lg-8 col-md-offset-2">
              <label CLASS="pull-left">Ubicación</label>
              <input type="text" name="ubicacion" id="ubicacion" value="{{$data->ubicacion}}" placeholder="INGRESE LA UBICACIÓN DEL EVENTO" class="form-control" required>
            </div>
          </div>
          <div class="form-group">
            <div class="col-md-8 col-md-offset-2">
              <div  id="mapCanvas" style="display:none"></div>
              <input type="hidden" id="info" type="text" size="50" value="">
            </div>
          </div>
          <div class="form-group">
            <div class="col-md-12 col-md-offset-2">
              <div class="col-lg-2">
                <label>Empieza</label>
                <input type="text" name="fecha_inicio" autocomplete="off" value="{{$data->fecha_inicio}}" id="start" placeholder="EMPIEZA" class="form-control" required>
              </div>
              <div class="col-lg-2">
                <label>Hora Inicio</label>
                <select autocomplete="off" name="hora_inicio" class="form-control select2">
                  @foreach($horarios as $h)
                  <option @if(substr($data->hora_inicio,0,-3) == $h) selected @endif  value="{{$h}}">{{$h}}</option>
                  @endforeach
                </select>
              </div>
              <div class="col-lg-2">
                <label>Finaliza</label>
                <input autocomplete="off" type="text" value="{{$data->fecha_fin}}" name="fecha_fin" value="{{$data->finaliza}}" id="end" placeholder="FINALIZA" class="form-control" required>
              </div>
              <div class="col-lg-2">
                <label>Hora Cierre</label>
                <select autocomplete="off" name="hora_fin" class="form-control select2">
                  @foreach($horarios as $h)
                  <option @if(substr($data->hora_fin,0,-3) == $h) selected @endif  value="{{$h}}">{{$h}}</option>
                  @endforeach
                </select>
              </div>
            </div>
          </div>
          <div class="col-md-12 col-md-offset-0">
            <label class="title-crop" data-toggle="tooltip" title="Seleccione su imagen">
              <img id="avatar" width=640 height=360 style="width:" src="{{asset($data->img_path)}}" alt="avatar">
              <input type="file" class="sr-only" id="input" name="image" accept="image/*">
            </label>
            <div class="alert" role="alert"></div>
            <div class="modal fade" id="modal" tabindex="-1" role="dialog" aria-labelledby="modalLabel" aria-hidden="true">
              <div class="modal-dialog" role="document">
                <div class="modal-content">
                  <div class="modal-header">
                    <h5 class="modal-title" id="modalLabel">Recorte la imagen para su Evento</h5>
                    <!--<button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                    -->
                  </div>
                  <div class="modal-body">
                    <div class="img-container">
                      <img id="image" src="https://avatars0.githubusercontent.com/u/3456749">
                    </div>
                  </div>
                  <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                    <button type="button" class="btn btn-primary" id="crop">Guardar</button>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="form-group">
            <div class="col-lg-8 col-md-offset-2">
              <label class="pull-left" for="comment">Descripción del Evento:</label>
              <textarea class="form-control" name="descripcion" rows="5" id="comment">{{$data->descripcion}}</textarea>
            </div>
          </div>
          <div class="form-group">
            <div class="col-lg-8 col-md-offset-2">
              <label class="pull-left" for="comment">Pagina Web (Opcional) :</label>
              <input type="text" value="{{$data->web}}" name="url" placeholder="INGRESE LA URL DE SU EVENTO" class="form-control">
            </div>
          </div>
          <div class="form-group col-md-12">
            <div class="col-lg-4 col-md-offset-2">
              <label CLASS="pull-left">Nombre del Organizador</label>
              <input type="text" name="organizador" value="{{$data->organizador}}" placeholder="NOMBRE DEL ORGANIZADOR" class="form-control" required>
            </div>
            <div class="col-lg-4">
              <label><u>PRECIO</u></label><br>
              <div clas="col-md-2">
                <input  @if($data->valor == null) checked @endif  class="magic-checkbox" type="radio" name="gratis_check" id="gratis_check">
                <label for="gratis_check" class="pull-left">GRATIS</label>
              </div>
              <div clas="col-md-2 ">
                <input @if($data->valor > 0) checked @endif class="magic-checkbox" type="radio" name="pago_check" id="pago_check">
                <label for="pago_check" class="pull-right">PAGO</label>
              </div>
              <!-- <label CLASS="pull-left">VALOR <span id="gratis" style="display:none;color:red"> (Su evento es gratis)</span> </label>
              <input type="number" name="valor" id="valor" value="0" placeholder="PRECIO DEL EVENTO" class="form-control" required> -->
            </div>
          </div>
          <div class="form-group" id="valor_div" style="display:none">
            <div class="col-lg-3 col-md-offset-2">
              <label class="pull-left" for="comment">¿CUAL ES EL VALOR DE SU EVENTO ?</label>
              <input type="text" name="valor" id="valor" value="{{$data->valor}}" placeholder="INGRESE EL VALOR AQUI" class="form-control">
            </div>
          </div>
          <div class="form-group">
            <div class="col-lg-offset-1 col-lg-10">
              <button class="btn btn-sm btn-primary " type="submit">PUBLICAR</button>
            </div>
          </div>
          <input type="hidden" name="geo_latitud" id="geo_latitud"  class="form-control" >
          <input type="hidden" name="geo_longitud" id="geo_longitud"  class="form-control" >
          <input type="hidden" name="img" id="img"  class="form-control">
        </form>
      </div>
    </div>
  </div>
</div>
@endsection
@section('js')
<script src="{{asset('template/js/datees.js')}}" type="text/javascript" ></script>
<script type="text/javascript" src="http://maps.google.com/maps/api/js?&libraries=places&key=AIzaSyD9FZU87YCCSGovLg57u9dvi2jLDDz3uyw"></script>
<script src="{{ asset('template/js/crop_eventos.js')}}"></script>

<script>
$(document).ready(function() {
    $('.select2').select2();
    var startDate = new Date();
    var FromEndDate = new Date();
    var ToEndDate = new Date();
    $('#mapCanvas').css('display','block');

    var valor_edit = "{{$data->valor}}";
    if(valor_edit > 0)
      $('#pago_check').trigger('change');

    $('#valor').keyup();

    ToEndDate.setDate(ToEndDate.getDate() + 365);
    $('#start').datepicker({
            autoclose: true,
            startDate: new Date(),
            language: "es",
            format: "yyyy-mm-dd"
        })
        .on('changeDate', function(selected) {
            if (selected.date != null) {
                startDate = new Date(selected.date.valueOf());
                var endDate = addDays(startDate, 7);
                startDate.setDate(startDate.getDate(new Date(selected.date.valueOf())));
                $('#end').datepicker('setEndDate', endDate);
                $('#end').datepicker('setStartDate', startDate);
                $('#end').datepicker('setDate', endDate);
            } else {
                $('#start').datepicker('setStartDate', startDate);
            }
        });
    $('#end')
        .datepicker({
            autoclose: true,
            startDate: new Date(),
            language: "es",
            endDate: '+30d',
            format: "yyyy-mm-dd"
        });

});




$("#pago_check").change(function() {
    if(this.checked) {
     $('#gratis_check').prop('checked',false);
     $('#valor_div').fadeIn(500);
    }
});
$("#gratis_check").change(function() {
    if(this.checked) {
     $('#pago_check').prop('checked',false);
     $('#valor_div').fadeOut(500);
     $('#valor').val(0);
     
    }
});





var $input = $('#valor');

$input.on('focus', function(e) {
    if ($input.val() == 0)
        $('#valor').val('');
});
//on keyup, start the countdown
$input.on('keyup', function() {
    valor = $('#valor').val();
    if (valor == 0) {
        $('#gratis').fadeIn(1500)
    } else {
        $('#gratis').fadeOut(100)
    }
});




window.addEventListener('DOMContentLoaded', function() {
    var avatar = document.getElementById('avatar');
    var image = document.getElementById('image');
    var input = document.getElementById('input');
    var $progress = $('.progress');
    var $progressBar = $('.progress-bar');
    var $alert = $('.alert');
    var $modal = $('#modal');
    var cropper;

    $('[data-toggle="tooltip"]').tooltip();

    input.addEventListener('change', function(e) {
        var files = e.target.files;
        var done = function(url) {
            input.value = '';
            image.src = url;
            $alert.hide();
            $modal.modal('show');
        };
        var reader;
        var file;
        var url;

        if (files && files.length > 0) {
            file = files[0];

            if (URL) {
                done(URL.createObjectURL(file));
            } else if (FileReader) {
                reader = new FileReader();
                reader.onload = function(e) {
                    done(reader.result);
                };
                reader.readAsDataURL(file);
            }
        }
    });

    $modal.on('shown.bs.modal', function() {
        cropper = new Cropper(image, {
            aspectRatio: 16/9,
            multiple: true,
        });
    }).on('hidden.bs.modal', function() {
        cropper.destroy();
        cropper = null;
    });

    document.getElementById('crop').addEventListener('click', function() {
        var initialAvatarURL;
        var canvas;

        $modal.modal('hide');

        if (cropper) {
            canvas = cropper.getCroppedCanvas({
                width: 600,
                height: 400,
            });

            $('#img').val(canvas.toDataURL());
            initialAvatarURL = avatar.src;
            avatar.src = canvas.toDataURL();
            $progress.show();
            $alert.removeClass('alert-success alert-warning');
            canvas.toBlob(function(blob) {

            });
        }
    });
});




$('#ubicacion').focusout(function() {
    codeAddress();
});

var marker;
var map;

function codeAddress() {
    var dir_completa = $('#ubicacion').val();
    geocoder.geocode({
        'address': dir_completa
    }, function(results, status) {
        if (status == google.maps.GeocoderStatus.OK) {
            if (status != google.maps.GeocoderStatus.ZERO_RESULTS) {
                var latitude = results[0].geometry.location.lat();
                var longitude = results[0].geometry.location.lng();
                $('#geo_latitud').val(latitude);
                $('#geo_longitud').val(longitude);

                marker.setPosition(results[0].geometry.location);
                map.setZoom(15);
                map.panTo(marker.position);
                $("#mapCanvas").fadeIn(1200);
            }
        } else {
            toastr.error('No pudimos capturar su ubicacion')
        }
    });
}

var geocoder = new google.maps.Geocoder();




function updateMarkerPosition(latLng) {
    document.getElementById('info').value = [
        latLng.lat(),
        latLng.lng()
    ].join(',');

    $("#geo_latitud").val(latLng.lat());
    $("#geo_longitud").val(latLng.lng());

}

function initialize(coord) {

    var latitud = "{{$data->latitud}}";
    var longitud = "{{$data->longitud}}";

    var latLng = new google.maps.LatLng(latitud, longitud);
    $.getJSON("{{url('assets/mapstyle/mapstyle.json')}}", function(mapStyle) {
        map = new google.maps.Map(document.getElementById('mapCanvas'), {
            center: {
                lat: -35.2,
                lng: -58.2520396
            },
            zoom: 9,
            styles: mapStyle,
            gestureHandling: 'greedy',
            mapTypeControl: false,
            fullscreenControl: false,
        });

        marker = new google.maps.Marker({
            position: latLng,
            map: map,
            draggable: true,
            title: "Mueva el marcador a la posición donde se realizara el evento"
        });

        map.panTo(latLng);

        updateMarkerPosition(latLng);



        google.maps.event.addListener(marker, 'drag', function() {

            updateMarkerPosition(marker.getPosition());
        });

    });




}

google.maps.event.addDomListener(window, 'load', function() {
    var np = $("#provincia option:selected").text();
    geocoder.geocode({
        'address': np + ",Argentina"
    }, function(results, status) {
        if (status == google.maps.GeocoderStatus.OK) {
            if (status != google.maps.GeocoderStatus.ZERO_RESULTS) {
                var latitude = results[0].geometry.location.lat();
                var longitude = results[0].geometry.location.lng();
                $('#geo_latitud').val(latitude);
                $('#geo_longitud').val(longitude);
                initialize({
                    'lat': latitude,
                    'lng': longitude
                })
            }
        } else {
            toast.error('No hemos podido capturar su ubicacion')
        }
    });

    ;
});
</script>
@endsection
