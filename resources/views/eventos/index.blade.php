@extends('template.template')
@section('linkscss')
<link type="text/css" rel="stylesheet" href="{{ asset('/assets/select2/select2.css') }}">
<link type="text/css" rel="stylesheet" href="{{ asset('/assets/toastr/toastr.min.css') }}">
<link type="text/css" rel="stylesheet" href="{{ asset('/assets/bootstrap-datepicker/css/datepicker.css') }}">
<link type="text/css" rel="stylesheet" href="{{ asset('/assets/sweetalert/sweet-alert.css') }}">
<link href="{{asset('template/css/estilo_tabla.css')}}" rel="stylesheet">
@endsection
@section('content')

<section class="bf-table" id="data" style="display: none; padding-bottom: 20px ">
   <div class="card card-cascade narrower">
      <!--Card image-->
      <div class="view view-cascade gradient-card-header blue-gradient narrower py-2 mx-4 mb-3 d-flex justify-content-between align-items-center">
        
         <span class="white-text">Publicá tu evento</span>
          <div style="text-align:right">
	    <a type="button" class="btn btn-outline btn-danger"  href="{{ URL::to('eventos/create') }}">Nuevo evento</a> 
         </div>
      </div>
      <!--/Card image-->
      <div class="px-4">
        
         <div class="table-wrapper table-responsive">
            <!--Table-->
            <div class="btn-group pull-left" data-bind="foreach: filters">
          <span data-bind="click: function() { vm.remove_filters(name) }" title="Click para quitar este filtro" class="label label_table"><i class="fa fa-times-circle" aria-hidden="true"></i>&nbsp;&nbsp;<a style="color: white;" data-bind="text: value"></a></span>
        </div>
            <table class="table table-stripted tabla_custom table-hover mb-0">
					<thead>
						<tr>
							<th class="col_1" style="padding: 0px;">
								<input id="nombre" type="text" placeholder="Nombre" class="form-control" data-bind="event: { change: function(){vm.filterNombre($element)} }">
							</th>
							<th class="col_1" style="padding: 0px;">
									<input id="ubicacion" type="text" placeholder="Ubicación" class="form-control" data-bind="event: { change: function(){vm.filterUbicacion($element)} }">
								</th>
							<th class="col_1" style="padding: 0px;">
									<input id="fecha_inicio" type="text" placeholder="Inicia" class="form-control" data-bind="event: { change: function(){vm.filterInicia($element)} }">
							</th>
							<th class="col_1">
										Valor
							</th>
							<th class="col_1">
										Imagen
							</th>
							<th class="col_1" style="padding: 0px;">
									<input id="organizador" type="text" placeholder="Organizador" class="form-control" data-bind="event: { change: function(){vm.filterOrganiza($element)} }">
							</th>
							<th class="col_1">
								Acciones
							</th>
							<!--<th class="text-left">Acciones</th> -->
						</tr>
					</thead>
					<tbody data-bind="foreach: data().data">
						<tr>
							<td class="col_1 text-left"  	style="width:200px;"	data-bind="text: nombre"></td>
							<td class="col_1 text-left" 	style="width:200px;"	data-bind="text: ubicacion"></td>
							<td class="col_1 text-left" 	style="width:100px;" 	data-bind="text: fecha_inicio"></td>
							<!-- td class="col_1 text-left" 	style="width:100px;" 	data-bind="text: fecha_fin"></td -->
							<td class="col_1 text-left" 	style="width:70px;"	data-bind="text: valor > 50 ? valor : 'GRATIS'""></td>
							<td class="col_1 text-left" 	style="width:110px;">
								<img data-bind="attr: { src: '{{url(asset(''))}}'+img_path }"  alt="" style="border:1px solid;" border=3 height=45 width=80>
							</td>
							<td class="col_1 text-left" 	style="width:150px;"	data-bind="text: organizador"></td>


							<!-- ko if:  id_usuario == "{{Auth::user()->id}}" -->
							<td>
								<a href="javascript:void(0)" data-bind="attr: { href: '{{ url("eventos") }}' +'/'+id+'/edit', }" class="btn btn-outline btn-danger btn-sm">Editar</a>
								<a href="javascript:void(0)" data-bind="event:{ click: function(){ vm.eliminar(id) } }" class="btn btn-outline btn-danger btn-sm">Eliminar</a>
			                </td>
			                <!-- /ko -->

			                <!-- ko if:  id_usuario != "{{Auth::user()->id}}" -->
							<td></td>
			                <!-- /ko -->


							<!--
							<td class="col_1 text-center">
							<a href="javascript:void(0)" data-bind="attr: { href: '{{ url("producto") }}' +'/'+id+'/edit', }"  class="btn btn-outline btn-warning btn-sm">Editar</a>
							<a href="javascript:void(0)" data-bind="event:{ click: function(){ vm.eliminar(id) } }"  class="btn btn-outline btn-danger btn-sm">Eliminar</a>
							</td>
						-->
						</tr>
					</tbody>
				</table>
		</div>
         
         <div class="d-flex justify-content-between">
            <div style="padding-top: 40px">
                <p>Mostrando del <span data-bind="text: data().from == null ? '0' : data().from"></span> al <span data-bind="text: data().to == null ? '0' : data().to"></span> de <span data-bind="text: data().total == null ? '0' : data().total"></span> registros</p>               
            </div>
            <nav class="my-4">
               <ul class="pagination pagination-circle pg-blue mb-0">
                  <li class="page-item" data-bind="css: data().prev_page_url == null ? 'disabled' : ''"><a class="page-link waves-effect waves-effect" data-bind="click: function() { ir(1) }">« Primera</a></li>
                  <li class="page-item" data-bind="if: data().current_page-100 > 0, click: function() { ir(data().current_page-100) } "><span class="page-link waves-effect waves-effect" data-bind="text: data().current_page-100"></span></li>
                  <li class="page-item" data-bind="if: data().current_page-50 > 0, click: function() { ir(data().current_page-50) } "><span class="page-link waves-effect waves-effect" data-bind="text: data().current_page-50"></span></li>
                  <li class="page-item" data-bind="if: data().current_page-10 > 0, click: function() { ir(data().current_page-10) } "><span class="page-link waves-effect waves-effect" data-bind="text: data().current_page-10"></span></li>
                  <li class="page-item" data-bind="if: data().current_page-5 > 0, click: function() { ir(data().current_page-5) } "><span class="page-link waves-effect waves-effect" data-bind="text: data().current_page-5"></span></li>
                  <li class="page-item" data-bind="if: data().current_page-2 > 0, click: function() { ir(data().current_page-2) } "><span class="page-link waves-effect waves-effect" data-bind="text: data().current_page-2"></span></li>
                  <li class="page-item" data-bind="if: data().current_page-1 > 0, click: function() { ir(data().current_page-1) } "><span class="page-link waves-effect waves-effect" data-bind="text: data().current_page-1"></span></li>
                  <li class="page-item active" ><span class="page-link waves-effect waves-effect" data-bind="text: data().current_page"></span></li>
                  <li class="page-item" data-bind="if: data().current_page+1 <= data().last_page, click: function() { ir(data().current_page+1) } "><span class="page-link waves-effect waves-effect" data-bind="text: data().current_page+1"></span></li>
                  <li class="page-item" data-bind="if: data().current_page+2 <= data().last_page, click: function() { ir(data().current_page+2) } "><span class="page-link waves-effect waves-effect" data-bind="text: data().current_page+2"></span></li>
                  <li class="page-item" data-bind="if: data().current_page+5 <= data().last_page, click: function() { ir(data().current_page+5) } "><span class="page-link waves-effect waves-effect" data-bind="text: data().current_page+5"></span></li>
                  <li class="page-item" data-bind="if: data().current_page+10 <= data().last_page, click: function() { ir(data().current_page+10) } "><span class="page-link waves-effect waves-effect" data-bind="text: data().current_page+10"></span></li>
                  <li class="page-item" data-bind="if: data().current_page+50 <= data().last_page, click: function() { ir(data().current_page+50) } "><span class="page-link waves-effect waves-effect" data-bind="text: data().current_page+50"></span></li>
                  <li class="page-item" data-bind="if: data().current_page+100 <= data().last_page, click: function() { ir(data().current_page+100) } "><span class="page-link waves-effect waves-effect" data-bind="text: data().current_page+100"></span></li>
                  <li class="page-item" data-bind="css: data().last_page == null || data().current_page >= data().last_page ? 'disabled' : ''"><a class="page-link waves-effect waves-effect" data-bind="click: function() { ir(data().last_page) }">Última »</a></li>
               </ul>

            </nav>

         </div>

      </div>
   </div>
</section>
@endsection

@section('js')
<script src='{{ asset("assets/toastr/toastr.min.js") }}'></script>
<script src="{{ asset('assets/select2/select2.full.js') }}"></script>
<script src="{{ asset('assets/moment/moment-with-locales.min.js')}}"></script>
<script src="{{ asset('assets/bootstrap-datepicker/js/bootstrap-datepicker.js') }}"></script>
<script src="{{ asset('assets/bootstrap-datepicker/js/locales/bootstrap-datepicker.es.js') }}"></script>
<script>
moment.locale('es');
var url = "{{ url('api/eventos') }}";
var url_normal = "{{ url('eventos') }}";
var vm = function() {
	self = this;
	self.data = ko.observableArray([]);

	self.nombre = ''
	self.ubicacion = ''
	self.fecha_inicio = ''
	self.fecha_fin = ''
	self.organizador = ''
	
	self.page = '1';
	self.filters = ko.observableArray([]);
	self.init = function() {
		self.ajax();
		$('.select2').select2();
	}
	self.filterNombre = function(element) {
		self.page = 1;
		self.nombre = $(element).val()
		self.update_filters();
		self.ajax();
	};
	self.filterUbicacion = function(element) {
		self.page = 1;
		self.ubicacion = $(element).val()
		self.update_filters();
		self.ajax();
	};
	self.filterInicia = function(element) {
		self.page = 1;
		self.fecha_inicio = $(element).val()
		self.update_filters();
		self.ajax();
	};
	self.filterTermina = function(element) {
		self.page = 1;
		self.fecha_fin = $(element).val()
		self.update_filters();
		self.ajax();
	};
	self.filterOrganiza = function(element) {
		self.page = 1;
		self.organizador = $(element).val()
		self.update_filters();
		self.ajax();
	};

	self.update_filters = function() {
		self.filters([]);
		if (self.nombre != '') self.filters.push({
			key: 'nombre',
			value: self.nombre,
			name: 'nombre'
		});
		if (self.ubicacion != '') self.filters.push({
			key: 'ubicacion',
			value: self.ubicacion,
			name: 'ubicacion'
		});	
		if (self.fecha_inicio != '') self.filters.push({
			key: 'fecha_inicio',
			value: self.fecha_inicio,
			name: 'fecha_inicio'
		});	
		if (self.fecha_fin != '') self.filters.push({
			key: 'fecha_fin',
			value: self.fecha_fin,
			name: 'fecha_fin'
		});	
		if (self.organizador != '') self.filters.push({
			key: 'organizador',
			value: self.organizador,
			name: 'organizador'
		});	
	};
	self.remove_filters = function(name) {
		if (name == 'nombre') {
			self.nombre = '';
		}
		if (name == 'ubicacion') {
			self.ubicacion = '';
		}
		if (name == 'fecha_inicio') {
			self.fecha_inicio  = '';
		}
		if (name == 'fecha_fin') {
			self.fecha_fin  = '';
		}
		if (name == 'organizador') {
			self.organizador  = '';
		}

		$('#' + name).val("").trigger("change");
		self.update_filters();
		self.ajax();
	};
	self.ir = function(page) {
		self.page = page;
		self.ajax();
		$('html, body').animate({
			scrollTop: 0
		}, 'fast');
	};
	self.eliminar = function(id) {
	    swal({
	      title: "Está seguro que desea eliminar este evento?",
	      text: "",
	      icon: "warning",
	      buttons: true,
	      dangerMode: true,
	    })
	    .then((willDelete) => {
	      if (willDelete) {
	          $.ajax({
	            url: url_normal+'/'+id,
	            type: 'DELETE',
	            data    :   {
	              '_token': "{{ csrf_token() }}"
	            },
	            success: function(result) {
	              toastr.success(result);
	              self.ajax();
	            }
	          });
	        self.ajax();
	      }
	    });
	  };
	self.ajax = function() {
		$.getJSON(url, {
			page: self.page,
			nombre: self.nombre,
			ubicacion: self.ubicacion,
			fecha_inicio: self.fecha_inicio,
			fecha_fin: self.fecha_fin,
			organizador: self.organizador
		})
		.done(function(data) {
			console.log(data);
			Pace.restart()
			vm.data(data);
			$("#data").fadeIn();
			$(".loading").remove();
		})
		.error(function(d) {
			toastr.error('Se encontro un error intente nuevamente');
		});
	};
};
vm = new vm();
vm.init();
ko.applyBindings(vm, document.getElementById("data"));

@if(session()->has('message'))
toastr.success('{{ session()->get('message ') }}');
@endif
</script>
@endsection
