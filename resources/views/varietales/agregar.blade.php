  @extends('template.template')
  @section('head_content')
  @endsection
  @section('content')
  <div class="row">
    <div class="col-lg-12 text-center">
      <div class="ibox float-e-margins">
        <div class="ibox-title">
          <h5>Alta de Varietal</h5>
        </div>
        <div class="ibox-content">
         <form class="validator form-horizontal" action="{{url('varietales')}}" method="post">
           {{ csrf_field() }}

          <div class="form-group"><label class="col-lg-3 control-label">Varietal</label>
            <div class="col-lg-6">
              <input type="text" name="varietal" placeholder="INGRESE EL VARIETAL" class="form-control" required>
            </div>
          </div>

            <div class="form-group"><label class="col-lg-3 control-label">Tipo</label>
            <div class="col-lg-6">
              <select class="form-control chosen-select" name="tipo" id="tipo" value="{{ old('tipo') }}">
                @foreach($producto_tipo as $pt)
                <option value="{{$pt->id}}" >{{$pt->tipo}}</option>
                @endforeach
              </select>
            </div>
          </div>

          <div class="form-group">
            <div class="col-lg-offset-1 col-lg-10">
              <button class="btn btn-sm btn-primary " type="submit">Completar Registro</button>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
  </div>
  @endsection

