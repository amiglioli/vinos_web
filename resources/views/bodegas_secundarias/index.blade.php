@extends('template.template')
@section('linkscss')
<link type="text/css" rel="stylesheet" href="{{ asset('/assets/select2/select2.css') }}">
<link type="text/css" rel="stylesheet" href="{{ asset('/assets/toastr/toastr.min.css') }}">
<link type="text/css" rel="stylesheet" href="{{ asset('/assets/bootstrap-datepicker/css/datepicker.css') }}">
<link type="text/css" rel="stylesheet" href="{{ asset('/assets/sweetalert/sweet-alert.css') }}">
<link href="{{asset('template/css/estilo_tabla.css')}}" rel="stylesheet">
@endsection
@section('content')
@include('bodegas_secundarias.modal')
<div class="row wrapper red-bg">
  <div class="col-lg-10">
    <h2>Bodegas</h2>
  </div>
  <div class="col-lg-2">
  </div>
</div>
<section class="bf-table" id="data" style="display: none; padding-bottom: 20px ">
   <div class="card card-cascade narrower">
      <!--Card image-->
      <div class="view view-cascade gradient-card-header blue-gradient narrower py-2 mx-4 mb-3 d-flex justify-content-between align-items-center">
      
         <span class="white-text">Bodegas secundarias</span>
          <div style="text-align:right">
	   <a type="button" class="btn btn-outline btn-danger"  data-toggle="modal" data-target="#myModal2">Nueva bodega secundaria  </a>
         </div>
      </div>
      <!--/Card image-->
      <div class="px-4">
        
         <div class="table-wrapper table-responsive">
            <!--Table-->
            <div class="btn-group pull-left" data-bind="foreach: filters">
          <span data-bind="click: function() { vm.remove_filters(name) }" title="Click para quitar este filtro" class="label label_table"><i class="fa fa-times-circle" aria-hidden="true"></i>&nbsp;&nbsp;<a style="color: white;" data-bind="text: value"></a></span>
        </div>
            <table class="table table-bordered tabla_custom table-hover mb-0">
                <thead>
            <tr>
              <th class="col_1" style="padding: 0px;">
                        <select id="nombre" class="form-control select2" data-bind="event:{ change: function(){ vm.filterUsuario($element) } }" style="width:100%">
                          <option value="">USUARIOS</option>
                          @foreach($usuarios as $usuario)
                          <option value="{{ $usuario->id }}">{{ $usuario->name }}</option>
                          @endforeach
                        </select>
              </th>
                    </tr>
                </thead>
          <tbody data-bind="foreach: data().data">
            <tr>
                  <td class="col_1 text-left"   data-bind="text: name"></td>

              </tr>
          </tbody>
        </table>
        <p>Mostrando del <span data-bind="text: data().from == null ? '0' : data().from"></span> al <span data-bind="text: data().to == null ? '0' : data().to"></span> de <span data-bind="text: data().total == null ? '0' : data().total"></span> registros</p>
        <ul class="pagination">
          <li data-bind="css: data().prev_page_url == null ? 'disabled' : ''"><a data-bind="click: function() { ir(1) }">« Primera</a></li>
          <li data-bind="if: data().current_page-100 > 0, click: function() { ir(data().current_page-100) } "><span data-bind="text: data().current_page-100"></span></li>
          <li data-bind="if: data().current_page-50 > 0, click: function() { ir(data().current_page-50) } "><span data-bind="text: data().current_page-50"></span></li>
          <li data-bind="if: data().current_page-10 > 0, click: function() { ir(data().current_page-10) } "><span data-bind="text: data().current_page-10"></span></li>
          <li data-bind="if: data().current_page-5 > 0, click: function() { ir(data().current_page-5) } "><span data-bind="text: data().current_page-5"></span></li>
          <li data-bind="if: data().current_page-2 > 0, click: function() { ir(data().current_page-2) } "><span data-bind="text: data().current_page-2"></span></li>
          <li data-bind="if: data().current_page-1 > 0, click: function() { ir(data().current_page-1) } "><span data-bind="text: data().current_page-1"></span></li>
          <li class="active"><span data-bind="text: data().current_page"></span></li>
          <li data-bind="if: data().current_page+1 <= data().last_page, click: function() { ir(data().current_page+1) } "><span data-bind="text: data().current_page+1"></span></li>
          <li data-bind="if: data().current_page+2 <= data().last_page, click: function() { ir(data().current_page+2) } "><span data-bind="text: data().current_page+2"></span></li>
          <li data-bind="if: data().current_page+5 <= data().last_page, click: function() { ir(data().current_page+5) } "><span data-bind="text: data().current_page+5"></span></li>
          <li data-bind="if: data().current_page+10 <= data().last_page, click: function() { ir(data().current_page+10) } "><span data-bind="text: data().current_page+10"></span></li>
          <li data-bind="if: data().current_page+50 <= data().last_page, click: function() { ir(data().current_page+50) } "><span data-bind="text: data().current_page+50"></span></li>
          <li data-bind="if: data().current_page+100 <= data().last_page, click: function() { ir(data().current_page+100) } "><span data-bind="text: data().current_page+100"></span></li>
          <li data-bind="css: data().last_page == null || data().current_page >= data().last_page ? 'disabled' : ''"><a data-bind="click: function() { ir(data().last_page) }">Última »</a></li>
        </ul>

        <a href="{{ url('admin/usuarios/create') }}" class="btn-create" data-toggle="tooltip" title="Agregar Usuario" data-placement="left"><i class="fa fa-pencil"></i></a>
      </div>
    </div>
  </div>
</div>
@endsection

@section('js')
<script src='{{ asset("assets/toastr/toastr.min.js") }}'></script>
<script src="{{ asset('assets/select2/select2.full.js') }}"></script>
<script src="{{ asset('assets/moment/moment-with-locales.min.js')}}"></script>
<script src="{{ asset('assets/bootstrap-datepicker/js/bootstrap-datepicker.js') }}"></script>
<script src="{{ asset('assets/bootstrap-datepicker/js/locales/bootstrap-datepicker.es.js') }}"></script>
<script>
  moment.locale('es');
  var url = "{{ url('api/bodegas_secundarias') }}";
  var vm = function(){
        self              = this;
        self.data         = ko.observableArray([]);
        self.nombre            = '';
        self.nombre_text       = '';
        self.email             = '';
        self.email_text        = '';
        self.page         = '1';
        self.filters      = ko.observableArray([]);
        self.init = function(){
          self.ajax();
        $('.select2').select2();
        }
            self.filterUsuario = function(element){
          self.page       = 1;
          self.nombre    = $(element).val()
          self.nombre_text   = $(element).find("option:selected").text();
          self.update_filters();
          self.ajax();
            };
          self.filterEmail = function(element){
          self.page       = 1;
          self.email     = $(element).val()
          self.email_text  = $(element).find("option:selected").text();
          self.update_filters();
          self.ajax();
            };

      self.update_filters = function(){
        self.filters([]);
        if(self.nombre  != '') self.filters.push({ key: 'nombre', value: self.nombre_text,   name: 'nombre'  });
        if(self.email     != '') self.filters.push({ key: 'email',  value: self.email_text,    name: 'email'  });
      };
      self.remove_filters = function(name){
        if(name == 'nombre'){
          self.nombre  = '';   self.nombre_text = '';
        }
        if(name == 'email'){
          self.email   = '';   self.email_text = '';
        }
        $('#'+name).val("").trigger("change");
        self.update_filters();
        self.ajax();
      };
      self.ir = function(page){
          self.page = page;
          self.ajax();
          $('html, body').animate({ scrollTop: 0 }, 'fast');
        };
        self.ajax = function(){
            $.getJSON(url,{
              page:     self.page,
              nombre:  self.nombre,
              email:     self.email
            })
            .done(function(data){
              console.log(data);
                Pace.restart()
                vm.data(data);
                $("#data").fadeIn();
                $(".loading").remove();
            })
            .error(function(d){
              toastr.error('Se encontro un error intente nuevamente');
            });
        };
      };
      vm = new vm();
    vm.init();
    ko.applyBindings(vm, document.getElementById("data"));

    @if(session()->has('message'))
    toastr.success('{{ session()->get('message') }}');
    @endif
</script>

@endsection
