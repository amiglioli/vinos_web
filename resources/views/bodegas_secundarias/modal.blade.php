<div class="modal inmodal" id="myModal2" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog">
    <form class="validator form-horizontal" action="{{ URL::to('bodegas_secundarias') }}" method="post">
      {{ csrf_field() }}
      <div class="modal-content animated flipInY">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
          <h4 class="modal-title">Alta de bodegas secundarias</h4>
        </div>
        <div class="modal-body">
          <div class="form-group{{ $errors->has('nombre_bodega') ? ' has-error' : '' }} has-success">
            <label for="bodega" class="col-md-4 control-label">Nombre Bodega</label>

            <div class="col-md-6">
              <input id="nombre_bodega" type="text" class="form-control " name="nombre_bodega" value="{{ old('nombre_bodega') }}" required>
              @if ($errors->has('descripcion'))
              <span class="help-block">
                <strong>{{ $errors->first('nombre_bodega') }}</strong>
              </span>
              @endif
            </div>
          </div>

          <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }} has-success">
            <label for="email" class="col-md-4 control-label">Email</label>

            <div class="col-md-6">
              <input id="email" type="text" class="form-control" name="email" value="{{ old('email') }}" required>
              @if ($errors->has('descripcion'))
              <span class="help-block">
                <strong>{{ $errors->first('email') }}</strong>
              </span>
              @endif
            </div>
          </div>

        </div>

        <div class="modal-footer">
          <button type="button" class="btn btn-danger" data-dismiss="modal">Cancelar</button>
          <button type="submit" class="btn btn-primary">Enviar</button>
        </div>
      </div>
    </form>
  </div>
</div>