  @extends('template.template')
  @section('head_content')
  @endsection
  @section('content')
  <div class="row">
    <div class="col-lg-12 text-center">
      <div class="ibox float-e-margins">
        <div class="ibox-title">
          <h5>Edición del Tipo : <span style="color:green">{{$tipo->tipo}}</span></h5>
        </div>
        <div class="ibox-content">
         <form class="validator form-horizontal" action="{{ url('producto_tipo',$tipo->id) }}" method="post">
           @method('PUT')
           {{ csrf_field() }}
          <input name="id" type="hidden" value="{{ $tipo->id }}"/>
          <div class="form-group"><label class="col-lg-3 control-label">Tipo</label>
            <div class="col-lg-6">
              <input type="text" name="nombre" value="{{$tipo->tipo}}" placeholder="INGRESE EL TIPO" class="form-control" required>
            </div>
          </div>

          <div class="form-group"><label class="col-lg-3 control-label">Producto</label>
            <div class="col-lg-6">
              <select class="form-control chosen-select" name="producto" id="producto" value="{{ old('producto') }}">
                @foreach($producto as $p)
                <option  @if($p->id == $tipo->producto_id)) selected @endif value="{{$p->id}}" >{{$p->nombre}}</option>
                @endforeach
              </select>
            </div>
          </div>

          <div class="form-group">
            <div class="col-lg-offset-1 col-lg-10">
              <button class="btn btn-sm btn-primary " type="submit">Guardad cambios</button>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
  </div>
  @endsection
