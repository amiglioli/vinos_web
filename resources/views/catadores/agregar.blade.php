@extends('template.template')
@section('head_content')
<link href="{{asset('template/css/bootstrap-datetimepicker.css')}}" rel="stylesheet">
@endsection
@section('content')
<div class="row">
  <div class="col-lg-12 text-center">
    @if ($errors->any())
        @foreach ($errors->all() as $error)
        <div class="alert alert-danger" role="alert">
          <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <strong>Error!</strong> {{$error}}!
        </div>
        @endforeach
    @endif
    <div class="ibox float-e-margins">
      <div class="ibox-title">
        <h5>Alta de Catadores</h5>
      </div>
      <div class="ibox-content">
        <form class="validator form-horizontal" action="{{url('catadores')}}" method="post">
          {{ csrf_field() }}

          <div class="form-group"><label class="col-lg-3 control-label">DNI</label>
            <div class="col-lg-6">
              <input type="number" name="dni" placeholder="INGRESE EL DNI" class="form-control" required>
            </div>
          </div>

          <div class="form-group"><label class="col-lg-3 control-label">APELLIDO</label>
            <div class="col-lg-6">
              <input type="text" name="apellido" placeholder="INGRESE EL APELLIDO" class="form-control" required>
            </div>
          </div>


          <div class="form-group"><label class="col-lg-3 control-label">NOMBRE</label>
            <div class="col-lg-6">
              <input type="text" name="nombre" placeholder="INGRESE EL PRODUCTO" class="form-control" required>
            </div>
          </div>


          <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}" id="data_1">
            <label for="edad" class="col-md-3 control-label">Edad</label>

            <div class="col-md-6 input-group date" >
              <span class="input-group-addon"><i class="fa fa-calendar"></i></span><input name="edad" id="edad" type="text" class="form-control" value="{{ old('edad') }}">
              @if ($errors->has('edad'))
              <span class="help-block">
                <strong>{{ $errors->first('edad') }}</strong>
              </span>
              @endif
            </div>
          </div>

          <div class="form-group"><label class="col-lg-3 control-label">ESPECIALIZACION</label>
            <div class="col-lg-6">
              <select class="select2_demo_2 form-control" name="especializacion" id="especializacion" value="{{ old('especializacion') }}">
                <option value="">ESPECIALIZACIONES</option>
                @foreach($especializacion as $e)
                <option value="{{$e->id}}" >{{$e->especializacion}}</option>
                @endforeach
              </select>
            </div>
          </div>

          <div class="form-group">
            <div class="col-lg-offset-1 col-lg-10">
              <button class="btn btn-sm btn-primary " type="submit">Completar Registro</button>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>
@endsection
@section('js')
<script src="//cdn.rawgit.com/Eonasdan/bootstrap-datetimepicker/e8bddc60e73c1ec2475f827be36e1957af72e2ea/src/js/bootstrap-datetimepicker.js"></script>
<script>
$(document).ready(function() {
  $(".select2_demo_2").select2();
  $('#edad').datetimepicker({
    keepOpen:true,
    showClose: true,
    viewMode: 'years',
    locale:"es",
    format: 'Y-MM-DD',
    widgetPositioning:{
      horizontal: 'auto',
      vertical: 'bottom'
    }
  });
});
</script>
@endsection
