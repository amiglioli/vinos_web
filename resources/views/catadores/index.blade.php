@extends('template.template')
@section('linkscss')
<link type="text/css" rel="stylesheet" href="{{ asset('/assets/select2/select2.css') }}">
<link type="text/css" rel="stylesheet" href="{{ asset('/assets/toastr/toastr.min.css') }}">
<link type="text/css" rel="stylesheet" href="{{ asset('/assets/bootstrap-datepicker/css/datepicker.css') }}">
<link type="text/css" rel="stylesheet" href="{{ asset('/assets/sweetalert/sweet-alert.css') }}">
<link href="{{asset('template/css/estilo_tabla.css')}}" rel="stylesheet">
@endsection
@section('content')
<div class="row wrapper red-bg">
	<div class="col-lg-10">
		<h2>Catadores</h2>
	</div>
	<div class="col-lg-2">
	</div>
</div>
<section class="bf-table" id="data" style="display: none; padding-bottom: 20px ">
	<div class="card card-cascade narrower">
		<!--Card image-->
		<div class="view view-cascade gradient-card-header blue-gradient narrower py-2 mx-4 mb-3 d-flex justify-content-between align-items-center">
			
			<span class="white-text">Listado de Catadores</span>
			<div style="text-align:right">
				<a type="button" class="btn btn-outline btn-danger" href="{{ URL::to('catadores/create') }}"">Agregar catador</a>
			</div>
		</div>
		<!--/Card image-->
		<div class="px-4">
			<div class="table-wrapper table-responsive">
				<!--Table-->
				<div class="btn-group pull-left" data-bind="foreach: filters">
					<span data-bind="click: function() { vm.remove_filters(name) }" title="Click para quitar este filtro" class="label label_table"><i class="fa fa-times-circle" aria-hidden="true"></i>&nbsp;&nbsp;<a style="color: white;" data-bind="text: value"></a></span>
				</div>
				<table class="table table-striped tabla_custom table-hover mb-0">
					<thead>
						<tr>
							<th class="col_1" style="padding: 0px;">
								<input id="dni" type="text" placeholder="DNI" class="form-control" data-bind="event: { change: function(){vm.filterDni($element)} }">
							</th>
							<th class="col_1" style="padding: 0px;">
								<input id="apellido" type="text" placeholder="Apellido" class="form-control" data-bind="event: { change: function(){vm.filterApellido($element)} }">
							</th>
							<th class="col_1" style="padding: 0px;">
								<input id="nombre" type="text" placeholder="Nombre" class="form-control" data-bind="event: { change: function(){vm.filterNombre($element)} }">
							</th>
							<th class="col_1 text-center" >
								Edad
							</th>
							<th class="col_1" style="padding: 0px;">
								<select id="especializacion" class="form-control select2" data-bind="event:{ change: function(){ vm.filterEspecialiacion($element) } }" style="width:100%">
									<option value="">Especialización</option>
									@foreach($especializaciones as $e)
									<option value="{{ $e->id }}">{{ $e->especializacion }}</option>
									@endforeach
								</select>
							</th>
							<th class="text-left">Cantidad de catas</th>
							<th class="text-left">Acciones</th>
						</tr>
					</thead>
					<tbody data-bind="foreach: data().data">
						<tr>
							<td class="col_1 text-left" style="width:110px" 	data-bind="text: dni"></td>
							<td class="col_1 text-left" style="width:150px" 	data-bind="text: apellido"></td>
							<td class="col_1 text-left" style="width:150px" 	data-bind="text: nombre"></td>
							<td class="col_1 text-left" style="width:100px" 		data-bind="text: calculateAge(edad)"></td>
							<td class="col_1 text-left" style="width:250px" 	data-bind="text: especializacion.especializacion"></td>
							<td class="col_1 text-center"> <a href="javascript:void(0)" data-bind="text: catas_cerradas.length,attr: { href: '{{ url("catadores") }}' +'/'+id }" class="btn btn-outline btn-warning  btn-xs"></a></td>
							<td class="col_1 text-center">
								<!-- ko if: catas_cerradas.length == 0 -->
								<a href="javascript:void(0)" data-bind="attr: { href: '{{ url("catadores") }}' +'/'+id+'/edit', }"  class="btn btn-outline btn-warning btn-xs">Editar</a>
								<a href="javascript:void(0)" data-bind="event:{ click: function(){ vm.eliminar(id) } }"  class="btn btn-outline btn-danger  btn-xs">Eliminar</a>
								<!-- /ko -->
							</td>
						</tr>
					</tbody>
				</table>
			</div>
			<div class="d-flex justify-content-between">
				<div style="padding-top: 40px">
					<p>Mostrando del <span data-bind="text: data().from == null ? '0' : data().from"></span> al <span data-bind="text: data().to == null ? '0' : data().to"></span> de <span data-bind="text: data().total == null ? '0' : data().total"></span> registros</p>
				</div>
				<nav class="my-4">
					<ul class="pagination pagination-circle pg-blue mb-0">
						<li class="page-item" data-bind="css: data().prev_page_url == null ? 'disabled' : ''"><a class="page-link waves-effect waves-effect" data-bind="click: function() { ir(1) }">« Primera</a></li>
						<li class="page-item" data-bind="if: data().current_page-100 > 0, click: function() { ir(data().current_page-100) } "><span class="page-link waves-effect waves-effect" data-bind="text: data().current_page-100"></span></li>
						<li class="page-item" data-bind="if: data().current_page-50 > 0, click: function() { ir(data().current_page-50) } "><span class="page-link waves-effect waves-effect" data-bind="text: data().current_page-50"></span></li>
						<li class="page-item" data-bind="if: data().current_page-10 > 0, click: function() { ir(data().current_page-10) } "><span class="page-link waves-effect waves-effect" data-bind="text: data().current_page-10"></span></li>
						<li class="page-item" data-bind="if: data().current_page-5 > 0, click: function() { ir(data().current_page-5) } "><span class="page-link waves-effect waves-effect" data-bind="text: data().current_page-5"></span></li>
						<li class="page-item" data-bind="if: data().current_page-2 > 0, click: function() { ir(data().current_page-2) } "><span class="page-link waves-effect waves-effect" data-bind="text: data().current_page-2"></span></li>
						<li class="page-item" data-bind="if: data().current_page-1 > 0, click: function() { ir(data().current_page-1) } "><span class="page-link waves-effect waves-effect" data-bind="text: data().current_page-1"></span></li>
						<li class="page-item active" ><span class="page-link waves-effect waves-effect" data-bind="text: data().current_page"></span></li>
						<li class="page-item" data-bind="if: data().current_page+1 <= data().last_page, click: function() { ir(data().current_page+1) } "><span class="page-link waves-effect waves-effect" data-bind="text: data().current_page+1"></span></li>
						<li class="page-item" data-bind="if: data().current_page+2 <= data().last_page, click: function() { ir(data().current_page+2) } "><span class="page-link waves-effect waves-effect" data-bind="text: data().current_page+2"></span></li>
						<li class="page-item" data-bind="if: data().current_page+5 <= data().last_page, click: function() { ir(data().current_page+5) } "><span class="page-link waves-effect waves-effect" data-bind="text: data().current_page+5"></span></li>
						<li class="page-item" data-bind="if: data().current_page+10 <= data().last_page, click: function() { ir(data().current_page+10) } "><span class="page-link waves-effect waves-effect" data-bind="text: data().current_page+10"></span></li>
						<li class="page-item" data-bind="if: data().current_page+50 <= data().last_page, click: function() { ir(data().current_page+50) } "><span class="page-link waves-effect waves-effect" data-bind="text: data().current_page+50"></span></li>
						<li class="page-item" data-bind="if: data().current_page+100 <= data().last_page, click: function() { ir(data().current_page+100) } "><span class="page-link waves-effect waves-effect" data-bind="text: data().current_page+100"></span></li>
						<li class="page-item" data-bind="css: data().last_page == null || data().current_page >= data().last_page ? 'disabled' : ''"><a class="page-link waves-effect waves-effect" data-bind="click: function() { ir(data().last_page) }">Última »</a></li>
					</ul>
				</nav>
			</div>
		</div>
	</div>
</section>
@endsection
@section('js')
<script src='{{ asset("assets/toastr/toastr.min.js") }}'></script>
<script src="{{ asset('assets/select2/select2.full.js') }}"></script>
<script src="{{ asset('assets/moment/moment-with-locales.min.js')}}"></script>
<script src="{{ asset('assets/bootstrap-datepicker/js/bootstrap-datepicker.js') }}"></script>
<script src="{{ asset('assets/bootstrap-datepicker/js/locales/bootstrap-datepicker.es.js') }}"></script>
<script>
	moment.locale('es');
	var url = "{{ url('api/catadores') }}";
	var url_normal = "{{ url('catadores') }}";
	var vm = function() {
		self = this;
		self.data = ko.observableArray([]);
		self.dni = '';
		self.apellido = '';
		self.nombre = '';
		self.edad = '';
		self.edad_text = '';
		self.especializacion = '';
		self.especializacion_text = '';
		self.page = '1';
		self.filters = ko.observableArray([]);
		self.init = function() {
			self.ajax();
			$('.select2').select2();
		}
		self.filterDni = function(element) {
			self.page = 1;
			self.dni = $(element).val()
			self.update_filters();
			self.ajax();
		};
		self.filterApellido = function(element) {
			self.page = 1;
			self.apellido = $(element).val();
			self.update_filters();
			self.ajax();
		};
		self.filterNombre = function(element) {
			self.page = 1;
			self.nombre = $(element).val();
			self.update_filters();
			self.ajax();
		};
		self.filterEdad = function(element) {
			self.page = 1;
			self.edad = $(element).val()
			self.edad_text = $(element).find("option:selected").text();
			self.update_filters();
			self.ajax();
		};
		self.filterEspecialiacion = function(element) {
			self.page = 1;
			self.especializacion = $(element).val()
			self.especializacion_text = $(element).find("option:selected").text();
			self.update_filters();
			self.ajax();
		};
		self.update_filters = function() {
			self.filters([]);
			if (self.dni != '') self.filters.push({
				key: 'dni',
				value: self.dni,
				name: 'dni'
			});
			if (self.apellido != '') self.filters.push({
				key: 'apellido',
				value: self.apellido,
				name: 'apellido'
			});
			if (self.nombre != '') self.filters.push({
				key: 'nombre',
				value: self.nombre,
				name: 'nombre'
			});
			if (self.edad != '') self.filters.push({
				key: 'edad',
				value: self.edad_text,
				name: 'edad'
			});
			if (self.especializacion != '') self.filters.push({
				key: 'especializacion',
				value: self.especializacion_text,
				name: 'especializacion'
			});
		};
		self.remove_filters = function(name) {
			if (name == 'dni') {
				self.dni = '';
			}
			if (name == 'apellido') {
				self.apellido = '';
			}
			if (name == 'nombre') {
				self.nombre = '';
			}
			if (name == 'edad') {
				self.edad = '';
				self.edad_text = '';
			}
			if (name == 'especializacion') {
				self.especializacion = '';
				self.especializacion_text = '';
			}
			$('#' + name).val("").trigger("change");
			self.update_filters();
			self.ajax();
		};
		self.ir = function(page) {
			self.page = page;
			self.ajax();
			$('html, body').animate({
				scrollTop: 0
			}, 'fast');
		};
		self.eliminar = function(id) {
			$.ajax({
				url: url_normal+'/'+id,
				type: 'DELETE',
				data    :   {
					'_token': "{{ csrf_token() }}"
				},
				success: function(result) {
					toastr.success(result);
					self.ajax();
				}
			});
		};
		self.ajax = function() {
			$.getJSON(url, {
				page: self.page,
				dni: self.dni,
				apellido: self.apellido,
				nombre: self.nombre,
				edad: self.edad,
				especializacion:self.especializacion
			})
			.done(function(data) {
				console.log(data);
				Pace.restart()
				vm.data(data);
				$("#data").fadeIn();
				$(".loading").remove();
			})
			.error(function(d) {
				toastr.error('Se encontro un error intente nuevamente');
			});
		};
	};
	vm = new vm();
	vm.init();
	ko.applyBindings(vm, document.getElementById("data"));

	@if(session()->has('message'))
	toastr.success('{{ session()->get('message ') }}');
	@endif
	</script>
	@endsection
