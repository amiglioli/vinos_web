  @extends('template.template')
  @section('head_content')
  @endsection
  @section('content')
  <div class="row">
    <div class="col-lg-12 text-center">
      <div class="ibox float-e-margins">
        <div class="ibox-title">
          <h5>Alta de Pais</h5>
        </div>
        <div class="ibox-content">
         <form class="validator form-horizontal" action="{{url('paises')}}" method="post">
           {{ csrf_field() }}

          <div class="form-group"><label class="col-lg-3 control-label">Pais</label>
            <div class="col-lg-6">
              <input type="text" name="pais" placeholder="INGRESE EL PAIS" class="form-control" required>
            </div>
          </div>

          <div class="form-group">
            <div class="col-lg-offset-1 col-lg-10">
              <button class="btn btn-sm btn-primary " type="submit">Completar Registro</button>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
  </div>
  @endsection

