  @extends('template.template')
  @section('head_content')
  @endsection
  @section('content')
  <div class="row">
    <div class="col-lg-12 text-center">
      <div class="ibox float-e-margins">
        <div class="ibox-title">
          <h5>Edición del Pais : <span style="color:green">{{$pais->nombre}}</span></h5>
        </div>
        <div class="ibox-content">
         <form class="validator form-horizontal" action="{{ url('paises',$pais->id) }}" method="post">
           @method('PUT')
           {{ csrf_field() }}
          <input name="id" type="hidden" value="{{ $pais->id }}"/>
          <div class="form-group"><label class="col-lg-3 control-label">Pais</label>
            <div class="col-lg-6">
              <input type="text" name="pais" value="{{$pais->nombre}}" placeholder="INGRESE EL PAIS" class="form-control" required>
            </div>
          </div>
          <div class="form-group">
            <div class="col-lg-offset-1 col-lg-10">
              <button class="btn btn-sm btn-primary " type="submit">Guardad cambios</button>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
  </div>
  @endsection
