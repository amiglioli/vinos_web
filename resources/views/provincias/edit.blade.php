  @extends('template.template')
  @section('head_content')
  @endsection
  @section('content')
  <div class="row">
    <div class="col-lg-12 text-center">
      <div class="ibox float-e-margins">
        <div class="ibox-title">
          <h5>Editar Provincia</h5>
        </div>
        <div class="ibox-content">
         <form class="validator form-horizontal" action="{{url('provincias',$provincia->provincia_id)}}" method="post">
           @method('PUT')
           {{ csrf_field() }}
          <input name="id" type="hidden" value="{{ $provincia->provincia_id }}"/>
          <div class="form-group"><label class="col-lg-3 control-label">Provincia</label>
            <div class="col-lg-6">
              <input type="text" name="provincia" placeholder="INGRESE LA PROVINCIA" value="{{$provincia->provincia}}" class="form-control" required>
            </div>
          </div>

          <div class="form-group"><label class="col-lg-3 control-label">Pais</label>
            <div class="col-lg-6">
              <select class="form-control chosen-select" name="pais" id="pais" value="{{ old('pais') }}">
                @foreach($paises as $pais)
                <option @if($provincia->pais_id == $pais->id)  selected @endif value="{{$pais->id}}" >{{$pais->nombre}}</option>
                @endforeach
              </select>
            </div>
          </div>


          <div class="form-group">
            <div class="col-lg-offset-1 col-lg-10">
              <button class="btn btn-sm btn-primary " type="submit">Completar Registro</button>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
  </div>
  @endsection

