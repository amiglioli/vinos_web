@extends('template.template')
<meta name="csrf-token" content="{{ csrf_token() }}">
<style>
.img-container {
/* Never limit the container height here */
max-width: 100%;
}
</style>
@section('linkscss')
<link href="{{asset('template/css/cropper.min.css')}}" rel="stylesheet">
<link type="text/css" rel="stylesheet" href="{{ asset('/assets/toastr/toastr.min.css') }}">
@endsection
@section('head_content')
@endsection
@section('content')
<div class="row">
  <div class="row wrapper red-bg">
  <div class="col-lg-10">
  <h2>Subir Imágenes</h2>   </div>
  <div class="col-lg-2">
  </div>
</div>
<div class="col-lg-12">
  <div class="ibox float-e-margins">
    <div class="ibox-title  back-change">
      <h5>Cargue las imagenes para su Perfil</h5>
    </div>
    <div class="ibox-content">
      <p>
        Recorte las dimenciones que desee para su imagen
      </p>
      <div class="row">
        <div class="col-md-6">
          <div class="image-crop">
            <img src="{{asset('template/img/fondo_cropic.png')}}">
          </div>
        </div>
        <div class="col-md-6">
          <h4>Previsualización de imagen</h4>
          <div class="img-preview img-preview-sm"></div>
          <h4>Principales métodos</h4>
          <p>
            Puede subir una nueva imagen al recuadro y cargarla facilmente como promoción.
          </p>
          <div class="btn-group">
            <label title="Subir Imagen" for="inputImage" class="btn btn-outline btn-warning">
              <input type="file" accept="image/*" name="file" id="inputImage" class="hide">
              Subir Imagen
            </label>
            
          </div>
          <h4>Otros métodos</h4>
          <p>
            Estas son alguna de las opciones para recortar su imagen
          </p>
          <div class="btn-group">
            <button class="btn btn-white" id="zoomIn" type="button">Zoom +</button>
            <button class="btn btn-white" id="zoomOut" type="button">Zoom -</button>
            <button class="btn btn-white" id="rotateLeft" type="button">Rotar Izquierda</button>
            <button class="btn btn-white" id="rotateRight" type="button">Rotar Derecha  </button>
          </div>
          <div class="btn-group">  <button title="Guardar" id="download" class="btn btn-danger">Guardar Datos</button></div>
        </div>
      </div>
    </div>
  </div>
</div>
</div>
@endsection
@section('js')
<script src="{{asset('template/js/cropper.min.js')}}" type="text/javascript" ></script>
<script src="{{asset('template/js/datees.js')}}" type="text/javascript" ></script>
<script src='{{ asset("assets/toastr/toastr.min.js") }}'></script>
<script>

$(document).ready(function(){
  var base_url = "{{ url('') }}";

   var $image = $(".image-crop > img")
   $($image).cropper({
       aspectRatio: 16/9,
       preview: ".img-preview",
       done: function(data) {
          //console.log(data);
       }
   });

    var $inputImage = $("#inputImage");
    if (window.FileReader) {
       $inputImage.change(function() {
           var fileReader = new FileReader(),
                   files = this.files,
                   file;

           if (!files.length) {
               return;
           }

           file = files[0];

           if (/^image\/\w+$/.test(file.type)) {
               fileReader.readAsDataURL(file);
               fileReader.onload = function () {
                   $inputImage.val("");
                   $image.cropper("reset", true).cropper("replace", this.result);
               };
           } else {
               showMessage("Please choose an image file.");
           }
       });
     } else {
        $inputImage.addClass("hide");
     }

     $("#download").click(function() {
         base64 = $image.cropper("getDataURL");
         save_img(base64);
     });

     $("#zoomIn").click(function() {
         $image.cropper("zoom", 0.1);
     });

     $("#zoomOut").click(function() {
         $image.cropper("zoom", -0.1);
     });

     $("#rotateLeft").click(function() {
         $image.cropper("rotate", 45);
     });

     $("#rotateRight").click(function() {
         $image.cropper("rotate", -45);
     });

     $("#setDrag").click(function() {
         $image.cropper("setDragMode", "crop");
     });

    function save_img(base54)
    {
        $.ajax({
          headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
          url: "{{url('perfil')}}",
          data:{
            img: base64,
          },
          type:"POST",
          success:function(data){
            window.location.href = "{{url('perfil/imagenes')}}";
          },
          error:function(){
            toastr.error('Se encontro un error intente nuevamente');
          }
        });
    }


});

 </script>






@endsection
