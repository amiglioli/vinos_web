@extends('template.template')
@section('linkscss')
<meta name="csrf-token" content="{{ csrf_token() }}">
<link type="text/css" rel="stylesheet" href="{{ asset('template/css/blueimp-gallery.min.css') }}">
<link type="text/css" rel="stylesheet" href="{{ asset('/assets/sweetalert/sweet-alert.css') }}">
<link type="text/css" rel="stylesheet" href="{{ asset('/template/css/lightbox.css') }}">
<link href="{{asset('template/css/estilo_tabla.css')}}" rel="stylesheet">
<link href="{{asset('template/css/cropper.min.css')}}" rel="stylesheet">
@section('content')

<div class="row">
  <div class="row wrapper red-bg">
  <div class="col-lg-10">
  <h2>Subir Imágenes</h2>   </div>
  <div class="col-lg-2">
  </div>
</div>
<div class="col-lg-12">
  <div class="ibox float-e-margins">
    <div class="ibox-title  back-change">
      <h5>Cargue las imágenes para su Perfil</h5>
    </div>
    <div class="ibox-content">
      <p>
        Recorte las dimenciones que desee para su imagen
      </p>
      <div class="row">
        <div class="col-md-6">
          <div class="image-crop">
            <img src="{{asset('template/img/fondo_cropic.png')}}">
          </div>
        </div>
        <div class="col-md-6">
        <h4>Previsualización de imagen</h4>
         <div class="img-preview img-preview-sm"></div> 
          <!--<h4>Principales métodos</h4>
          <p>
            Puede subir una nueva imagen al recuadro y cargarla facilmente como promoción.
          </p> -->
          <div class="btn-group">
            <label title="Subir Imagen" for="inputImage" class="btn btn-danger">
              <input type="file" accept="image/*" name="file" id="inputImage" class="hide">
              Subir Imagen
            </label>
        	  <div class="btn-group">
        	    <button title="Guardar" id="download" class="btn btn-danger">Guardar Imágen</button>
        	</div>    
          </div>
          <!-- <h4>Otros métodos</h4> -->
          
          <div class="btn-group">
            <button class="btn btn-white" id="zoomIn" type="button">Zoom +</button>
            <button class="btn btn-white" id="zoomOut" type="button">Zoom -</button>
            <button class="btn btn-white" id="rotateLeft" type="button">Rotar Derecha</button>
            <button class="btn btn-white" id="rotateRight" type="button">Rotar Izquierda  </button>
          </div>
          <!--<div class="btn-group">  <button title="Guardar" id="download" class="btn btn-danger">Guardar Datos</button></div> -->
        </div>
      </div>
    </div>
  </div>
</div>
</div>
<!-- ///////////////////////////////////////////////////////////////////////////////////////////////////////////// -->
<section class="bf-table" id="data" style="padding-bottom: 20px ">
	<div class="card card-cascade narrower">
		<!--Card image-->
		<div class="view view-cascade gradient-card-header blue-gradient narrower py-2 mx-4 mb-3 d-flex justify-content-between align-items-center">
			<span class="white-text">Mis Imágenes</span>
		</div>
		<div class="container col-md-12">
			
			<div class="row text-center text-lg-left">
				@foreach($data as $d)
				<div class="col-lg-3 col-md-3 col-xs-6" style="height:180">
					<a href="#" class="d-block mb-4 h-100">
						<img width="320" height="180" class="img-fluid img-thumbnail pull-left" src="{{asset('imgusuario/'.$d->path)}}" alt="">
					</a>
					<div class = "caption">		
						<p>				
						<a href ="#" title="Usted establecera la siguiente imagen como foto de perfil " onclick="setimgperfil({{$d->id}})" class="btn btn-primary btn-xs" role = "button">
								Establecer como perfil
							</a>
							<button href = "#" onclick="eliminar({{$d->id}})" class="btn btn-primary btn-xs" role = "button">
								Eliminar
							</button>
						</p>						
					</div>
				</div>
				@endforeach
			</div>
			<br>
		</div>
	</div>
</section>
@endsection
@section('js')
<script src="{{asset('template/js/jquery.blueimp-gallery.min.js') }}"></script>
<script src="{{asset('template/js/cropper.min.js')}}" type="text/javascript" ></script>
<script src="{{asset('template/js/lightbox.js') }}"></script>
<script src="{{asset('template/js/perfectLayout.js')}}"></script>
<script>
var url_normal = "{{ url('perfil') }}";
var base_url = "{{ url('') }}";
var count = 0;
$(document).ready(function(){



   var $image = $(".image-crop > img")
   $($image).cropper({
       aspectRatio: 16/9,
       preview: ".img-preview",
       done: function(data) {
          //console.log(data);
       }
   });

    var $inputImage = $("#inputImage");
    if (window.FileReader) {
       $inputImage.change(function() {
           count++
           var fileReader = new FileReader(),
                   files = this.files,
                   file;

           if (!files.length) {
               return;
           }

           file = files[0];

           if (/^image\/\w+$/.test(file.type)) {
               fileReader.readAsDataURL(file);
               fileReader.onload = function () {
                   $inputImage.val("");
                   $image.cropper("reset", true).cropper("replace", this.result);
               };
           } else {
               showMessage("Please choose an image file.");
           }
       });
     } else {
        $inputImage.addClass("hide");
     }

     $("#download").click(function() {
         base64 = $image.cropper("getDataURL");
         if(count > 0){
          save_img(base64)
         }
        
         
     });

     $("#zoomIn").click(function() {
         $image.cropper("zoom", 0.1);
     });

     $("#zoomOut").click(function() {
         $image.cropper("zoom", -0.1);
     });

     $("#rotateLeft").click(function() {
         $image.cropper("rotate", 45);
     });

     $("#rotateRight").click(function() {
         $image.cropper("rotate", -45);
     });

     $("#setDrag").click(function() {
         $image.cropper("setDragMode", "crop");
     });

    function save_img(base54)
    {
        $.ajax({
          headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
          url: "{{url('perfil')}}",
          data:{
            img: base64,
          },
          type:"POST",
          success:function(data){
            window.location.href = "{{url('perfil/imagenes')}}";
          },
          error:function(){
            toastr.error('Se encontro un error intente nuevamente');
          }
        });
    }


});

	function setimgperfil(id)
	{
		swal({
			title: "Esta imagen será predeterminada en su perfil y en el Mapa REDVIN?",
			text: "",
			icon: "warning",
			buttons: true,
			dangerMode: true,
		})
		.then((willDelete) => {
			if (willDelete) {
				$.ajax({
					url: url_normal+'/setperfil/'+id,
					type: 'GET',
					success: function(result) {
            location.reload();
						toastr.success(result);
					}
				});
				
			} /*else {
				swal("Su imagen ha sido salvado!");
			}*/
		});
	}


	function eliminar(id)
	{
		swal({
			title: "Está seguro de eliminar esta imagen?",
			text: "",
			icon: "warning",
			buttons: true,
			dangerMode: true,
		})
		.then((willDelete) => {
			if (willDelete) {
				$.ajax({
					url: url_normal+'/'+id,
					type: 'DELETE',
					data    :   {
						'_token': "{{ csrf_token() }}"
					},
					success: function(result) {
						//toastr.success(result);
						location.reload();
					}
				});
				
			} /*else {
				swal("Su imagen ha sido salvado!");
			}*/
		});
	}


 </script>
</script>
@endsection
