@extends('template.template')
@section('head_content')
<link href="{{asset('template/css/cropper.min.css')}}" rel="stylesheet">
<style>
body{
font-family: Poppins;
font-size: 13px;
}
#mapCanvas {
width: 100%;
height: 540px;
border:1px solid;
}
.imgprofilei {
border-radius: 50%;
border:4px solid;
width: 160px;
height: 150px;
}
#infoPanel {
margin-left: 30%;
}
.caption div {
box-shadow: 0 0 5px #C8C8C8;
transition: all 0.3s ease 0s;
}
.img-circle {
border-radius: 50%;
}
.img-circle {
border-radius: 0;
}
.ratio {
background-position: center center;
background-repeat: no-repeat;
background-size: cover;
border:2px solid;
height: 0;
padding-bottom: 100%;
position: relative;
width: 100%;
}
.img-circle {
border-radius: 50%;
}
.img-responsive {
display: block;
height: auto;
max-width: 100%;
}
.image-upload > input
{
display: none;
}
.select2-selection {
text-align: left;
}
</style>
@endsection
@section('content')
<div class="row wrapper red-bg">
  <div class="col-lg-10">
    <h2>Mi Perfil</h2>
    <ol class="breadcrumb">
      <!--<li>
        <a href="">{{$usuario->persona_contacto}}</a>
      </li> -->
    </ol>
  </div>
  <div class="col-lg-2">
  </div>
</div>
<div class="row">
  <br>
  <div class="col-lg-6 text-center">
    <div class="ibox float-e-margins">
      <div class="ibox-content">
        <form class="validator form-horizontal" id="form-perfil" action="{{ url('perfil',Auth::user()->id) }}" method="post"  enctype="multipart/form-data">
          @method('PUT')
          {{ csrf_field() }}
          <!-- <div class="row">
            <div class="col-md-12">
              <div class="image-crop">
                @if(count(Auth::user()->imagen) > 0)
                <img style="width:100%;" src="{{URL::asset('/imgprofile/'.Auth::user()->imagen[0]->path)}}">
                <input type="hidden" name="imagen_actual" value="{{Auth::user()->imagen[0]->path}}"  type="text"/>
                @else
                <img style="width:100%;" src="{{asset('template/img/fondo_cropic.png')}}">
                @endif
              </div>
            </div>
            <div class="col-md-12">
              <br>
                    <div class="btn-group">
                <button class="btn btn-white" id="zoomIn" type="button">Zoom +</button>
                <button class="btn btn-white" id="zoomOut" type="button">Zoom -</button>
                <button class="btn btn-white" id="rotateLeft" type="button">Rotar Izquierda</button>
                <button class="btn btn-white" id="rotateRight" type="button">Rotar Derecha  </button>
              </div><br><br>
              <div class="btn-group">
                <label title="Subir Imagen" for="inputImage" class="btn btn-outline btn-warning">
                  <input type="file" accept="image/*" name="file" id="inputImage" class="hide">
                  Subir Imagen
                </label>
                <label title="Guardar" id="download" style="visibility: hidden" class="btn btn-outline btn-warning">Guardar</label>
                <input type="hidden" name="imagen" id="imagen">
                
              </div>
              <br><br>
            </div>
          </div> -->
          <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
            <label for="name" class="col-md-3 control-label">Nombre</label>
            <div class="col-md-8">
              <input id="entidad" type="text" class="form-control" name="entidad" value="{{ old('entidad', $usuario->user->name)}}" required >
              @if ($errors->has('entidad'))
              <span class="help-block">
                <strong>{{ $errors->first('entidad') }}</strong>
              </span>
              @endif
            </div>
          </div>
          <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
            <label for="name" class="col-md-3 control-label">Provincia</label>
            <div class="col-md-8">
              <!-- <input id="domicilio_provincia" type="text" class="form-control" name="domicilio_provincia" value="{{ old('domicilio_provincia', $usuario->provincia)}}" required > -->
              <select style="  text-align: left;" data-placeholder="Seleccione su Provincia" class="form-control select2" name="provincia" value=""  id="select_id"  tabindex="1">
                @foreach($provincias as $p)
                <option @if($usuario->provincia == $p->provincia_id) selected @endif value="{{$p->provincia_id}}">{{strtoupper($p->provincia)}}</option>
                @endforeach
              </select>
              @if ($errors->has('provincia'))
              <span class="help-block">
                <strong>{{ $errors->first('provincia') }}</strong>
              </span>
              @endif
            </div>
          </div>
          <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
            <label for="name" class="col-md-3 control-label">Partido</label>
            <div class="col-md-8">
              <select data-placeholder="Seleccione su partido" class="form-control select2" name="partido" value=""  id="partido"  tabindex="1">
              </select>
              @if ($errors->has('provincia'))
              <span class="help-block">
                <strong>{{ $errors->first('provincia') }}</strong>
              </span>
              @endif
            </div>
          </div>
          <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
            <label for="name" class="col-md-3 control-label">Localidad</label>
            <div class="col-md-8">
              <input id="localidad" type="text" class="form-control" name="localidad" value="{{ old('localidad', $usuario->localidad)}}" required>
              @if ($errors->has('localidad'))
              <span class="help-block">
                <strong>{{ $errors->first('localidad') }}</strong>
              </span>
              @endif
            </div>
          </div>
          <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
            <label for="name" class="col-md-3 control-label">Código Postal</label>
            <div class="col-md-8">
              <input id="codigo_postal" type="text" class="form-control" name="codigo_postal" value="{{ old('codigo_postal', $usuario->codigo_postal)}}" required >
              @if ($errors->has('codigo_postal'))
              <span class="help-block">
                <strong>{{ $errors->first('codigo_postal') }}</strong>
              </span>
              @endif
            </div>
          </div>
          <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
            <label for="name" class="col-md-3 control-label">Calle</label>
            <div class="col-md-8">
              <input id="domicilio" type="text" class="form-control" name="domicilio" value="{{ old('domicilio', $usuario->domicilio)}}" required >
              @if ($errors->has('direccion'))
              <span class="help-block">
                <strong>{{ $errors->first('domicilio') }}</strong>
              </span>
              @endif
            </div>
          </div>
          <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
            <label for="name" class="col-md-3 control-label">Número </label>
            <div class="col-md-8">
              <input id="numero" type="text" class="form-control" name="numero" value="{{ old('numero', $usuario->numero)}}" required >
              @if ($errors->has('direccion'))
              <span class="help-block">
                <strong>{{ $errors->first('numero') }}</strong>
              </span>
              @endif
            </div>
          </div>
          <div class="form-group{{ $errors->has('telefono') ? ' has-error' : '' }}">
            <label for="name" class="col-md-3 control-label">Teléfono </label>
            <div class="col-md-8">
              <input id="telefono" type="text" class="form-control" name="telefono" value="{{ old('telefono', $usuario->telefono)}}" required >
              @if ($errors->has('telefono'))
              <span class="help-block">
                <strong>{{ $errors->first('telefono') }}</strong>
              </span>
              @endif
            </div>
          </div>
          <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}" style="display:none">
            <label for="name" class="col-md-3 control-label">Latitud</label>
            <div class="col-md-8">
              <input id="geo_latitud" type="text" class="form-control" name="geo_latitud" value="{{ old('geo_latitud', $usuario->geo_latitud)}}" required  readonly="">
              @if ($errors->has('geo_latitud'))
              <span class="help-block">
                <strong>{{ $errors->first('geo_latitud') }}</strong>
              </span>
              @endif
            </div>
          </div>
          <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}" style="display:none">
            <label for="name" class="col-md-3 control-label">Longitud</label>
            <div class="col-md-8">
              <input id="geo_longitud" type="text" class="form-control" name="geo_longitud" value="{{ old('geo_longitud', $usuario->geo_longitud)}}" required  readonly="">
              @if ($errors->has('geo_longitud'))
              <span class="help-block">
                <strong>{{ $errors->first('geo_longitud') }}</strong>
              </span>
              @endif
            </div>
          </div>
          <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
            <label for="name" class="col-md-3 control-label">Página Web</label>
            <div class="col-md-8">
              <input id="web" type="text" class="form-control" name="web" value="{{ old('web', $usuario->web)}}" >
              @if ($errors->has('web'))
              <span class="help-block">
                <strong>{{ $errors->first('web') }}</strong>
              </span>
              @endif
            </div>
          </div>
          <div class="form-group">
            
            <button type="submit" class="btn btn-danger">Guardar Datos</button>
            
          </div>
        </form>
      </div>
    </div>
  </div>
  <div class="col-md-6">
    <div class="form-group">
      <div  id="mapCanvas" style="Width:100%;"></div>
      <input type="hidden" id="info" type="text" size="50" value="">
    </div>
    
    
    
    
  </div>
</div>
@endsection


@section('js')

<!--
<script src="{{asset('template/js/jquery-2.1.1.js')}}" type="text/javascript" ></script>
<script src="{{asset('template/js/bootstrap.min.js')}}" type="text/javascript" ></script>
<script src="{{asset('template/js/plugins/iCheck/icheck.min.js')}}" type="text/javascript" ></script>
-->
<script src="{{asset('template/js/plugins/chosen/chosen.jquery.js')}}" type="text/javascript" ></script>
<script type="text/javascript" src="http://maps.google.com/maps/api/js?&libraries=places&key=AIzaSyD9FZU87YCCSGovLg57u9dvi2jLDDz3uyw"></script>
<script src="{{asset('template/js/cropper.min.js')}}" type="text/javascript" ></script>
<script src="{{asset('template/js/crop_perfil.js')}}" type="text/javascript" ></script>



<script type="text/javascript">
var marker;
var map;
$(document).ready(function(){
  crop_perfil();
  $('.select2').select2();
  var provincia_id = $('#select_id').val();
  carga_localidades(provincia_id);
});

$('#form-perfil').submit(function(e){
    e.preventDefault();
    $("#download").trigger( "click" );
    $(this).unbind('submit').submit()
});

function readURL(input) {
  if (input.files && input.files[0]) {
    var reader = new FileReader();

    reader.onload = function (e) {
      $('#img').attr('src', e.target.result);
    }

    reader.readAsDataURL(input.files[0]);
  }
}

$("#file-input").change(function(){
  readURL(this);
});


$('#numero').focusout(function(){
  codeAddress();
});

function codeAddress() {
  var address = 'San Diego, CA';
  var geocoder = new google.maps.Geocoder();

  var direccion = $("#domicilio").val();
  var domicilio_localidad = $("#localidad").val();
  var numero = $("#numero").val();

  var partido = $('#partido option:selected').text();
  var provincia = $('#partido option:selected').text()

  var dir_completa = direccion+' '+numero+', '+domicilio_localidad+', '+provincia+', Argentina';

  geocoder.geocode({'address': dir_completa}, function(results, status) {
    if(status == google.maps.GeocoderStatus.OK) {
      if(status != google.maps.GeocoderStatus.ZERO_RESULTS) {
        var latitude = results[0].geometry.location.lat();
        var longitude = results[0].geometry.location.lng();
        $('#geo_latitud').val(latitude);
        $('#geo_longitud').val(longitude);

        marker.setPosition(results[0].geometry.location);
        map.setZoom(15);
        map.panTo(marker.position);
      }
    } else {
      toastr.error("No hemos podido capturar esa ubicacion")
    }
  });
}


var geocoder = new google.maps.Geocoder();

function geocodePosition(pos) {
  geocoder.geocode({
    latLng: pos
  }, function(responses) {
    if (responses && responses.length > 0) {
      updateMarkerAddress(responses[0].formatted_address);
      ubicacion = responses[0].formatted_address.split(",");

      if (responses[0].address_components) {
        for (var i in responses[0].address_components) {
          if (typeof(responses[0].address_components[i]) === "object" && responses[0].address_components[i].types[0] == "postal_code") {
            var result = responses[0].address_components[i].long_name;
          }
        }
      }
      if (!result) {
        //alert("Error, there is no postal code");
      } else {
        $("#domicilio_cp").val(result);
      }

      direccion = ubicacion[0];
      partido  = ubicacion[1];
      provincia  = ubicacion[2];
      pais = responses[0].formatted_address.split(",").splice(-1,1);
      $("#domicilio_direccion").val(direccion);
      $("#domicilio_pais").val(pais);
      $("#web").val('');
    } else {
      updateMarkerAddress('Keine Koordinaten gefunden!');
    }
  });
}


function updateMarkerPosition(latLng) {
  document.getElementById('info').value = [
    latLng.lat(),
    latLng.lng()
  ].join(', ');

  $("#geo_latitud").val(latLng.lat());
  $("#geo_longitud").val(latLng.lng());

}

function updateMarkerAddress(str) {
  //console.log(str);
  //document.getElementById('searchTextField').value = str;
  //document.getElementById("searchTextField").focus();
  //document.getElementById("searchTextField").blur();
}

function initialize() {
  var latitud_usuario =  "{{$usuario->geo_latitud}}";
  var longitud_usuario = "{{$usuario->geo_longitud}}";

  var latLng = new google.maps.LatLng(latitud_usuario,longitud_usuario);


      $.getJSON("{{url('assets/mapstyle/mapstyle.json')}}", function(mapStyle) {
        map = new google.maps.Map(document.getElementById('mapCanvas'), {
            zoom: 17,
            center: latLng,
            streetViewControl: false,
            styles:mapStyle,
            mapTypeControl: false
        });

      marker = new google.maps.Marker({
        position: latLng,
        map: map,
        draggable: true,
        title:"Mueva el punto a la posición deseada"
      });

        updateMarkerPosition(latLng);
        // Add dragging event listeners.
        google.maps.event.addListener(marker, 'dragstart', function() {
          updateMarkerAddress('Dragging...');
        });

        google.maps.event.addListener(marker, 'drag', function() {

          updateMarkerPosition(marker.getPosition());
        });

        google.maps.event.addListener(marker, 'dragend', function() {
          geocodePosition(marker.getPosition());
        });

    });
}

google.maps.event.addDomListener(window, 'load', initialize);


$('#select_id').change(function(){
  carga_localidades($(this).val());
});

function carga_localidades(id)
{
  ///console.log(id);

  var base_url = "<?= URL::to('') ?>";
  base_url = base_url.trim();
  $.ajax({
    url:  base_url+'/localidades/'+id,
    type:  'GET',
    dataType : 'json',
    beforeSend: function () {
      $("#resultado").html("Procesando, espere por favor...");
    },
    success:  function (response) {
      $("#partido").children().remove();
      var html = '';
      $.each(response, function( index, value ) {
        //console.log(value.municipio_id,value.municipio);
        if("{{$usuario->partido}}" == value.municipio_id)
        var op = "<option selected  value='"+value.municipio_id+"'>"+value.municipio+"</option>";
        else
        var op = "<option  value='"+value.municipio_id+"'>"+value.municipio+"</option>";

        $('#partido').append(op);
      });
      $('#partido').trigger("change.select2");

    }
  });
}
</script>
@endsection
