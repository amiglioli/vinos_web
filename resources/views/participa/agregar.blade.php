@extends('template.template')
@section('head_content')
<link href="{{asset('template/css/bootstrap-datetimepicker.css')}}" rel="stylesheet">
@endsection
@section('content')
<div class="row">
  <div class="col-lg-12 text-center">
    <div class="ibox float-e-margins">
      <div class="ibox-title" style="background-color: #d02026;color:#ffffff">
        <h5>Inscribite para participar en las CATAS REDVIN degustando y puntuando los vinos participantes</h5>
      </div>
      <div class="ibox-content">
        <form class="validator form-horizontal" action="{{url('participa')}}" method="post">
          {{ csrf_field() }}

          <div class="form-group"><label class="col-lg-3 control-label">DNI</label>
            <div class="col-lg-6">
              <input type="number" name="dni" placeholder="INGRESE EL DNI" maxlength="8" value="{{ old('dni') }}" class="form-control" required>
            </div>
          </div>

          <div class="form-group"><label class="col-lg-3 control-label">APELLIDO</label>
            <div class="col-lg-6">
              <input type="text" name="apellido" value="{{ old('apellido') }}" placeholder="INGRESE EL APELLIDO" class="form-control" required>
            </div>
          </div>


          <div class="form-group"><label class="col-lg-3 control-label">NOMBRE</label>
            <div class="col-lg-6">
              <input type="text" name="nombre" value="{{ old('nombre') }}" placeholder="INGRESE EL NOMBRE" class="form-control" required>
            </div>
          </div>


          <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}" id="data_1">
            <label for="edad" class="col-md-3 control-label">FECHA DE NACIMIENTO</label>
            <div class="col-md-6 " >
              <input name="edad" id="edad" value="{{ old('edad') }}" autocomplete="off" type="text" class="form-control">
              @if ($errors->has('edad'))
              <span class="help-block">
                <strong>{{ $errors->first('edad') }}</strong>
              </span>
              @endif
            </div>
          </div>

          <div class="form-group"><label class="col-lg-3 control-label">ESPECIALIZACIÓN</label>
            <div class="col-lg-6">
              <select class="select2_demo_2 form-control" name="especializacion" id="especializacion" value="{{ old('especializacion') }}">
                <option value="">ESPECIALIZACIONES</option>
                @foreach($especializacion as $e)
                <option value="{{$e->id}}" >{{$e->especializacion}}</option>
                @endforeach
              </select>
            </div>
          </div>
          <div class="form-group"><label class="col-lg-3 control-label">CELULAR</label>
            <div class="col-lg-6">
              <input type="text" name="celular" value="{{ old('celular') }}" data-mask="(999) 999-9999" placeholder="INGRESE EL CELULAR" class="form-control" required>
            </div>
          </div>
          <div class="form-group"><label class="col-lg-3 control-label">EMAIL</label>
            <div class="col-lg-6">
              <input type="text"  name="email" value="{{ old('email') }}" placeholder="INGRESE EL EMAIL" class="form-control" required>
            </div>
          </div>


          <div class="form-group">
            <div class="col-lg-offset-1 col-lg-10">
              <button class="btn btn-sm btn-primary " type="submit">ENVIAR</button>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>
@endsection
@section('js')
<script src="{{asset('template/js/bootstrap-datetimepicker.js')}}"></script>
<script src="{{asset('template/js/plugins/jasny/jasny-bootstrap.min.js')}}"></script>


<script>
$(document).ready(function() {
  $(".select2_demo_2").select2();
  var dt = new Date();
  var edad = dt.setFullYear(new Date().getFullYear()-18);
  var anios =  moment(edad).format('YYYY-MM-DD');

  $('#edad').datetimepicker({
    keepOpen:true,
    showClose: true,
    viewMode: 'years',
    maxDate: anios,
    locale:"es",
    format: 'Y-MM-DD',
    widgetPositioning:{
      horizontal: 'auto',
      vertical: 'bottom'
    }
  });
  $('#edad').val("");
});
</script>
@endsection
