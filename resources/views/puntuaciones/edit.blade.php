  @extends('template.template')
  @section('head_content')
  @endsection
  @section('content')
  <div class="row">
    <div class="col-lg-12 text-center">
      <div class="ibox float-e-margins">
        <div class="ibox-title">
          <h5>Edición del Producto : <span style="color:green">{{$puntuacion->puntuacion}}</span></h5>
        </div>
        <div class="ibox-content">
         <form class="validator form-horizontal" action="{{ url('puntuacion_tranquilos',$puntuacion->id) }}" method="post">
           @method('PUT')
           {{ csrf_field() }}
          <input name="id" type="hidden" value="{{ $puntuacion->id }}"/>
          <div class="form-group"><label class="col-lg-3 control-label">Producto</label>
            <div class="col-lg-6">
              <input type="text" name="puntuacion" value="{{$puntuacion->puntuacion}}" placeholder="INGRESE SU PUNTUACÍÓN" class="form-control" required>
            </div>
          </div>
          <div class="form-group">
            <div class="col-lg-offset-1 col-lg-10">
              <button class="btn btn-sm btn-primary " type="submit">Guardad cambios</button>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
  </div>
  @endsection
