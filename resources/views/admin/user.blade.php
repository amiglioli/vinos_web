@extends('template/template')
@section('css')
<link href='{{ asset("assets/sweetalert2/sweetalert2.min.css") }}' type="text/css"     rel="stylesheet"></link>
@endsection
@section('content')
<div class="panel">
	<div class="panel-body nopadding">
		@if (count($errors) > 0)
		<div class="col-md-12">
			<div class="alert alert-danger col-md-12">
				<ul>
					@foreach ($errors->all() as $error)
					<li>{{ $error }}</li>
					@endforeach
				</ul>
			</div>
		</div>
		@endif
		@if(session()->has('error'))
		<div class="col-md-12">
			<div class="alert alert-danger col-md-12">
				<ul>
					<li>{{ session()->get('error') }}</li>
				</ul>
			</div>
		</div>
		@endif
		<form method="POST" enctype="multipart/form-data" action="{{ url('admin/usuarios',$d->id) }}" class="col-md-12">
			@if($d->id != "") {{ method_field('PATCH') }} @endif
			<input name="id" type="hidden" value="{{ $d->id }}"/>
			<input name="_token" type="hidden" value="{{ csrf_token() }}"/>
			<div class="col-md-6">
				<div class="form-group">
					<div class="col-md-7">
						<label class="control-label">Nombre:</label>
						<input type="text" placeholder="Nombre" class="form-control" name="nombre" value="{{ $d->name }}">
					</div>
				</div>
				<div class="form-group">
					<div class="col-md-7">
						<label class="control-label">E-mail:</label>
						<input type="text" placeholder="E-mail" class="form-control" name="email" value="{{ $d->email }}">
					</div>
				</div>
				@if($d->id != ""	)
				<div class="form-group">
					<div class="col-md-7">
						<label class="control-label">Contraseña Actual:</label>
						<input type="password" placeholder="Contraseña Actual" class="form-control" name="password_actual">
					</div>
				</div>
				@endif
				<div class="form-group">
					<div class="col-md-7">
						<label class="control-label">Contraseña Nueva:</label>
						<input type="password" placeholder="Contraseña Nueva" class="form-control" name="password">
					</div>
				</div>
				<div class="form-group">
					<div class="col-md-7">
						<label class="control-label">Confirmar Contraseña:</label>
						<input type="password" placeholder="Confirmar Contraseña" class="form-control" name="password_confirmation">
					</div>
				</div>
			</div>
			<div class="col-md-3 form-horizontal">
				<div class="form-group">
					<label class="control-label">Roles</label>
					<div>
						@foreach ($roles as $role)
						@if(Auth::user()->hasRole('admin'))

								<input name="role[]" id="{{$role->id}}" value="{{$role->id}}" type="checkbox" @if( $d->hasRole($role->name) ) checked @endif> {{ $role->label }}

						@else if( Auth::user()->hasRole($role->name) )
						<div class="checkbox">
							<label>
								<input name="role[]" id="{{$role->id}}" value="{{$role->id}}" type="checkbox" @if( $d->hasRole($role->name) ) checked @endif> {{ $role->label }}
							</label>
						</div>
						@endif
						@endforeach
					</div>
				</div>
			</div>
			<div class="col-md-11" style="margin-top: 10px">
				<div class="form-group">
					<div class="col-md-6">
						<button type="submit" name="submit" class="btn btn-default" id="submit"><i class="fa fa-save"></i> Guardar</button>
						<a href="javascript:void(0)" class="btn btn-default" onclick="eliminar( {{ $d->id }} ) "><i class="fa fa-trash"></i> Eliminar</a>
					</div>
				</div>
			</div>
		</form>
		<form id="eliminar+{{ $d->id }}" action="{{ url('admin/usuarios').'/'.$d->id }}" method="POST" style="display: none;">
			<input type="hidden" value="{{ $d->id }}">
			{{ method_field('DELETE') }}
			{{ csrf_field() }}
		</form>
	</div>
</div>
@endsection
@section('js')
<script src='{{ asset("assets/sweetalert2/sweetalert2.min.js") }}'></script>
<script src='{{ asset("assets/toastr/toastr.min.js") }}'></script>
<script type="text/javascript">
function eliminar(id){
swal({
	title: '{{ env("APP_NAME") }}',
text: '¿Está seguro de eliminar este Usuario?',
showCloseButton: true,
showCancelButton: true,
confirmButtonText: 'Eliminar',
cancelButtonText:  'Cancelar',
		}).then((result) => {
			if (result) {
			document.getElementById('eliminar+'+id).submit();
			}
		});
}
</script>
@endsection
