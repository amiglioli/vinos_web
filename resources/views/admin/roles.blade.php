@extends('template/template')
@section('linkscss')
<link href="{{ asset('/assets/select2/select2.min.css') }}" rel="stylesheet">
<link href="{{ asset('/css/tabla_filtros.css') }}" type="text/css" rel="stylesheet">
<link href="{{asset('template/css/estilo_tabla.css')}}" rel="stylesheet">
@endsection
@section('content')
<div class="row wrapper red-bg">
  <div class="col-lg-10">
    <h2>Roles</h2>
  </div>
  <div class="col-lg-2">
  </div>
</div>
<section class="bf-table" id="data" style="padding-bottom: 20px ">
   <div class="card card-cascade narrower">
      <!--Card image-->
      <div class="view view-cascade gradient-card-header blue-gradient narrower py-2 mx-4 mb-3 d-flex justify-content-between align-items-center">
        
         <span class="white-text">Grupos</span>
          <div style="text-align:right">
	   <a type="button" class="btn btn-outline btn-danger" href="{{ url('admin/roles/create') }}">Nuevo grupo</a>
         </div>
      </div>
      <!--/Card image-->
      <div class="px-4">
        
         <div class="table-wrapper table-responsive">
            <table class="table table-bordered tabla_custom table-hover mb-0">
				<thead>
					<tr>
						<th class="col_1" style="padding:10px;">Nombre</th>
						<th class="col_4" style="padding:10px;">Permisos</th>
						<th class="col_4" style="padding:10px;">Acciones</th>
					</tr>
				</thead>
				<tbody>
					@foreach ($roles as $role)
					<tr>
						<td class="col_1">{{ $role->name }}</td>
						<td class="col_4">
							@if($role->permissions)
							<ul data-bind="foreach: { data: roles, as: 'role' }" style="margin-bottom: 0px">
								@foreach($role->permissions as $permission)
								<li>
									<a href="javascript:void(0)" style="color: inherit;">{{ $permission->label }}</a>
								</li>
								@endforeach
							</ul>
							@endif
						</td>
						<td class="col_1 text-center">
							<a class="btn btn-outline btn-sm btn-info" href="{{ url('admin/roles/'.$role->id.'/edit') }}">Detalle</a>
						</td>
					</tr>
					@endforeach
				</tbody>
			</table>
		</div>  
        <div class="d-flex justify-content-between">
            <div style="padding-top: 40px">
            </div>
            <nav class="my-4">
              {!! $roles->render() !!}
            </nav>
         </div>

      </div>
   </div>
</section>
@endsection
@section('js')
@endsection